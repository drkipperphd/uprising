playerCards = {}
playerCards.badges = {}
playerCards.marks = {}
playerCards.strips = {}
playerCards.ranks = {}

local plyMeta = FindMetaTable( "Player" )

function RegisterCard( ctype, id, texture )

	local destination = nil

	if ctype == "badge" then

		destination = playerCards.badges

	elseif ctype == "mark" then

		destination = playerCards.marks

	elseif ctype == "strip" then

		destination = playerCards.strips

	end

	if destination then

		destination[ id ] = texture

	end

end

-- This is nasty, but it's a necessary evil.
function addCards()

	-- Register all the badges.
	RegisterCard( "badge", 0, "challenge_badge_default_result.png" )
	RegisterCard( "badge", 1, "challenge_badge_01_result.png" )
	RegisterCard( "badge", 2, "challenge_badge_02_result.png" )
	RegisterCard( "badge", 3, "challenge_badge_03_result.png" )
	RegisterCard( "badge", 4, "challenge_badge_04_result.png" )
	RegisterCard( "badge", 5, "challenge_badge_05_result.png" )
	RegisterCard( "badge", 6, "challenge_badge_06_result.png" )
	RegisterCard( "badge", 7, "challenge_badge_07_result.png" )
	RegisterCard( "badge", 8, "challenge_badge_08_result.png" )
	RegisterCard( "badge", 9, "challenge_badge_09_result.png" )
	RegisterCard( "badge", 10, "challenge_badge_10_result.png" )
	RegisterCard( "badge", 11, "challenge_badge_114_result.png" )
	RegisterCard( "badge", 12, "challenge_badge_115_result.png" )
	RegisterCard( "badge", 13, "challenge_badge_11_result.png" )
	RegisterCard( "badge", 14, "challenge_badge_12_result.png" )
	RegisterCard( "badge", 15, "challenge_badge_13_result.png" )
	RegisterCard( "badge", 16, "challenge_badge_14_result.png" )
	RegisterCard( "badge", 17, "challenge_badge_15_result.png" )
	RegisterCard( "badge", 18, "challenge_badge_16_result.png" )
	RegisterCard( "badge", 19, "challenge_badge_17_result.png" )
	RegisterCard( "badge", 20, "challenge_badge_18_result.png" )
	RegisterCard( "badge", 21, "challenge_badge_19_result.png" )
	RegisterCard( "badge", 22, "challenge_badge_20_result.png" )
	RegisterCard( "badge", 23, "challenge_badge_21_result.png" )
	RegisterCard( "badge", 24, "challenge_badge_22_result.png" )
	RegisterCard( "badge", 25, "challenge_badge_23_result.png" )
	RegisterCard( "badge", 26, "challenge_badge_24_result.png" )
	RegisterCard( "badge", 27, "challenge_badge_25_result.png" )
	RegisterCard( "badge", 28, "challenge_badge_26_result.png" )
	RegisterCard( "badge", 29, "challenge_badge_27_result.png" )
	RegisterCard( "badge", 30, "challenge_badge_28_result.png" )
	RegisterCard( "badge", 31, "challenge_badge_29_result.png" )
	RegisterCard( "badge", 32, "challenge_badge_37_result.png" )
	RegisterCard( "badge", 33, "challenge_badge_38_result.png" )
	RegisterCard( "badge", 34, "challenge_badge_39_result.png" )
	RegisterCard( "badge", 35, "challenge_badge_40_result.png" )
	RegisterCard( "badge", 36, "challenge_badge_41_result.png" )
	RegisterCard( "badge", 37, "challenge_badge_42_result.png" )
	RegisterCard( "badge", 38, "challenge_badge_43_result.png" )
	RegisterCard( "badge", 39, "challenge_badge_44_result.png" )
	RegisterCard( "badge", 40, "challenge_badge_45_result.png" )
	RegisterCard( "badge", 41, "challenge_badge_46_result.png" )
	RegisterCard( "badge", 42, "challenge_badge_47_result.png" )
	RegisterCard( "badge", 43, "challenge_badge_48_result.png" )
	RegisterCard( "badge", 44, "challenge_badge_49_result.png" )
	RegisterCard( "badge", 45, "challenge_badge_50_result.png" )
	RegisterCard( "badge", 46, "challenge_badge_51_result.png" )
	RegisterCard( "badge", 47, "challenge_badge_52_result.png" )
	RegisterCard( "badge", 48, "challenge_badge_53_result.png" )
	RegisterCard( "badge", 49, "challenge_badge_54_result.png" )
	RegisterCard( "badge", 50, "challenge_badge_75_result.png" )
	RegisterCard( "badge", 51, "challenge_badge_76_result.png" )
	RegisterCard( "badge", 52, "challenge_badge_77_result.png" )
	RegisterCard( "badge", 53, "challenge_badge_78_result.png" )
	RegisterCard( "badge", 54, "challenge_badge_79_result.png" )
	RegisterCard( "badge", 55, "challenge_badge_80_result.png" )
	RegisterCard( "badge", 56, "challenge_badge_81_result.png" )
	RegisterCard( "badge", 57, "challenge_badge_83_result.png" )
	RegisterCard( "badge", 58, "challenge_badge_84_result.png" )
	RegisterCard( "badge", 59, "challenge_badge_85_result.png" )
	RegisterCard( "badge", 60, "challenge_badge_86_result.png" )
	RegisterCard( "badge", 61, "challenge_badge_87_result.png" )
	RegisterCard( "badge", 62, "challenge_badge_88_result.png" )
	RegisterCard( "badge", 63, "challenge_badge_89_result.png" )
	RegisterCard( "badge", 64, "challenge_badge_90_result.png" )
	RegisterCard( "badge", 65, "challenge_badge_91_result.png" )
	RegisterCard( "badge", 66, "challenge_badge_92_result.png" )
	RegisterCard( "badge", 67, "challenge_badge_97_result.png" )
	RegisterCard( "badge", 68, "challenge_badge_GMachievements_03_result.png" )
	RegisterCard( "badge", 69, "challenge_badge_NY_marathon_01_result.png" )
	RegisterCard( "badge", 70, "challenge_badge_afghan_01_result.png" )
	RegisterCard( "badge", 71, "challenge_badge_afghan_02_result.png" )
	RegisterCard( "badge", 72, "challenge_badge_afghan_03_result.png" )
	RegisterCard( "badge", 73, "challenge_badge_afro_01_result.png" )
	RegisterCard( "badge", 74, "challenge_badge_afro_02_result.png" )
	RegisterCard( "badge", 75, "challenge_badge_afro_03_result.png" )
	RegisterCard( "badge", 76, "challenge_badge_afro_04_result.png" )
	RegisterCard( "badge", 77, "challenge_badge_airbase_01_result.png" )
	RegisterCard( "badge", 78, "challenge_badge_airbase_02_result.png" )
	RegisterCard( "badge", 79, "challenge_badge_airbase_03_result.png" )
	RegisterCard( "badge", 80, "challenge_badge_anniversary_2_result.png" )
	RegisterCard( "badge", 81, "challenge_badge_anubis_result.png" )
	RegisterCard( "badge", 82, "challenge_badge_aul_01_result.png" )
	RegisterCard( "badge", 83, "challenge_badge_aul_02_result.png" )
	RegisterCard( "badge", 84, "challenge_badge_aul_03_result.png" )
	RegisterCard( "badge", 85, "challenge_badge_bank_01_result.png" )
	RegisterCard( "badge", 86, "challenge_badge_bank_02_result.png" )
	RegisterCard( "badge", 87, "challenge_badge_bank_03_result.png" )
	RegisterCard( "badge", 88, "challenge_badge_blackgold_01_result.png" )
	RegisterCard( "badge", 89, "challenge_badge_blackgold_02_result.png" )
	RegisterCard( "badge", 90, "challenge_badge_blackgold_03_result.png" )
	RegisterCard( "badge", 91, "challenge_badge_bra_01_result.png" )
	RegisterCard( "badge", 92, "challenge_badge_brainstorm_result.png" )
	RegisterCard( "badge", 93, "challenge_badge_camo03_result.png" )
	RegisterCard( "badge", 94, "challenge_badge_candidate01_result.png" )
	RegisterCard( "badge", 95, "challenge_badge_crossriver_01_result.png" )
	RegisterCard( "badge", 96, "challenge_badge_crossriver_02_result.png" )
	RegisterCard( "badge", 97, "challenge_badge_crossriver_03_result.png" )
	RegisterCard( "badge", 98, "challenge_badge_d17_01_result.png" )
	RegisterCard( "badge", 99, "challenge_badge_d17_02_result.png" )
	RegisterCard( "badge", 100, "challenge_badge_d17_03_result.png" )
	RegisterCard( "badge", 102, "challenge_badge_dm_01_result.png" )
	RegisterCard( "badge", 103, "challenge_badge_dm_02_result.png" )
	RegisterCard( "badge", 104, "challenge_badge_dm_03_result.png" )
	RegisterCard( "badge", 105, "challenge_badge_dragon_result.png" )
	RegisterCard( "badge", 106, "challenge_badge_dtown_01_result.png" )
	RegisterCard( "badge", 107, "challenge_badge_dtown_02_result.png" )
	RegisterCard( "badge", 108, "challenge_badge_dtown_03_result.png" )
	RegisterCard( "badge", 109, "challenge_badge_euro2016_result.png" )
	RegisterCard( "badge", 110, "challenge_badge_factory_01_result.png" )
	RegisterCard( "badge", 111, "challenge_badge_factory_02_result.png" )
	RegisterCard( "badge", 112, "challenge_badge_factory_03_result.png" )
	RegisterCard( "badge", 113, "challenge_badge_farm_01_result.png" )
	RegisterCard( "badge", 114, "challenge_badge_farm_02_result.png" )
	RegisterCard( "badge", 115, "challenge_badge_farm_03_result.png" )
	RegisterCard( "badge", 116, "challenge_badge_fastcup_result.png" )
	RegisterCard( "badge", 117, "challenge_badge_gamescom_result.png" )
	RegisterCard( "badge", 118, "challenge_badge_gold_01_result.png" )
	RegisterCard( "badge", 119, "challenge_badge_gold_02_result.png" )
	RegisterCard( "badge", 120, "challenge_badge_gold_03_result.png" )
	RegisterCard( "badge", 121, "challenge_badge_gold_04_result.png" )
	RegisterCard( "badge", 122, "challenge_badge_hangar_01_result.png" )
	RegisterCard( "badge", 123, "challenge_badge_hangar_02_result.png" )
	RegisterCard( "badge", 124, "challenge_badge_hangar_03_result.png" )
	RegisterCard( "badge", 125, "challenge_badge_hawkrock_01_result.png" )
	RegisterCard( "badge", 126, "challenge_badge_hawkrock_02_result.png" )
	RegisterCard( "badge", 127, "challenge_badge_hawkrock_03_result.png" )
	RegisterCard( "badge", 128, "challenge_badge_hero_result.png" )
	RegisterCard( "badge", 129, "challenge_badge_hlw01_result.png" )
	RegisterCard( "badge", 130, "challenge_badge_hunt_01_result.png" )
	RegisterCard( "badge", 131, "challenge_badge_hunt_02_result.png" )
	RegisterCard( "badge", 132, "challenge_badge_kra_01_result.png" )
	RegisterCard( "badge", 133, "challenge_badge_lava01_result.png" )
	RegisterCard( "badge", 134, "challenge_badge_lhouse_01_result.png" )
	RegisterCard( "badge", 135, "challenge_badge_lhouse_02_result.png" )
	RegisterCard( "badge", 136, "challenge_badge_lhouse_03_result.png" )
	RegisterCard( "badge", 137, "challenge_badge_note_01_result.png" )
	RegisterCard( "badge", 138, "challenge_badge_ny_result.png" )
	RegisterCard( "badge", 139, "challenge_badge_oildepot_01_result.png" )
	RegisterCard( "badge", 140, "challenge_badge_oildepot_02_result.png" )
	RegisterCard( "badge", 141, "challenge_badge_oildepot_03_result.png" )
	RegisterCard( "badge", 142, "challenge_badge_opencup_01_result.png" )
	RegisterCard( "badge", 143, "challenge_badge_opencup_02_result.png" )
	RegisterCard( "badge", 144, "challenge_badge_opencup_03_result.png" )
	RegisterCard( "badge", 145, "challenge_badge_opencup_04_result.png" )
	RegisterCard( "badge", 146, "challenge_badge_opencup_05_result.png" )
	RegisterCard( "badge", 147, "challenge_badge_opencup_06_result.png" )
	RegisterCard( "badge", 148, "challenge_badge_opencup_07_result.png" )
	RegisterCard( "badge", 149, "challenge_badge_opencup_08_result.png" )
	RegisterCard( "badge", 150, "challenge_badge_opencup_09_result.png" )
	RegisterCard( "badge", 151, "challenge_badge_opencup_10_result.png" )
	RegisterCard( "badge", 152, "challenge_badge_opencup_11_result.png" )
	RegisterCard( "badge", 153, "challenge_badge_opencup_12_result.png" )
	RegisterCard( "badge", 154, "challenge_badge_opencup_13_result.png" )
	RegisterCard( "badge", 155, "challenge_badge_opencup_14_result.png" )
	RegisterCard( "badge", 156, "challenge_badge_palace_01_result.png" )
	RegisterCard( "badge", 157, "challenge_badge_palace_02_result.png" )
	RegisterCard( "badge", 158, "challenge_badge_palace_03_result.png" )
	RegisterCard( "badge", 159, "challenge_badge_pin-up_result.png" )
	RegisterCard( "badge", 160, "challenge_badge_pt66_result.png" )
	RegisterCard( "badge", 161, "challenge_badge_roccat_result.png" )
	RegisterCard( "badge", 162, "challenge_badge_rw_01_result.png" )
	RegisterCard( "badge", 163, "challenge_badge_rw_02_result.png" )
	RegisterCard( "badge", 164, "challenge_badge_rw_03_result.png" )
	RegisterCard( "badge", 165, "challenge_badge_rw_04_result.png" )
	RegisterCard( "badge", 166, "challenge_badge_rw_05_result.png" )
	RegisterCard( "badge", 167, "challenge_badge_rw_06_result.png" )
	RegisterCard( "badge", 168, "challenge_badge_rw_07_result.png" )
	RegisterCard( "badge", 169, "challenge_badge_rw_08_result.png" )
	RegisterCard( "badge", 170, "challenge_badge_rw_09_result.png" )
	RegisterCard( "badge", 171, "challenge_badge_shuttle_01_result.png" )
	RegisterCard( "badge", 172, "challenge_badge_shuttle_02_result.png" )
	RegisterCard( "badge", 173, "challenge_badge_shuttle_03_result.png" )
	RegisterCard( "badge", 174, "challenge_badge_sm_01_result.png" )
	RegisterCard( "badge", 175, "challenge_badge_sm_02_result.png" )
	RegisterCard( "badge", 176, "challenge_badge_sm_03_result.png" )
	RegisterCard( "badge", 177, "challenge_badge_sm_04_result.png" )
	RegisterCard( "badge", 178, "challenge_badge_sm_05_result.png" )
	RegisterCard( "badge", 179, "challenge_badge_sm_06_result.png" )
	RegisterCard( "badge", 180, "challenge_badge_sm_07_result.png" )
	RegisterCard( "badge", 181, "challenge_badge_steam01_result.png" )
	RegisterCard( "badge", 182, "challenge_badge_streetwars_01_result.png" )
	RegisterCard( "badge", 183, "challenge_badge_streetwars_02_result.png" )
	RegisterCard( "badge", 184, "challenge_badge_streetwars_03_result.png" )
	RegisterCard( "badge", 185, "challenge_badge_towers_01_result.png" )
	RegisterCard( "badge", 186, "challenge_badge_towers_02_result.png" )
	RegisterCard( "badge", 187, "challenge_badge_towers_03_result.png" )
	RegisterCard( "badge", 188, "challenge_badge_vdv_result.png" )
	RegisterCard( "badge", 189, "challenge_badge_vol_01_result.png" )
	RegisterCard( "badge", 190, "challenge_badge_vol_02_result.png" )
	RegisterCard( "badge", 191, "challenge_badge_vol_03_result.png" )
	RegisterCard( "badge", 192, "challenge_badge_w03_anniversary_result.png" )
	RegisterCard( "badge", 193, "challenge_badge_west_anniversary_1_result.png" )
	RegisterCard( "badge", 194, "challenge_badge_ww2_result.png" )
	RegisterCard( "badge", 195, "challenge_badge_xmas_03_result.png" )
	RegisterCard( "badge", 196, "challenge_badge_zodiac_01_result.png" )
	RegisterCard( "badge", 197, "challenge_badge_zsd02_01_result.png" )
	RegisterCard( "badge", 198, "challenge_badge_zsd02_02_result.png" )
	RegisterCard( "badge", 199, "challenge_badge_zsd02_03_result.png" )
	RegisterCard( "badge", 200, "challenge_badge_zsd02_04_result.png" )
	RegisterCard( "badge", 201, "challenge_badge_zsd02_05_result.png" )
	RegisterCard( "badge", 202, "challenge_badge_zsd02_06_result.png" )
	RegisterCard( "badge", 203, "challenge_badge_zsd02_07_result.png" )
	RegisterCard( "badge", 204, "challenge_badge_zsd02_08_result.png" )
	RegisterCard( "badge", 205, "challenge_badge_zsd02_09_result.png" )
	RegisterCard( "badge", 206, "challenge_badge_zsd02_10_result.png" )
	RegisterCard( "badge", 207, "challenge_badge_zsd02_11_result.png" )
	RegisterCard( "badge", 208, "challenge_badge_zsd02_12_result.png" )
	RegisterCard( "badge", 209, "challenge_badge_zsd02_13_result.png" )
	RegisterCard( "badge", 210, "challenge_badge_zsd02_result.png" )
	RegisterCard( "badge", 211, "challenge_badge_zsd_01_result.png" )
	RegisterCard( "badge", 212, "challenge_badge_zsd_02_result.png" )
	RegisterCard( "badge", 213, "challenge_badge_zsd_03_result.png" )
	RegisterCard( "badge", 214, "challenge_badge_zsd_04_result.png" )
	RegisterCard( "badge", 215, "challenge_badge_zsd_05_result.png" )
	RegisterCard( "badge", 216, "challenge_badge_zsd_06_result.png" )

	-- Register the marks.
	RegisterCard( "mark", 0, "challenge_mark_default_result.png" )
	RegisterCard( "mark", 1, "challenge_mark_01_result.png" )
	RegisterCard( "mark", 2, "challenge_mark_02_result.png" )
	RegisterCard( "mark", 3, "challenge_mark_03_result.png" )
	RegisterCard( "mark", 4, "challenge_mark_04_result.png" )
	RegisterCard( "mark", 5, "challenge_mark_05_result.png" )
	RegisterCard( "mark", 6, "challenge_mark_06_result.png" )
	RegisterCard( "mark", 7, "challenge_mark_07_result.png" )
	RegisterCard( "mark", 8, "challenge_mark_08_result.png" )
	RegisterCard( "mark", 9, "challenge_mark_09_result.png" )
	RegisterCard( "mark", 10, "challenge_mark_10_result.png" )
	RegisterCard( "mark", 11, "challenge_mark_119_result.png" )
	RegisterCard( "mark", 12, "challenge_mark_11_result.png" )
	RegisterCard( "mark", 13, "challenge_mark_120_result.png" )
	RegisterCard( "mark", 14, "challenge_mark_122_result.png" )
	RegisterCard( "mark", 15, "challenge_mark_12_result.png" )
	RegisterCard( "mark", 16, "challenge_mark_13_result.png" )
	RegisterCard( "mark", 17, "challenge_mark_14_result.png" )
	RegisterCard( "mark", 18, "challenge_mark_15_result.png" )
	RegisterCard( "mark", 19, "challenge_mark_16_result.png" )
	RegisterCard( "mark", 20, "challenge_mark_17_result.png" )
	RegisterCard( "mark", 21, "challenge_mark_18_result.png" )
	RegisterCard( "mark", 22, "challenge_mark_19_result.png" )
	RegisterCard( "mark", 23, "challenge_mark_1april_02_result.png" )
	RegisterCard( "mark", 24, "challenge_mark_1april_result.png" )
	RegisterCard( "mark", 25, "challenge_mark_20_result.png" )
	RegisterCard( "mark", 26, "challenge_mark_21_result.png" )
	RegisterCard( "mark", 27, "challenge_mark_22_result.png" )
	RegisterCard( "mark", 28, "challenge_mark_23_result.png" )
	RegisterCard( "mark", 29, "challenge_mark_24_result.png" )
	RegisterCard( "mark", 30, "challenge_mark_25_result.png" )
	RegisterCard( "mark", 31, "challenge_mark_26_result.png" )
	RegisterCard( "mark", 32, "challenge_mark_27_result.png" )
	RegisterCard( "mark", 33, "challenge_mark_28_result.png" )
	RegisterCard( "mark", 34, "challenge_mark_29_result.png" )
	RegisterCard( "mark", 35, "challenge_mark_30_result.png" )
	RegisterCard( "mark", 36, "challenge_mark_31_result.png" )
	RegisterCard( "mark", 37, "challenge_mark_32_result.png" )
	RegisterCard( "mark", 38, "challenge_mark_33_result.png" )
	RegisterCard( "mark", 39, "challenge_mark_34_result.png" )
	RegisterCard( "mark", 40, "challenge_mark_35_result.png" )
	RegisterCard( "mark", 41, "challenge_mark_36_result.png" )
	RegisterCard( "mark", 42, "challenge_mark_37_result.png" )
	RegisterCard( "mark", 43, "challenge_mark_38_result.png" )
	RegisterCard( "mark", 44, "challenge_mark_39_result.png" )
	RegisterCard( "mark", 45, "challenge_mark_40_result.png" )
	RegisterCard( "mark", 46, "challenge_mark_41_result.png" )
	RegisterCard( "mark", 47, "challenge_mark_42_result.png" )
	RegisterCard( "mark", 48, "challenge_mark_43_result.png" )
	RegisterCard( "mark", 49, "challenge_mark_44_result.png" )
	RegisterCard( "mark", 50, "challenge_mark_45_result.png" )
	RegisterCard( "mark", 51, "challenge_mark_46_result.png" )
	RegisterCard( "mark", 52, "challenge_mark_80_result.png" )
	RegisterCard( "mark", 53, "challenge_mark_81_result.png" )
	RegisterCard( "mark", 54, "challenge_mark_82_result.png" )
	RegisterCard( "mark", 55, "challenge_mark_83_result.png" )
	RegisterCard( "mark", 56, "challenge_mark_84_result.png" )
	RegisterCard( "mark", 57, "challenge_mark_85_result.png" )
	RegisterCard( "mark", 58, "challenge_mark_86_result.png" )
	RegisterCard( "mark", 59, "challenge_mark_87_result.png" )
	RegisterCard( "mark", 60, "challenge_mark_88_result.png" )
	RegisterCard( "mark", 61, "challenge_mark_89_result.png" )
	RegisterCard( "mark", 62, "challenge_mark_90_result.png" )
	RegisterCard( "mark", 63, "challenge_mark_91_result.png" )
	RegisterCard( "mark", 64, "challenge_mark_92_result.png" )
	RegisterCard( "mark", 65, "challenge_mark_93_result.png" )
	RegisterCard( "mark", 66, "challenge_mark_94_result.png" )
	RegisterCard( "mark", 67, "challenge_mark_95_result.png" )
	RegisterCard( "mark", 68, "challenge_mark_96_result.png" )
	RegisterCard( "mark", 69, "challenge_mark_97_result.png" )
	RegisterCard( "mark", 70, "challenge_mark_98_result.png" )
	RegisterCard( "mark", 71, "challenge_mark_99_result.png" )
	RegisterCard( "mark", 72, "challenge_mark_GMachievements_02_result.png" )
	RegisterCard( "mark", 73, "challenge_mark_NY_marathon_01_result.png" )
	RegisterCard( "mark", 74, "challenge_mark_afghan_01_result.png" )
	RegisterCard( "mark", 75, "challenge_mark_afghan_02_result.png" )
	RegisterCard( "mark", 76, "challenge_mark_afghan_03_result.png" )
	RegisterCard( "mark", 77, "challenge_mark_afro_01_result.png" )
	RegisterCard( "mark", 78, "challenge_mark_afro_02_result.png" )
	RegisterCard( "mark", 79, "challenge_mark_afro_03_result.png" )
	RegisterCard( "mark", 80, "challenge_mark_afro_04_result.png" )
	RegisterCard( "mark", 81, "challenge_mark_airbase_01_result.png" )
	RegisterCard( "mark", 82, "challenge_mark_airbase_02_result.png" )
	RegisterCard( "mark", 83, "challenge_mark_airbase_03_result.png" )
	RegisterCard( "mark", 84, "challenge_mark_anniversary_3_result.png" )
	RegisterCard( "mark", 85, "challenge_mark_anniversary_result.png" )
	RegisterCard( "mark", 86, "challenge_mark_aul_01_result.png" )
	RegisterCard( "mark", 87, "challenge_mark_aul_02_result.png" )
	RegisterCard( "mark", 88, "challenge_mark_aul_03_result.png" )
	RegisterCard( "mark", 89, "challenge_mark_bank_01_result.png" )
	RegisterCard( "mark", 90, "challenge_mark_bank_02_result.png" )
	RegisterCard( "mark", 91, "challenge_mark_bank_03_result.png" )
	RegisterCard( "mark", 92, "challenge_mark_blackgold_01_result.png" )
	RegisterCard( "mark", 93, "challenge_mark_blackgold_02_result.png" )
	RegisterCard( "mark", 94, "challenge_mark_blackgold_03_result.png" )
	RegisterCard( "mark", 95, "challenge_mark_bra_01_result.png" )
	RegisterCard( "mark", 96, "challenge_mark_brainstorm_result.png" )
	RegisterCard( "mark", 97, "challenge_mark_clan_race_01_result.png" )
	RegisterCard( "mark", 98, "challenge_mark_crossriver_01_result.png" )
	RegisterCard( "mark", 99, "challenge_mark_crossriver_02_result.png" )
	RegisterCard( "mark", 100, "challenge_mark_crossriver_03_result.png" )
	RegisterCard( "mark", 101, "challenge_mark_cw_01_result.png" )
	RegisterCard( "mark", 102, "challenge_mark_cw_02_result.png" )
	RegisterCard( "mark", 103, "challenge_mark_cw_03_result.png" )
	RegisterCard( "mark", 104, "challenge_mark_d17_01_result.png" )
	RegisterCard( "mark", 105, "challenge_mark_d17_02_result.png" )
	RegisterCard( "mark", 106, "challenge_mark_d17_03_result.png" )
	RegisterCard( "mark", 108, "challenge_mark_dm_01_result.png" )
	RegisterCard( "mark", 109, "challenge_mark_dm_02_result.png" )
	RegisterCard( "mark", 110, "challenge_mark_dm_03_result.png" )
	RegisterCard( "mark", 111, "challenge_mark_dm_04_result.png" )
	RegisterCard( "mark", 112, "challenge_mark_dm_05_result.png" )
	RegisterCard( "mark", 113, "challenge_mark_dm_06_result.png" )
	RegisterCard( "mark", 114, "challenge_mark_dragon_result.png" )
	RegisterCard( "mark", 115, "challenge_mark_dtown_01_result.png" )
	RegisterCard( "mark", 116, "challenge_mark_dtown_02_result.png" )
	RegisterCard( "mark", 117, "challenge_mark_dtown_03_result.png" )
	RegisterCard( "mark", 118, "challenge_mark_engineer_01_result.png" )
	RegisterCard( "mark", 119, "challenge_mark_euro2016_result.png" )
	RegisterCard( "mark", 120, "challenge_mark_factory_01_result.png" )
	RegisterCard( "mark", 121, "challenge_mark_factory_02_result.png" )
	RegisterCard( "mark", 122, "challenge_mark_factory_03_result.png" )
	RegisterCard( "mark", 123, "challenge_mark_farm_01_result.png" )
	RegisterCard( "mark", 124, "challenge_mark_farm_02_result.png" )
	RegisterCard( "mark", 125, "challenge_mark_farm_03_result.png" )
	RegisterCard( "mark", 126, "challenge_mark_fastcup_result.png" )
	RegisterCard( "mark", 127, "challenge_mark_gold_01_result.png" )
	RegisterCard( "mark", 128, "challenge_mark_gold_02_result.png" )
	RegisterCard( "mark", 129, "challenge_mark_gold_03_result.png" )
	RegisterCard( "mark", 130, "challenge_mark_gold_04_result.png" )
	RegisterCard( "mark", 131, "challenge_mark_gold_05_result.png" )
	RegisterCard( "mark", 132, "challenge_mark_gun_result.png" )
	RegisterCard( "mark", 133, "challenge_mark_hangar_01_result.png" )
	RegisterCard( "mark", 134, "challenge_mark_hangar_02_result.png" )
	RegisterCard( "mark", 135, "challenge_mark_hangar_03_result.png" )
	RegisterCard( "mark", 136, "challenge_mark_hawkrock_01_result.png" )
	RegisterCard( "mark", 137, "challenge_mark_hawkrock_02_result.png" )
	RegisterCard( "mark", 138, "challenge_mark_hawkrock_03_result.png" )
	RegisterCard( "mark", 139, "challenge_mark_hlw01_result.png" )
	RegisterCard( "mark", 140, "challenge_mark_hunt_01_result.png" )
	RegisterCard( "mark", 141, "challenge_mark_hunt_02_result.png" )
	RegisterCard( "mark", 142, "challenge_mark_hunt_03_result.png" )
	RegisterCard( "mark", 143, "challenge_mark_hunt_04_result.png" )
	RegisterCard( "mark", 144, "challenge_mark_hunt_05_result.png" )
	RegisterCard( "mark", 145, "challenge_mark_hunt_06_result.png" )
	RegisterCard( "mark", 146, "challenge_mark_hunt_07_result.png" )
	RegisterCard( "mark", 147, "challenge_mark_kra_01_result.png" )
	RegisterCard( "mark", 148, "challenge_mark_lava01_result.png" )
	RegisterCard( "mark", 149, "challenge_mark_lhouse_01_result.png" )
	RegisterCard( "mark", 150, "challenge_mark_lhouse_02_result.png" )
	RegisterCard( "mark", 151, "challenge_mark_lhouse_03_result.png" )
	RegisterCard( "mark", 152, "challenge_mark_magmaeye_01_result.png" )
	RegisterCard( "mark", 153, "challenge_mark_man_result.png" )
	RegisterCard( "mark", 154, "challenge_mark_medic_01_result.png" )
	RegisterCard( "mark", 155, "challenge_mark_meetings01_result.png" )
	RegisterCard( "mark", 156, "challenge_mark_nb_01_result.png" )
	RegisterCard( "mark", 157, "challenge_mark_nb_02_result.png" )
	RegisterCard( "mark", 158, "challenge_mark_nb_03_result.png" )
	RegisterCard( "mark", 159, "challenge_mark_nb_04_result.png" )
	RegisterCard( "mark", 160, "challenge_mark_nb_05_result.png" )
	RegisterCard( "mark", 161, "challenge_mark_nb_06_result.png" )
	RegisterCard( "mark", 162, "challenge_mark_nb_07_result.png" )
	RegisterCard( "mark", 163, "challenge_mark_nb_08_result.png" )
	RegisterCard( "mark", 164, "challenge_mark_note_01_result.png" )
	RegisterCard( "mark", 165, "challenge_mark_ny_result.png" )
	RegisterCard( "mark", 166, "challenge_mark_oildepot_01_result.png" )
	RegisterCard( "mark", 167, "challenge_mark_oildepot_02_result.png" )
	RegisterCard( "mark", 168, "challenge_mark_oildepot_03_result.png" )
	RegisterCard( "mark", 169, "challenge_mark_opencup_01_result.png" )
	RegisterCard( "mark", 170, "challenge_mark_opencup_02_result.png" )
	RegisterCard( "mark", 171, "challenge_mark_opencup_03_result.png" )
	RegisterCard( "mark", 172, "challenge_mark_palace_01_result.png" )
	RegisterCard( "mark", 173, "challenge_mark_palace_02_result.png" )
	RegisterCard( "mark", 174, "challenge_mark_palace_03_result.png" )
	RegisterCard( "mark", 175, "challenge_mark_peace_result.png" )
	RegisterCard( "mark", 176, "challenge_mark_pin-up_result.png" )
	RegisterCard( "mark", 177, "challenge_mark_pt66_result.png" )
	RegisterCard( "mark", 178, "challenge_mark_rifleman_01_result.png" )
	RegisterCard( "mark", 179, "challenge_mark_rio_01_result.png" )
	RegisterCard( "mark", 180, "challenge_mark_rio_02_result.png" )
	RegisterCard( "mark", 181, "challenge_mark_rio_03_result.png" )
	RegisterCard( "mark", 182, "challenge_mark_roccat_result.png" )
	RegisterCard( "mark", 183, "challenge_mark_rw_01_result.png" )
	RegisterCard( "mark", 184, "challenge_mark_rw_02_result.png" )
	RegisterCard( "mark", 185, "challenge_mark_rw_03_result.png" )
	RegisterCard( "mark", 186, "challenge_mark_rw_04_result.png" )
	RegisterCard( "mark", 187, "challenge_mark_rw_05_result.png" )
	RegisterCard( "mark", 188, "challenge_mark_rw_06_result.png" )
	RegisterCard( "mark", 189, "challenge_mark_rw_07_result.png" )
	RegisterCard( "mark", 190, "challenge_mark_rw_08_result.png" )
	RegisterCard( "mark", 191, "challenge_mark_rw_09_result.png" )
	RegisterCard( "mark", 192, "challenge_mark_rw_10_result.png" )
	RegisterCard( "mark", 193, "challenge_mark_rw_11_result.png" )
	RegisterCard( "mark", 194, "challenge_mark_rw_12_result.png" )
	RegisterCard( "mark", 195, "challenge_mark_rw_13_result.png" )
	RegisterCard( "mark", 196, "challenge_mark_shuttle_01_result.png" )
	RegisterCard( "mark", 197, "challenge_mark_shuttle_02_result.png" )
	RegisterCard( "mark", 198, "challenge_mark_shuttle_03_result.png" )
	RegisterCard( "mark", 199, "challenge_mark_skull_result.png" )
	RegisterCard( "mark", 200, "challenge_mark_sm_01_result.png" )
	RegisterCard( "mark", 201, "challenge_mark_sm_stage_05_result.png" )
	RegisterCard( "mark", 202, "challenge_mark_sm_stage_06_result.png" )
	RegisterCard( "mark", 203, "challenge_mark_sm_stage_07_result.png" )
	RegisterCard( "mark", 204, "challenge_mark_sm_stage_08_result.png" )
	RegisterCard( "mark", 205, "challenge_mark_sm_stage_09_result.png" )
	RegisterCard( "mark", 206, "challenge_mark_sm_stage_10_result.png" )
	RegisterCard( "mark", 207, "challenge_mark_sm_stage_11_result.png" )
	RegisterCard( "mark", 208, "challenge_mark_sm_stage_12_result.png" )
	RegisterCard( "mark", 209, "challenge_mark_sm_stage_13_result.png" )
	RegisterCard( "mark", 210, "challenge_mark_sm_stage_14_result.png" )
	RegisterCard( "mark", 211, "challenge_mark_sm_stage_15_result.png" )
	RegisterCard( "mark", 212, "challenge_mark_sm_stage_16_result.png" )
	RegisterCard( "mark", 213, "challenge_mark_sm_stage_17_result.png" )
	RegisterCard( "mark", 214, "challenge_mark_sm_stage_18_result.png" )
	RegisterCard( "mark", 215, "challenge_mark_sm_stage_19_result.png" )
	RegisterCard( "mark", 216, "challenge_mark_sniper_01_result.png" )
	RegisterCard( "mark", 217, "challenge_mark_steam01_result.png" )
	RegisterCard( "mark", 218, "challenge_mark_streetwars_01_result.png" )
	RegisterCard( "mark", 219, "challenge_mark_streetwars_02_result.png" )
	RegisterCard( "mark", 220, "challenge_mark_streetwars_03_result.png" )
	RegisterCard( "mark", 221, "challenge_mark_tower_OK_result.png" )
	RegisterCard( "mark", 222, "challenge_mark_towers_01_result.png" )
	RegisterCard( "mark", 223, "challenge_mark_towers_02_result.png" )
	RegisterCard( "mark", 224, "challenge_mark_towers_03_result.png" )
	RegisterCard( "mark", 225, "challenge_mark_vdv_result.png" )
	RegisterCard( "mark", 226, "challenge_mark_vol_01_result.png" )
	RegisterCard( "mark", 227, "challenge_mark_vol_02_result.png" )
	RegisterCard( "mark", 228, "challenge_mark_vol_03_result.png" )
	RegisterCard( "mark", 229, "challenge_mark_vol_04_result.png" )
	RegisterCard( "mark", 230, "challenge_mark_w03_anniversary_result.png" )
	RegisterCard( "mark", 231, "challenge_mark_watcher_result.png" )
	RegisterCard( "mark", 232, "challenge_mark_ww2_result.png" )
	RegisterCard( "mark", 233, "challenge_mark_xmas_01_result.png" )
	RegisterCard( "mark", 234, "challenge_mark_xmas_02_result.png" )
	RegisterCard( "mark", 235, "challenge_mark_xmas_03_result.png" )
	RegisterCard( "mark", 236, "challenge_mark_znatok_result.png" )
	RegisterCard( "mark", 237, "challenge_mark_zodiac_01_result.png" )
	RegisterCard( "mark", 238, "challenge_mark_zodiac_02_result.png" )
	RegisterCard( "mark", 239, "challenge_mark_zodiac_03_result.png" )
	RegisterCard( "mark", 240, "challenge_mark_zodiac_04_result.png" )
	RegisterCard( "mark", 241, "challenge_mark_zodiac_05_result.png" )
	RegisterCard( "mark", 242, "challenge_mark_zodiac_06_result.png" )
	RegisterCard( "mark", 243, "challenge_mark_zodiac_07_result.png" )
	RegisterCard( "mark", 244, "challenge_mark_zodiac_08_result.png" )
	RegisterCard( "mark", 245, "challenge_mark_zodiac_09_result.png" )
	RegisterCard( "mark", 246, "challenge_mark_zodiac_10_result.png" )
	RegisterCard( "mark", 247, "challenge_mark_zodiac_11_result.png" )
	RegisterCard( "mark", 248, "challenge_mark_zodiac_12_result.png" )
	RegisterCard( "mark", 249, "challenge_mark_zsd02_01_result.png" )
	RegisterCard( "mark", 250, "challenge_mark_zsd02_02_result.png" )
	RegisterCard( "mark", 251, "challenge_mark_zsd02_03_result.png" )
	RegisterCard( "mark", 252, "challenge_mark_zsd02_04_result.png" )
	RegisterCard( "mark", 253, "challenge_mark_zsd02_05_result.png" )
	RegisterCard( "mark", 254, "challenge_mark_zsd02_06_result.png" )
	RegisterCard( "mark", 255, "challenge_mark_zsd02_result.png" )
	RegisterCard( "mark", 256, "challenge_mark_zsd_01_result.png" )
	RegisterCard( "mark", 257, "challenge_mark_zsd_02_result.png" )
	RegisterCard( "mark", 258, "challenge_mark_zsd_03_result.png" )
	RegisterCard( "mark", 259, "challenge_mark_zsd_04_result.png" )
	RegisterCard( "mark", 260, "challenge_mark_zsd_05_result.png" )
	RegisterCard( "mark", 261, "challenge_mark_zsd_06_result.png" )
	RegisterCard( "mark", 262, "challenge_mark_zsd_09_result.png" )
	RegisterCard( "mark", 263, "challenge_mark_zsd_10_result.png" )

	-- Register the strips.
	RegisterCard( "strip", 0, "challenge_strip_default_result.png" )
	RegisterCard( "strip", 1, "challenge_strip_01_result.png" )
	RegisterCard( "strip", 2, "challenge_strip_02_result.png" )
	RegisterCard( "strip", 3, "challenge_strip_03_result.png" )
	RegisterCard( "strip", 4, "challenge_strip_04_result.png" )
	RegisterCard( "strip", 5, "challenge_strip_05_result.png" )
	RegisterCard( "strip", 6, "challenge_strip_06_result.png" )
	RegisterCard( "strip", 7, "challenge_strip_07_result.png" )
	RegisterCard( "strip", 8, "challenge_strip_08_result.png" )
	RegisterCard( "strip", 9, "challenge_strip_09_result.png" )
	RegisterCard( "strip", 10, "challenge_strip_108_result.png" )
	RegisterCard( "strip", 11, "challenge_strip_109_result.png" )
	RegisterCard( "strip", 12, "challenge_strip_10_result.png" )
	RegisterCard( "strip", 13, "challenge_strip_110_result.png" )
	RegisterCard( "strip", 14, "challenge_strip_111_result.png" )
	RegisterCard( "strip", 15, "challenge_strip_112_result.png" )
	RegisterCard( "strip", 16, "challenge_strip_113_result.png" )
	RegisterCard( "strip", 17, "challenge_strip_114_result.png" )
	RegisterCard( "strip", 18, "challenge_strip_116_result.png" )
	RegisterCard( "strip", 19, "challenge_strip_117_result.png" )
	RegisterCard( "strip", 20, "challenge_strip_118_result.png" )
	RegisterCard( "strip", 21, "challenge_strip_119_result.png" )
	RegisterCard( "strip", 22, "challenge_strip_11_result.png" )
	RegisterCard( "strip", 23, "challenge_strip_121_result.png" )
	RegisterCard( "strip", 24, "challenge_strip_122_result.png" )
	RegisterCard( "strip", 25, "challenge_strip_123_result.png" )
	RegisterCard( "strip", 26, "challenge_strip_124_result.png" )
	RegisterCard( "strip", 27, "challenge_strip_126_result.png" )
	RegisterCard( "strip", 28, "challenge_strip_128_result.png" )
	RegisterCard( "strip", 29, "challenge_strip_129_result.png" )
	RegisterCard( "strip", 30, "challenge_strip_12_result.png" )
	RegisterCard( "strip", 31, "challenge_strip_130_result.png" )
	RegisterCard( "strip", 32, "challenge_strip_131_result.png" )
	RegisterCard( "strip", 33, "challenge_strip_132_result.png" )
	RegisterCard( "strip", 34, "challenge_strip_133_result.png" )
	RegisterCard( "strip", 35, "challenge_strip_134_result.png" )
	RegisterCard( "strip", 36, "challenge_strip_135_result.png" )
	RegisterCard( "strip", 37, "challenge_strip_136_result.png" )
	RegisterCard( "strip", 38, "challenge_strip_137_result.png" )
	RegisterCard( "strip", 39, "challenge_strip_13_result.png" )
	RegisterCard( "strip", 40, "challenge_strip_14_result.png" )
	RegisterCard( "strip", 41, "challenge_strip_15_result.png" )
	RegisterCard( "strip", 42, "challenge_strip_16_result.png" )
	RegisterCard( "strip", 43, "challenge_strip_17_result.png" )
	RegisterCard( "strip", 44, "challenge_strip_18_result.png" )
	RegisterCard( "strip", 45, "challenge_strip_19_result.png" )
	RegisterCard( "strip", 46, "challenge_strip_20_result.png" )
	RegisterCard( "strip", 47, "challenge_strip_21_result.png" )
	RegisterCard( "strip", 48, "challenge_strip_22_result.png" )
	RegisterCard( "strip", 49, "challenge_strip_23_result.png" )
	RegisterCard( "strip", 50, "challenge_strip_24_result.png" )
	RegisterCard( "strip", 51, "challenge_strip_25_result.png" )
	RegisterCard( "strip", 52, "challenge_strip_26_result.png" )
	RegisterCard( "strip", 53, "challenge_strip_27_result.png" )
	RegisterCard( "strip", 54, "challenge_strip_28_result.png" )
	RegisterCard( "strip", 55, "challenge_strip_29_result.png" )
	RegisterCard( "strip", 56, "challenge_strip_30_result.png" )
	RegisterCard( "strip", 57, "challenge_strip_31_result.png" )
	RegisterCard( "strip", 58, "challenge_strip_32_result.png" )
	RegisterCard( "strip", 59, "challenge_strip_33_result.png" )
	RegisterCard( "strip", 60, "challenge_strip_34_result.png" )
	RegisterCard( "strip", 61, "challenge_strip_35_result.png" )
	RegisterCard( "strip", 62, "challenge_strip_42_result.png" )
	RegisterCard( "strip", 63, "challenge_strip_43_result.png" )
	RegisterCard( "strip", 64, "challenge_strip_44_result.png" )
	RegisterCard( "strip", 65, "challenge_strip_45_result.png" )
	RegisterCard( "strip", 66, "challenge_strip_46_result.png" )
	RegisterCard( "strip", 67, "challenge_strip_47_result.png" )
	RegisterCard( "strip", 68, "challenge_strip_48_result.png" )
	RegisterCard( "strip", 69, "challenge_strip_49_result.png" )
	RegisterCard( "strip", 70, "challenge_strip_50_result.png" )
	RegisterCard( "strip", 71, "challenge_strip_51_result.png" )
	RegisterCard( "strip", 72, "challenge_strip_86_result.png" )
	RegisterCard( "strip", 73, "challenge_strip_87_result.png" )
	RegisterCard( "strip", 74, "challenge_strip_88_result.png" )
	RegisterCard( "strip", 75, "challenge_strip_89_result.png" )
	RegisterCard( "strip", 76, "challenge_strip_90_result.png" )
	RegisterCard( "strip", 77, "challenge_strip_91_result.png" )
	RegisterCard( "strip", 78, "challenge_strip_92_result.png" )
	RegisterCard( "strip", 79, "challenge_strip_93_result.png" )
	RegisterCard( "strip", 80, "challenge_strip_94_result.png" )
	RegisterCard( "strip", 81, "challenge_strip_NY_marathon_01_result.png" )
	RegisterCard( "strip", 82, "challenge_strip_afro_01_result.png" )
	RegisterCard( "strip", 83, "challenge_strip_afro_02_result.png" )
	RegisterCard( "strip", 84, "challenge_strip_afro_03_result.png" )
	RegisterCard( "strip", 85, "challenge_strip_afro_04_result.png" )
	RegisterCard( "strip", 86, "challenge_strip_anniversary_01_result.png" )
	RegisterCard( "strip", 87, "challenge_strip_anniversary_02_result.png" )
	RegisterCard( "strip", 88, "challenge_strip_bra_01_result.png" )
	RegisterCard( "strip", 89, "challenge_strip_brainstorm_result.png" )
	RegisterCard( "strip", 90, "challenge_strip_camo02_01_result.png" )
	RegisterCard( "strip", 91, "challenge_strip_camo02_02_result.png" )
	RegisterCard( "strip", 92, "challenge_strip_camo04_result.png" )
	RegisterCard( "strip", 93, "challenge_strip_camo07_result.png" )
	RegisterCard( "strip", 94, "challenge_strip_cw_01_result.png" )
	RegisterCard( "strip", 95, "challenge_strip_cw_02_result.png" )
	RegisterCard( "strip", 96, "challenge_strip_cw_03_result.png" )
	RegisterCard( "strip", 98, "challenge_strip_dm_01_result.png" )
	RegisterCard( "strip", 99, "challenge_strip_dm_02_result.png" )
	RegisterCard( "strip", 100, "challenge_strip_dm_03_result.png" )
	RegisterCard( "strip", 101, "challenge_strip_dm_04_result.png" )
	RegisterCard( "strip", 102, "challenge_strip_dragon_result.png" )
	RegisterCard( "strip", 103, "challenge_strip_elite_warface_result.png" )
	RegisterCard( "strip", 104, "challenge_strip_esl_01_result.png" )
	RegisterCard( "strip", 105, "challenge_strip_esl_02_result.png" )
	RegisterCard( "strip", 106, "challenge_strip_esl_03_result.png" )
	RegisterCard( "strip", 107, "challenge_strip_euro2016_result.png" )
	RegisterCard( "strip", 108, "challenge_strip_fastcup_result.png" )
	RegisterCard( "strip", 109, "challenge_strip_guard01_result.png" )
	RegisterCard( "strip", 110, "challenge_strip_hands_result.png" )
	RegisterCard( "strip", 111, "challenge_strip_hawkrock_01_result.png" )
	RegisterCard( "strip", 112, "challenge_strip_hawkrock_02_result.png" )
	RegisterCard( "strip", 113, "challenge_strip_hawkrock_03_result.png" )
	RegisterCard( "strip", 114, "challenge_strip_hlw01_result.png" )
	RegisterCard( "strip", 115, "challenge_strip_hunt_01_result.png" )
	RegisterCard( "strip", 116, "challenge_strip_hunt_02_result.png" )
	RegisterCard( "strip", 117, "challenge_strip_hunt_03_result.png" )
	RegisterCard( "strip", 118, "challenge_strip_kra_01_result.png" )
	RegisterCard( "strip", 119, "challenge_strip_lava01_result.png" )
	RegisterCard( "strip", 120, "challenge_strip_lava02_result.png" )
	RegisterCard( "strip", 121, "challenge_strip_lava03_result.png" )
	RegisterCard( "strip", 122, "challenge_strip_lava04_result.png" )
	RegisterCard( "strip", 123, "challenge_strip_lava05_result.png" )
	RegisterCard( "strip", 124, "challenge_strip_mod_result.png" )
	RegisterCard( "strip", 125, "challenge_strip_nb_01_result.png" )
	RegisterCard( "strip", 126, "challenge_strip_nb_02_result.png" )
	RegisterCard( "strip", 127, "challenge_strip_nb_03_result.png" )
	RegisterCard( "strip", 128, "challenge_strip_nb_04_result.png" )
	RegisterCard( "strip", 129, "challenge_strip_nb_05_result.png" )
	RegisterCard( "strip", 130, "challenge_strip_nb_06_result.png" )
	RegisterCard( "strip", 131, "challenge_strip_nb_07_result.png" )
	RegisterCard( "strip", 132, "challenge_strip_nb_08_result.png" )
	RegisterCard( "strip", 133, "challenge_strip_nb_09_result.png" )
	RegisterCard( "strip", 134, "challenge_strip_nb_10_result.png" )
	RegisterCard( "strip", 135, "challenge_strip_nb_11_result.png" )
	RegisterCard( "strip", 136, "challenge_strip_nb_12_result.png" )
	RegisterCard( "strip", 137, "challenge_strip_nb_13_result.png" )
	RegisterCard( "strip", 138, "challenge_strip_nb_14_result.png" )
	RegisterCard( "strip", 139, "challenge_strip_nb_15_result.png" )
	RegisterCard( "strip", 140, "challenge_strip_nb_16_result.png" )
	RegisterCard( "strip", 141, "challenge_strip_nb_17_result.png" )
	RegisterCard( "strip", 142, "challenge_strip_nb_18_result.png" )
	RegisterCard( "strip", 143, "challenge_strip_nb_19_result.png" )
	RegisterCard( "strip", 144, "challenge_strip_nb_20_result.png" )
	RegisterCard( "strip", 145, "challenge_strip_nb_21_result.png" )
	RegisterCard( "strip", 146, "challenge_strip_nb_22_result.png" )
	RegisterCard( "strip", 147, "challenge_strip_nb_23_result.png" )
	RegisterCard( "strip", 148, "challenge_strip_nb_24_result.png" )
	RegisterCard( "strip", 149, "challenge_strip_nb_25_result.png" )
	RegisterCard( "strip", 150, "challenge_strip_nb_26_result.png" )
	RegisterCard( "strip", 151, "challenge_strip_nb_27_result.png" )
	RegisterCard( "strip", 152, "challenge_strip_nb_28_result.png" )
	RegisterCard( "strip", 153, "challenge_strip_nb_29_result.png" )
	RegisterCard( "strip", 154, "challenge_strip_nb_30_result.png" )
	RegisterCard( "strip", 155, "challenge_strip_nb_31_result.png" )
	RegisterCard( "strip", 156, "challenge_strip_nb_32_result.png" )
	RegisterCard( "strip", 157, "challenge_strip_nb_33_result.png" )
	RegisterCard( "strip", 158, "challenge_strip_nb_34_result.png" )
	RegisterCard( "strip", 159, "challenge_strip_nb_35_result.png" )
	RegisterCard( "strip", 160, "challenge_strip_nb_36_result.png" )
	RegisterCard( "strip", 161, "challenge_strip_nb_37_result.png" )
	RegisterCard( "strip", 162, "challenge_strip_nb_38_result.png" )
	RegisterCard( "strip", 163, "challenge_strip_nb_39_result.png" )
	RegisterCard( "strip", 164, "challenge_strip_nb_40_result.png" )
	RegisterCard( "strip", 165, "challenge_strip_nb_41_result.png" )
	RegisterCard( "strip", 166, "challenge_strip_nb_42_result.png" )
	RegisterCard( "strip", 167, "challenge_strip_nb_43_result.png" )
	RegisterCard( "strip", 168, "challenge_strip_nb_44_result.png" )
	RegisterCard( "strip", 169, "challenge_strip_nb_45_result.png" )
	RegisterCard( "strip", 170, "challenge_strip_nb_46_result.png" )
	RegisterCard( "strip", 171, "challenge_strip_nb_47_result.png" )
	RegisterCard( "strip", 172, "challenge_strip_nb_48_result.png" )
	RegisterCard( "strip", 173, "challenge_strip_nb_49_result.png" )
	RegisterCard( "strip", 174, "challenge_strip_nb_50_result.png" )
	RegisterCard( "strip", 175, "challenge_strip_nb_51_result.png" )
	RegisterCard( "strip", 176, "challenge_strip_nb_52_result.png" )
	RegisterCard( "strip", 177, "challenge_strip_nb_53_result.png" )
	RegisterCard( "strip", 178, "challenge_strip_nb_54_result.png" )
	RegisterCard( "strip", 179, "challenge_strip_note_01_result.png" )
	RegisterCard( "strip", 180, "challenge_strip_ny_result.png" )
	RegisterCard( "strip", 181, "challenge_strip_opencup_01_result.png" )
	RegisterCard( "strip", 182, "challenge_strip_opencup_02_result.png" )
	RegisterCard( "strip", 183, "challenge_strip_pin-up_result.png" )
	RegisterCard( "strip", 184, "challenge_strip_pt66_result.png" )
	RegisterCard( "strip", 185, "challenge_strip_rg_01_result.png" )
	RegisterCard( "strip", 186, "challenge_strip_rg_02_result.png" )
	RegisterCard( "strip", 187, "challenge_strip_rg_03_result.png" )
	RegisterCard( "strip", 188, "challenge_strip_rg_04_result.png" )
	RegisterCard( "strip", 189, "challenge_strip_roccat_result.png" )
	RegisterCard( "strip", 190, "challenge_strip_rw_01_result.png" )
	RegisterCard( "strip", 191, "challenge_strip_rw_02_result.png" )
	RegisterCard( "strip", 192, "challenge_strip_rw_03_result.png" )
	RegisterCard( "strip", 193, "challenge_strip_rw_04_result.png" )
	RegisterCard( "strip", 194, "challenge_strip_rw_05_result.png" )
	RegisterCard( "strip", 195, "challenge_strip_rw_06_result.png" )
	RegisterCard( "strip", 196, "challenge_strip_rw_07_result.png" )
	RegisterCard( "strip", 197, "challenge_strip_rw_08_result.png" )
	RegisterCard( "strip", 198, "challenge_strip_rw_09_result.png" )
	RegisterCard( "strip", 199, "challenge_strip_rw_10_result.png" )
	RegisterCard( "strip", 200, "challenge_strip_rw_11_result.png" )
	RegisterCard( "strip", 201, "challenge_strip_sm_01_result.png" )
	RegisterCard( "strip", 202, "challenge_strip_sm_02_result.png" )
	RegisterCard( "strip", 203, "challenge_strip_sm_03_result.png" )
	RegisterCard( "strip", 204, "challenge_strip_sm_04_result.png" )
	RegisterCard( "strip", 205, "challenge_strip_sm_05_result.png" )
	RegisterCard( "strip", 206, "challenge_strip_snake_result.png" )
	RegisterCard( "strip", 207, "challenge_strip_steam01_result.png" )
	RegisterCard( "strip", 208, "challenge_strip_vdv_result.png" )
	RegisterCard( "strip", 209, "challenge_strip_vol_01_result.png" )
	RegisterCard( "strip", 210, "challenge_strip_vol_02_result.png" )
	RegisterCard( "strip", 211, "challenge_strip_vol_03_result.png" )
	RegisterCard( "strip", 212, "challenge_strip_vol_04_result.png" )
	RegisterCard( "strip", 213, "challenge_strip_vol_05_result.png" )
	RegisterCard( "strip", 214, "challenge_strip_vol_06_result.png" )
	RegisterCard( "strip", 215, "challenge_strip_vol_07_result.png" )
	RegisterCard( "strip", 216, "challenge_strip_w01_anniversary_result.png" )
	RegisterCard( "strip", 217, "challenge_strip_w02_anniversary_result.png" )
	RegisterCard( "strip", 218, "challenge_strip_w03_anniversary_result.png" )
	RegisterCard( "strip", 219, "challenge_strip_ww2_2_result.png" )
	RegisterCard( "strip", 220, "challenge_strip_ww2_result.png" )
	RegisterCard( "strip", 221, "challenge_strip_xmas_01_result.png" )
	RegisterCard( "strip", 222, "challenge_strip_xmas_03_result.png" )
	RegisterCard( "strip", 223, "challenge_strip_zodiac_01_result.png" )
	RegisterCard( "strip", 224, "challenge_strip_zsd02_01_result.png" )
	RegisterCard( "strip", 225, "challenge_strip_zsd02_02_result.png" )
	RegisterCard( "strip", 226, "challenge_strip_zsd02_03_result.png" )
	RegisterCard( "strip", 227, "challenge_strip_zsd02_04_result.png" )
	RegisterCard( "strip", 228, "challenge_strip_zsd02_05_result.png" )
	RegisterCard( "strip", 229, "challenge_strip_zsd02_06_result.png" )
	RegisterCard( "strip", 230, "challenge_strip_zsd02_07_result.png" )
	RegisterCard( "strip", 231, "challenge_strip_zsd02_result.png" )
	RegisterCard( "strip", 232, "challenge_strip_zsd_01_result.png" )
	RegisterCard( "strip", 233, "challenge_strip_zsd_02_result.png" )
	RegisterCard( "strip", 234, "challenge_strip_zsd_03_result.png" )
	RegisterCard( "strip", 235, "challenge_strip_zsd_04_result.png" )
	RegisterCard( "strip", 236, "challenge_strip_zsd_05_result.png" )
	RegisterCard( "strip", 237, "challenge_strip_zsd_06_result.png" )
	RegisterCard( "strip", 238, "challenge_strip_zsd_07_result.png" )
	RegisterCard( "strip", 239, "challenge_strip_zsd_08_result.png" )
	RegisterCard( "strip", 240, "challenge_strip_zsd_09_result.png" )
	RegisterCard( "strip", 241, "challenge_strip_zsd_10_result.png" )
	RegisterCard( "strip", 242, "challenge_strip_zsd_11_result.png" )
	RegisterCard( "strip", 243, "challenge_stripe_GMachievements_01_result.png" )

end

addCards()

-- Thankfully it's easier to add the ranks...
function addRanks()

	for k, v in pairs( file.Find( "materials/uprising/ranks/*.png", "GAME" ) ) do

		table.insert( playerCards.ranks, v )

	end

end

addRanks()

-- Just an easy accessor for getting the rank material, saves time.
function plyMeta:GetRankMaterial()

	if self:GetLevel() >= #playerCards.ranks then

		return Material( "uprising/ranks/".. playerCards.ranks[ #playerCards.ranks ] )

	else

		if self:IsBot() then

			return Material( "uprising/ranks/".. playerCards.ranks[ math.random( 1, 80 ) ] )

		else

			return Material( "uprising/ranks/".. playerCards.ranks[ self:GetLevel() + 1 ] )

		end

	end

end

function openCardEditor()

	local time = SysTime()

	local editor = vgui.Create( "DFrame" )
	editor:SetSize( ScrW(), ScrH() )
	editor:MakePopup()
	editor:Center()
	editor.Paint = function( panel, w, h )
	
		Derma_DrawBackgroundBlur( panel, time )
	
	end

	local wank = editor:Add( "DPanel" )
	wank:Dock( FILL )
	wank.previewBadge = nil
	wank.badgeID = nil
	wank.previewMark = nil
	wank.markID = nil
	wank.previewStrip = nil
	wank.stripID = nil

	wank.Paint = function( panel, w, h )

		local basew = w / 2 - 256

		if panel.previewStrip then

			surface.SetMaterial( Material( "uprising/playercards/".. panel.previewStrip ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( basew, h / 2 - 64, 512, 128 )

			surface.SetMaterial( LocalPlayer():GetRankMaterial() )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( basew + 128, h / 2 - 32, 64, 64 )

			draw.SimpleTextOutlined( LocalPlayer():Name(), "RobotoBig", basew + 128 + 64, h / 2, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 2, Color( 0, 0, 0 ) )

		end

		if panel.previewBadge then

			surface.SetMaterial( Material( "uprising/playercards/".. panel.previewBadge ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( basew, h / 2 - 64, 128, 128 )

		end

		if panel.previewMark then

			surface.SetMaterial( Material( "uprising/playercards/".. panel.previewMark ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( basew, h / 2 - 64, 128, 128 )

		end

	end

	local topbit = wank:Add( "DPanel" )
	topbit:Dock( TOP )
	topbit:SetTall( 64 )
	topbit.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local badgetext = topbit:Add( "DLabel" )
	badgetext:Dock( LEFT )
	badgetext:SetWide( 128 )
	badgetext:SetText( "Badges" )
	badgetext:SetFont( "HUD2" )
	badgetext:SetTextColor( Color( 205, 205, 205 ) )
	badgetext:SetContentAlignment( 5 )

	local marktext = topbit:Add( "DLabel" )
	marktext:Dock( LEFT )
	marktext:SetWide( 128 )
	marktext:SetText( "Marks" )
	marktext:SetFont( "HUD2" )
	marktext:SetTextColor( Color( 205, 205, 205 ) )
	marktext:SetContentAlignment( 5 )

	local stripetext = topbit:Add( "DLabel" )
	stripetext:Dock( RIGHT )
	stripetext:SetWide( 256 )
	stripetext:SetText( "Stripes" )
	stripetext:SetFont( "HUD2" )
	stripetext:SetTextColor( Color( 205, 205, 205 ) )
	stripetext:SetContentAlignment( 5 )

	local title = topbit:Add( "DLabel" )
	title:Dock( FILL )
	title:SetWide( 256 )
	title:SetText( "Card Editor" )
	title:SetFont( "Roboto" )
	title:SetTextColor( Color( 205, 205, 205 ) )
	title:SetContentAlignment( 5 )

	local iconbg = wank:Add( "DPanel" )
	iconbg:Dock( LEFT )
	iconbg:SetWide( 128 )
	iconbg.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local iconscroll = iconbg:Add( "DScrollPanel" )
	iconscroll:Dock( FILL )
	iconscroll.selected = false

	local scrollbar = iconscroll:GetVBar( )
	
	function scrollbar:Paint( w, h )
	
		surface.SetDrawColor( 35, 35, 35, 55 )
		surface.DrawRect( 8, 0, 4, h )
		
	end
	
	function scrollbar.btnGrip:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 0, 10, h )
	
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 6, 1, 8, h - 2 )
		
	end
	
	function scrollbar.btnUp:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end
	
	function scrollbar.btnDown:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end

	local iconbg2 = wank:Add( "DPanel" )
	iconbg2:Dock( LEFT )
	iconbg2:SetWide( 128 )
	iconbg2.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local iconscroll2 = iconbg2:Add( "DScrollPanel" )
	iconscroll2:Dock( FILL )
	iconscroll2.selected = false

	local scrollbar = iconscroll2:GetVBar( )
	
	function scrollbar:Paint( w, h )
	
		surface.SetDrawColor( 35, 35, 35, 55 )
		surface.DrawRect( 8, 0, 4, h )
		
	end
	
	function scrollbar.btnGrip:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 0, 10, h )
	
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 6, 1, 8, h - 2 )
		
	end
	
	function scrollbar.btnUp:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end
	
	function scrollbar.btnDown:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end

	for k, v in pairs( playerCards.badges ) do

		local aaa = iconscroll:Add( "DButton" )
		aaa:Dock( TOP )
		aaa:SetTall( 64 )
		aaa:SetText( "" )
		aaa.icon = playerCards.badges[ k ]
		aaa.id = k
		aaa.Paint = function( panel, w, h )

			if panel.id > 0 then

				surface.SetDrawColor( 255, 255, 255 )
				surface.SetMaterial( Material( "uprising/playercards/".. panel.icon ) )
				surface.DrawTexturedRect( w * 0.25, 0, h, h )

			else

				draw.SimpleTextOutlined( "BLANK", "HUD", w / 2 + 4, h / 2, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

			end

			if iconscroll.selected == panel then

				surface.SetDrawColor( 255, 255, 255 )
				surface.DrawOutlinedRect( 8, 0, w - 8, h )

			end

		end

		aaa.DoClick = function( panel )

			wank.previewBadge = panel.icon
			wank.badgeID = panel.id
			iconscroll.selected = panel

		end

	end

	for k, v in pairs( playerCards.marks ) do

		local aaa = iconscroll2:Add( "DButton" )
		aaa:Dock( TOP )
		aaa:SetTall( 64 )
		aaa:SetText( "" )
		aaa.icon = playerCards.marks[ k ]
		aaa.id = k
		aaa.Paint = function( panel, w, h )

			if panel.id > 0 then

				surface.SetDrawColor( 255, 255, 255 )
				surface.SetMaterial( Material( "uprising/playercards/".. panel.icon ) )
				surface.DrawTexturedRect( w * 0.25, 0, h, h )

			else

				draw.SimpleTextOutlined( "BLANK", "HUD", w / 2 + 4, h / 2, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

			end

			if iconscroll2.selected == panel then

				surface.SetDrawColor( 255, 255, 255 )
				surface.DrawOutlinedRect( 8, 0, w - 8, h )

			end

		end

		aaa.DoClick = function( panel )

			wank.previewMark = panel.icon
			wank.markID = panel.id
			iconscroll2.selected = panel

		end

	end

	local stripbg = wank:Add( "DPanel" )
	stripbg:Dock( RIGHT )
	stripbg:SetWide( 256 )
	stripbg.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local stripscroll = stripbg:Add( "DScrollPanel" )
	stripscroll:Dock( FILL )
	stripscroll.selected = false

	local scrollbar = stripscroll:GetVBar( )
	
	function scrollbar:Paint( w, h )
	
		surface.SetDrawColor( 35, 35, 35, 55 )
		surface.DrawRect( 8, 0, 4, h )
		
	end
	
	function scrollbar.btnGrip:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 0, 10, h )
	
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 6, 1, 8, h - 2 )
		
	end
	
	function scrollbar.btnUp:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end
	
	function scrollbar.btnDown:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end

	for k, v in pairs( playerCards.strips ) do

		local aaa = stripscroll:Add( "DButton" )
		aaa:Dock( TOP )
		aaa:DockMargin( 2, 0, 2, 0 )
		aaa:SetTall( 64 )
		aaa:SetText( "" )
		aaa.icon = v
		aaa.id = k
		aaa.Paint = function( panel, w, h )

			if panel.id > 0 then

				surface.SetDrawColor( 255, 255, 255 )
				surface.SetMaterial( Material( "uprising/playercards/".. panel.icon ) )
				surface.DrawTexturedRect( 0, 0, w, h )

			else

				draw.SimpleTextOutlined( "BLANK", "HUD", w / 2 + 4, h / 2, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

			end

			if stripscroll.selected == panel then

				surface.SetDrawColor( 255, 255, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )

			end

		end

		aaa.DoClick = function( panel )

			wank.previewStrip = panel.icon
			wank.stripID = panel.id
			stripscroll.selected = panel

		end

	end

	local update = stripbg:Add( "DButton" )
	update:Dock( BOTTOM )
	update:SetTall( 64 )
	update:SetText( "Apply Changes" )
	update:SetToolTip( "Select an icon and a stripe to continue." )
	update:SetFont( "HUD2" )
	update:SetTextColor( Color( 205, 205, 205 ) )
	update.DoClick = function( panel )

		local badge, mark, strip = LocalPlayer():GetPlayerCard()

		if wank.previewBadge and wank.previewMark and wank.previewStrip then

			if( wank.badgeID != badge or wank.markID != mark or wank.stripID != strip ) then

				net.Start( "UpdatePlayerCard" )

					net.WriteInt( wank.badgeID, 12 )
					net.WriteInt( wank.markID, 12 )
					net.WriteInt( wank.stripID, 12 )

				net.SendToServer()

				surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

				editor:Close()

			end

		end

	end
	update.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local badge, mark, strip = LocalPlayer():GetPlayerCard()

	wank.previewBadge = playerCards.badges[ tonumber( badge ) ]
	wank.badgeID = tonumber( badge )

	wank.previewMark = playerCards.marks[ tonumber( mark ) ]
	wank.markID = tonumber( mark )

	wank.previewStrip = playerCards.strips[ tonumber( strip ) ]
	wank.stripID = tonumber( strip )

end
