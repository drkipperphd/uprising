util.AddNetworkString( "UpdatePlayerCard" )
util.AddNetworkString( "RequestPlayerCard" )

local plyMeta = FindMetaTable( "Player" )

net.Receive( "UpdatePlayerCard", function( len, ply )

	local badge = net.ReadInt( 12 )
	local mark = net.ReadInt( 12 )
	local strip = net.ReadInt( 12 )

	ply:UpdateBadge( badge )
	ply:UpdateMark( mark )
	ply:UpdateStripe( strip )

end )

function plyMeta:UpdateBadge( id )

	self:SetNWInt( "playerCard_badge", id )
	self:SetPData( "playerCard_badge", id )

end

function plyMeta:UpdateMark( id )

	self:SetNWInt( "playerCard_mark", id )
	self:SetPData( "playerCard_mark", id )

end

function plyMeta:UpdateStripe( id )

	self:SetNWInt( "playerCard_stripe", id )
	self:SetPData( "playerCard_stripe", id )

end

hook.Add( "PlayerAuthed", "playerCard_Setup", function( ply )

	local badge = ply:GetPData( "playerCard_badge", -1 )
	local mark = ply:GetPData( "playerCard_mark", -1 )
	local stripe = ply:GetPData( "playerCard_stripe", -1 )

	if badge == -1 then

		print( "no card badge for ".. ply:Name() )

		ply:SetPData( "playerCard_badge", 1 )
		ply:SetNWInt( "playerCard_badge", 1 )

	end

	if mark == -1 then

		print( "no card mark for ".. ply:Name() )

		ply:SetPData( "playerCard_mark", 1 )
		ply:SetNWInt( "playerCard_mark", 1 )

	end

	if stripe == -1 then

		print( "no card stripe for ".. ply:Name() )

		ply:SetPData( "playerCard_stripe", 1 )
		ply:SetNWInt( "playerCard_stripe", 1 )

	end

	if tonumber( badge ) > 0 then

		ply:SetNWInt( "playerCard_badge", badge )

	end

	if tonumber( mark ) > 0 then

		ply:SetNWInt( "playerCard_mark", mark )

	end

	if tonumber( stripe ) > 0 then

		ply:SetNWInt( "playerCard_stripe", stripe )

	end

end )