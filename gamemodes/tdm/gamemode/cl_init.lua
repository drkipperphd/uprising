include( "cl_wepselect.lua" )
include( "cl_deathnotice.lua" )
include( "cl_voice.lua" )
include( "cl_levels.lua" )
include( "cl_playercards.lua" )
include( "cl_weapon_skins.lua" )
include( "shared.lua" )
include( "sh_levels.lua" )
include( "sh_mapvote.lua" )
include( "sh_playercards.lua" )
include( "sh_weapon_skins.lua" )

local blur = Material( "pp/blurscreen" )
local health = 0
local armor = 0
local ammo = 0
local time = 0
local btimer = 0
local otimer = 0
local ctimer = 0
local rtimer = 0

local pickingtype = "none"

local currentwep = nil
	
local primarySelected = false
local secondarySelected = false
local grenadeSelected = false
local meleeSelected = false
local pattachments = {}
local sattachments = {}
local pskin = {}
local sskin = {}
local Bullet = surface.GetTextureID("cw2/gui/bullet")

local ring = Material( "vgui/clumpspread_ring" )
local soundwaves = Material( "hl2u/soundwaves.png", "smooth noclamp" )
local striderbase = Material( "uprising/mission_03_bossmeter.png", "smooth noclamp" )
local striderbar = Material( "uprising/mission_03_bossbar.png", "smooth noclamp" )
local plus = Material( "uprising/plus.png", "smooth noclamp" )
local shield = Material( "uprising/shield.png", "smooth noclamp" )
local gradient = surface.GetTextureID("gui/gradient_up")
local gradient2 = surface.GetTextureID("gui/gradient_down")
local currentsong
local songvolume = 0.5
local songenabled = true
local coop = string.find( game.GetMap(), "mission" )
local coopcount = false
local coopcountnum = 0
local gametype = string.lower( GetConVar( "uprising_gametype" ):GetString( ) )
local nicegametype = GetConVar( "uprising_gametype" ):GetString( )
local maxkills = GetConVar( "uprising_maxkills" ):GetInt( )
local songenabled2 = CreateClientConVar( "song_enabled", "true", true, false )
local songvolume2 = CreateClientConVar( "song_volume", "50", true, false )
local playerfrags = {}
local armsracelist = {}
local armsracelist2 = {}
local damageindicators = {}
local damageindicator = Material( "uprising/damage.png", "smooth noclamp" )
local nadeindicator = Material( "uprising/damage_nade.png", "smooth noclamp" )

local nums = { Material( "uprising/num1.png" ), Material( "uprising/num2.png" ), Material( "uprising/num3.png" ), Material( "uprising/num4.png" ), Material( "uprising/num5.png" ) }
local cletters = { Material( "uprising/letters.png" ), Material( "uprising/letterr.png" ), Material( "uprising/lettera.png" ), Material( "uprising/lettert.png" ) }

local lettercolors = {}

local welcomescreen
local tuttext

local ownedweapons = ownedweapons or {}
local ownedboxes = ownedboxes or {}
ownedskins = ownedskins or {}

local modeloffsets = {}
local vmodeloffsets = {}

local drawhit = false
local hitalpha = 0
local HitCol = Color( 255, 255, 255, 255 )

local roundEnded = false

local customisationsounds = { "uprising/frontend/customisationsounds/ui_weapon_customize_barrel_01.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_barrel_02.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_bipot_off.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_bipot_on.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_grip_off.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_grip_on_01.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_mag_on_01.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_mag_on_02.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_muzzle_01.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_muzzle_02.wav", "uprising/frontend/customisationsounds/ui_weapon_customize_muzzle_03.wav" }

-- Let's give it that sweet, sweet hitmarker.
net.Receive( "Hitmarker", function()

	local headshot = net.ReadBool()

	-- Make sure it only draws when it hits, set the alpha to 255 so it's fully visible.
	drawhit = true
	hitalpha = 255

	if headshot then

		HitCol = Color( 255, 0, 0, 255 )

		-- Give a fancier one for headshots!

		surface.PlaySound( "uprising/ui_hit_heatshot2.wav" )

	else

		HitCol = Color( 255, 255, 255, 255 )

		surface.PlaySound( "uprising/ui_hit_common_0".. math.random( 2, 3 ) ..".wav" )

	end

end )

hook.Remove( "HUDPaint", "HitmarkerDrawer" )

function AddOffset( ent, campos, lookat )

	modeloffsets[ ent ] = {}
	modeloffsets[ ent ].campos = campos
	modeloffsets[ ent ].lookat = lookat

end

function AddVOffset( ent, campos, lookat )

	if !vmodeloffsets[ ent ] then

		vmodeloffsets[ ent ] = {}

	end

	vmodeloffsets[ ent ].campos = campos
	vmodeloffsets[ ent ].lookat = lookat

end

function AddVBone( ent, bone )

	if !vmodeloffsets[ ent ] then

		vmodeloffsets[ ent ] = {}

	end

	vmodeloffsets[ ent ].bone = bone

end

-- AddOffset( weapon entity, camera position, lookat position )
-- Assault Rifle Offsets
AddOffset( "draggo_ak74m", Vector( 0, 65, 10 ), Vector( 1, 0, 5 ) )
AddVOffset( "draggo_ak74m", Vector( 3, 25, 5 ), Vector( 3, 0, 0 ) )
AddVBone( "draggo_ak74m", "Ak74u_bone" )

AddOffset( "draggo_cm901", Vector( 0, 50, 9 ), Vector( 4, 0, 4 ) )
AddVOffset( "draggo_cm901", Vector( 5, 25, 4 ), Vector( 5, 0, 1 ) )
AddVBone( "draggo_cm901", "j_gun" )

AddOffset( "draggo_l85a2", Vector( 0, 50, 8 ), Vector( 3, 0, 3 ) )
AddVOffset( "draggo_l85a2", Vector( -20, 20, 2 ), Vector( 0, 0, 2 ) )
AddVBone( "draggo_l85a2", "Weapon" )

AddOffset( "draggo_g3a3", Vector( 0, 50, 8 ), Vector( -5, 0, -3 ) )
AddVOffset( "draggo_g3a3", Vector( 14, 20, -4 ), Vector( 14, 0, -4 ) )
AddVBone( "draggo_g3a3", "G3SG1" )

AddOffset( "draggo_scarh", Vector( 0, 52, 8 ), Vector( 2, 0, 3 ) )
AddVOffset( "draggo_scarh", Vector( 7, 22, 1 ), Vector( 7, 0, 1 ) )
AddVBone( "draggo_scarh", "j_gun" )

AddOffset( "draggo_famasf1", Vector( 0, 50, 6 ), Vector( 3, 0, 1 ) )
AddVOffset( "draggo_famasf1", Vector( 0, 20, 1 ), Vector( 0, 0, 1 ) )
AddVBone( "draggo_famasf1", "FAM_MAIN" )

AddOffset( "draggo_auga1", Vector( 0, 50, 6 ), Vector( -7, 0, -4 ) )
AddVOffset( "draggo_auga1", Vector( 2, 20, 0 ), Vector( 2, 0, 0 ) )
AddVBone( "draggo_auga1", "body" )

AddOffset( "draggo_masada", Vector( 0, 50, 6 ), Vector( -4, 0, 3 ) )
AddVOffset( "draggo_masada", Vector( 1, 22, -1 ), Vector( 1, 0, -1 ) )
AddVBone( "draggo_masada", "Masada" )

AddOffset( "draggo_garand", Vector( 0, 50, 6 ), Vector( 3, -80, 1 ) )
AddVOffset( "draggo_garand", Vector( 2, 40, 0 ), Vector( 2, 0, 0 ) )
AddVBone( "draggo_garand", "j_gun" )

AddOffset( "draggo_galil", Vector( 0, 50, 5 ), Vector( -2, 0, 0 ) )
AddVOffset( "draggo_galil", Vector( 4, 30, -1 ), Vector( 4, 0, -1 ) )
AddVBone( "draggo_galil", "j_gun" )

AddOffset( "draggo_galilchan", Vector( 0, 50, 5 ), Vector( -2, 0, 0 ) )
AddVOffset( "draggo_galilchan", Vector( 4, 30, -1 ), Vector( 4, 0, -1 ) )
AddVBone( "draggo_galilchan", "j_gun" )

-- LMG Offsets
AddOffset( "draggo_mg4", Vector( 0, 50, 6 ), Vector( -1, 0, 1 ) )
AddVOffset( "draggo_mg4", Vector( 5, 30, 0 ), Vector( 5, 0, 0 ) )
AddVBone( "draggo_mg4", "j_gun" )

AddOffset( "draggo_rpk", Vector( 0, 60, 7 ), Vector( -6, 0, 2 ) )
AddVOffset( "draggo_rpk", Vector( 3, 30, -3 ), Vector( 3, 0, -3 ) )
AddVBone( "draggo_rpk", "Weapon" )

-- PDW Offsets
AddOffset( "draggo_mp5", Vector( 0, 50, 10 ), Vector( 5, 0, 5 ) )
AddVOffset( "draggo_mp5", Vector( 0, 25, -1 ), Vector( 0, 0, -1 ) )
AddVBone( "draggo_mp5", "j_gun" )

AddOffset( "draggo_cbjms", Vector( 0, 50, 8 ), Vector( 5, 0, 3 ) )
AddVOffset( "draggo_cbjms", Vector( 0, 25, -2 ), Vector( 0, 0, -2 ) )
AddVBone( "draggo_cbjms", "j_gun" )

AddOffset( "draggo_bizon", Vector( 0, 50, 9 ), Vector( 7, 0, 4 ) )
AddVOffset( "draggo_bizon", Vector( 3, 25, 0 ), Vector( 3, 0, 0 ) )
AddVBone( "draggo_bizon", "j_gun" )

AddOffset( "draggo_p90", Vector( 0, 45, 10 ), Vector( 8, 0, 5 ) )
AddVOffset( "draggo_p90", Vector( 0, 25, 0 ), Vector( 0, 0, 0 ) )
AddVBone( "draggo_p90", "j_gun" )

AddOffset( "draggo_vector", Vector( 0, 45, 10 ), Vector( 8, 0, 5 ) )
AddVOffset( "draggo_vector", Vector( 1, 30, -2 ), Vector( 1, 0, -2 ) )
AddVBone( "draggo_vector", "j_gun" )

AddOffset( "draggo_mp7", Vector( 0, 60, 10 ), Vector( 5, 20, 5 ) )
AddVOffset( "draggo_mp7", Vector( 0, 20, -2 ), Vector( 0, 0, -2 ) )
AddVBone( "draggo_mp7", "mp7_main" )

-- Sniper Offsets
AddOffset( "draggo_dragunov", Vector( 0, 70, 8 ), Vector( -1.5, 0, 3 ) )
AddVOffset( "draggo_dragunov", Vector( 10, 35, 0 ), Vector( 10, 0, 0 ) )
AddVBone( "draggo_dragunov", "j_gun" )

AddOffset( "draggo_awm", Vector( 0, 60, 10 ), Vector( 1, 0, 5 ) )
AddVOffset( "draggo_awm", Vector( 10, 40, 0 ), Vector( 10, 0, 0 ) )
AddVBone( "draggo_awm", "j_gun" )

AddOffset( "draggo_intervenion", Vector( 0, 80, 3 ), Vector( -6, 0, 2 ) )
AddVOffset( "draggo_intervenion", Vector( 15, 40, 0 ), Vector( 15, 0, 0 ) )
AddVBone( "draggo_intervenion", "j_gun" )

AddOffset( "draggo_m98b", Vector( 0, 50, 6 ), Vector( -5, 0, 1 ) )
AddVOffset( "draggo_m98b", Vector( -3, 30, -1 ), Vector( -3, 0, -1 ) )
AddVBone( "draggo_m98b", "M98_Body" )

AddOffset( "draggo_sr25", Vector( 0, 50, 6 ), Vector( -4, 0, 1 ) )
AddVOffset( "draggo_sr25", Vector( 0, 30, 0 ), Vector( 0, 0, 0 ) )
AddVBone( "draggo_sr25", "objM4_Main" )

-- Carbine Offsets
AddOffset( "draggo_m4a1", Vector( 0, 50, 6 ), Vector( 1, 0, 1 ) )
AddVOffset( "draggo_m4a1", Vector( 5, 30, 0 ), Vector( 5, 0, 0 ) )
AddVBone( "draggo_m4a1", "j_gun" )

AddOffset( "draggo_ak5c", Vector( 0, 50, 8 ), Vector( 2, 0, 3 ) )
AddVOffset( "draggo_ak5c", Vector( -1, 20, -1 ), Vector( -1, 0, -1 ) )
AddVBone( "draggo_ak5c", "M4_Body" )

AddOffset( "draggo_g36c", Vector( 0, 45, 8 ), Vector( 5, 0, 5 ) )
AddVOffset( "draggo_g36c", Vector( 2, 25, -1 ), Vector( 2, 0, -1 ) )
AddVBone( "draggo_g36c", "j_gun" )

-- Shotgun Offsets
AddOffset( "draggo_870", Vector( 0, 60, 7 ), Vector( -4, 0, 2 ) )
AddVOffset( "draggo_870", Vector( 12, 25, -3 ), Vector( 12, 0, -3 ) )
AddVBone( "draggo_870", "Spas_Body" )

AddOffset( "draggo_870mcs", Vector( 0, 60, 7 ), Vector( 4, 0, 2 ) )
AddVOffset( "draggo_870mcs", Vector( 9, 25, 0 ), Vector( 9, 0, 0 ) )
AddVBone( "draggo_870mcs", "j_gun" )

AddOffset( "draggo_ksg", Vector( 0, 60, 7 ), Vector( 2, 0, 2 ) )
AddVOffset( "draggo_ksg", Vector( 0, 25, 0 ), Vector( 0, 0, 0 ) )
AddVBone( "draggo_ksg", "j_gun" )

AddOffset( "draggo_model1887", Vector( 0, 60, 7 ), Vector( -4, 0, 2 ) )
AddVOffset( "draggo_model1887", Vector( 10, 30, 0 ), Vector( 10, 0, 0 ) )
AddVBone( "draggo_model1887", "j_gun" )

AddOffset( "draggo_spas12", Vector( 0, 50, 9 ), Vector( 3, 0, 4 ) )
AddVOffset( "draggo_spas12", Vector( 10, 25, 0 ), Vector( 10, 0, 0 ) )
AddVBone( "draggo_spas12", "j_gun" )

AddOffset( "draggo_usas12", Vector( 0, 50, 9 ), Vector( 5, -8, 4 ) )
AddVOffset( "draggo_usas12", Vector( 5, 30, 0 ), Vector( 5, 0, 0 ) )
AddVBone( "draggo_usas12", "j_gun" )

-- Pistol Offsets
AddOffset( "draggo_p226", Vector( 5, 20, 9 ), Vector( 5, 0, 4 ) )
AddVOffset( "draggo_p226", Vector( 1, 10, 0.5 ), Vector( 1, 0, 0.5 ) )
AddVBone( "draggo_p226", "weapon" )

AddOffset( "draggo_usp", Vector( 1, 15, 7 ), Vector( 1, 0, 2 ) )
AddVOffset( "draggo_usp", Vector( 0, 10, -0.25 ), Vector( 0, 0, -0.25 ) )
AddVBone( "draggo_usp", "Body" )

AddOffset( "draggo_glockp80", Vector( 0, 15, 7 ), Vector( 0, 0, 2 ) )
AddVOffset( "draggo_glockp80", Vector( 1, 10, -0.75 ), Vector( 1, 0, -0.75 ) )
AddVBone( "draggo_glockp80", "gun_main" )

AddOffset( "draggo_pl14", Vector( 1, 15, 8 ), Vector( 1, 0, 3 ) )
AddVOffset( "draggo_pl14", Vector( 9, 8, -2 ), Vector( 9, 0, -2 ) )
AddVBone( "draggo_pl14", "body" )

AddOffset( "draggo_p99", Vector( 5, 18, 9 ), Vector( 5, 0, 4 ) )
AddVOffset( "draggo_p99", Vector( 4, 10, 1 ), Vector( 4, 0, 1 ) )
AddVBone( "draggo_p99", "wpn_body" )

AddOffset( "draggo_unica6", Vector( 3, 25, 9 ), Vector( 3, 0, 4 ) )
AddVOffset( "draggo_unica6", Vector( 0, 15, 0 ), Vector( 0, 0, 0 ) )
AddVBone( "draggo_unica6", "Weapon" )

AddOffset( "draggo_python", Vector( 3, 25, 9 ), Vector( 3, 0, 4 ) )
AddVOffset( "draggo_python", Vector( 3, 15, 0 ), Vector( 3, 0, 0 ) )
AddVBone( "draggo_python", "j_gun" )

-- Make sure they have all their weapons.
net.Receive( "SyncWeapons", function()

	ownedweapons = util.JSONToTable( net.ReadString() )

end )

-- Make sure they have all their boxes.
net.Receive( "SyncBoxes", function()

	ownedboxes = util.JSONToTable( net.ReadString() )

end )

-- Setup all the fonts, if they're over 1080p make sure they get smaller text.
if ScrH( ) <= 1080 then

	surface.CreateFont( "HUD2", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 10 )
	} )

	surface.CreateFont( "HUD2Blurred", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 10 ),
		blursize = 1,
		shadow = true
	} )

	surface.CreateFont( "HUD", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 8 )
	} )

	surface.CreateFont( "HUDBlurred", {
		font = "Roboto Cn",
		weight = 520,
		size = ScreenScale( 8 ),
		blursize = 1,
		shadow = true
	} )

	surface.CreateFont( "Roboto", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 14 )
	} )

	surface.CreateFont( "RobotoBig", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 20 )
	} )

	surface.CreateFont( "WinFont", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 40 )
	} )

	surface.CreateFont( "RobotoSmall", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 9 )
	} )

	surface.CreateFont( "RobotoSmaller", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 6 )
	} )
	
else

	surface.CreateFont( "HUD2", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 7 )
	} )

	surface.CreateFont( "HUD2Blurred", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 7 ),
		blursize = 1,
		shadow = true
	} )

	surface.CreateFont( "HUD", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 5 )
	} )

	surface.CreateFont( "HUDBlurred", {
		font = "Roboto Cn",
		weight = 520,
		size = ScreenScale( 5 ),
		blursize = 1,
		shadow = true
	} )

	surface.CreateFont( "Roboto", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 11 )
	} )

	surface.CreateFont( "RobotoBig", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 17 )
	} )

	surface.CreateFont( "WinFont", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 37 )
	} )

	surface.CreateFont( "RobotoSmall", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 6 )
	} )

	surface.CreateFont( "RobotoSmaller", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 4 )
	} )
	
end

function SortPlayerFrags( )

	playerfrags = player.GetAll( )
	
	table.sort( playerfrags, function( a, b )
	
		return a:Frags( ) > b:Frags( )
		
	end )
	
end

net.Receive( "UpdateTopKills", SortPlayerFrags )
net.Receive( "UpdateArmsRace", function( )

	armsracelist = net.ReadTable( )
	
	armsracelist2 = table.SortByKey( armsracelist )
	
end )

timer.Simple( 3, function( )

	for i = 1, 5 do

		lettercolors[ i ] = Color( 255, 255, 255 )
		
	end
	
end )

net.Receive( "CoopCountdown", function( )

	if !coopcount then
	
		coopcount = true
		
	end

	coopcountnum = math.Round( net.ReadInt( 4 ) ) - 1
	
	if coopcountnum == 0 then
	
		if math.random( 1, 10 ) > 7 then
		
			surface.PlaySound( "uprising/go.wav" )
			
		else
		
			surface.PlaySound( "uprising/start.wav" )
			
		end
	
		timer.Simple( 1, function( )
		
			coopcount = false
			
		end )
		
	else
	
		surface.PlaySound( "uprising/".. coopcountnum ..".wav" )
		
	end
	
end )

net.Receive( "StatusUpdate", function( )

	timer.Simple( 1, function( )

		if !#lettercolors == CaptureNum then
		
			for i = 1, CaptureNum do
			
				lettercolors[ i ] = Color( 255, 255, 255 )
				
			end
			
		end
		
		local pointnum = GetGlobalInt( "PointNumber" )
			
		if GetGlobalInt( "PointOwner" ) == 1 and GetGlobalInt( "PointOwner" ) ~= 0 then
			
			lettercolors[ pointnum ] = team.GetColor( 1 )
			
		elseif GetGlobalInt( "PointOwner" ) == 2 then
		
			lettercolors[ pointnum ] = team.GetColor( 2 )
			
		else
			
			lettercolors[ pointnum ] = Color( 255, 255, 255 )
			
		end
		
	end )
	
end )

local function DrawBlur( panel, thickness, density )

	local x, y = panel:LocalToScreen( 0, 0 )

	surface.SetDrawColor( 255, 255, 255 )

	surface.SetMaterial( blur )

	for i = 1, density do


		blur:SetFloat( "$blur", ( i / density ) * ( thickness ) )

		blur:Recompute()

		render.UpdateScreenEffectTexture()

		surface.DrawTexturedRect( x * -1, y * -1, ScrW(), ScrH() )


	end


end

-- Populate the scoreboard whenever it changes!
-- populatelist( Parent Panel, Team, Winning Team )
local function populatelist( panel, teamnum, winner )

	local players = team.GetPlayers( teamnum )
	
	table.sort( players, function( a, b )

		return a:Frags() > b:Frags() 

	end )

	local SideInfo = panel:Add( "DPanel" )
	SideInfo:Dock( TOP )
	SideInfo:SetTall( 32 )
	SideInfo.Paint = function( panel, w, h )
		
		surface.SetDrawColor( 85, 75, 75, 115 )
		surface.SetTexture( gradient2 )
		surface.DrawTexturedRect( 0, 0, w, h )
		surface.SetDrawColor( 45, 35, 35, 155 )
		surface.DrawRect( 0, 0, w, h )

		if winner and winner == teamnum then

			draw.SimpleText( team.GetName( winner ).. " - Winner!", "HUD", 8, h * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
		else

			draw.SimpleText( team.GetName( teamnum ), "HUD", 8, h * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

		end

	end
		
	local ScorePing = SideInfo:Add( "DLabel" )
	ScorePing:Dock( RIGHT )
	ScorePing:SetWide( 64 )
	ScorePing:SetFont( "HUD" )
	ScorePing:SetText( "Ping" )
	ScorePing:SetContentAlignment( 4 )
		
	local ScoreDeath = SideInfo:Add( "DLabel" )
	ScoreDeath:Dock( RIGHT )
	ScoreDeath:SetWide( 64 )
	ScoreDeath:SetFont( "HUD" )
	ScoreDeath:SetText( "D" )
	ScoreDeath:SetContentAlignment( 4 )
		
	local ScoreKill = SideInfo:Add( "DLabel" )
	ScoreKill:Dock( RIGHT )
	ScoreKill:SetWide( 64 )
	ScoreKill:SetFont( "HUD" )
	ScoreKill:SetText( "K" )
	ScoreKill:SetContentAlignment( 4 )

	for k, v in pairs( players ) do

		local PlayerInfo = panel:Add( "DPanel" )
		PlayerInfo:Dock( TOP )
		PlayerInfo:SetTall( 64 )
		PlayerInfo.Paint = function( panel, w, h )
			
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 0, w, h )
				
		end
			
		local PlayerAvatar = PlayerInfo:Add( "AvatarImage" )
		PlayerAvatar:Dock( LEFT )
		PlayerAvatar:SetWide( PlayerInfo:GetTall( ) )
		PlayerAvatar:SetPlayer( v, 64 )
			
		local PAvatarButton = PlayerAvatar:Add( "DButton" )
		PAvatarButton:Dock( FILL )
		PAvatarButton:SetText( "" )
		PAvatarButton.Paint = function( )
			
		end
			
		PAvatarButton.DoClick = function( )

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
			
			v:ShowProfile( )
				
		end
			
		local PlayerName = PlayerInfo:Add( "DPanel" )
		PlayerName:Dock( LEFT )
		
		PlayerName:SetWide( ScrW( ) * 0.25 )
		
		local icon = v:GetRankMaterial()

		PlayerName.Paint = function( panel, w, h )

			if IsValid( v ) then

				surface.SetMaterial( icon )
				surface.DrawTexturedRect( 0, 0, 64, 64 )

				draw.SimpleText( v:Name() or "Disconnected", "RobotoSmall", 64, h * 0.5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
				draw.SimpleText( v:GetUserGroup(), "RobotoSmaller", 64, h * 0.8, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			else

				draw.SimpleText( "Disconnected", "RobotoSmall", 0, h * 0.4, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

			end

		end
			
		local PingIcon = PlayerInfo:Add( "DPanel" )
		PingIcon:Dock( RIGHT )
		PingIcon:SetWide( 64 )
		PingIcon:SetTooltip( v:Ping( ) )
		PingIcon.Paint = function( panel, w, h )
			
			local basepos = 4
			local basewidth = 4
			local baseheight = h - 24
			local ping = 0

			if IsValid( v ) and v:IsPlayer() then

				ping = v:Ping( )

			end
				
			if ping <= 50 then
				
				surface.SetDrawColor( 0, 255, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			elseif ping <= 150 and ping > 50 then
				
				surface.SetDrawColor( 105, 205, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
					
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
				
			elseif ping <= 250 and ping > 150 then
				
				surface.SetDrawColor( 155, 155, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			else
				
				surface.SetDrawColor( 255, 0, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			end
				
		end
			
		local Deaths = PlayerInfo:Add( "DLabel" )
		Deaths:Dock( RIGHT )
		Deaths:SetWide( 64 )
		Deaths:SetFont( "HUD" )
		Deaths:SetText( v:Deaths( ) )
			
		local Kills = PlayerInfo:Add( "DLabel" )
		Kills:Dock( RIGHT )
		Kills:SetWide( 64 )
		Kills:SetFont( "HUD" )
		Kills:SetText( v:Frags( ) )

	end

end

-- Receive this when the round ends!
net.Receive( "RoundEnded", function( )

	hook.Remove( "HUDPaint", "ShowEndRoundScreen" )

	local winner

	local frags = {}
	local team1players = team.GetPlayers( 1 )
	local team2players = team.GetPlayers( 2 )

	frags[ 1 ] = team.GetScore( 1 )
	frags[ 2 ] = team.GetScore( 2 )

	-- Figure out the winner.
	if frags[ 1 ] > frags[ 2 ] then

		winner = 1

	elseif frags[ 2 ] > frags[ 1 ] then

		winner = 2

	else

		winner = 0

	end

	local players = player.GetAll( )
	
	-- Sort the players by their kills.
	table.sort( players, function( a, b ) return a:Frags( ) > b:Frags( ) end )

	local margin = ScrW() * 0.05

	winpanel = vgui.Create( "DFrame" )
	winpanel:SetSize( ScrW( ), ScrH( ) )
	winpanel:Center( )
	winpanel:MakePopup( )
	winpanel:ShowCloseButton( false )
	winpanel.lastupdate = 0
	winpanel.lastcount = #player.GetAll()
	winpanel.Paint = function( panel, w, h )
	
		Derma_DrawBackgroundBlur( panel, time )
	
	end

	local wininfo = winpanel:Add( "DPanel" )
	wininfo:Dock( TOP )
	wininfo:SetTall( ScrH() * 0.1 )
	wininfo:DockMargin( margin, 8, margin, 8 )
	wininfo.Paint = function( panel, w, h )

		surface.SetDrawColor( 0, 0, 0, 205 )
		surface.DrawRect( w / 2 - 128, h / 2, 256, h )

		draw.SimpleText( frags[ 1 ], "Roboto", w / 2 - 32, h * 0.75, Color( 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
		draw.SimpleText( " - ", "Roboto", w / 2, h * 0.75, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		draw.SimpleText( frags[ 2 ], "Roboto", w / 2 + 32, h * 0.75, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
	
	end

	local scoreboardbg = winpanel:Add( "DPanel" )
	scoreboardbg:Dock( TOP )
	scoreboardbg:SetTall( ScrH() * 0.675 )
	scoreboardbg:DockMargin( margin, 8, margin, 8 )
	scoreboardbg.Paint = function( panel, w, h ) end

	local scoreinfo1 = scoreboardbg:Add( "DPanel" )
	scoreinfo1:Dock( TOP )
	scoreinfo1:SetTall( scoreboardbg:GetTall() / 2 )
	scoreinfo1.Paint = function( panel, w, h ) end

	local team1 = scoreinfo1:Add( "DScrollPanel" )
	team1:Dock( FILL )
	team1.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 0, w, h )

	end

	local scoreinfo2 = scoreboardbg:Add( "DPanel" )
	scoreinfo2:Dock( TOP )
	scoreinfo2:SetTall( scoreboardbg:GetTall() / 2 )
	scoreinfo2.Paint = function( panel, w, h ) end

	local team2 = scoreinfo2:Add( "DScrollPanel" )
	team2:Dock( FILL )
	team2.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 0, w, h )

	end

	populatelist( team1, 1, winner )
	populatelist( team2, 2, winner )

	winpanel.lastupdate = 0
	winpanel.lastcount = #player.GetAll()

	winpanel.Think = function( self )

		-- Update the scoreboard whenever the list changes.
		if( self.lastupdate < CurTime() ) then

			self.lastupdate = CurTime() + 1

			if( self.lastcount != #player.GetAll() ) then

				team1:Clear( true )
				team2:Clear( true )

				populatelist( team1, 1, winner )
				populatelist( team2, 2, winner )

			end

			self.lastcount = #player.GetAll()

		end

	end
	
end )

-- Receive this when a vote starts.
net.Receive( "MapVotes_Start", function()

	MapVotes.CurrentMaps = {}
	MapVotes.Allow = true
	MapVotes.Votes = {}

	local maps = net.ReadTable()
	local selected
	local votetimer = 20

	-- Count down at the top.
	timer.Create( "MapVotes_CountDown", 1, votetimer, function()

		votetimer = votetimer - 1

	end )

	if winpanel then

		-- Add the parent panels and the buttons.
		winpanel.PaintOver = function( panel, w, h )

			if votetimer then

				surface.SetDrawColor( 0, 0, 0, 205 )
				surface.DrawRect( w / 2 - 64, 16, 128, 32 )
				draw.SimpleText( votetimer, "RobotoSmall", w / 2, 32, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

			end

		end

		mappanel = winpanel:Add( "DPanel" )
		mappanel:Dock( BOTTOM )
		mappanel:SetTall( ScrH() * 0.15 )
		mappanel:SetDrawBackground( false )
		mappanel.buttons = {}

		mappanel1 = mappanel:Add( "DPanel" )
		mappanel1:Dock( BOTTOM )
		mappanel1:SetTall( ScrH() * 0.075 )
		mappanel1:SetDrawBackground( false )

		mappanel2 = mappanel:Add( "DPanel" )
		mappanel2:Dock( BOTTOM )
		mappanel2:SetTall( ScrH() * 0.075 )
		mappanel2:SetDrawBackground( false )

		for k, v in pairs( maps ) do

			local mapbutton

			if k <= 4 then

				-- Add the first half to mappanel1
				mapbutton = mappanel1:Add( "DButton" )

			else

				-- Add the second half to mappanel2
				mapbutton = mappanel2:Add( "DButton" )

			end

			mapbutton:Dock( LEFT )
			mapbutton:SetWide( winpanel:GetWide() * 0.25 )
			mapbutton:SetText( "" )
			mapbutton.Voters = {}
			mapbutton.Winner = false
			mapbutton.Paint = function( panel, w, h )

				if selected == panel then

					surface.SetDrawColor( 40, 40, 40, 155 )

				else

					surface.SetDrawColor( 35, 35, 35, 155 )

				end

				if #panel.Voters > 0 then

					draw.SimpleText( #panel.Voters.. " votes", "RobotoSmall", w / 2, h / 2 - 16, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

				end

				if panel.Winner then

					surface.SetDrawColor( 45, 45, 45, 155 )

				end

				surface.DrawRect( 0, 0, w, h )
				surface.DrawRect( 0, h - 32, w, 32 )

			end
			mapbutton.DoClick = function( panel )

				surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

				selected = panel

				net.Start( "MapVotes_Update" )

					net.WriteUInt( 1, 3 )
					net.WriteString( v )

				net.SendToServer( )

				for k, v in pairs( mappanel.buttons ) do

					v:SetEnabled( false )

				end

			end

			mappanel.buttons[ v ] = mapbutton

			local maplabel = mapbutton:Add( "DLabel" )
			maplabel:Dock( BOTTOM )
			maplabel:SetTall( 32 )
			maplabel:SetFont( "HUD" )

			if gamemaps[ v ] then

				maplabel:SetText( gamemaps[ v ] )

			else

				maplabel:SetText( v )

			end
			
			maplabel:SetContentAlignment( 5 )

		end

		PrintTable( mappanel.buttons )

	end

end )

-- Update the vote amount!
net.Receive( "MapVotes_Update", function()

	local update_type = net.ReadUInt( 3 )

	if update_type == 1 then

		local ply = net.ReadEntity()

		if IsValid( ply ) then

			local map = net.ReadString()
			local button = mappanel.buttons[ map ]

			MapVotes.Votes[ ply:SteamID() ] = map

			PrintTable( MapVotes.Votes )

			for k, v in pairs( mappanel.buttons ) do

				if v != button and table.HasValue( v.Voters, ply ) then

					table.RemoveByValue( v.Voters, ply )

				end

			end

			if IsValid( mappanel ) then

				table.insert( button.Voters, ply )

			end

		end

	elseif update_type == 3 then

		local map = net.ReadString()

		for k, v in pairs( mappanel.buttons ) do

			v:SetDisabled( true )

		end

		mappanel.buttons[ map ].Winner = true

	end

end )

local shopback
local shopinfo
local finbutton
local openbutton
local wepslist

local dmgbars = 0
local rpmbars = 0
local recbars = 0
local hipbars = 0
local adsbars = 0
local clipbars = 0

-- Populate the loadout screen
local function BuildInfo( panel )

	local weaponpreviewmodel = nil
	local pweapon = nil
	local sweapon = nil
	local gweapon = nil
	local attachmenticon = Material( "uprising/settings-work-tool.png", "smooth" )
	
	for k, v in pairs( panel:GetChildren( ) ) do
	
		v:Remove( )
		
	end
	
	-- Add the player model
	local pmodel = panel:Add( "DModelPanel" )
	pmodel:Dock( LEFT )
	pmodel:SetWide( ScrW() * 0.25 )
	pmodel:SetFOV( 45 )
	pmodel:SetModel( LocalPlayer( ).model )
	
	-- Make the two left columns that hold the weapon lists
	local weaponlistbg = panel:Add( "DPanel" )
	weaponlistbg:Dock( LEFT )
	weaponlistbg:SetWide( ScrW( ) * 0.25 )
	weaponlistbg:DockMargin( ScrW( ) * 0.008, ScrW( ) * 0.008, 0, 0 )
	weaponlistbg:SetDrawBackground( false )
	
	weaponlistbg2 = panel:Add( "DPanel" )
	weaponlistbg2:Dock( LEFT )
	weaponlistbg2:SetWide( ScrW( ) * 0.25 )
	weaponlistbg2:DockMargin( ScrW( ) * 0.008, ScrW( ) * 0.008, 0, 0 )
	weaponlistbg2:SetDrawBackground( false )

	surface.SetFont( "Roboto" )
	local th = select( 2, surface.GetTextSize( "$" ) )

	surface.SetFont( "HUD" )
	local th2 = select( 2, surface.GetTextSize( "A" ) )

	-- Add the weapon stats bar at the right
	weaponinfo = panel:Add( "DPanel" )
	weaponinfo:Dock( RIGHT )
	weaponinfo:SetWide( ScrW( ) * 0.2 )
	weaponinfo:DockMargin( ScrW( ) * 0.008, ScrW( ) * 0.008, 8, 0 )
	weaponinfo.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	-- Populate the weapon list with various stats.
	-- It goes as follows:
	-- 		    NAME
	--		   DAMAGE
	-- 		   RECOIL
	-- 		    RPM
	-- 		 HIP ACCURACY
	--		 ADS ACCURACY
	-- 		AMMO CAPACITY
	local wepname = weaponinfo:Add( "DLabel" )
	wepname:Dock( TOP )
	wepname:SetTall( th2 + 4 )
	wepname:SetFont( "HUD" )

	if LocalPlayer().primary then

		wepname:SetText( weapons.Get( LocalPlayer().primary ).PrintName )

	else

		wepname:SetText( "" )

	end

	wepname:SetContentAlignment( 5 )

	local dmgbox = weaponinfo:Add( "DPanel" )
	dmgbox:Dock( TOP )
	dmgbox:DockMargin( 8, 8, 8, 8 )
	dmgbox:SetTall( th2 * 2 )
	dmgbox.Paint = function() end

	local dmg = dmgbox:Add( "DLabel" )
	dmg:Dock( TOP )
	dmg:SetTall( th2 )
	dmg:SetText( "Damage: 0" )
	dmg:SetFont( "HUD" )
	dmg:SetContentAlignment( 5 )
	dmg.int = 0

	local dmgbar = dmgbox:Add( "DPanel" )
	dmgbar:Dock( TOP )
	dmgbar:SetTall( th2 )
	dmgbar.Paint = function( panel, w, h )

		local dmgperc = dmg.int / 100
		dmgbars = Lerp( 2.5 * FrameTime(), dmgbars, dmgperc )
		surface.SetDrawColor( 25, 25, 35, 205 )
		surface.DrawRect( 0, h / 2 - 2, w, 4 )

		surface.SetDrawColor( 225, 225, 235, 105 )
		surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( dmgbars, 0, 1 ), 4 )

		if dmgperc > 1 then

			surface.SetDrawColor( 225, 225, 235, 155 )
			surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( dmgbars - 1, 0, 1 ), 4 )

		end

		dmg:SetText( "Damage: ".. dmg.int )

	end

	local recbox = weaponinfo:Add( "DPanel" )
	recbox:Dock( TOP )
	recbox:DockMargin( 8, 8, 8, 8 )
	recbox:SetTall( th2 * 2 )
	recbox.Paint = function() end

	local rec = recbox:Add( "DLabel" )
	rec:Dock( TOP )
	rec:SetTall( th2 )
	rec:SetText( "Recoil: 0" )
	rec:SetFont( "HUD" )
	rec:SetContentAlignment( 5 )
	rec.int = 0

	local recbar = recbox:Add( "DPanel" )
	recbar:Dock( TOP )
	recbar:SetTall( th2 )
	recbar.Paint = function( panel, w, h )

		local recperc = rec.int
		recbars = Lerp( 2.5 * FrameTime(), recbars, recperc )
		surface.SetDrawColor( 25, 25, 35, 205 )
		surface.DrawRect( 0, h / 2 - 2, w, 4 )

		surface.SetDrawColor( 225, 225, 235, 105 )
		surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( recbars, 0, 1 ), 4 )

		if recperc > 1 then

			surface.SetDrawColor( 225, 225, 235, 155 )
			surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( recbars - 1, 0, 1 ), 4 )

		end

		rec:SetText( "Recoil: ".. recperc )

	end

	local rpmbox = weaponinfo:Add( "DPanel" )
	rpmbox:Dock( TOP )
	rpmbox:DockMargin( 8, 8, 8, 8 )
	rpmbox:SetTall( th2 * 2 )
	rpmbox.Paint = function() end

	local rpm = rpmbox:Add( "DLabel" )
	rpm:Dock( TOP )
	rpm:SetTall( th2 )
	rpm:SetText( "Fire Delay: 0 Seconds" )
	rpm:SetFont( "HUD" )
	rpm:SetContentAlignment( 5 )
	rpm.int = 0

	local rpmbar = rpmbox:Add( "DPanel" )
	rpmbar:Dock( TOP )
	rpmbar:SetTall( th2 )
	rpmbar.Paint = function( panel, w, h )

		local rpmperc = rpm.int / 500
		rpmbars = Lerp( 2.5 * FrameTime(), rpmbars, rpmperc )
		surface.SetDrawColor( 25, 25, 35, 205 )
		surface.DrawRect( 0, h / 2 - 2, w, 4 )

		surface.SetDrawColor( 225, 225, 235, 105 )
		surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( rpmbars, 0, 1 ), 4 )

		if rpmperc > 1 then

			surface.SetDrawColor( 225, 225, 235, 155 )
			surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( rpmbars - 1, 0, 1 ), 4 )

		end

		rpm:SetText( "Rounds Per Minute: ".. rpm.int )

	end

	local hipbox = weaponinfo:Add( "DPanel" )
	hipbox:Dock( TOP )
	hipbox:DockMargin( 8, 8, 8, 8 )
	hipbox:SetTall( th2 * 2 )
	hipbox.Paint = function() end

	local hip = hipbox:Add( "DLabel" )
	hip:Dock( TOP )
	hip:SetTall( th2 )
	hip:SetText( "Hip Accuracy: 0" )
	hip:SetFont( "HUD" )
	hip:SetContentAlignment( 5 )
	hip.int = 0

	local hipbar = hipbox:Add( "DPanel" )
	hipbar:Dock( TOP )
	hipbar:SetTall( th2 )
	hipbar.Paint = function( panel, w, h )

		local hipperc = hip.int / 1
		hipbars = Lerp( 2.5 * FrameTime(), hipbars, hipperc )
		surface.SetDrawColor( 25, 25, 35, 205 )
		surface.DrawRect( 0, h / 2 - 2, w, 4 )

		surface.SetDrawColor( 225, 225, 235, 105 )
		surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( hipbars, 0, 1 ), 4 )

		if hipperc > 1 then

			surface.SetDrawColor( 225, 225, 235, 155 )
			surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( hipbars - 1, 0, 1 ), 4 )

		end

		hip:SetText( "Hip Accuracy: ".. hip.int )

	end

	local adsbox = weaponinfo:Add( "DPanel" )
	adsbox:Dock( TOP )
	adsbox:DockMargin( 8, 8, 8, 8 )
	adsbox:SetTall( th2 * 2 )
	adsbox.Paint = function() end

	local ads = adsbox:Add( "DLabel" )
	ads:Dock( TOP )
	ads:SetTall( th2 )
	ads:SetText( "Aim Accuracy: 0" )
	ads:SetFont( "HUD" )
	ads:SetContentAlignment( 5 )
	ads.int = 0

	local adsbar = adsbox:Add( "DPanel" )
	adsbar:Dock( TOP )
	adsbar:SetTall( th2 )
	adsbar.Paint = function( panel, w, h )

		local adsperc = ads.int / 1
		adsbars = Lerp( 2.5 * FrameTime(), adsbars, adsperc )
		surface.SetDrawColor( 25, 25, 35, 205 )
		surface.DrawRect( 0, h / 2 - 2, w, 4 )

		surface.SetDrawColor( 225, 225, 235, 105 )
		surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( adsbars, 0, 1 ), 4 )

		if adsperc > 1 then

			surface.SetDrawColor( 225, 225, 235, 155 )
			surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( adsbars - 1, 0, 1 ), 4 )

		end

		ads:SetText( "Aim Accuracy: ".. ads.int )

	end

	local clipbox = weaponinfo:Add( "DPanel" )
	clipbox:Dock( TOP )
	clipbox:DockMargin( 8, 8, 8, 8 )
	clipbox:SetTall( th2 * 2 )
	clipbox.Paint = function() end

	local clip = clipbox:Add( "DLabel" )
	clip:Dock( TOP )
	clip:SetTall( th2 )
	clip:SetText( "Clip Size: 0" )
	clip:SetFont( "HUD" )
	clip:SetContentAlignment( 5 )
	clip.int = 0

	local clipbar = clipbox:Add( "DPanel" )
	clipbar:Dock( TOP )
	clipbar:SetTall( th2 )
	clipbar.Paint = function( panel, w, h )

		local clipperc = clip.int / 100
		clipbars = Lerp( 2.5 * FrameTime(), clipbars, clipperc )
		surface.SetDrawColor( 25, 25, 35, 205 )
		surface.DrawRect( 0, h / 2 - 2, w, 4 )

		surface.SetDrawColor( 225, 225, 235, 105 )
		surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( clipbars, 0, 1 ), 4 )

		if clipperc > 1 then

			surface.SetDrawColor( 225, 225, 235, 155 )
			surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( clipbars - 1, 0, 1 ), 4 )

		end

		clip:SetText( "Clip Size: ".. clip.int )

	end

	local weapontbl

	-- Let's make sure those stats STAY filled...

	if ( pickingtype == "primary" or pickingtype == "pattachments" or pickingtype == "none" ) and LocalPlayer().primary then

		weapontbl = weapons.Get( LocalPlayer().primary )

	elseif pickingtype == "secondary" or pickingtype == "sattachments" and LocalPlayer().secondary then

		weapontbl = weapons.Get( LocalPlayer().secondary )

	elseif pickingtype == "grenade" and LocalPlayer().grenade then

		weapontbl = weapons.Get( LocalPlayer().grenade )

	elseif pickingtype == "melee" and LocalPlayer().melee then

		weapontbl = weapons.Get( LocalPlayer().melee )

	end

	if weapontbl then

		wepname:SetText( weapontbl.PrintName )

		if dmg and weapontbl.Damage then

			dmg.int = weapontbl.Damage

		end

		if rec and weapontbl.Recoil then

			rec.int = weapontbl.Recoil

		end

		if rpm and weapontbl.RPM then

			local num = math.Round( weapontbl.RPM, 2 )

			rpm.int = num

		end

		if hip and weapontbl.HipSpread then

			local num = weapontbl.HipSpread

			hip.int = num

		end

		if ads and weapontbl.AimSpread then

			local num = 1 - weapontbl.AimSpread

			ads.int = num

		end

		if clip and weapontbl.Primary.ClipSize then

			clip.int = weapontbl.Primary.ClipSize

		end

	end

	-- Put the weapon on the playermodel.
	local weapon = ents.CreateClientProp( Model( "weapons/w_rif_m4a1.mdl" ) )
	weapon:Spawn( )
	weapon:SetParent( pmodel.Entity, pmodel.Entity:LookupAttachment( "anim_attachment_RH" ) )
	weapon:AddEffects( EF_BONEMERGE )
	weapon:SetNoDraw( true )
	
	pmodel.WeaponModel = weapon
	pmodel.WepEnt = "weapon_smg2"
	pmodel.WepAng = Angle( 0, 0, 0 )
	pmodel.WepPos = Vector( 0, 0, 0 )
	
	-- Updates the weapon by grabbing the world model and reattaching.
	function pmodel:UpdateWeapon( class )
	
		local wep = weapons.Get( class )
		if !wep then return end
		
		pmodel.WepEnt = wep
		
		if wep.WMAng then
		
			pmodel.WepAng = Angle( wep.WMAng.x, wep.WMAng.y, wep.WMAng.z )
			
		end
		
		if wep.WMPos then
		
			pmodel.WepPos = wep.WMPos
			
		end
		
		weapon:SetModel( Model( wep.WorldModel ) or Model( wep.WM ) )
		
		local pos, ang = pmodel.Entity:GetBonePosition( pmodel.Entity:LookupBone("ValveBiped.Bip01_R_Hand"))
				
		if pos and ang then
			
			local wep = pmodel.WepEnt
				
			if pmodel.WepAng then
				
				ang:RotateAroundAxis( ang:Right(), pmodel.WepAng.p )
				ang:RotateAroundAxis( ang:Up(), pmodel.WepAng.y)
				ang:RotateAroundAxis( ang:Forward(), pmodel.WepAng.r)
					
			end
				
			if pmodel.WepPos then

				pos = pos + pmodel.WepPos.x * ang:Right() 
				pos = pos + pmodel.WepPos.y * ang:Forward()
				pos = pos + pmodel.WepPos.z * ang:Up()
					
			end
				
			weapon:SetRenderOrigin(pos)
			weapon:SetRenderAngles(ang)
			weapon:DrawModel()
				
		end		
		
		pmodel.OldDraw()

	end
	
	pmodel.OldDraw = function( panel )

		local curparent = pmodel
		local rightx = pmodel:GetWide()
		local leftx = 0
		local topy = 0
		local bottomy = pmodel:GetTall()
		local previous = curparent
		while( curparent:GetParent() != nil ) do
			curparent = curparent:GetParent()
			local x, y = previous:GetPos()
			topy = math.Max( y, topy + y )
			leftx = math.Max( x, leftx + x )
			bottomy = math.Min( y + previous:GetTall(), bottomy + y )
			rightx = math.Min( x + previous:GetWide(), rightx + x )
			previous = curparent
		end
		render.SetScissorRect( leftx, topy, rightx, bottomy, true )

		local ret = pmodel:PreDrawModel( pmodel.Entity )
		if ( ret != false ) then
			pmodel.Entity:DrawModel()
			
			local pos, ang = pmodel.Entity:GetBonePosition( pmodel.Entity:LookupBone("ValveBiped.Bip01_R_Hand"))
				
			if pos and ang then
			
				local wep = pmodel.WepEnt
				
				if pmodel.WepAng then
				
					ang:RotateAroundAxis( ang:Right(), pmodel.WepAng.p )
					ang:RotateAroundAxis( ang:Up(), pmodel.WepAng.y)
					ang:RotateAroundAxis( ang:Forward(), pmodel.WepAng.r)
					
				end
				
				if pmodel.WepPos then

					pos = pos + pmodel.WepPos.x * ang:Right() 
					pos = pos + pmodel.WepPos.y * ang:Forward()
					pos = pos + pmodel.WepPos.z * ang:Up()
					
				end
				
				weapon:SetRenderOrigin(pos)
				weapon:SetRenderAngles(ang)
				weapon:DrawModel()
				
			end
			
			pmodel:PostDrawModel( pmodel.Entity )
		end

		render.SetScissorRect( 0, 0, 0, 0, false )

	end
	
	pmodel.DrawModel = function( )
	
		pmodel.OldDraw( )
		
	end
	
	if LocalPlayer( ).primary then
	
		pmodel:UpdateWeapon( LocalPlayer( ).primary )
		
	end

	local selected
	
	-- Primary weapon box
	local primary = weaponlistbg:Add( "DButton" )
	primary:Dock( TOP )
	primary:DockMargin( 0, 38, 0, 8 )
	primary:SetTall( panel:GetTall( ) * 0.2 )
	primary:SetText( "" )
	primary:SetFont( "HUD" )
	primary:SetTextColor( Color( 205, 205, 205 ) )
	primary.alpha = 155
	primary.weptext = ""
	primary.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
	
		primary.alpha = 255
		
	end
	primary.OnCursorExited = function( )
	
		primary.alpha = 155
		
	end
	primary.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "primary" then
		
			pickingtype = "primary"
			
			pattachments = {}
			
			BuildInfo( panel )

		end
		
	end
	
	local smooth = 155
	surface.SetFont( "HUD" )
	local tw, th = surface.GetTextSize( "ARY" )
	
	-- Let's decorate the primary box.
	primary.Paint = function( panel, w, h )
	
		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )
					
		if pickingtype == "primary" then

			surface.SetDrawColor( 175, 175, 255, 255 )

		else

			surface.SetDrawColor( 125, 125, 125, 255 )

		end

		surface.DrawOutlinedRect( 0, 0, w, h )

		draw.SimpleText( "PRIMARY", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
		
		if pickingtype == "primary" then

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

		else

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
		
		end
		
	end
	
	if LocalPlayer( ).primary then
	
		pweapon = LocalPlayer( ).primary
		primary.weptext = weapons.Get( pweapon ).PrintName
	
	end
	
	-- Add the preview model to the primary box
	primarymodel = primary:Add( "VModelPanel" )
	primarymodel.OnCursorEntered = function()

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

	end

	primarymodel:Dock( FILL )
	primarymodel:SetWeapon( pweapon )
	
	local modelpos

	if primarymodel.Entity then

		if vmodeloffsets[ pweapon ] then

			modelpos = primarymodel.Entity:GetBonePosition( primarymodel.Entity:LookupBone( vmodeloffsets[ pweapon ].bone ) )

		else

			modelpos = primarymodel.Entity:GetBonePosition( 0 )

		end

		local lookat = Vector( -1.5, 0, 3 )
		local campos = Vector( 0, 70, 8 )

		if modeloffsets[ pweapon ] then
			
			primarymodel:SetLookAt( modelpos + vmodeloffsets[ pweapon ].lookat )
			primarymodel:SetCamPos( modelpos + vmodeloffsets[ pweapon ].campos )

		else

			primarymodel:SetLookAt( modelpos - lookat )
			primarymodel:SetCamPos( modelpos - campos )

		end

	end

	primarymodel.LayoutEntity = function() end
	
	-- Make the picking type a primary weapon.
	primarymodel.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "primary" then
		
			pickingtype = "primary"
			
			BuildInfo( panel )
			
		end
		
	end
		
	-- Add the attachment button to the corner.
	local attachmentbutton = primary:Add( "DButton" )
	attachmentbutton:SetSize( 32, 32 )
	attachmentbutton:SetPos( weaponlistbg:GetWide( ) - 38, primary:GetTall() - 38 )
	attachmentbutton:SetText( "" )
	attachmentbutton.Paint = function( panel, w, h )
	
		if LocalPlayer().primary then
	
			surface.SetMaterial( attachmenticon )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( 0, 0, 32, 32 )
			
		end
		
	end
		
	attachmentbutton.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		-- Only work if there's a weapon to customise
		if LocalPlayer().primary then
		
			if pickingtype ~= "pattachments" then
					
				pickingtype = "pattachments"
						
				BuildInfo( panel )
						
			end
			
		else
		
			surface.PlaySound( "buttons/button10.wav" )

		end
		
	end

	-- Once more, with feeling.
	local secondary = weaponlistbg:Add( "DButton" )
	secondary:Dock( TOP )
	secondary:DockMargin( 0, 0, 0, 8 )
	secondary:SetTall( panel:GetTall( ) * 0.2 )
	secondary:SetText( "" )
	secondary:SetFont( "HUD" )
	secondary:SetTextColor( Color( 205, 205, 205 ) )
	secondary.alpha = 155
	secondary.weptext = ""
	secondary.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
	
		secondary.alpha = 255
		
	end
	secondary.OnCursorExited = function( )
	
		secondary.alpha = 155
		
	end
	secondary.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "secondary" then
		
			pickingtype = "secondary"
			
			sattachments = {}
			
			BuildInfo( panel )
			
		end
		
	end
	
	local smooth = 155
	
	secondary.Paint = function( panel, w, h )
	
		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )
					
		if pickingtype == "secondary" then

			surface.SetDrawColor( 175, 175, 255, 255 )

		else

			surface.SetDrawColor( 125, 125, 125, 255 )

		end

		surface.DrawOutlinedRect( 0, 0, w, h )

		draw.SimpleText( "SECONDARY", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
		
		if pickingtype == "secondary" then

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

		else

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
		
		end
		
	end
		
	if LocalPlayer( ).secondary then
	
		sweapon = LocalPlayer( ).secondary

		secondary.weptext = weapons.Get( sweapon ).PrintName
		
	end
	
	secondarymodel = secondary:Add( "VModelPanel" )
	secondarymodel.OnCursorEntered = function()

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

	end
	secondarymodel:Dock( FILL )
	secondarymodel:SetWeapon( sweapon )
	
	local modelpos

	if secondarymodel.Entity then

		if vmodeloffsets[ pweapon ] then

			modelpos = secondarymodel.Entity:GetBonePosition( secondarymodel.Entity:LookupBone( vmodeloffsets[ sweapon ].bone ) )

		else

			modelpos = secondarymodel.Entity:GetBonePosition( 0 )

		end

		local lookat = Vector( -1.5, 0, 3 )
		local campos = Vector( 0, 70, 8 )

		if modeloffsets[ sweapon ] then
			
			secondarymodel:SetLookAt( modelpos + vmodeloffsets[ sweapon ].lookat )
			secondarymodel:SetCamPos( modelpos + vmodeloffsets[ sweapon ].campos )

		else

			secondarymodel:SetLookAt( modelpos - lookat )
			secondarymodel:SetCamPos( modelpos - campos )

		end

	end

	secondarymodel.LayoutEntity = function() end
		
	secondarymodel.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "secondary" then
		
			pickingtype = "secondary"
			
			BuildInfo( panel )
			
		end
		
	end

	local sattachmentbutton = secondary:Add( "DButton" )
	sattachmentbutton:SetSize( 32, 32 )
	sattachmentbutton:SetPos( weaponlistbg:GetWide( ) - 38, secondary:GetTall() - 38 )
	sattachmentbutton:SetText( "" )
	sattachmentbutton.Paint = function( panel, w, h )
	
		if LocalPlayer().secondary then
	
			surface.SetMaterial( attachmenticon )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( 0, 0, 32, 32 )
			
		end
		
	end
		
	sattachmentbutton.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
		
		if LocalPlayer().secondary then
		
			if pickingtype ~= "sattachments" then
					
				pickingtype = "sattachments"
						
				BuildInfo( panel )
						
			end
			
		else
		
			surface.PlaySound( "buttons/button10.wav" )
			
		end
		
	end
	
	local grenade = weaponlistbg:Add( "DButton" )
	grenade:Dock( TOP )
	grenade:DockMargin( 0, 0, 0, 8 )
	grenade:SetTall( panel:GetTall( ) * 0.2 )
	grenade:SetText( "" )
	grenade:SetFont( "HUD" )
	grenade:SetTextColor( Color( 205, 205, 205 ) )
	grenade.alpha = 155
	grenade.weptext = ""
	grenade.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
	
		grenade.alpha = 255
		
	end
	grenade.OnCursorExited = function( )
	
		grenade.alpha = 155
		
	end
	grenade.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "grenade" then
		
			pickingtype = "grenade"
			
			BuildInfo( panel )
			
		end
		
	end
	
	local smooth = 155
	
	grenade.Paint = function( panel, w, h )
	
		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )
					
		if pickingtype == "grenade" then

			surface.SetDrawColor( 175, 175, 255, 255 )

		else

			surface.SetDrawColor( 125, 125, 125, 255 )

		end
			
		surface.DrawOutlinedRect( 0, 0, w, h )

		draw.SimpleText( "GRENADE", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
		
		if pickingtype == "grenade" then

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

		else

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
		
		end
		
	end
	
	if LocalPlayer( ).grenade then
	
		gweapon = LocalPlayer( ).grenade
		grenade.weptext = weapons.Get( gweapon ).PrintName
		
	end
	
	grenademodel = grenade:Add( "DModelPanel" )
	grenademodel.OnCursorEntered = function()

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

	end
	grenademodel:Dock( FILL )
	grenademodel:DockMargin( 8, 8, 8, 8 )
					
	if gweapon then
					
		if weapons.Get( gweapon ).WM then
					
			grenademodel:SetModel( weapons.Get( gweapon ).WM )
						
		elseif weapons.Get( gweapon ).WorldModel then
					
			grenademodel:SetModel( weapons.Get( gweapon ).WorldModel )
						
		else
					
			grenademodel:SetModel( Model( "models/weapons/w_pistol.mdl" ) )
		
		end
					
		local viewmins, viewmaxs = grenademodel.Entity:GetRenderBounds( )
					
		grenademodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 2.25, 0 ) )
		grenademodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
		grenademodel.LayoutEntity = function( )
					
		end
		
	end
		
	grenademodel.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "grenade" then
		
			pickingtype = "grenade"
			
			BuildInfo( panel )
				
		end
			
	end
	
	local melee = weaponlistbg:Add( "DButton" )
	melee:Dock( TOP )
	melee:DockMargin( 0, 0, 0, 8 )
	melee:SetTall( panel:GetTall( ) * 0.2 )
	melee:SetText( "" )
	melee:SetFont( "HUD" )
	melee:SetTextColor( Color( 205, 205, 205 ) )
	melee.alpha = 155
	melee.weptext = ""
	melee.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
	
		melee.alpha = 255
		
	end
	melee.OnCursorExited = function( )
	
		melee.alpha = 155
		
	end
	melee.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		if pickingtype ~= "melee" then
		
			pickingtype = "melee"
			
			BuildInfo( panel )
			
		end
		
	end
	
	local smooth = 155
	
	melee.Paint = function( panel, w, h )
	
		surface.SetDrawColor( 15, 15, 15, 105 )
		surface.DrawRect( 0, 0, w, h )
					
		if pickingtype == "melee" then

			surface.SetDrawColor( 175, 175, 255, 255 )

		else

			surface.SetDrawColor( 125, 125, 125, 255 )

		end
		
		surface.DrawOutlinedRect( 0, 0, w, h )
		draw.SimpleText( "MELEE", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
		
		if pickingtype == "melee" then

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

		else

			draw.SimpleText( panel.weptext, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
		
		end
		
	end
	
	if LocalPlayer( ).melee then
	
		mweapon = LocalPlayer( ).melee
		melee.weptext = weapons.Get( mweapon ).PrintName
		
	end
	
		meleemodel = melee:Add( "DModelPanel" )
		meleemodel.OnCursorEntered = function()

			surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

		end
		meleemodel:Dock( FILL )
		meleemodel:DockMargin( 8, 8, 8, 8 )
					
	if mweapon then
					
		if weapons.Get( mweapon ).WM then
					
			meleemodel:SetModel( weapons.Get( mweapon ).WM )
						
		elseif weapons.Get( mweapon ).WorldModel then
					
			meleemodel:SetModel( weapons.Get( mweapon ).WorldModel )
						
		else
					
			meleemodel:SetModel( Model( "models/weapons/w_pistol.mdl" ) )
		
		end
					
		local viewmins, viewmaxs = meleemodel.Entity:GetRenderBounds( )
					
		meleemodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 2.25, 0 ) )
		meleemodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
		meleemodel.LayoutEntity = function( )
					
		end
		
	end
		
		meleemodel.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
		
			if pickingtype ~= "melee" then
			
				pickingtype = "melee"
				
				BuildInfo( panel )
				
			end
			
		end
	
	local weaponlist = weaponlistbg2:Add( "DScrollPanel" )
	weaponlist:Dock( FILL )
	weaponlist:SetDrawBackground( false )
	weaponlist:DockMargin( 0, 0, 0, 30 )
	weaponlist:InvalidateLayout( true )
	
	local scrollbar = weaponlist:GetVBar( )
	
	function scrollbar:Paint( w, h )
	
		surface.SetDrawColor( 35, 35, 35, 55 )
		surface.DrawRect( 8, 0, 4, h )
		
	end
	
	function scrollbar.btnGrip:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 0, 10, h )
	
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 6, 1, 8, h - 2 )
		
	end
	
	function scrollbar.btnUp:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end
	
	function scrollbar.btnDown:Paint( w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 5, 3, 10, 10 )
	
		surface.SetDrawColor( 35, 35, 35, 255 )
		surface.DrawRect( 6, 4, 8, 8 )
		
	end
	
	local class = ClassTable[ LocalPlayer( ).role ]
	
	if pickingtype == "primary" then
	
		for i = 1, #class.primary do
		
			local catent = class.primary[ i ]
			local catname = string.upper( GetWepCat( catent[ 1 ].ent ) )
			local contains = false

			for k, v in pairs( ownedweapons ) do

				for k2, v2 in pairs( catent ) do

					if v2.ent == v and weapons.Get( v2.ent ) then

						contains = true
						break

					end

				end

			end

			if contains then
		
				local categorypanel = weaponlist:Add( "DPanel" )
				categorypanel:Dock( TOP )
				categorypanel:SetTall( 30 )
				
				if i == 1 then
				
					categorypanel:DockMargin( 0, 0, 0, 8 )
					
				else
				
					categorypanel:DockMargin( 0, 0, 0, 8 )
					
				end
				
				categorypanel:SetDrawBackground( false )
				
				local categoryname = categorypanel:Add( "DPanel" )
				categoryname:Dock( FILL )
				categoryname.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 1 )
					
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )
					
					draw.SimpleText( catname, "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
				end

			end
		
			-- iterate through all available primary weapons, then grab the entities.
			for k, v in SortedPairs( class.primary[ i ] ) do

				for k2, v2 in pairs( ownedweapons ) do
				
					-- As long as the entity is on the server, and the player owns the weapon, we can go on.
					if v.ent == v2 and weapons.Get( v.ent ) then
					
						local weapon = weaponlist:Add( "DPanel" )
						weapon:Dock( TOP )
						weapon:SetTall( panel:GetTall( ) * 0.2 )
						weapon:DockMargin( 0, 0, 0, 8 )
						weapon.Paint = function( panel, w, h )
	
							surface.SetDrawColor( 15, 15, 15, 105 )
							surface.DrawRect( 0, 0, w, h )

							-- If the weapon is the players primary, change the colour of the outline.
							if LocalPlayer( ).primary == v2 then

								surface.SetDrawColor( 175, 175, 255, 255 )

							else

								surface.SetDrawColor( 125, 125, 125, 255 )

							end

							surface.DrawOutlinedRect( 0, 0, w, h )

							draw.SimpleText( LocalPlayer( ):GetWepXP( v.ent ).. " XP", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
							
							if LocalPlayer( ).primary == v2 then

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

							else

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
							
							end
							
						end
							
						local weaponmodel = weapon:Add( "VModelPanel" )
						weaponmodel.OnCursorEntered = function()

							surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

						end
						weaponmodel:Dock( FILL )
						weaponmodel:DockMargin( 0, 0, 0, 1 )
						
						weaponmodel:SetWeapon( v.ent )
						
						local modelpos

						if weaponmodel.Entity then

							if vmodeloffsets[ v.ent ] then

								modelpos = weaponmodel.Entity:GetBonePosition( weaponmodel.Entity:LookupBone( vmodeloffsets[ v.ent ].bone ) )

							else

								modelpos = weaponmodel.Entity:GetBonePosition( 0 )

							end

							local lookat = Vector( -1.5, 0, 3 )
							local campos = Vector( 0, 70, 8 )

							if modeloffsets[ v.ent ] then
								
								weaponmodel:SetLookAt( modelpos + vmodeloffsets[ v.ent ].lookat )
								weaponmodel:SetCamPos( modelpos + vmodeloffsets[ v.ent ].campos )

							else

								weaponmodel:SetLookAt( modelpos - lookat )
								weaponmodel:SetCamPos( modelpos - campos )

							end

						end

						weaponmodel.LayoutEntity = function() end
						
						weaponmodel.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
							
							primarymodel:SetWeapon( v.ent )

							local pmodelpos

							if vmodeloffsets[ v.ent ] then

								pmodelpos = primarymodel.Entity:GetBonePosition( primarymodel.Entity:LookupBone( vmodeloffsets[ v.ent ].bone ) )

							else

								pmodelpos = primarymodel.Entity:GetBonePosition( 0 )

							end

							local lookat = Vector( -1.5, 0, 3 )
							local campos = Vector( 0, 70, 8 )

							if vmodeloffsets[ v.ent ] then
								
								primarymodel:SetLookAt( pmodelpos + vmodeloffsets[ v.ent ].lookat )
								primarymodel:SetCamPos( pmodelpos + vmodeloffsets[ v.ent ].campos )

							else

								primarymodel:SetLookAt( pmodelpos - lookat )
								primarymodel:SetCamPos( pmodelpos - campos )

							end

							primarymodel.LayoutEntity = function( ) end

							primarySelected = true
							LocalPlayer( ).primary = v.ent
							currentwep = weapons.Get( v.ent )

							primary.weptext = weapons.Get( v.ent ).PrintName

							pmodel:UpdateWeapon( v.ent )

							local weapontbl = weapons.Get( v.ent )

							wepname:SetText( weapontbl.PrintName )

							if dmg and weapontbl.Damage then

								dmg.int = weapontbl.Damage * weapontbl.Shots

							end

							if rec and weapontbl.Recoil then

								rec.int = weapontbl.Recoil

							end

							if rpm and weapontbl.RPM then

								local num = math.Round( weapontbl.RPM, 2 )

								rpm.int = num

							end

							if hip and weapontbl.HipSpread then

								local num = weapontbl.HipSpread

								hip.int = num

							end

							if ads and weapontbl.AimSpread then

								local num = 1 - weapontbl.AimSpread

								ads.int = num

							end

							if clip and weapontbl.Primary.ClipSize then

								clip.int = weapontbl.Primary.ClipSize

							end
							
						end

					end
					
				end
				
			end
			
		end
		
	elseif pickingtype == "secondary" then
	
		for i = 1, #class.secondary do
		
			local catent = class.secondary[ i ]
			local catname = GetWepCat( catent[ 1 ].ent )
			local contains = false

			for k, v in pairs( ownedweapons ) do

				for k2, v2 in pairs( catent ) do

					if v2.ent == v and weapons.Get( v2.ent ) then

						contains = true
						break

					end

				end

			end

			if contains then
			
				local categorypanel = weaponlist:Add( "DPanel" )
				categorypanel:Dock( TOP )
				categorypanel:SetTall( 30 )
					
				if i == 1 then
					
					categorypanel:DockMargin( 0, 0, 0, 8 )
						
				else
					
					categorypanel:DockMargin( 0, 0, 0, 8 )
						
				end
				
				categorypanel:SetDrawBackground( false )
					
				local categoryname = categorypanel:Add( "DPanel" )
				categoryname:Dock( FILL )
				categoryname.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 1 )
					
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )
						
					draw.SimpleText( string.upper( catname ), "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
				end

			end

			for k, v in SortedPairs( class.secondary[ i ] ) do

				for k2, v2 in pairs( ownedweapons ) do
				
					if v.ent == v2 and weapons.Get( v.ent ) then

						local weapon = weaponlist:Add( "DPanel" )
						weapon:Dock( TOP )
						weapon:SetTall( panel:GetTall( ) * 0.2 )
						weapon:DockMargin( 0, 0, 0, 8 )
						weapon.Paint = function( panel, w, h )

							surface.SetDrawColor( 15, 15, 15, 105 )
							surface.DrawRect( 0, 0, w, h )

							if LocalPlayer( ).secondary == v2 then

								surface.SetDrawColor( 175, 175, 255, 255 )

							else

								surface.SetDrawColor( 125, 125, 125, 255 )

							end

							surface.DrawOutlinedRect( 0, 0, w, h )

							draw.SimpleText( LocalPlayer( ):GetWepXP( v.ent ).. " XP", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
							
							if LocalPlayer( ).secondary == v2 then

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

							else

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
							
							end
						
						end

						local weaponmodel = weapon:Add( "DModelPanel" )
						weaponmodel.OnCursorEntered = function()

							surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

						end
						weaponmodel:Dock( FILL )
						weaponmodel:DockMargin( 0, 0, 0, 1 )
						
						if weapons.Get( v.ent ).WM then
						
							weaponmodel:SetModel( weapons.Get( v.ent ).WM )
							secondarypreviewmodel = weapons.Get( v.ent ).WM
							
						elseif weapons.Get( v.ent ).WorldModel then
						
							weaponmodel:SetModel( weapons.Get( v.ent ).WorldModel )
							secondarypreviewmodel = weapons.Get( v.ent ).WorldModel
							
						else
						
							weaponmodel:SetModel( Model( "models/weapons/w_pistol.mdl" ) )
							
						end
						local modelpos = weaponmodel.Entity:GetBonePosition( weaponmodel.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
						local lookat = Vector( 3, 0, 4 )
						local campos = Vector( 3, 25, 9 )

						if modeloffsets[ v.ent ] then
							
							weaponmodel:SetLookAt( modelpos - modeloffsets[ v.ent ].lookat )
							weaponmodel:SetCamPos( modelpos - modeloffsets[ v.ent ].campos )

						else

							weaponmodel:SetLookAt( modelpos - lookat )
							weaponmodel:SetCamPos( modelpos - campos )

						end
						weaponmodel.LayoutEntity = function( )
						
						end
						
						weaponmodel.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
						
							secondarymodel:SetWeapon( v.ent )
							
							local modelpos

							if vmodeloffsets[ v.ent ] then

								modelpos = secondarymodel.Entity:GetBonePosition( secondarymodel.Entity:LookupBone( vmodeloffsets[ v.ent ].bone ) )

							else

								modelpos = secondarymodel.Entity:GetBonePosition( 0 )

							end

							local lookat = Vector( -1.5, 0, 3 )
							local campos = Vector( 0, 70, 8 )

							if vmodeloffsets[ sweapon ] then
								
								secondarymodel:SetLookAt( modelpos + vmodeloffsets[ v.ent ].lookat )
								secondarymodel:SetCamPos( modelpos + vmodeloffsets[ v.ent ].campos )

							else

								secondarymodel:SetLookAt( modelpos - lookat )
								secondarymodel:SetCamPos( modelpos - campos )

							end

							secondarymodel.LayoutEntity = function() end
							secondarySelected = true
							LocalPlayer( ).secondary = v.ent
							currentwep = weapons.Get( v.ent )

							secondary.weptext = weapons.Get( v.ent ).PrintName

							pmodel.UpdateWeapon( v.ent )

							local weapontbl = weapons.Get( v.ent )

							wepname:SetText( weapontbl.PrintName )

							if dmg and weapontbl.Damage then

								dmg.int = weapontbl.Damage * weapontbl.Shots

							end

							if rec and weapontbl.Recoil then

								rec.int = weapontbl.Recoil

							end

							if rpm and weapontbl.RPM then

								local num = math.Round( weapontbl.RPM, 2 )

								rpm.int = num

							end

							if hip and weapontbl.HipSpread then

								local num = weapontbl.HipSpread

								hip.int = num

							end

							if ads and weapontbl.AimSpread then

								local num = 1 - weapontbl.AimSpread

								ads.int = num

							end

							if clip and weapontbl.Primary.ClipSize then

								clip.int = weapontbl.Primary.ClipSize

							end

							
						end
						
						weaponmodel.Paint = function( self )

							if ( !IsValid( self.Entity ) ) then return end
								
							local x, y = self:LocalToScreen( 0, 0 )
								
							self:LayoutEntity( self.Entity )
								
							local ang = self.aLookAngle
							if ( !ang ) then
								ang = (self.vLookatPos-self.vCamPos):Angle()
							end
								
							local w, h = self:GetSize()
							cam.Start3D( self.vCamPos, ang, self.fFOV, x, y, w, h, 5, self.FarZ )
								
								render.SuppressEngineLighting( true )
								render.SetLightingOrigin( self.Entity:GetPos() )
								render.ResetModelLighting( self.colAmbientLight.r/255, self.colAmbientLight.g/255, self.colAmbientLight.b/255 )
								render.SetColorModulation( self.colColor.r/255, self.colColor.g/255, self.colColor.b/255 )
								render.SetBlend( self.colColor.a/255 )
								
								for i=0, 6 do
									local col = self.DirectionalLight[ i ]
									if ( col ) then
										render.SetModelLighting( i, col.r/255, col.g/255, col.b/255 )
									end
								end
								self:DrawModel()
														
								render.SuppressEngineLighting( false )
							cam.End3D()
							
							self.LastPaint = RealTime()
								
						end

					end
					
				end
				
			end
			
		end
		
	elseif pickingtype == "grenade" then
	
		for i = 1, #class.grenade do
		
			local catent = class.grenade[ i ]
			local catname = GetWepCat( catent[ 1 ].ent )
			for k, v in pairs( ownedweapons ) do

				for k2, v2 in pairs( catent ) do

					if v2.ent == v and weapons.Get( v2.ent ) then

						contains = true
						break

					end

				end

			end

			if contains then
			
				local categorypanel = weaponlist:Add( "DPanel" )
				categorypanel:Dock( TOP )
				categorypanel:SetTall( 30 )
					
				if i == 1 then
					
					categorypanel:DockMargin( 0, 0, 0, 8 )
						
				else
					
					categorypanel:DockMargin( 0, 0, 0, 8 )
						
				end
				
				categorypanel:SetDrawBackground( false )
					
				local categoryname = categorypanel:Add( "DPanel" )
				categoryname:Dock( FILL )
				categoryname.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 1 )
					
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )
							
					draw.SimpleText( string.upper( catname ), "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

				end

			end

			for k, v in SortedPairs( class.grenade[ i ] ) do
				
				for k2, v2 in pairs( ownedweapons ) do
				
					if v.ent == v2 and weapons.Get( v.ent ) then

						local weapon = weaponlist:Add( "DPanel" )
						weapon:Dock( TOP )
						weapon:SetTall( panel:GetTall( ) * 0.2 )
						weapon:DockMargin( 0, 0, 0, 8 )
						weapon.Paint = function( panel, w, h )

							surface.SetDrawColor( 15, 15, 15, 105 )
							surface.DrawRect( 0, 0, w, h )

							if LocalPlayer( ).grenade == v2 then

								surface.SetDrawColor( 175, 175, 255, 255 )

							else

								surface.SetDrawColor( 125, 125, 125, 255 )

							end

							surface.DrawOutlinedRect( 0, 0, w, h )

							draw.SimpleText( LocalPlayer( ):GetWepXP( v.ent ).. " XP", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
							
							if LocalPlayer( ).grenade == v2 then

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

							else

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
							
							end

						
						end

						local weaponmodel = weapon:Add( "DModelPanel" )
						weaponmodel.OnCursorEntered = function()

							surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

						end
						weaponmodel:Dock( FILL )
						weaponmodel:DockMargin( 0, 0, 0, 1 )
						
						if weapons.Get( v.ent ).WM then
						
							weaponmodel:SetModel( weapons.Get( v.ent ).WM )
							grenadepreviewmodel = weapons.Get( v.ent ).WM
							
						elseif weapons.Get( v.ent ).WorldModel then
						
							weaponmodel:SetModel( weapons.Get( v.ent ).WorldModel )
							grenadepreviewmodel = weapons.Get( v.ent ).WorldModel
							
						else
						
							weaponmodel:SetModel( Model( "models/weapons/w_pistol.mdl" ) )
							
						end

						local viewmins, viewmaxs = weaponmodel.Entity:GetRenderBounds( )
						
						weaponmodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 2.3, 0 ) )
						weaponmodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
						weaponmodel.LayoutEntity = function( )
						
						end
						
						weaponmodel.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
						
							grenademodel:SetModel( weaponmodel:GetModel( ) )
							local viewmins, viewmaxs = grenademodel.Entity:GetRenderBounds( )
											
							grenademodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 2.3, 0 ) )
							grenademodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
							grenadeSelected = true
							LocalPlayer( ).grenade = v.ent
							currentwep = weapons.Get( v.ent )

							grenade.weptext = weapons.Get( v.ent ).PrintName

							local weapontbl = weapons.Get( v.ent )

							wepname:SetText( weapontbl.PrintName )

							if dmg and weapontbl.Damage then

								dmg.int = weapontbl.Damage * weapontbl.Shots

							end

							if rec and weapontbl.Recoil then

								rec.int = weapontbl.Recoil

							end

							if rpm and weapontbl.RPM then

								local num = math.Round( weapontbl.RPM, 2 )

								rpm.int = num

							end

							if hip and weapontbl.HipSpread then

								local num = weapontbl.HipSpread

								hip.int = num

							end

							if ads and weapontbl.AimSpread then

								local num = 1 - weapontbl.AimSpread

								ads.int = num

							end

							if clip and weapontbl.Primary.ClipSize then

								clip.int = weapontbl.Primary.ClipSize

							end

							
						end
						
						weaponmodel.Paint = function( self )

							if ( !IsValid( self.Entity ) ) then return end
								
							local x, y = self:LocalToScreen( 0, 0 )
								
							self:LayoutEntity( self.Entity )
								
							local ang = self.aLookAngle
							if ( !ang ) then
								ang = (self.vLookatPos-self.vCamPos):Angle()
							end
								
							local w, h = self:GetSize()
							cam.Start3D( self.vCamPos, ang, self.fFOV, x, y, w, h, 5, self.FarZ )
								
								render.SuppressEngineLighting( true )
								render.SetLightingOrigin( self.Entity:GetPos() )
								render.ResetModelLighting( self.colAmbientLight.r/255, self.colAmbientLight.g/255, self.colAmbientLight.b/255 )
								render.SetColorModulation( self.colColor.r/255, self.colColor.g/255, self.colColor.b/255 )
								render.SetBlend( self.colColor.a/255 )
								
								for i=0, 6 do
									local col = self.DirectionalLight[ i ]
									if ( col ) then
										render.SetModelLighting( i, col.r/255, col.g/255, col.b/255 )
									end
								end
								self:DrawModel()
														
								render.SuppressEngineLighting( false )
							cam.End3D()	

							self.LastPaint = RealTime()
								
						end

					end
					
				end
				
			end
			
		end

		elseif pickingtype == "melee" then
	
		for i = 1, #class.melee do
		
			local catent = class.melee[ i ]
			local catname = GetWepCat( catent[ 1 ].ent )
			for k, v in pairs( ownedweapons ) do

				for k2, v2 in pairs( catent ) do

					if v2.ent == v and weapons.Get( v2.ent ) then

						contains = true
						break

					end

				end

			end

			if contains then

				local categorypanel = weaponlist:Add( "DPanel" )
				categorypanel:Dock( TOP )
				categorypanel:SetTall( 30 )
					
				if i == 1 then
					
					categorypanel:DockMargin( 0, 0, 0, 8 )
						
				else
					
					categorypanel:DockMargin( 0, 0, 0, 8 )
						
				end
				
				categorypanel:SetDrawBackground( false )
					
				local categoryname = categorypanel:Add( "DPanel" )
				categoryname:Dock( FILL )
				categoryname.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 1 )
					
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )
							
					draw.SimpleText( string.upper( catname ), "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

				end

			end

			for k, v in SortedPairs( class.melee[ i ] ) do

				for k2, v2 in pairs( ownedweapons ) do
				
					if v.ent == v2 and weapons.Get( v.ent ) then

						local weapon = weaponlist:Add( "DPanel" )
						weapon:Dock( TOP )
						weapon:SetTall( panel:GetTall( ) * 0.2 )
						weapon:DockMargin( 0, 0, 0, 8 )
						weapon.Paint = function( panel, w, h )

							surface.SetDrawColor( 15, 15, 15, 105 )
							surface.DrawRect( 0, 0, w, h )

							if LocalPlayer( ).melee == v2 then

								surface.SetDrawColor( 175, 175, 255, 255 )

							else

								surface.SetDrawColor( 125, 125, 125, 255 )

							end

							surface.DrawOutlinedRect( 0, 0, w, h )

							draw.SimpleText( LocalPlayer( ):GetWepXP( v.ent ).. " XP", "HUD", 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
							
							if LocalPlayer( ).melee == v2 then

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 175, 175, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

							else

								draw.SimpleText( weapons.Get( v2 ).PrintName, "HUD", w - 8, th, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
							
							end
						
						end
						
						local weaponmodel = weapon:Add( "DModelPanel" )
						weaponmodel.OnCursorEntered = function()

							surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

						end
						weaponmodel:Dock( FILL )
						weaponmodel:DockMargin( 0, 0, 0, 1 )
						
						if weapons.Get( v.ent ).WM then
						
							weaponmodel:SetModel( weapons.Get( v.ent ).WM )
							meleepreviewmodel = weapons.Get( v.ent ).WM
							
						elseif weapons.Get( v.ent ).WorldModel then
						
							weaponmodel:SetModel( weapons.Get( v.ent ).WorldModel )
							meleepreviewmodel = weapons.Get( v.ent ).WorldModel
							
						else
						
							weaponmodel:SetModel( Model( "models/weapons/w_pistol.mdl" ) )
							
						end

						local viewmins, viewmaxs = weaponmodel.Entity:GetRenderBounds( )
						
						weaponmodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 2.3, 0 ) )
						weaponmodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
						weaponmodel.LayoutEntity = function( )
						
						end
						
						weaponmodel.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
						
							meleemodel:SetModel( weaponmodel:GetModel( ) )
							local viewmins, viewmaxs = meleemodel.Entity:GetRenderBounds( )
											
							meleemodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 2.3, 0 ) )
							meleemodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
							meleeSelected = true
							LocalPlayer( ).melee = v.ent
							currentwep = weapons.Get( v.ent )

							melee.weptext = weapons.Get( v.ent ).PrintName
							
							local weapontbl = weapons.Get( v.ent )

							wepname:SetText( weapontbl.PrintName )

							if dmg and weapontbl.Damage then

								dmg.int = weapontbl.Damage * weapontbl.Shots

							end

							if rec and weapontbl.Recoil then

								rec.int = weapontbl.Recoil

							end

							if rpm and weapontbl.RPM then

								local num = math.Round( weapontbl.RPM, 2 )

								rpm.int = num

							end

							if hip and weapontbl.HipSpread then

								local num = weapontbl.HipSpread

								hip.int = num

							end

							if ads and weapontbl.AimSpread then

								local num = 1 - weapontbl.AimSpread

								ads.int = num

							end

							if clip and weapontbl.Primary.ClipSize then

								clip.int = weapontbl.Primary.ClipSize

							end

						end
						
						weaponmodel.Paint = function( self )

							if ( !IsValid( self.Entity ) ) then return end
								
							local x, y = self:LocalToScreen( 0, 0 )
								
							self:LayoutEntity( self.Entity )
								
							local ang = self.aLookAngle
							if ( !ang ) then
								ang = (self.vLookatPos-self.vCamPos):Angle()
							end
								
							local w, h = self:GetSize()
							cam.Start3D( self.vCamPos, ang, self.fFOV, x, y, w, h, 5, self.FarZ )
								
								render.SuppressEngineLighting( true )
								render.SetLightingOrigin( self.Entity:GetPos() )
								render.ResetModelLighting( self.colAmbientLight.r/255, self.colAmbientLight.g/255, self.colAmbientLight.b/255 )
								render.SetColorModulation( self.colColor.r/255, self.colColor.g/255, self.colColor.b/255 )
								render.SetBlend( self.colColor.a/255 )
								
								for i=0, 6 do
									local col = self.DirectionalLight[ i ]
									if ( col ) then
										render.SetModelLighting( i, col.r/255, col.g/255, col.b/255 )
									end
								end
								self:DrawModel()
														
								render.SuppressEngineLighting( false )
							cam.End3D()	

							self.LastPaint = RealTime()
								
						end
		
					end

				end
				
			end
			
		end

	elseif pickingtype == "pattachments" then
	
		local weaponid = LocalPlayer( ).primary
		local attachments = {}
		local attachmentstable = { }
		local theattachments = { }
		
		if weapons.Get( weaponid ) then
		
			attachments = weapons.Get( weaponid ).Attachments
			
		end
		
		for k, v in pairs( attachments ) do
			
			table.insert( attachmentstable, v )
			
		end
		
		local categorypanel = weaponlist:Add( "DPanel" )
			categorypanel:Dock( TOP )
			categorypanel:SetTall( 30 )
				
			if i == 1 then
				
				categorypanel:DockMargin( 0, 0, 0, 8 )
					
			else
				
				categorypanel:DockMargin( 0, 0, 0, 8 )
					
			end
			
			categorypanel:SetDrawBackground( false )
				
			local categoryname = categorypanel:Add( "DPanel" )
			categoryname:Dock( FILL )
			categoryname.Paint = function( panel, w, h )

				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 1 )
				
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
						
				draw.SimpleText( weapons.Get( weaponid ).PrintName .. " - " .. LocalPlayer( ):GetWepXP( weaponid ) .. " XP", "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

			end
		
		for k, v in pairs( attachmentstable ) do
		
			local selected = nil
		
			local weapon = weaponlist:Add( "DPanel" )
			weapon:Dock( TOP )
			weapon:SetTall( panel:GetTall( ) * 0.2 )
			
			if k == 1 then
			
				weapon:DockMargin( 0, 0, 0, 8 )
			else
			
				weapon:DockMargin( 0, 0, 0, 8 )
				
			end
			
			weapon.Paint = function( panel, w, h )

				surface.SetDrawColor( 15, 15, 15, 105 )
				surface.DrawRect( 0, 0, w, h )

				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
					
			end
				
			local wname = weapon:Add( "DPanel" )
			wname:Dock( TOP )
			wname:SetTall( weapon:GetTall( ) * 0.2 )
			wname.Paint = function( panel, w, h )
					
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 2 )
					
			end
				
			local wnametext = wname:Add( "DLabel" )
			wnametext:Dock( FILL )
			wnametext:SetText( v.header )
			wnametext:SetContentAlignment( 5 )
			wnametext:SetFont( "HUD" )

			local weaponscroll = weapon:Add( "DScrollPanel" )
			weaponscroll:Dock( FILL )

			local weaponlayout = weaponscroll:Add( "DIconLayout" )
			weaponlayout:Dock( FILL )
		
			for key, att in pairs( v.atts ) do
			
				local att2 = AttachmentTable[ att ]

				local fulldesc = ""

				for a, b in pairs( CustomizableWeaponry:findAttachment( att ).description ) do

					fulldesc = b.t.. " " .. fulldesc .. " "

				end
				
				local atticon = weaponlayout:Add( "DButton" )
				atticon:SetTall( 64 )
				atticon:SetWide( weaponlistbg2:GetWide() / 3 - 12 )
				atticon.Paint = function( panel, w, h )
					
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetTexture( CustomizableWeaponry:findAttachment( att ).displayIcon )
					surface.DrawTexturedRect( w / 2 - 32, 0, 64, 64 )

					draw.SimpleText( CustomizableWeaponry:findAttachment( att ).displayName, "RobotoSmaller", w * 0.5, 52, Color( 205, 205, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
					if att2 && att2.level then
					
						if att2.level > LocalPlayer( ):GetWepXP( weaponid ) then
						
							surface.SetDrawColor( 205, 55, 55, 15 )
							surface.DrawRect( 0, 0, w, 72 )
							
							draw.SimpleText( "Req ".. att2.level .. " XP", "RobotoSmaller", w * 0.5, 12, Color( 205, 205, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							
						end
						
					end
					
					if selected == panel then
					
						surface.DrawOutlinedRect( 0, 0, w, h )
						
					end
					
				end
				
				if att2 && att2.level then
					
					if att2.level > LocalPlayer( ):GetWepXP( weaponid ) then

						atticon:SetEnabled( false )
						
					end
					
				end
				
				atticon:SetText( "" )
				atticon:SetTextColor( Color( 215, 215, 215 ) )
				atticon:SetTooltip( fulldesc )
				atticon.DoClick = function( panel )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
					surface.PlaySound( table.Random( customisationsounds ) )

					if selected == panel then

						theattachments[ wnametext:GetText() ] = nil
						selected = nil

						if primarymodel then

							--primarymodel:RemoveAttachment( v.header, att )

						end
						
					else
						
						theattachments[ wnametext:GetText() ] = att
						selected = panel

						if primarymodel then

							--primarymodel:SetAttachment( v.header, att )

						end
						
					end

					pattachments[ weaponid ] = theattachments
					
				end

				if pattachments[ weaponid ] then

					if pattachments[ weaponid ][ wnametext:GetText() ] then

						if pattachments[weaponid][ wnametext:GetText() ] == att then

							theattachments[ wnametext:GetText() ] = att
							selected = atticon

							if primarymodel then

								--primarymodel:SetAttachment( v.header, att )

							end

						end

					end

				end
				
			end
			
		end

		if ownedskins and ownedskins[ weaponid ] and #ownedskins[ weaponid ] > 0 then

			local selected = nil

			local weapon = weaponlist:Add( "DPanel" )
			weapon:Dock( TOP )
			weapon:SetTall( panel:GetTall( ) * 0.2 )
			weapon:DockMargin( 0, 0, 0, 8 )
			
			weapon.Paint = function( panel, w, h )

				surface.SetDrawColor( 15, 15, 15, 105 )
				surface.DrawRect( 0, 0, w, h )

				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
					
			end
				
			local wname = weapon:Add( "DPanel" )
			wname:Dock( TOP )
			wname:SetTall( weapon:GetTall( ) * 0.1 )
			wname.Paint = function( panel, w, h )
					
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 2 )
					
			end
				
			local wnametext = wname:Add( "DLabel" )
			wnametext:Dock( FILL )
			wnametext:SetText( "SKINS" )
			wnametext:SetContentAlignment( 5 )
			wnametext:SetFont( "HUD" )

			local weaponscroll = weapon:Add( "DScrollPanel" )
			weaponscroll:Dock( FILL )

			local scrollbar = weaponscroll:GetVBar( )
	
			function scrollbar:Paint( w, h )
			
				surface.SetDrawColor( 35, 35, 35, 55 )
				surface.DrawRect( 8, 0, 4, h )
				
			end
			
			function scrollbar.btnGrip:Paint( w, h )
			
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 5, 0, 10, h )
			
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 6, 1, 8, h - 2 )
				
			end
			
			function scrollbar.btnUp:Paint( w, h )
			
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 5, 3, 10, 10 )
			
				surface.SetDrawColor( 35, 35, 35, 255 )
				surface.DrawRect( 6, 4, 8, 8 )
				
			end
			
			function scrollbar.btnDown:Paint( w, h )
			
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 5, 3, 10, 10 )
			
				surface.SetDrawColor( 35, 35, 35, 255 )
				surface.DrawRect( 6, 4, 8, 8 )
				
			end
		
			local weaponlayout = weaponscroll:Add( "DIconLayout" )
			weaponlayout:Dock( FILL )
		
			for k, v in SortedPairsByValue( ownedskins[ weaponid ] ) do

				local skintable = WeaponSkins[ weaponid ][ v ]
				local preview = Material( skintable.texture.. "_icon.png" )
			
				local atticon = weaponlayout:Add( "DButton" )
				atticon:SetTall( 64 )
				atticon:SetWide( weaponlistbg2:GetWide() * 0.25 - 8 )
				atticon.Paint = function( panel, w, h )
					
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( preview )
					surface.DrawTexturedRect( w / 2 - 32, 0, 64, 64 )

					draw.SimpleTextOutlined( v, "RobotoSmaller", w * 0.5, 52, Color( 205, 205, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
			
					if selected == panel then
					
						surface.DrawOutlinedRect( 0, 0, w, h )
						
					end
					
				end
								
				atticon:SetText( "" )
				atticon.DoClick = function( panel )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
					surface.PlaySound( table.Random( customisationsounds ) )

					if selected == panel then

						pskin[ weaponid ] = ""
						selected = nil

						if primarymodel then

							primarymodel:ApplyWepSkin( "", skintable.submaterials )

						end

					else

						pskin[ weaponid ] = v
						selected = panel

						if primarymodel then

							primarymodel:ApplyWepSkin( skintable.texture, skintable.submaterials )

						end

					end
					
				end

				if pskin[ weaponid ] == v then

					selected = atticon

				end

			end

			weapon:SizeToContents()
			weaponlayout:SizeToContents()

		end
		
	elseif pickingtype == "sattachments" then
	
		local weaponid = LocalPlayer( ).secondary
		local attachments = {}
		local attachmentstable = { }
		local theattachments = { }
		
		if weapons.Get( weaponid ) then
		
			attachments = weapons.Get( weaponid ).Attachments
			
		end
		
		for k, v in pairs( attachments ) do
			
			table.insert( attachmentstable, v )
			
		end
		
		local categorypanel = weaponlist:Add( "DPanel" )
			categorypanel:Dock( TOP )
			categorypanel:SetTall( 30 )
				
			if i == 1 then
				
				categorypanel:DockMargin( 0, 0, 0, 8 )
					
			else
				
				categorypanel:DockMargin( 0, 0, 0, 8 )
					
			end
			
			categorypanel:SetDrawBackground( false )
				
			local categoryname = categorypanel:Add( "DPanel" )
			categoryname:Dock( FILL )
			categoryname.Paint = function( panel, w, h )

				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 1 )
				
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
						
				draw.SimpleText( weapons.Get( weaponid ).PrintName .. " - " .. LocalPlayer( ):GetWepXP( weaponid ) .. " XP", "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

			end
		
		for k, v in pairs( attachmentstable ) do
		
			local selected = nil
		
			local weapon = weaponlist:Add( "DPanel" )
			weapon:Dock( TOP )
			weapon:SetTall( panel:GetTall( ) * 0.2 )
			
			if k == 1 then
			
				weapon:DockMargin( 0, 0, 0, 8 )
			else
			
				weapon:DockMargin( 0, 0, 0, 8 )
				
			end
			
			weapon.Paint = function( panel, w, h )

				surface.SetDrawColor( 15, 15, 15, 105 )
				surface.DrawRect( 0, 0, w, h )

				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
					
			end
				
			local wname = weapon:Add( "DPanel" )
			wname:Dock( TOP )
			wname:SetTall( weapon:GetTall( ) * 0.2 )
			wname.Paint = function( panel, w, h )
					
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 2 )
					
			end
				
			local wnametext = wname:Add( "DLabel" )
			wnametext:Dock( FILL )
			wnametext:SetText( v.header )
			wnametext:SetContentAlignment( 5 )
			wnametext:SetFont( "HUD" )

			local weaponscroll = weapon:Add( "DScrollPanel" )
			weaponscroll:Dock( FILL )

			local weaponlayout = weaponscroll:Add( "DIconLayout" )
			weaponlayout:Dock( FILL )
		
			for key, att in pairs( v.atts ) do
			
				local att2 = AttachmentTable[ att ]

				local fulldesc = ""

				for a, b in pairs( CustomizableWeaponry:findAttachment( att ).description ) do

					fulldesc = b.t.. " " .. fulldesc .. " "

				end
				
				local atticon = weaponlayout:Add( "DButton" )
				atticon:SetTall( 64 )
				atticon:SetWide( weaponlistbg2:GetWide() / 3 - 12 )
				atticon.Paint = function( panel, w, h )
					
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetTexture( CustomizableWeaponry:findAttachment( att ).displayIcon )
					surface.DrawTexturedRect( w / 2 - 32, 0, 64, 64 )

					draw.SimpleText( CustomizableWeaponry:findAttachment( att ).displayName, "RobotoSmaller", w * 0.5, 52, Color( 205, 205, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
					if att2 && att2.level then
					
						if att2.level > LocalPlayer( ):GetWepXP( weaponid ) then
						
							surface.SetDrawColor( 205, 55, 55, 15 )
							surface.DrawRect( 0, 0, w, 72 )
							
							draw.SimpleText( "Req ".. att2.level .. " XP", "RobotoSmaller", w * 0.5, 12, Color( 205, 205, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
							
						end
						
					end
					
					if selected == panel then
					
						surface.DrawOutlinedRect( 0, 0, w, h )
						
					end
					
				end
				
				if att2 && att2.level then
					
					if att2.level > LocalPlayer( ):GetWepXP( weaponid ) then

						atticon:SetEnabled( false )
						
					end
					
				end
				
				atticon:SetText( "" )
				atticon:SetTextColor( Color( 215, 215, 215 ) )
				atticon:SetTooltip( fulldesc )
				atticon.DoClick = function( panel )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
					surface.PlaySound( table.Random( customisationsounds ) )

					if selected == panel then

						theattachments[ wnametext:GetText() ] = nil
						selected = nil
						
					else
						
						theattachments[ wnametext:GetText() ] = att
						selected = panel
						
					end

					sattachments[ weaponid ] = theattachments
					
				end

				if sattachments[ weaponid ] then

					if sattachments[ weaponid ][ wnametext:GetText() ] then

						if sattachments[weaponid][ wnametext:GetText() ] == att then

							theattachments[ wnametext:GetText() ] = att
							selected = atticon

						end

					end

				end
				
			end
			
		end

		if ownedskins and ownedskins[ weaponid ] and #ownedskins[ weaponid ] > 0 then

			local selected = nil

			local weapon = weaponlist:Add( "DPanel" )
			weapon:Dock( TOP )
			weapon:SetTall( panel:GetTall( ) * 0.2 )
			weapon:DockMargin( 0, 0, 0, 8 )
			
			weapon.Paint = function( panel, w, h )

				surface.SetDrawColor( 15, 15, 15, 105 )
				surface.DrawRect( 0, 0, w, h )

				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
					
			end
				
			local wname = weapon:Add( "DPanel" )
			wname:Dock( TOP )
			wname:SetTall( weapon:GetTall( ) * 0.1 )
			wname.Paint = function( panel, w, h )
					
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 2 )
					
			end
				
			local wnametext = wname:Add( "DLabel" )
			wnametext:Dock( FILL )
			wnametext:SetText( "SKINS" )
			wnametext:SetContentAlignment( 5 )
			wnametext:SetFont( "HUD" )

			local weaponscroll = weapon:Add( "DScrollPanel" )
			weaponscroll:Dock( FILL )

			local scrollbar = weaponscroll:GetVBar( )
	
			function scrollbar:Paint( w, h )
			
				surface.SetDrawColor( 35, 35, 35, 55 )
				surface.DrawRect( 8, 0, 4, h )
				
			end
			
			function scrollbar.btnGrip:Paint( w, h )
			
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 5, 0, 10, h )
			
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 6, 1, 8, h - 2 )
				
			end
			
			function scrollbar.btnUp:Paint( w, h )
			
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 5, 3, 10, 10 )
			
				surface.SetDrawColor( 35, 35, 35, 255 )
				surface.DrawRect( 6, 4, 8, 8 )
				
			end
			
			function scrollbar.btnDown:Paint( w, h )
			
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 5, 3, 10, 10 )
			
				surface.SetDrawColor( 35, 35, 35, 255 )
				surface.DrawRect( 6, 4, 8, 8 )
				
			end
		
			local weaponlayout = weaponscroll:Add( "DIconLayout" )
			weaponlayout:Dock( FILL )
		
			for k, v in SortedPairsByValue( ownedskins[ weaponid ] ) do

				local skintable = WeaponSkins[ weaponid ][ v ]
				local preview = Material( skintable.texture.. "_icon.png" )
			
				local atticon = weaponlayout:Add( "DButton" )
				atticon:SetTall( 64 )
				atticon:SetWide( weaponlistbg2:GetWide() * 0.25 - 8 )
				atticon.Paint = function( panel, w, h )
					
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.SetMaterial( preview )
					surface.DrawTexturedRect( w / 2 - 32, 0, 64, 64 )

					draw.SimpleTextOutlined( v, "RobotoSmaller", w * 0.5, 52, Color( 205, 205, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
			
					if selected == panel then
					
						surface.DrawOutlinedRect( 0, 0, w, h )
						
					end
					
				end
								
				atticon:SetText( "" )
				atticon.DoClick = function( panel )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
					surface.PlaySound( table.Random( customisationsounds ) )

					if selected == panel then

						sskin[ weaponid ] = ""
						selected = nil

						if secondarymodel then

							secondarymodel:ApplyWepSkin( "", v )

						end

					else

						sskin[ weaponid ] = v
						selected = panel

						if secondarymodel then

							secondarymodel:ApplyWepSkin( skintable.texture, v )

						end

					end
					
				end

				if sskin[ weaponid ] == v then

					selected = atticon

				end

			end

			weapon:SizeToContents()
			weaponlayout:SizeToContents()

		end

	end
	
	pmodel:SetModel( LocalPlayer( ).model )
			
	local viewmins, viewmaxs = pmodel.Entity:GetRenderBounds( )
				
	pmodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.5, -0.25, 0.5 ) )
	pmodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )
	pmodel.LayoutEntity = function( panel, entity )
	
		if currentwep then
		
			if currentwep.HoldType ~= "pistol" then
			
				local sequence = entity:LookupSequence( "idle_passive" )
				entity:ResetSequence( sequence )
				
			else
			
				local sequence = entity:LookupSequence( "idle_all_01" )
				entity:ResetSequence( sequence )
				
			end
		
		end
	
	end
	
	local finish = weaponlistbg:Add( "DButton" )
	finish:Dock( BOTTOM )
	finish:DockMargin( 0, 0, 0, 0 )
	finish:SetTall( 32 )
	finish:SetText( "DEPLOY" )
	finish:SetToolTip( "Select a primary & secondary weapon to continue." )
	finish:SetFont( "HUD2" )
	finish:SetTextColor( Color( 205, 205, 205 ) )
	finish:SetDisabled( true )
	finish.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )
	
	end
	
	finish.DoClick = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
	
		net.Start( "PlayerReady" )
		
			net.WriteInt( LocalPlayer( ).team, 8 )
			net.WriteInt( LocalPlayer( ).role, 8 )
			net.WriteString( LocalPlayer( ).primary )
			net.WriteString( LocalPlayer( ).secondary )
			net.WriteString( LocalPlayer( ).grenade )
			net.WriteString( LocalPlayer( ).melee )

			if pattachments[ LocalPlayer( ).primary ] then

				net.WriteTable( pattachments[ LocalPlayer( ).primary ] )

			else

				net.WriteTable( {} )

			end
			
			if sattachments[ LocalPlayer( ).secondary ] then

				net.WriteTable( sattachments[ LocalPlayer( ).secondary ] )

			else

				net.WriteTable( {} )

			end

			if pskin[ LocalPlayer( ).primary ] then

				net.WriteString( pskin[ LocalPlayer( ).primary ] )

			else

				net.WriteString( "" )

			end

			if sskin[ LocalPlayer().secondary ] then

				net.WriteString( sskin[ LocalPlayer().secondary ] )

			else

				net.WriteString( "" )

			end
			
		net.SendToServer( )
		
		pickingtype = "none"
		
		if panel:GetParent( ) then
		
			panel:GetParent( ):Remove( )
			
		end
		
		LocalPlayer( ).team = nil
		LocalPlayer( ).Class = LocalPlayer( ).role

		surface.PlaySound( "uprising/frontend/UI_Battledash_Notification_Wave 0 0 0.wav")
		
	end
	
	finish.Think = function( self )
	
		-- You can only submit if everything's been selected.
		if LocalPlayer( ).primary and LocalPlayer( ).secondary and LocalPlayer( ).grenade and LocalPlayer( ).melee then

			local pressed = false
			
			-- Let you skip with the space bar.
			if input.IsKeyDown( KEY_SPACE ) and !self:GetDisabled() and !pressed then

				self:DoClick()
				pressed = true

			end
		
			self:SetDisabled( false )
			
		else
		
			self:SetDisabled( true )
			
		end
		
	end
			
end

-- Make the basic loadout screen, blurred background & class selection.
local function Loadout()
	local disabled = true
	
	if !LocalPlayer( ).team then
	
		LocalPlayer( ).team = LocalPlayer( ):Team( )
		
	end
	
	local panel = vgui.Create( "DFrame" )
	panel:SetSize( ScrW( ), ScrH( ) )
	panel:Center( )
	panel:MakePopup( )
	panel:SetTitle( "" )
	panel:SetDraggable( false )
	panel.Paint = function( panel, w, h )
	
		Derma_DrawBackgroundBlur( panel, time )
		
	end
	
	local classmenu = panel:Add( "DPanel" )
	classmenu:Dock( TOP )
	classmenu:SetTall( ScrH( ) * 0.04 )
	classmenu:DockMargin( ScrW( ) * 0.2, 0, ScrW( ) * 0.2, 0 )
	classmenu:DockPadding( 1, 0, 1, 0 )
	classmenu:SetDrawBackground( false )
	classmenu.PaintOver = function( panel, w, h )
	
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )
		
	end
	
	local playerinfo = panel:Add( "DPanel" )
	playerinfo:Dock( FILL )
	playerinfo:SetDrawBackground( false )
	
	-- Iterate through the classes and add a button for each of 'em.
	for k, v in pairs( ClassTable ) do
	
		local class = classmenu:Add( "DButton" )
		class:Dock( LEFT )
		class:SetWide( ScrW( ) * 0.15 )
		class:DockMargin( 0, 0, 0, 0 )
		class:SetFont( "HUD" )
		class:SetTextColor( Color( 205, 205, 205 ) )
		class:SetText( string.upper( v.name ) )
		--class:SetToolTip( v.desc )
		class.alpha = 155
		class.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
			class.alpha = 255
			
		end
		class.OnCursorExited = function( )
		
			class.alpha = 155
			
		end
		class.DoClick = function( self )

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
		
			for k2, v2 in pairs( playerinfo:GetChildren( ) ) do
			
				v2:Remove( )
				
			end
		
			if LocalPlayer( ).team == nil then
			
				LocalPlayer( ).team = LocalPlayer( ):Team( )
				
			end
			
			if LocalPlayer( ).role != k then
			
				LocalPlayer( ).primary = nil
				
			end
			
			if LocalPlayer( ).role == nil then
			
				LocalPlayer( ).role = k
				
			end
			
			if LocalPlayer( ).team == TEAM_BLUFOR then
			
				LocalPlayer( ).model = v.bModel[ math.random( #v.bModel ) ]
				LocalPlayer( ).team = TEAM_BLUFOR
				
			elseif LocalPlayer( ).team == TEAM_OPFOR then
			
				LocalPlayer( ).model = v.oModel[ math.random( #v.bModel ) ]
				LocalPlayer( ).team = TEAM_OPFOR
				
			end
			
			LocalPlayer( ).role = k
			
			surface.PlaySound( "uprising/frontend/UI_Battledash_Open_Wave 0 0 0.wav")
			
			BuildInfo( playerinfo )
			
		end
		
		local smooth = 155
		
		class.Paint = function( panel, w, h )
		
			smooth = math.Approach( smooth, class.alpha, FrameTime( ) * 120 )
		
			if LocalPlayer().role == k then
			
				surface.SetDrawColor( 85, 105, 95, smooth )
				
			else
		
				surface.SetDrawColor( 85, 85, 95, smooth )
				
			end
			
			surface.SetTexture( gradient )
			surface.DrawTexturedRect( 0, 1, w, h - 2 )
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 1, w, h - 2 )
			
		end

		if LocalPlayer().Class and LocalPlayer().Class == k then

			timer.Simple( 0.05, function()

				class:DoClick()

			end )

		end
		
	end
	
end

local teama

local function populatelist( panel, teamnum, winner )

	local players = team.GetPlayers( teamnum )
	
	table.sort( players, function( a, b )

		return a:Frags() > b:Frags() 

	end )

	local SideInfo = panel:Add( "DPanel" )
	SideInfo:Dock( TOP )
	SideInfo:SetTall( 32 )
	SideInfo.Paint = function( panel, w, h )
		
		surface.SetDrawColor( 85, 75, 75, 115 )
		surface.SetTexture( gradient2 )
		surface.DrawTexturedRect( 0, 0, w, h )
		surface.SetDrawColor( 45, 35, 35, 155 )
		surface.DrawRect( 0, 0, w, h )

		draw.SimpleText( team.GetName( teamnum ), "HUD", 8, h * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

	end	

	local joinbutt = SideInfo:Add( "DButton" )
	joinbutt:Dock( LEFT )
	joinbutt:SetFont( "HUD" )
	joinbutt:SetText( "  JOIN ".. team.GetName( teamnum ) .."  " )
	joinbutt:SetTextColor( Color( 255, 255, 255 ) )
	joinbutt:SizeToContents()
	joinbutt.Paint = function( panel, w, h )

		surface.SetDrawColor( 85, 85, 95, 25 )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 25 )
		surface.DrawRect( 0, 1, w, h - 2 )

		surface.SetDrawColor( 255, 255, 255, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )
		
	end
	joinbutt.DoClick = function( panel )

		local opposite = 0

		if teamnum == 1 then

			opposite = 2

		else

			opposite = 1

		end

		if team.NumPlayers( teamnum ) <= team.NumPlayers( opposite ) then

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
		
			LocalPlayer().team = teamnum
			Loadout()
			teama:Remove()

		else

			surface.PlaySound( "buttons/button10.wav" )

		end
	
	end
		
	local ScorePing = SideInfo:Add( "DLabel" )
	ScorePing:Dock( RIGHT )
	ScorePing:SetWide( 64 )
	ScorePing:SetFont( "HUD" )
	ScorePing:SetText( "Ping" )
	ScorePing:SetContentAlignment( 4 )
		
	local ScoreDeath = SideInfo:Add( "DLabel" )
	ScoreDeath:Dock( RIGHT )
	ScoreDeath:SetWide( 64 )
	ScoreDeath:SetFont( "HUD" )
	ScoreDeath:SetText( "D" )
	ScoreDeath:SetContentAlignment( 4 )
		
	local ScoreKill = SideInfo:Add( "DLabel" )
	ScoreKill:Dock( RIGHT )
	ScoreKill:SetWide( 64 )
	ScoreKill:SetFont( "HUD" )
	ScoreKill:SetText( "K" )
	ScoreKill:SetContentAlignment( 4 )

	for k, v in pairs( players ) do

		local PlayerInfo = panel:Add( "DPanel" )
		PlayerInfo:Dock( TOP )
		PlayerInfo:SetTall( 64 )
		PlayerInfo.Paint = function( panel, w, h )
			
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 0, w, h )
				
		end
			
		local PlayerAvatar = PlayerInfo:Add( "AvatarImage" )
		PlayerAvatar:Dock( LEFT )
		PlayerAvatar:SetWide( PlayerInfo:GetTall( ) )
		PlayerAvatar:SetPlayer( v, 64 )
			
		local PAvatarButton = PlayerAvatar:Add( "DButton" )
		PAvatarButton:Dock( FILL )
		PAvatarButton:SetText( "" )
		PAvatarButton.Paint = function( )
			
		end
			
		PAvatarButton.DoClick = function( )

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
			
			v:ShowProfile( )
				
		end
			
		local PlayerName = PlayerInfo:Add( "DPanel" )
		PlayerName:Dock( LEFT )
		
		PlayerName:SetWide( ScrW( ) * 0.25 )

		local icon = v:GetRankMaterial()

		PlayerName.Paint = function( panel, w, h )

			if IsValid( v ) then
		
				surface.SetMaterial( icon )
				surface.DrawTexturedRect( 0, 0, 64, 64 )

				draw.SimpleText( v:Name() or "Disconnected", "RobotoSmall", 64, h * 0.5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
				draw.SimpleText( v:GetUserGroup(), "RobotoSmaller", 64, h * 0.8, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			else

				draw.SimpleText( "Disconnected", "RobotoSmall", 0, h * 0.4, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

			end

		end
			
		local PingIcon = PlayerInfo:Add( "DPanel" )
		PingIcon:Dock( RIGHT )
		PingIcon:SetWide( 64 )
		PingIcon:SetTooltip( v:Ping( ) )
		PingIcon.Paint = function( panel, w, h )
			
			local basepos = 4
			local basewidth = 4
			local baseheight = h - 24
			local ping = 0

			if IsValid( v ) and v:IsPlayer() then

				ping = v:Ping( )

			end
				
			if ping <= 50 then
				
				surface.SetDrawColor( 0, 255, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			elseif ping <= 150 and ping > 50 then
				
				surface.SetDrawColor( 105, 205, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
					
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
				
			elseif ping <= 250 and ping > 150 then
				
				surface.SetDrawColor( 155, 155, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			else
				
				surface.SetDrawColor( 255, 0, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			end
				
		end
			
		local Deaths = PlayerInfo:Add( "DLabel" )
		Deaths:Dock( RIGHT )
		Deaths:SetWide( 64 )
		Deaths:SetFont( "HUD" )
		Deaths:SetText( v:Deaths( ) )
			
		local Kills = PlayerInfo:Add( "DLabel" )
		Kills:Dock( RIGHT )
		Kills:SetWide( 64 )
		Kills:SetFont( "HUD" )
		Kills:SetText( v:Frags( ) )

	end

end

local function populatelist( panel, teamnum )

	local players = team.GetPlayers( teamnum )
	
	table.sort( players, function( a, b )

		return a:Frags() > b:Frags() 

	end )

	local SideInfo = panel:Add( "DPanel" )
	SideInfo:Dock( TOP )
	SideInfo:SetTall( 32 )
	SideInfo.Paint = function( panel, w, h )
		
		surface.SetDrawColor( 85, 75, 75, 115 )
		surface.SetTexture( gradient2 )
		surface.DrawTexturedRect( 0, 0, w, h )
		surface.SetDrawColor( 45, 35, 35, 155 )
		surface.DrawRect( 0, 0, w, h )

	end	

	local joinbutt = SideInfo:Add( "DButton" )
	joinbutt:Dock( LEFT )
	joinbutt:SetFont( "HUD" )
	joinbutt:SetText( "  JOIN ".. team.GetName( teamnum ) .."  " )
	joinbutt:SetTextColor( Color( 255, 255, 255 ) )
	joinbutt:SizeToContents()
	joinbutt.Paint = function( panel, w, h )

		surface.SetDrawColor( 85, 85, 95, 25 )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 25 )
		surface.DrawRect( 0, 1, w, h - 2 )

		surface.SetDrawColor( 255, 255, 255, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )
		
	end

	joinbutt.DoClick = function( panel )

		local opposite = 0

		if teamnum == 1 then

			opposite = 2

		else

			opposite = 1

		end

		if team.NumPlayers( teamnum ) < team.NumPlayers( opposite ) or ( LocalPlayer():Team() != 1 and LocalPlayer():Team() != 2 ) then

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
		
			LocalPlayer().team = teamnum
			Loadout()
			teama:Remove()

		else

			surface.PlaySound( "buttons/button10.wav" )

		end
	
	end
		
	local ScorePing = SideInfo:Add( "DLabel" )
	ScorePing:Dock( RIGHT )
	ScorePing:SetWide( 64 )
	ScorePing:SetFont( "HUD" )
	ScorePing:SetText( "Ping" )
	ScorePing:SetContentAlignment( 4 )
		
	local ScoreDeath = SideInfo:Add( "DLabel" )
	ScoreDeath:Dock( RIGHT )
	ScoreDeath:SetWide( 64 )
	ScoreDeath:SetFont( "HUD" )
	ScoreDeath:SetText( "D" )
	ScoreDeath:SetContentAlignment( 4 )
		
	local ScoreKill = SideInfo:Add( "DLabel" )
	ScoreKill:Dock( RIGHT )
	ScoreKill:SetWide( 64 )
	ScoreKill:SetFont( "HUD" )
	ScoreKill:SetText( "K" )
	ScoreKill:SetContentAlignment( 4 )

	for k, v in pairs( players ) do

		local PlayerInfo = panel:Add( "DPanel" )
		PlayerInfo:Dock( TOP )
		PlayerInfo:SetTall( 64 )
		PlayerInfo.Paint = function( panel, w, h )
			
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 0, w, h )
				
		end
			
		local PlayerAvatar = PlayerInfo:Add( "AvatarImage" )
		PlayerAvatar:Dock( LEFT )
		PlayerAvatar:SetWide( PlayerInfo:GetTall( ) )
		PlayerAvatar:SetPlayer( v, 64 )
			
		local PAvatarButton = PlayerAvatar:Add( "DButton" )
		PAvatarButton:Dock( FILL )
		PAvatarButton:SetText( "" )
		PAvatarButton.Paint = function( )
			
		end
			
		PAvatarButton.DoClick = function( )

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
			
			v:ShowProfile( )
				
		end
			
		local PlayerName = PlayerInfo:Add( "DPanel" )
		PlayerName:Dock( LEFT )
		
		PlayerName:SetWide( ScrW( ) * 0.25 )

		local icon = v:GetRankMaterial()

		PlayerName.Paint = function( panel, w, h )

			if IsValid( v ) then
		
				surface.SetMaterial( icon )
				surface.DrawTexturedRect( 0, 0, 64, 64 )

				draw.SimpleText( v:Name() or "Disconnected", "RobotoSmall", 64, h * 0.5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
				draw.SimpleText( v:GetUserGroup(), "RobotoSmaller", 64, h * 0.8, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			else

				draw.SimpleText( "Disconnected", "RobotoSmall", 0, h * 0.4, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

			end

		end
			
		local PingIcon = PlayerInfo:Add( "DPanel" )
		PingIcon:Dock( RIGHT )
		PingIcon:SetWide( 64 )
		PingIcon:SetTooltip( v:Ping( ) )
		PingIcon.Paint = function( panel, w, h )
			
			local basepos = 4
			local basewidth = 4
			local baseheight = h - 24
			local ping = 0

			if IsValid( v ) and v:IsPlayer() then

				ping = v:Ping( )

			end
				
			if ping <= 50 then
				
				surface.SetDrawColor( 0, 255, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			elseif ping <= 150 and ping > 50 then
				
				surface.SetDrawColor( 105, 205, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
					
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
				
			elseif ping <= 250 and ping > 150 then
				
				surface.SetDrawColor( 155, 155, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			else
				
				surface.SetDrawColor( 255, 0, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			end
				
		end
			
		local Deaths = PlayerInfo:Add( "DLabel" )
		Deaths:Dock( RIGHT )
		Deaths:SetWide( 64 )
		Deaths:SetFont( "HUD" )
		Deaths:SetText( v:Deaths( ) )
			
		local Kills = PlayerInfo:Add( "DLabel" )
		Kills:Dock( RIGHT )
		Kills:SetWide( 64 )
		Kills:SetFont( "HUD" )
		Kills:SetText( v:Frags( ) )

	end

end

local function TeamChangeMenu()

	local margin = ScrW() * 0.05

	if teama then

		teama:Remove()

	end

	teama = vgui.Create( "DFrame" )
	teama:Dock( FILL )
	teama:MakePopup()
	teama:Center()
	teama.Paint = function( panel, w, h )
	
		Derma_DrawBackgroundBlur( panel, time )

	end

	local scorebit = teama:Add( "DPanel" )
	scorebit:Dock( TOP )
	scorebit:SetTall( ScrH() * 0.8 )
	scorebit:DockMargin( 8, ScrH() * 0.1, 8, ScrH() * 0.1 )
	scorebit.Paint = function( panel, w, h ) end

	local scoreinfo1 = scorebit:Add( "DPanel" )
	scoreinfo1:Dock( TOP )
	scoreinfo1:SetTall( scorebit:GetTall() / 2 )
	scoreinfo1.Paint = function( panel, w, h ) end

	local team1 = scoreinfo1:Add( "DScrollPanel" )
	team1:Dock( FILL )
	team1.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 225 )
		surface.DrawRect( 0, 0, w, h )

	end

	local scoreinfo2 = scorebit:Add( "DPanel" )
	scoreinfo2:Dock( TOP )
	scoreinfo2:SetTall( scorebit:GetTall() / 2 )
	scoreinfo2.Paint = function( panel, w, h ) end

	local team2 = scoreinfo2:Add( "DScrollPanel" )
	team2:Dock( FILL )
	team2.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 225 )
		surface.DrawRect( 0, 0, w, h )

	end

	populatelist( team1, 1 )
	populatelist( team2, 2 )

	teama.lastupdate = 0
	teama.lastcount = #player.GetAll()

	teama.Think = function( self )

		if( self.lastupdate < CurTime() ) then

			self.lastupdate = CurTime() + 1

			if( self.lastcount != #player.GetAll() ) then

				team1:Clear( true )
				team2:Clear( true )

				populatelist( team1, 1 )
				populatelist( team2, 2 )

			end

			self.lastcount = #player.GetAll()

		end

	end
	
end

-- Build the shop panel
-- BuildShop( Parent Panel [panel], Category [string], IsBox [boolean] )
local function BuildShop( panel, cat, box, skins )

	net.Receive( "PurchaseComplete", function( )

		local weapon = net.ReadString()
		BuildShop( panel, cat, box or false, skins or false )

		if box then

			LocalPlayer():ChatPrint( "You just purchased a ".. weapon )

		else

			LocalPlayer():ChatPrint( "You just purchased a ".. weapons.Get( weapon ).PrintName )

		end

	end )

	net.Receive( "OpenComplete", function( )

		local weapon = net.ReadString()
		surface.SetFont( "RobotoSmall" )
		local th3 = select( 2, surface.GetTextSize( "A" ) )

		if string.find( string.lower( weapon ), "starter" ) then

			if shopback then

				shopback:Remove()

			end

			if shopinfo then

				shopinfo:Remove()

			end

			if finbutton then

				finbutton:Remove()

			end

			if openbutton then

				openbutton:Remove()

			end

			-- If this was made as part of the virgin screen, and it's stage two, then give them that morale boost!
			if welcomescreen and tuttext then

				tuttext:SetTall( th3 * 5 )
				tuttext.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 0, 1, w, h - 2 )

					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )

					draw.SimpleText( "Now you've got your starter crate, it's time to fight!", "RobotoSmall", w / 2, h * 0.25, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( "You can access the weapon shop at any time with F3, and the loadout screen with F4.", "RobotoSmall", w / 2, h * 0.75, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					draw.SimpleText( "To hop into the loadout screen, push the button below", "RobotoSmall", w / 2, h * 0.5, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				end

				local finaltext = welcomescreen:Add( "DPanel" )
				finaltext:Dock( TOP )
				finaltext:DockMargin( ScrW() * 0.1, 8, ScrW() * 0.1, 0 )
				finaltext:SetTall( th3 * 3 )
				finaltext.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 0, 1, w, h - 2 )

					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )

					draw.SimpleText( "When you're ready, just join a team and you'll be sent to the loadout screen.", "RobotoSmall", w / 2, h * 0.5, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

				end

				local readybutton = welcomescreen:Add( "DButton" )
				readybutton:Dock( BOTTOM )
				readybutton:DockMargin( ScrW() * 0.1, 8, ScrW() * 0.1, 0 )
				readybutton:SetTall( th3 * 4 )
				readybutton:SetText( "I'm ready to fight!" )
				readybutton:SetFont( "Roboto" )
				readybutton:SetTextColor( Color( 205, 205, 205 ) )
				readybutton.Paint = function( panel, w, h )

					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 1 )
						
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )

				end

				readybutton.DoClick = function( )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

					if welcomescreen then

						welcomescreen:Remove()

					end

					TeamChangeMenu()

				end

			end

		else

			-- Otherwise, just build the shop without any starter crap.
			BuildShop( panel, cat, box or false )

		end

	end )

	surface.SetFont( "Roboto" )
	local th = select( 2, surface.GetTextSize( "$" ) )

	surface.SetFont( "HUD" )
	local th2 = select( 2, surface.GetTextSize( "A" ) )

	if !shopback then

		-- If it's the first opening, play a cute noise. ^w^
		surface.PlaySound( "uprising/frontend/UI_Battledash_Open_Wave 0 0 0.wav")

	end

	if shopback then

		shopback:Remove()

	end

	if shopinfo then

		shopinfo:Remove()

	end

	if finbutton then

		finbutton:Remove()

	end

	if openbutton then

		openbutton:Remove()

	end

	local selectedweapon

	-- If we're buying boxes, then build the box menu
	if box and cat != "starter" then

		-- Add an info panel like on the loadout
		-- It goes as follows:
		-- NAME
		-- MONEY
		-- ITEM
		-- THIS BOX CONTAINS:
		-- ITEMS

		shopinfo = panel:Add( "DPanel" )
		shopinfo:Dock( RIGHT )
		shopinfo:SetWide( ScrW() * 0.25 - 16 )
		shopinfo:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
		shopinfo.Paint = function( panel, w, h )

			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 1, w, h - 2 )
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )

		end

		local playername = shopinfo:Add( "DPanel" )
		playername:Dock( TOP )
		playername:SetTall( th2 * 2 )
		playername.Paint = function( panel, w, h )
							
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
							
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 1, 1, w - 2, h - 2 )
								
			surface.SetDrawColor( 85, 85, 85, 55 )
			surface.SetTexture( gradient )
			surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
		end

		local playerlabel = playername:Add( "DLabel" )
		playerlabel:Dock( TOP )
		playerlabel:SetTall( th2 )
		playerlabel:SetFont( "HUD" )
		playerlabel:SetContentAlignment( 5 )
		playerlabel:SetText( LocalPlayer():Name() )

		local moneylabel = playername:Add( "DLabel" )
		moneylabel:Dock( TOP )
		moneylabel:SetTall( th2 )
		moneylabel:SetFont( "RobotoSmaller" )
		moneylabel:SetContentAlignment( 5 )
		moneylabel:SetText( "Current Funds: $".. LocalPlayer():GetMoney() )

		local itemname = shopinfo:Add( "DLabel" )
		itemname:Dock( TOP )
		itemname:SetTall( th2 * 2 )
		itemname:SetFont( "HUD2" )
		itemname:SetContentAlignment( 5 )
		itemname:SetText( "" )

		local itemdesc = shopinfo:Add( "DLabel" )
		itemdesc:Dock( TOP )
		itemdesc:SetTall( th2 * 2 )
		itemdesc:SetFont( "HUD" )
		itemdesc:SetContentAlignment( 5 )
		itemdesc:SetText( "" )

		shopback = panel:Add( "DScrollPanel" )
		shopback:Dock( LEFT )
		shopback:SetWide( ScrW() * 0.75 - 16 )
		shopback:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
		shopback.Paint = function( panel, w, h )

			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 1, w, h - 2 )
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )

		end

		local shopgrid = shopback:Add( "DIconLayout" )
		shopgrid:Dock( FILL )
		shopgrid:DockMargin( 1, 1, 1, 1 )

		-- If the player's picked a category, then iterate through the boxtable and add them one-by-one.
		if cat then

			for k, v in pairs( BoxTable[ cat ] ) do

				local wepgrid = shopgrid:Add( "DPanel" )
				wepgrid:SetWide( shopback:GetWide() / 3 )
				wepgrid:SetTall( panel:GetTall() / 5 )
				wepgrid.Paint = function( panel, w, h )

				end

				local weapon = wepgrid:Add( "DPanel" )
				weapon:Dock( TOP )
				weapon:SetTall( wepgrid:GetTall() )
				weapon.owned = false
				weapon.price = nil
				weapon.ownedamount = 0

				for k2, v2 in pairs( ownedboxes ) do

					if v.name == k2 then

						weapon.owned = true
						weapon.ownedamount = v2

					end

				end

				if v.price then

					weapon.price = v.price

				end

				weapon.PaintOver = function( panel, w, h )
							
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h - 1 )

				end

				weapon.Paint = function( panel, w, h )

					if selectedweapon == panel then

						surface.SetDrawColor( 55, 75, 65, 55 )

					else

						surface.SetDrawColor( 55, 55, 65, 55 )

					end

					surface.DrawRect( 0, 0, w, h )

					if panel.price then

						draw.SimpleText( "$".. panel.price, "Roboto", 4, h - th * 0.5, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

					end

					draw.SimpleText( panel.ownedamount.. " Owned", "RobotoSmall", w - 4, h - th * 0.5, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
						
				end
							
				local wname = weapon:Add( "DPanel" )
				wname:Dock( TOP )
				wname:SetTall( weapon:GetTall( ) * 0.2 )
				wname.Paint = function( panel, w, h )
							
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )
							
					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 2 )
								
					surface.SetDrawColor( 85, 85, 85, 55 )
					surface.SetTexture( gradient )
					surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
				end
							
				local wnametext = wname:Add( "DLabel" )
				wnametext:Dock( FILL )
				wnametext:SetText( v.name )
				wnametext:SetContentAlignment( 5 )
				wnametext:SetFont( "HUD" )

				local dockw = wepgrid:GetWide() * 0.5 - weapon:GetTall() * 0.4

				local htmlpic = weapon:Add( "DHTML" )
				htmlpic:Dock( FILL )
				htmlpic:DockMargin( dockw, 0, dockw, 0 )
				htmlpic:SetHTML( [[<img src="]].. v.icon .. [[" style="width:]].. weapon:GetTall() * 0.8 ..[[px;height:]].. weapon:GetTall() * 0.8 ..[[px;">]])
				htmlpic:RunJavascript([[document.body.style.overflow = "hidden"]])

				local weaponbutton = htmlpic:Add( "DButton" )
				weaponbutton.OnCursorEntered = function()

					surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

				end
				weaponbutton:Dock( FILL )
				weaponbutton.Paint = function() end
				weaponbutton:SetText( "" )
				weaponbutton.DoClick = function( )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

					selectedweapon = weapon

					if finbutton then

						finbutton:Remove()

					end

					if openbutton then

						openbutton:Remove()

					end

					if LocalPlayer():CanSpend( v.price ) then

						finbutton = shopinfo:Add( "DButton" )
						finbutton:Dock( BOTTOM )
						finbutton:SetTall( 64 )
						finbutton:SetText( "Buy Crate" )
						finbutton:SetFont( "HUD" )
						finbutton:SetTextColor( Color( 205, 205, 205 ) )
						finbutton.Paint = function( panel, w, h )

							surface.SetDrawColor( 35, 35, 35, 155 )
							surface.DrawRect( 1, 1, w - 2, h - 1 )
								
							surface.SetDrawColor( 85, 85, 85, 55 )
							surface.SetTexture( gradient )
							surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h )

						end

						finbutton.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

							net.Start( "RequestPurchase" )

								net.WriteBool( true )
								net.WriteString( v.name )

							net.SendToServer( )

						end

					end

					if weapon.owned and weapon.ownedamount > 0 then

						openbutton = shopinfo:Add( "DButton" )
						openbutton:Dock( BOTTOM )
						openbutton:SetTall( 64 )
						openbutton:SetText( "Open Crate" )
						openbutton:SetFont( "HUD" )
						openbutton:SetTextColor( Color( 205, 205, 205 ) )
						openbutton.Paint = function( panel, w, h )

							surface.SetDrawColor( 35, 35, 35, 155 )
							surface.DrawRect( 1, 1, w - 2, h - 1 )
								
							surface.SetDrawColor( 85, 85, 85, 55 )
							surface.SetTexture( gradient )
							surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h )

						end

						openbutton.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

							net.Start( "RequestOpen" )

								net.WriteString( v.name )
								net.WriteString( cat )

							net.SendToServer( )

						end

					end

					itemname:SetText( v.name )

					if cat == "starter" then

						itemdesc:SetText( "Contains all of the following:" )

					else

						itemdesc:SetText( "Contains one of the following:" )

					end

					local weplist = {}

					for k2, v2 in pairs( v.weps ) do

						if weapons.Get( v2 ) then

							if !table.HasValue( weplist, weapons.Get( v2 ).PrintName ) then

								table.insert( weplist, weapons.Get( v2 ).PrintName )

							end

						end

					end

					if wepslist then

						wepslist:Remove()

					end

					wepslist = shopinfo:Add( "DScrollPanel" )
					wepslist:Dock( TOP )
					wepslist:SetTall( panel:GetTall() * 0.5 )

					for k3, v3 in pairs( weplist ) do

						local name = wepslist:Add( "DLabel" )
						name:Dock( TOP )
						name:SetTall( th2 * 1.5 )
						name:SetFont( "HUD" )
						name:SetText( v3 )
						name:SetContentAlignment( 5 )

					end
							
				end
						
			end

		end

	-- Otherwise, if the category is "starter", then build the special starter box menu.
	-- This argument only applies to the first join, as it'll only build the starter kit menu.
	elseif box and cat == "starter" then

		shopinfo = panel:Add( "DPanel" )
		shopinfo:Dock( RIGHT )
		shopinfo:SetWide( ScrW() * 0.25 - 16 )
		shopinfo:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
		shopinfo.Paint = function( panel, w, h )

			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 1, w, h - 2 )
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )

		end

		local playername = shopinfo:Add( "DPanel" )
		playername:Dock( TOP )
		playername:SetTall( th2 * 2 )
		playername.Paint = function( panel, w, h )
							
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
							
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 1, 1, w - 2, h - 2 )
								
			surface.SetDrawColor( 85, 85, 85, 55 )
			surface.SetTexture( gradient )
			surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
		end

		local playerlabel = playername:Add( "DLabel" )
		playerlabel:Dock( TOP )
		playerlabel:SetTall( th2 )
		playerlabel:SetFont( "HUD" )
		playerlabel:SetContentAlignment( 5 )
		playerlabel:SetText( LocalPlayer():Name() )

		local moneylabel = playername:Add( "DLabel" )
		moneylabel:Dock( TOP )
		moneylabel:SetTall( th2 )
		moneylabel:SetFont( "RobotoSmaller" )
		moneylabel:SetContentAlignment( 5 )
		moneylabel:SetText( "Current Funds: $".. LocalPlayer():GetMoney() )

		local itemname = shopinfo:Add( "DLabel" )
		itemname:Dock( TOP )
		itemname:SetTall( th2 * 2 )
		itemname:SetFont( "HUD2" )
		itemname:SetContentAlignment( 5 )
		itemname:SetText( "" )

		local itemdesc = shopinfo:Add( "DLabel" )
		itemdesc:Dock( TOP )
		itemdesc:SetTall( th2 * 2 )
		itemdesc:SetFont( "HUD" )
		itemdesc:SetContentAlignment( 5 )
		itemdesc:SetText( "" )

		shopback = panel:Add( "DScrollPanel" )
		shopback:Dock( LEFT )
		shopback:SetWide( ScrW() * 0.75 - 16 )
		shopback:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
		shopback.Paint = function( panel, w, h )

			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 1, w, h - 2 )
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )

		end

		local shopgrid = shopback:Add( "DIconLayout" )
		shopgrid:Dock( FILL )
		shopgrid:DockMargin( 1, 1, 1, 1 )

		if cat and cat == "starter" then

			for k, v in pairs( BoxTable[ cat ] ) do

				local wepgrid = shopgrid:Add( "DPanel" )
				wepgrid:SetWide( shopback:GetWide() / 3 )
				wepgrid:SetTall( panel:GetTall() / 5 )
				wepgrid.Paint = function( panel, w, h )

				end

				local weapon = wepgrid:Add( "DPanel" )
				weapon:Dock( TOP )
				weapon:SetTall( wepgrid:GetTall() )
				weapon.owned = false
				weapon.price = nil
				weapon.ownedamount = 0

				for k2, v2 in pairs( ownedboxes ) do

					if v.name == k2 then

						weapon.owned = true
						weapon.ownedamount = v2

					end

				end

				if v.price then

					weapon.price = v.price

				end

				weapon.PaintOver = function( panel, w, h )
							
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h - 1 )

				end

				weapon.Paint = function( panel, w, h )

					if selectedweapon == panel then

						surface.SetDrawColor( 55, 75, 65, 55 )

					else

						surface.SetDrawColor( 55, 55, 65, 55 )

					end

					surface.DrawRect( 0, 0, w, h )

					if panel.price then

						draw.SimpleText( "$".. panel.price, "Roboto", 4, h - th * 0.5, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

					end

					draw.SimpleText( panel.ownedamount.. " Owned", "RobotoSmall", w - 4, h - th * 0.5, Color( 215, 215, 215 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
						
				end
							
				local wname = weapon:Add( "DPanel" )
				wname:Dock( TOP )
				wname:SetTall( weapon:GetTall( ) * 0.2 )
				wname.Paint = function( panel, w, h )
							
					surface.SetDrawColor( 125, 125, 125, 255 )
					surface.DrawOutlinedRect( 0, 0, w, h )
							
					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 1, 1, w - 2, h - 2 )
								
					surface.SetDrawColor( 85, 85, 85, 55 )
					surface.SetTexture( gradient )
					surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
				end
							
				local wnametext = wname:Add( "DLabel" )
				wnametext:Dock( FILL )
				wnametext:SetText( v.name )
				wnametext:SetContentAlignment( 5 )
				wnametext:SetFont( "HUD" )

				local dockw = wepgrid:GetWide() * 0.5 - weapon:GetTall() * 0.4

				local htmlpic = weapon:Add( "DHTML" )
				htmlpic:Dock( FILL )
				htmlpic:DockMargin( dockw, 0, dockw, 0 )
				htmlpic:SetHTML( [[<img src="]].. v.icon .. [[" style="width:]].. weapon:GetTall() * 0.8 ..[[px;height:]].. weapon:GetTall() * 0.8 ..[[px;">]])
				htmlpic:RunJavascript([[document.body.style.overflow = "hidden"]])

				local weaponbutton = htmlpic:Add( "DButton" )
				weaponbutton.OnCursorEntered = function()

					surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

				end
				weaponbutton:Dock( FILL )
				weaponbutton.Paint = function() end
				weaponbutton:SetText( "" )
				weaponbutton.DoClick = function( )

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

					selectedweapon = weapon

					if finbutton then

						finbutton:Remove()

					end

					if openbutton then

						openbutton:Remove()

					end

					if LocalPlayer():CanSpend( v.price ) then

						finbutton = shopinfo:Add( "DButton" )
						finbutton:Dock( BOTTOM )
						finbutton:SetTall( 64 )
						finbutton:SetText( "Buy Crate" )
						finbutton:SetFont( "HUD" )
						finbutton:SetTextColor( Color( 205, 205, 205 ) )
						finbutton.Paint = function( panel, w, h )

							surface.SetDrawColor( 35, 35, 35, 155 )
							surface.DrawRect( 1, 1, w - 2, h - 1 )
								
							surface.SetDrawColor( 85, 85, 85, 55 )
							surface.SetTexture( gradient )
							surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h )

						end

						finbutton.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

							net.Start( "RequestPurchase" )

								net.WriteBool( true )
								net.WriteString( v.name )

							net.SendToServer( )

						end

					end

					if weapon.owned and weapon.ownedamount > 0 then

						openbutton = shopinfo:Add( "DButton" )
						openbutton:Dock( BOTTOM )
						openbutton:SetTall( 64 )
						openbutton:SetText( "Open Crate" )
						openbutton:SetFont( "HUD" )
						openbutton:SetTextColor( Color( 205, 205, 205 ) )
						openbutton.Paint = function( panel, w, h )

							surface.SetDrawColor( 35, 35, 35, 155 )
							surface.DrawRect( 1, 1, w - 2, h - 1 )
								
							surface.SetDrawColor( 85, 85, 85, 55 )
							surface.SetTexture( gradient )
							surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h )

						end

						openbutton.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

							net.Start( "RequestOpen" )

								net.WriteString( v.name )
								net.WriteString( cat )

							net.SendToServer( )

						end

					end

					itemname:SetText( v.name )

					if cat == "starter" then

						itemdesc:SetText( "Contains all of the following:" )

					else

						itemdesc:SetText( "Contains one of the following:" )

					end

					local weplist = {}

					for k2, v2 in pairs( v.weps ) do

						if weapons.Get( v2 ) then

							if !table.HasValue( weplist, weapons.Get( v2 ).PrintName ) then

								table.insert( weplist, weapons.Get( v2 ).PrintName )

							end

						end

					end

					if wepslist then

						wepslist:Remove()

					end

					wepslist = shopinfo:Add( "DScrollPanel" )
					wepslist:Dock( TOP )
					wepslist:SetTall( panel:GetTall() * 0.5 )

					for k3, v3 in pairs( weplist ) do

						local name = wepslist:Add( "DLabel" )
						name:Dock( TOP )
						name:SetTall( th2 * 1.5 )
						name:SetFont( "HUD" )
						name:SetText( v3 )
						name:SetContentAlignment( 5 )

					end
							
				end

			end

		end

	else

		if !skins then

			shopinfo = panel:Add( "DPanel" )
			shopinfo:Dock( RIGHT )
			shopinfo:SetWide( ScrW() * 0.25 - 16 )
			shopinfo:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
			shopinfo.Paint = function( panel, w, h )

				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )

			end

			local playername = shopinfo:Add( "DPanel" )
			playername:Dock( TOP )
			playername:SetTall( th2 * 2 )
			playername.Paint = function( panel, w, h )
								
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
								
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 2 )
									
				surface.SetDrawColor( 85, 85, 85, 55 )
				surface.SetTexture( gradient )
				surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
									
			end

			local playerlabel = playername:Add( "DLabel" )
			playerlabel:Dock( TOP )
			playerlabel:SetTall( th2 )
			playerlabel:SetFont( "HUD" )
			playerlabel:SetContentAlignment( 5 )
			playerlabel:SetText( LocalPlayer():Name() )

			local moneylabel = playername:Add( "DLabel" )
			moneylabel:Dock( TOP )
			moneylabel:SetTall( th2 )
			moneylabel:SetFont( "RobotoSmaller" )
			moneylabel:SetContentAlignment( 5 )
			moneylabel:SetText( "Current Funds: $".. LocalPlayer():GetMoney() )

			local dmgbox = shopinfo:Add( "DPanel" )
			dmgbox:Dock( TOP )
			dmgbox:DockMargin( 8, 8, 8, 8 )
			dmgbox:SetTall( th2 * 2 )
			dmgbox.Paint = function() end

			local dmg = dmgbox:Add( "DLabel" )
			dmg:Dock( TOP )
			dmg:SetTall( th2 )
			dmg:SetText( "Damage: 0" )
			dmg:SetFont( "HUD" )
			dmg:SetContentAlignment( 5 )
			dmg.int = 0

			local dmgbar = dmgbox:Add( "DPanel" )
			dmgbar:Dock( TOP )
			dmgbar:SetTall( th2 )
			dmgbar.Paint = function( panel, w, h )

				local dmgperc = dmg.int / 100
				dmgbars = Lerp( 2.5 * FrameTime(), dmgbars, dmgperc )
				surface.SetDrawColor( 25, 25, 35, 205 )
				surface.DrawRect( 0, h / 2 - 2, w, 4 )

				surface.SetDrawColor( 225, 225, 235, 105 )
				surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( dmgbars, 0, 1 ), 4 )

				if dmgperc > 1 then

					surface.SetDrawColor( 225, 225, 235, 155 )
					surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( dmgbars - 1, 0, 1 ), 4 )

				end

				dmg:SetText( "Damage: ".. dmg.int )

			end

			local recbox = shopinfo:Add( "DPanel" )
			recbox:Dock( TOP )
			recbox:DockMargin( 8, 8, 8, 8 )
			recbox:SetTall( th2 * 2 )
			recbox.Paint = function() end

			local rec = recbox:Add( "DLabel" )
			rec:Dock( TOP )
			rec:SetTall( th2 )
			rec:SetText( "Recoil: 0" )
			rec:SetFont( "HUD" )
			rec:SetContentAlignment( 5 )
			rec.int = 0

			local recbar = recbox:Add( "DPanel" )
			recbar:Dock( TOP )
			recbar:SetTall( th2 )
			recbar.Paint = function( panel, w, h )

				local recperc = rec.int
				recbars = Lerp( 2.5 * FrameTime(), recbars, recperc )
				surface.SetDrawColor( 25, 25, 35, 205 )
				surface.DrawRect( 0, h / 2 - 2, w, 4 )

				surface.SetDrawColor( 225, 225, 235, 105 )
				surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( recbars, 0, 1 ), 4 )

				if recperc > 1 then

					surface.SetDrawColor( 225, 225, 235, 155 )
					surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( recbars - 1, 0, 1 ), 4 )

				end

				rec:SetText( "Recoil: ".. recperc )

			end

			local rpmbox = shopinfo:Add( "DPanel" )
			rpmbox:Dock( TOP )
			rpmbox:DockMargin( 8, 8, 8, 8 )
			rpmbox:SetTall( th2 * 2 )
			rpmbox.Paint = function() end

			local rpm = rpmbox:Add( "DLabel" )
			rpm:Dock( TOP )
			rpm:SetTall( th2 )
			rpm:SetText( "Fire Delay: 0 Seconds" )
			rpm:SetFont( "HUD" )
			rpm:SetContentAlignment( 5 )
			rpm.int = 0

			local rpmbar = rpmbox:Add( "DPanel" )
			rpmbar:Dock( TOP )
			rpmbar:SetTall( th2 )
			rpmbar.Paint = function( panel, w, h )

				local rpmperc = rpm.int / 500
				rpmbars = Lerp( 2.5 * FrameTime(), rpmbars, rpmperc )
				surface.SetDrawColor( 25, 25, 35, 205 )
				surface.DrawRect( 0, h / 2 - 2, w, 4 )

				surface.SetDrawColor( 225, 225, 235, 105 )
				surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( rpmbars, 0, 1 ), 4 )

				if rpmperc > 1 then

					surface.SetDrawColor( 225, 225, 235, 155 )
					surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( rpmbars - 1, 0, 1 ), 4 )

				end

				rpm:SetText( "Rounds Per Minute: ".. rpm.int )

			end

			local hipbox = shopinfo:Add( "DPanel" )
			hipbox:Dock( TOP )
			hipbox:DockMargin( 8, 8, 8, 8 )
			hipbox:SetTall( th2 * 2 )
			hipbox.Paint = function() end

			local hip = hipbox:Add( "DLabel" )
			hip:Dock( TOP )
			hip:SetTall( th2 )
			hip:SetText( "Hip Accuracy: 0" )
			hip:SetFont( "HUD" )
			hip:SetContentAlignment( 5 )
			hip.int = 0

			local hipbar = hipbox:Add( "DPanel" )
			hipbar:Dock( TOP )
			hipbar:SetTall( th2 )
			hipbar.Paint = function( panel, w, h )

				local hipperc = hip.int / 1
				hipbars = Lerp( 2.5 * FrameTime(), hipbars, hipperc )
				surface.SetDrawColor( 25, 25, 35, 205 )
				surface.DrawRect( 0, h / 2 - 2, w, 4 )

				surface.SetDrawColor( 225, 225, 235, 105 )
				surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( hipbars, 0, 1 ), 4 )

				if hipperc > 1 then

					surface.SetDrawColor( 225, 225, 235, 155 )
					surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( hipbars - 1, 0, 1 ), 4 )

				end

				hip:SetText( "Hip Accuracy: ".. hip.int )

			end

			local adsbox = shopinfo:Add( "DPanel" )
			adsbox:Dock( TOP )
			adsbox:DockMargin( 8, 8, 8, 8 )
			adsbox:SetTall( th2 * 2 )
			adsbox.Paint = function() end

			local ads = adsbox:Add( "DLabel" )
			ads:Dock( TOP )
			ads:SetTall( th2 )
			ads:SetText( "Aim Accuracy: 0" )
			ads:SetFont( "HUD" )
			ads:SetContentAlignment( 5 )
			ads.int = 0

			local adsbar = adsbox:Add( "DPanel" )
			adsbar:Dock( TOP )
			adsbar:SetTall( th2 )
			adsbar.Paint = function( panel, w, h )

				local adsperc = ads.int / 1
				adsbars = Lerp( 2.5 * FrameTime(), adsbars, adsperc )
				surface.SetDrawColor( 25, 25, 35, 205 )
				surface.DrawRect( 0, h / 2 - 2, w, 4 )

				surface.SetDrawColor( 225, 225, 235, 105 )
				surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( adsbars, 0, 1 ), 4 )

				if adsperc > 1 then

					surface.SetDrawColor( 225, 225, 235, 155 )
					surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( adsbars - 1, 0, 1 ), 4 )

				end

				ads:SetText( "Aim Accuracy: ".. ads.int )

			end

			local clipbox = shopinfo:Add( "DPanel" )
			clipbox:Dock( TOP )
			clipbox:DockMargin( 8, 8, 8, 8 )
			clipbox:SetTall( th2 * 2 )
			clipbox.Paint = function() end

			local clip = clipbox:Add( "DLabel" )
			clip:Dock( TOP )
			clip:SetTall( th2 )
			clip:SetText( "Clip Size: 0" )
			clip:SetFont( "HUD" )
			clip:SetContentAlignment( 5 )
			clip.int = 0

			local clipbar = clipbox:Add( "DPanel" )
			clipbar:Dock( TOP )
			clipbar:SetTall( th2 )
			clipbar.Paint = function( panel, w, h )

				local clipperc = clip.int / 100
				clipbars = Lerp( 2.5 * FrameTime(), clipbars, clipperc )
				surface.SetDrawColor( 25, 25, 35, 205 )
				surface.DrawRect( 0, h / 2 - 2, w, 4 )

				surface.SetDrawColor( 225, 225, 235, 105 )
				surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( clipbars, 0, 1 ), 4 )

				if clipperc > 1 then

					surface.SetDrawColor( 225, 225, 235, 155 )
					surface.DrawRect( 0, h / 2 - 2, w * math.Clamp( clipbars - 1, 0, 1 ), 4 )

				end

				clip:SetText( "Clip Size: ".. clip.int )

			end

			shopback = panel:Add( "DScrollPanel" )
			shopback:Dock( LEFT )
			shopback:SetWide( ScrW() * 0.75 - 16 )
			shopback:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
			shopback.Paint = function( panel, w, h )

				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )

			end

			local shopgrid = shopback:Add( "DIconLayout" )
			shopgrid:Dock( FILL )
			shopgrid:DockMargin( 1, 1, 1, 1 )

			-- Iterate through all available weapons and add them to the shop.
			if cat then

				for k, v in pairs( WeaponTable[ cat ] ) do

					local weapontbl = weapons.Get( v.ent )

					if weapontbl and v.show then

						local wepgrid = shopgrid:Add( "DPanel" )
						wepgrid:SetWide( shopback:GetWide() / 3 )
						wepgrid:SetTall( panel:GetTall() / 5 )
						wepgrid.Paint = function( panel, w, h )

						end

						local weapon = wepgrid:Add( "DPanel" )
						weapon:Dock( TOP )
						weapon:SetTall( wepgrid:GetTall() )
						weapon.owned = false
						weapon.price = nil

						for k2, v2 in pairs( ownedweapons ) do

							if v2 == v.ent then

								weapon.owned = true

							end

						end

						if v.price then

							weapon.price = v.price

						end

						weapon.PaintOver = function( panel, w, h )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h - 1 )

						end

						weapon.Paint = function( panel, w, h )

							if selectedweapon == panel then

								surface.SetDrawColor( 55, 75, 65, 55 )

							else

								surface.SetDrawColor( 55, 55, 65, 55 )

							end

							surface.DrawRect( 0, 0, w, h )

							if panel.price then

								draw.SimpleText( "$".. panel.price, "Roboto", 4, h - th / 2, Color( 215, 215, 215 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

							end
							
						end
								
						local wname = weapon:Add( "DPanel" )
						wname:Dock( TOP )
						wname:SetTall( weapon:GetTall( ) * 0.2 )
						wname.Paint = function( panel, w, h )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h )
								
							surface.SetDrawColor( 35, 35, 35, 155 )
							surface.DrawRect( 1, 1, w - 2, h - 2 )
									
							surface.SetDrawColor( 85, 85, 85, 55 )
							surface.SetTexture( gradient )
							surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
									
						end
								
						local wnametext = wname:Add( "DLabel" )
						wnametext:Dock( FILL )
						wnametext:SetText( weapontbl.PrintName )
						wnametext:SetContentAlignment( 5 )
						wnametext:SetFont( "HUD" )
									
						local weaponmodel = weapon:Add( "DModelPanel" )
						weaponmodel.OnCursorEntered = function()

							surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

						end
						weaponmodel:Dock( FILL )
						weaponmodel:DockMargin( 0, 0, 0, 1 )
								
						if weapontbl.WM then
								
							weaponmodel:SetModel( weapontbl.WM )
							primaryweaponpreview = weapontbl.WM
									
						elseif weapontbl.WorldModel then
								
							weaponmodel:SetModel( weapontbl.WorldModel )
							primaryweaponpreview = weapontbl.WorldModel
									
						else
								
							weaponmodel:SetModel( Model( "models/weapons/w_pistol.mdl" ) )
									
						end
					
						if modeloffsets[ v.ent ] then

							local modelpos = weaponmodel.Entity:GetBonePosition( weaponmodel.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
							local lookat = Vector( 3, 0, 4 )
							local campos = Vector( 3, 25, 9 )

							if modeloffsets[ v.ent ] then
								
								weaponmodel:SetLookAt( modelpos - modeloffsets[ v.ent ].lookat )
								weaponmodel:SetCamPos( modelpos - modeloffsets[ v.ent ].campos )

							else

								weaponmodel:SetLookAt( modelpos - lookat )
								weaponmodel:SetCamPos( modelpos - campos )

							end

						else

							local viewmins, viewmaxs = weaponmodel.Entity:GetRenderBounds( )
									
							weaponmodel:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 1, 0 ) )
							weaponmodel:SetLookAt( ( viewmaxs + viewmins ) / 2 )

						end

						weaponmodel.LayoutEntity = function( )
						
						end
								
						weaponmodel.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

							if !weapon.owned then

								selectedweapon = weapon

								if finbutton then

									finbutton:Remove()

								end

								if LocalPlayer():CanSpend( v.price ) then

									finbutton = shopinfo:Add( "DButton" )
									finbutton:Dock( BOTTOM )
									finbutton:SetTall( 64 )
									finbutton:SetText( "Buy Weapon" )
									finbutton:SetFont( "HUD" )
									finbutton:SetTextColor( Color( 205, 205, 205 ) )
									finbutton.Paint = function( panel, w, h )

										surface.SetDrawColor( 35, 35, 35, 155 )
										surface.DrawRect( 1, 1, w - 2, h - 1 )
										
										surface.SetDrawColor( 85, 85, 85, 55 )
										surface.SetTexture( gradient )
										surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
										
										surface.SetDrawColor( 125, 125, 125, 255 )
										surface.DrawOutlinedRect( 0, 0, w, h )

									end

									finbutton.DoClick = function( )

										surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

										-- Request to purchase the weapon.
										net.Start( "RequestPurchase" )

											net.WriteBool( false )
											net.WriteString( v.ent )

										net.SendToServer( )

									end

								end

							end

							if dmg and weapontbl.Damage then

								dmg.int = weapontbl.Damage

							end

							if rec and weapontbl.Recoil then

								rec.int = weapontbl.Recoil

							end

							if rpm and weapontbl.RPM then

								local num = math.Round( weapontbl.RPM, 2 )

								rpm.int = num

							end

							if hip and weapontbl.HipSpread then

								local num = weapontbl.HipSpread

								hip.int = num

							end

							if ads and weapontbl.AimSpread then

								local num = 1 - weapontbl.AimSpread

								ads.int = num

							end

							if clip and weapontbl.Primary.ClipSize then

								clip.int = weapontbl.Primary.ClipSize

							end
									
						end
								
						weaponmodel.Paint = function( self )

							if ( !IsValid( self.Entity ) ) then return end
										
							local x, y = self:LocalToScreen( 0, 0 )
										
							self:LayoutEntity( self.Entity )
									
							local ang = self.aLookAngle
							if ( !ang ) then
								ang = (self.vLookatPos-self.vCamPos):Angle()
							end
										
							local w, h = self:GetSize()
							cam.Start3D( self.vCamPos, ang, self.fFOV, x, y, w, h, 5, self.FarZ )
										
								render.SuppressEngineLighting( true )
								render.SetLightingOrigin( self.Entity:GetPos() )
								render.ResetModelLighting( self.colAmbientLight.r/255, self.colAmbientLight.g/255, self.colAmbientLight.b/255 )
								render.SetColorModulation( self.colColor.r/255, self.colColor.g/255, self.colColor.b/255 )
								render.SetBlend( self.colColor.a/255 )
										
								for i=0, 6 do
									local col = self.DirectionalLight[ i ]
									if ( col ) then
										render.SetModelLighting( i, col.r/255, col.g/255, col.b/255 )
									end
								end
								self:DrawModel()
																
								render.SuppressEngineLighting( false )
							cam.End3D()

							if weapon.owned then

								surface.SetDrawColor( 55, 55, 95, 55 )
								surface.DrawRect( 2, 2, w - 1, h - 1 )

								draw.SimpleText( string.upper( "OWNED" ), "RobotoSmall", w / 2, h / 2, Color( 215, 215, 215 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

							end
									
							self.LastPaint = RealTime()

						end
										
					end

				end

			end

		else

			shopinfo = panel:Add( "DPanel" )
			shopinfo:Dock( RIGHT )
			shopinfo:SetWide( ScrW() * 0.25 - 16 )
			shopinfo:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
			shopinfo.Paint = function( panel, w, h )

				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )

			end

			local playername = shopinfo:Add( "DPanel" )
			playername:Dock( TOP )
			playername:SetTall( th2 * 2 )
			playername.Paint = function( panel, w, h )
								
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )
								
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 1, 1, w - 2, h - 2 )
									
				surface.SetDrawColor( 85, 85, 85, 55 )
				surface.SetTexture( gradient )
				surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
									
			end

			local playerlabel = playername:Add( "DLabel" )
			playerlabel:Dock( TOP )
			playerlabel:SetTall( th2 )
			playerlabel:SetFont( "HUD" )
			playerlabel:SetContentAlignment( 5 )
			playerlabel:SetText( LocalPlayer():Name() )

			local moneylabel = playername:Add( "DLabel" )
			moneylabel:Dock( TOP )
			moneylabel:SetTall( th2 )
			moneylabel:SetFont( "RobotoSmaller" )
			moneylabel:SetContentAlignment( 5 )
			moneylabel:SetText( "Current Funds: $".. LocalPlayer():GetMoney() )

			local weaponname = shopinfo:Add( "DLabel" )
			weaponname:Dock( TOP )
			weaponname:SetTall( th2 * 2 )
			weaponname:SetText( "" )
			weaponname:SetFont( "HUD" )
			weaponname:SetContentAlignment( 5 )

			local buypanel = shopinfo:Add( "DPanel" )
			buypanel:Dock( BOTTOM )
			buypanel:SetTall( 64 )
			buypanel:SetDrawBackground( false )

			local skinlist = shopinfo:Add( "DScrollPanel" )
			skinlist:Dock( FILL )

			shopback = panel:Add( "DScrollPanel" )
			shopback:Dock( LEFT )
			shopback:SetWide( ScrW() * 0.75 - 16 )
			shopback:DockMargin( 8, ScrH() * 0.025, 8, ScrH() * 0.025 )
			shopback.Paint = function( panel, w, h )

				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 125, 125, 125, 255 )
				surface.DrawOutlinedRect( 0, 0, w, h )

			end

			local shopgrid = shopback:Add( "DIconLayout" )
			shopgrid:Dock( FILL )
			shopgrid:DockMargin( 1, 1, 1, 1 )

			-- Iterate through all available weapons and add them to the shop.
			if cat then

				for k, v in pairs( WeaponTable[ cat ] ) do

					local weapontbl = weapons.Get( v.ent )
					local owned = false
					local hascat = false

					for k2, v2 in pairs( ownedweapons ) do

						if v2 == v.ent then

							owned = true
							hascat = true
							continue

						end

					end

					if weapontbl and WeaponSkins[ v.ent ] and owned then

						local wepgrid = shopgrid:Add( "DPanel" )
						wepgrid:SetWide( shopback:GetWide() / 2 - 16 )
						wepgrid:SetTall( panel:GetTall() / 3 )
						wepgrid.Paint = function( panel, w, h )

						end

						local weapon = wepgrid:Add( "DPanel" )
						weapon:Dock( TOP )
						weapon:SetTall( wepgrid:GetTall() )
						weapon.owned = false
						weapon.price = nil

						if v.price then

							weapon.price = v.price

						end

						weapon.PaintOver = function( panel, w, h )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h - 1 )

						end

						weapon.Paint = function( panel, w, h )

							if selectedweapon == panel then

								surface.SetDrawColor( 55, 75, 65, 55 )

							else

								surface.SetDrawColor( 55, 55, 65, 55 )

							end

							surface.DrawRect( 0, 0, w, h )
							
						end
								
						local wname = weapon:Add( "DPanel" )
						wname:Dock( TOP )
						wname:SetTall( weapon:GetTall( ) * 0.2 )
						wname.Paint = function( panel, w, h )
								
							surface.SetDrawColor( 125, 125, 125, 255 )
							surface.DrawOutlinedRect( 0, 0, w, h )
								
							surface.SetDrawColor( 35, 35, 35, 155 )
							surface.DrawRect( 1, 1, w - 2, h - 2 )
									
							surface.SetDrawColor( 85, 85, 85, 55 )
							surface.SetTexture( gradient )
							surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
									
						end
								
						local wnametext = wname:Add( "DLabel" )
						wnametext:Dock( FILL )
						wnametext:SetText( weapontbl.PrintName )
						wnametext:SetContentAlignment( 5 )
						wnametext:SetFont( "HUD" )
									
						local weaponmodel = weapon:Add( "VModelPanel" )
						weaponmodel.OnCursorEntered = function()

							surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )

						end
						weaponmodel:Dock( FILL )
						weaponmodel:DockMargin( 0, 0, 0, 1 )
								
						weaponmodel.LayoutEntity = function( )
						
						end
						weaponmodel:SetWeapon( v.ent )
						
						local modelpos

						if vmodeloffsets[ v.ent ] then

							modelpos = weaponmodel.Entity:GetBonePosition( weaponmodel.Entity:LookupBone( vmodeloffsets[ v.ent ].bone ) )

						else

							modelpos = weaponmodel.Entity:GetBonePosition( 0 )

						end

						local lookat = Vector( -1.5, 0, 3 )
						local campos = Vector( 0, 70, 8 )

						if modeloffsets[ v.ent ] then
							
							weaponmodel:SetLookAt( modelpos + vmodeloffsets[ v.ent ].lookat )
							weaponmodel:SetCamPos( modelpos + vmodeloffsets[ v.ent ].campos )

						else

							weaponmodel:SetLookAt( modelpos - lookat )
							weaponmodel:SetCamPos( modelpos - campos )

						end

						weaponmodel.DoClick = function( )

							surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

							selectedweapon = weapon

							weaponname:SetText( weapontbl.PrintName )

							local selectedskin = nil

							if finbutton then

								finbutton:Remove()

							end

							skinlist:Clear()

							for i, j in pairs( WeaponSkins ) do

								if i == v.ent then

									for id, details in pairs( j ) do

										local owned = false

										if ownedskins and ownedskins[ i ] then

											if ownedskins[ i ][ id ] then

												owned = true

											end

										end

										local skin = skinlist:Add( "DButton" )
										skin:Dock( TOP )
										skin:SetTall( 64 )
										skin.id = id
										skin.Paint = function( panel, w, h )

											local preview = Material( details.texture.. "_icon.png" )
											surface.SetDrawColor( 255, 255, 255, 255 )
											surface.SetMaterial( preview )
											surface.DrawTexturedRect( 2, 0, 64, 64 )

											if owned then

												draw.SimpleText( id .." - OWNED", "HUD", 72, h / 2, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

											else

												draw.SimpleText( id .." - $".. details.price, "HUD", 72, h / 2, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

											end

											if selectedskin == panel then

												surface.DrawOutlinedRect( 0, 0, w, h )

											end

										end
										skin:SetText( "" )
										skin.DoClick = function( panel, w, h )

											selectedskin = panel
											weaponmodel:ApplyWepSkin( details.texture, details.submaterials )

											if LocalPlayer():CanSpend( details.price ) and !owned then

												if finbutton then

													finbutton:Remove()

												end

												finbutton = buypanel:Add( "DButton" )
												finbutton:Dock( FILL )
												finbutton:SetText( "Buy Skin" )
												finbutton:SetFont( "HUD" )
												finbutton:SetTextColor( Color( 205, 205, 205 ) )
												finbutton.Paint = function( panel, w, h )

													surface.SetDrawColor( 35, 35, 35, 155 )
													surface.DrawRect( 1, 1, w - 2, h - 1 )
														
													surface.SetDrawColor( 85, 85, 85, 55 )
													surface.SetTexture( gradient )
													surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
														
													surface.SetDrawColor( 125, 125, 125, 255 )
													surface.DrawOutlinedRect( 0, 0, w, h )

												end

												finbutton.DoClick = function( )

													surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

													-- Request to purchase the skin.
													net.Start( "RequestPurchaseSkin" )

														net.WriteString( i )
														net.WriteString( skin.id )

													net.SendToServer( )

												end

											end

										end

									end

								end

							end

							--[[if LocalPlayer():CanSpend( v.price ) then

								finbutton = shopinfo:Add( "DButton" )
								finbutton:Dock( BOTTOM )
								finbutton:SetTall( 64 )
								finbutton:SetText( "Buy Weapon" )
								finbutton:SetFont( "HUD" )
								finbutton:SetTextColor( Color( 205, 205, 205 ) )
								finbutton.Paint = function( panel, w, h )

									surface.SetDrawColor( 35, 35, 35, 155 )
									surface.DrawRect( 1, 1, w - 2, h - 1 )
										
									surface.SetDrawColor( 85, 85, 85, 55 )
									surface.SetTexture( gradient )
									surface.DrawTexturedRect( 1, 1, w - 2, h - 2 )
										
									surface.SetDrawColor( 125, 125, 125, 255 )
									surface.DrawOutlinedRect( 0, 0, w, h )

								end

								finbutton.DoClick = function( )

									surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

									-- Request to purchase the weapon.
									net.Start( "RequestPurchase" )

										net.WriteBool( false )
										net.WriteString( v.ent )

									net.SendToServer( )

								end

							end]]
									
						end
								
						weaponmodel.Paint = function( self )

							if ( !IsValid( self.Entity ) ) then return end
										
							local x, y = self:LocalToScreen( 0, 0 )
										
							self:LayoutEntity( self.Entity )
									
							local ang = self.aLookAngle
							if ( !ang ) then
								ang = (self.vLookatPos-self.vCamPos):Angle()
							end
										
							local w, h = self:GetSize()
							cam.Start3D( self.vCamPos, ang, self.fFOV, x, y, w, h, 5, self.FarZ )
										
								render.SuppressEngineLighting( true )
								render.SetLightingOrigin( self.Entity:GetPos() )
								render.ResetModelLighting( self.colAmbientLight.r/255, self.colAmbientLight.g/255, self.colAmbientLight.b/255 )
								render.SetColorModulation( self.colColor.r/255, self.colColor.g/255, self.colColor.b/255 )
								render.SetBlend( self.colColor.a/255 )
										
								for i=0, 6 do
									local col = self.DirectionalLight[ i ]
									if ( col ) then
										render.SetModelLighting( i, col.r/255, col.g/255, col.b/255 )
									end
								end
								self:DrawModel()
																
								render.SuppressEngineLighting( false )
							cam.End3D()
									
							self.LastPaint = RealTime()
											
						end

					end

				end

			end

		end

	end

end

-- just kinda threw this together for the f1 menu.
function makeShopBars()
		local classmenu

	local function BuildWepTabs( panel )

		if classmenu then

			classmenu:Remove()

		end

		if shopback then

			shopback:Remove()

		end

		if shopinfo then

			shopinfo:Remove()

		end

		if finbutton then

			finbutton:Remove()

		end

		if openbutton then

			openbutton:Remove()

		end
			
		classmenu = panel:Add( "DPanel" )
		classmenu:Dock( TOP )
		classmenu:SetTall( ScrH( ) * 0.04 )
		classmenu:DockMargin( ScrW( ) * 0.05, ScrH() * 0.025, ScrW( ) * 0.05, 0 )
		classmenu:DockPadding( 1, 0, 0, 0 )
		classmenu:SetDrawBackground( false )
		classmenu.PaintOver = function( panel, w, h )
		
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
			
		end

		local count = table.Count( WeaponTable )
		local selected = nil

		for k, v in SortedPairs( WeaponTable ) do

			local cat = classmenu:Add( "DButton" )
			cat:Dock( LEFT )
			cat:SetWide( ScrW() * 0.9 / count )
			cat:DockMargin( 0, 0, 0, 0 )
			cat:SetFont( "HUD" )
			cat:SetTextColor( Color( 205, 205, 205 ) )
			cat:SetContentAlignment( 5 )
			cat:SetText( string.upper( k ) )
			cat.alpha = 155
			cat.OnCursorEntered = function( )
			
				surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
				cat.alpha = 255
				
			end
			cat.OnCursorExited = function( )
			
				cat.alpha = 155
				
			end
			cat.DoClick = function( self )

				selected = cat

				surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

				BuildShop( panel, k )

			end
			
			local smooth = 155
			
			cat.Paint = function( panel, w, h )
			
				smooth = math.Approach( smooth, cat.alpha, FrameTime( ) * 120 )
			
				surface.SetDrawColor( 85, 85, 95, smooth )

				surface.SetTexture( gradient )
				surface.DrawTexturedRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				
			end

		end

	end

	local function BuildSkinTabs( panel )

		if classmenu then

			classmenu:Remove()

		end

		if shopback then

			shopback:Remove()

		end

		if shopinfo then

			shopinfo:Remove()

		end

		if finbutton then

			finbutton:Remove()

		end

		if openbutton then

			openbutton:Remove()

		end
			
		classmenu = panel:Add( "DPanel" )
		classmenu:Dock( TOP )
		classmenu:SetTall( ScrH( ) * 0.04 )
		classmenu:DockMargin( ScrW( ) * 0.05, ScrH() * 0.025, ScrW( ) * 0.05, 0 )
		classmenu:DockPadding( 1, 0, 0, 0 )
		classmenu:SetDrawBackground( false )
		classmenu.PaintOver = function( panel, w, h )
		
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
			
		end

		local count = table.Count( WeaponTable ) - 2
		local selected = nil

		for k, v in SortedPairs( WeaponTable ) do

			local hascat = false

			for k2, v2 in pairs( v ) do

				PrintTable( v2 )

				for k3, v3 in pairs( WeaponSkins ) do

					if table.HasValue( v2, k3 ) then

						hascat = true
						continue

					end

				end

			end

			if hascat then

				local cat = classmenu:Add( "DButton" )
				cat:Dock( LEFT )
				cat:SetWide( ScrW() * 0.9 / count )
				cat:DockMargin( 0, 0, 0, 0 )
				cat:SetFont( "HUD" )
				cat:SetTextColor( Color( 205, 205, 205 ) )
				cat:SetContentAlignment( 5 )
				cat:SetText( string.upper( k ) )
				cat.alpha = 155
				cat.OnCursorEntered = function( )
				
					surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
					cat.alpha = 255
					
				end
				cat.OnCursorExited = function( )
				
					cat.alpha = 155
					
				end
				cat.DoClick = function( self )

					selected = cat

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

					BuildShop( panel, k, false, true )

				end
				
				local smooth = 155
				
				cat.Paint = function( panel, w, h )
				
					smooth = math.Approach( smooth, cat.alpha, FrameTime( ) * 120 )
				
					surface.SetDrawColor( 85, 85, 95, smooth )

					surface.SetTexture( gradient )
					surface.DrawTexturedRect( 0, 1, w, h - 2 )
					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 0, 1, w, h - 2 )
					
				end

			end

		end

	end

	local function BuildBoxTabs( panel )

		if classmenu then

			classmenu:Remove()

		end

		if shopback then

			shopback:Remove()

		end

		if shopinfo then

			shopinfo:Remove()

		end

		if finbutton then

			finbutton:Remove()

		end

		if openbutton then

			openbutton:Remove()

		end
			
		classmenu = panel:Add( "DPanel" )
		classmenu:Dock( TOP )
		classmenu:SetTall( ScrH( ) * 0.04 )
		classmenu:DockMargin( ScrW( ) * 0.05, ScrH() * 0.025, ScrW( ) * 0.05, 0 )
		classmenu:DockPadding( 1, 0, 0, 0 )
		classmenu:SetDrawBackground( false )
		classmenu.PaintOver = function( panel, w, h )
		
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
			
		end

		local count = table.Count( BoxTable )
		local selected = nil

		for k, v in SortedPairs( BoxTable ) do

			local cat = classmenu:Add( "DButton" )
			cat:Dock( LEFT )
			cat:SetWide( ScrW() * 0.9 / count )
			cat:DockMargin( 0, 0, 0, 0 )
			cat:SetFont( "HUD" )
			cat:SetTextColor( Color( 205, 205, 205 ) )
			cat:SetContentAlignment( 5 )
			cat:SetText( string.upper( k ) )
			cat.alpha = 155
			cat.OnCursorEntered = function( )
			
				surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
				cat.alpha = 255
				
			end
			cat.OnCursorExited = function( )
			
				cat.alpha = 155
				
			end
			cat.DoClick = function( self )

				selected = cat

				surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

				BuildShop( panel, k, true )

			end
			
			local smooth = 155
			
			cat.Paint = function( panel, w, h )
			
				smooth = math.Approach( smooth, cat.alpha, FrameTime( ) * 120 )
			
				surface.SetDrawColor( 85, 85, 95, smooth )

				surface.SetTexture( gradient )
				surface.DrawTexturedRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				
			end

		end

	end
	
	local panel = vgui.Create( "DFrame" )
	panel:SetSize( ScrW( ), ScrH( ) )
	panel:Center( )
	panel:MakePopup( )
	panel:SetTitle( "" )
	panel:SetDraggable( false )
	panel.Paint = function( panel, w, h )
	
		Derma_DrawBackgroundBlur( panel, time )
		
	end

	local tabselection = panel:Add( "DPanel" )
	tabselection:Dock( TOP )
	tabselection:SetTall( ScrH() * 0.04 )
	tabselection:DockMargin( ScrW() * 0.125, 0, ScrW() * 0.125, 0 )
	tabselection:DockPadding( 1, 0, 0, 0 )
	tabselection:SetDrawBackground( false )
	tabselection.PaintOver = function( panel, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local weptab = tabselection:Add( "DButton" )
	weptab:Dock( LEFT )
	weptab:SetWide( ScrW() * 0.25 )
	weptab:DockMargin( 0, 0, 0, 0 )
	weptab:SetFont( "HUD" )
	weptab:SetTextColor( Color( 205, 205, 205 ) )
	weptab:SetContentAlignment( 5 )
	weptab:SetText( "Weapons" )
	weptab.alpha = 155
	weptab.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
		weptab.alpha = 255
			
	end
	weptab.OnCursorExited = function( )
	
		weptab.alpha = 155
			
	end
	weptab.DoClick = function( self )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

		BuildWepTabs( panel )

	end
		
	local smooth = 155
		
	weptab.Paint = function( panel, w, h )
		
		smooth = math.Approach( smooth, weptab.alpha, FrameTime( ) * 120 )
		
		surface.SetDrawColor( 85, 85, 95, smooth )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
			
	end

	local skintab = tabselection:Add( "DButton" )
	skintab:Dock( LEFT )
	skintab:SetWide( ScrW() * 0.25 )
	skintab:DockMargin( 0, 0, 0, 0 )
	skintab:SetFont( "HUD" )
	skintab:SetTextColor( Color( 205, 205, 205 ) )
	skintab:SetContentAlignment( 5 )
	skintab:SetText( "Skins" )
	skintab.alpha = 155
	skintab.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
		skintab.alpha = 255
			
	end
	skintab.OnCursorExited = function( )
	
		skintab.alpha = 155
			
	end
	skintab.DoClick = function( self )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

		BuildSkinTabs( panel )

	end
		
	local smooth = 155
		
	skintab.Paint = function( panel, w, h )
		
		smooth = math.Approach( smooth, skintab.alpha, FrameTime( ) * 120 )
		
		surface.SetDrawColor( 85, 85, 95, smooth )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
			
	end

	local boxtab = tabselection:Add( "DButton" )
	boxtab:Dock( LEFT )
	boxtab:SetWide( ScrW() * 0.25 )
	boxtab:DockMargin( 0, 0, 0, 0 )
	boxtab:SetFont( "HUD" )
	boxtab:SetTextColor( Color( 205, 205, 205 ) )
	boxtab:SetContentAlignment( 5 )
	boxtab:SetText( "Random Boxes" )
	boxtab.alpha = 155
	boxtab.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
		boxtab.alpha = 255
			
	end
	boxtab.OnCursorExited = function( )
	
		boxtab.alpha = 155
			
	end
	boxtab.DoClick = function( self )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

		BuildBoxTabs( panel )

	end
		
	local smooth = 155
		
	boxtab.Paint = function( panel, w, h )
		
		smooth = math.Approach( smooth, boxtab.alpha, FrameTime( ) * 120 )
		
		surface.SetDrawColor( 85, 85, 95, smooth )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
			
	end
end

net.Receive( "SettingsMenu", function( )

	local classmenu

	local function BuildWepTabs( panel )

		if classmenu then

			classmenu:Remove()

		end

		if shopback then

			shopback:Remove()

		end

		if shopinfo then

			shopinfo:Remove()

		end

		if finbutton then

			finbutton:Remove()

		end

		if openbutton then

			openbutton:Remove()

		end
			
		classmenu = panel:Add( "DPanel" )
		classmenu:Dock( TOP )
		classmenu:SetTall( ScrH( ) * 0.04 )
		classmenu:DockMargin( ScrW( ) * 0.05, ScrH() * 0.025, ScrW( ) * 0.05, 0 )
		classmenu:DockPadding( 1, 0, 0, 0 )
		classmenu:SetDrawBackground( false )
		classmenu.PaintOver = function( panel, w, h )
		
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
			
		end

		local count = table.Count( WeaponTable )
		local selected = nil

		for k, v in SortedPairs( WeaponTable ) do

			local cat = classmenu:Add( "DButton" )
			cat:Dock( LEFT )
			cat:SetWide( ScrW() * 0.9 / count )
			cat:DockMargin( 0, 0, 0, 0 )
			cat:SetFont( "HUD" )
			cat:SetTextColor( Color( 205, 205, 205 ) )
			cat:SetContentAlignment( 5 )
			cat:SetText( string.upper( k ) )
			cat.alpha = 155
			cat.OnCursorEntered = function( )
			
				surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
				cat.alpha = 255
				
			end
			cat.OnCursorExited = function( )
			
				cat.alpha = 155
				
			end
			cat.DoClick = function( self )

				selected = cat

				surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

				BuildShop( panel, k )

			end
			
			local smooth = 155
			
			cat.Paint = function( panel, w, h )
			
				smooth = math.Approach( smooth, cat.alpha, FrameTime( ) * 120 )
			
				surface.SetDrawColor( 85, 85, 95, smooth )

				surface.SetTexture( gradient )
				surface.DrawTexturedRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				
			end

		end

	end

	local function BuildSkinTabs( panel )

		if classmenu then

			classmenu:Remove()

		end

		if shopback then

			shopback:Remove()

		end

		if shopinfo then

			shopinfo:Remove()

		end

		if finbutton then

			finbutton:Remove()

		end

		if openbutton then

			openbutton:Remove()

		end
			
		classmenu = panel:Add( "DPanel" )
		classmenu:Dock( TOP )
		classmenu:SetTall( ScrH( ) * 0.04 )
		classmenu:DockMargin( ScrW( ) * 0.05, ScrH() * 0.025, ScrW( ) * 0.05, 0 )
		classmenu:DockPadding( 1, 0, 0, 0 )
		classmenu:SetDrawBackground( false )
		classmenu.PaintOver = function( panel, w, h )
		
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
			
		end

		local count = table.Count( WeaponTable ) - 2
		local selected = nil

		for k, v in SortedPairs( WeaponTable ) do

			local hascat = false

			for k2, v2 in pairs( v ) do

				PrintTable( v2 )

				for k3, v3 in pairs( WeaponSkins ) do

					if table.HasValue( v2, k3 ) then

						hascat = true
						continue

					end

				end

			end

			if hascat then

				local cat = classmenu:Add( "DButton" )
				cat:Dock( LEFT )
				cat:SetWide( ScrW() * 0.9 / count )
				cat:DockMargin( 0, 0, 0, 0 )
				cat:SetFont( "HUD" )
				cat:SetTextColor( Color( 205, 205, 205 ) )
				cat:SetContentAlignment( 5 )
				cat:SetText( string.upper( k ) )
				cat.alpha = 155
				cat.OnCursorEntered = function( )
				
					surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
					cat.alpha = 255
					
				end
				cat.OnCursorExited = function( )
				
					cat.alpha = 155
					
				end
				cat.DoClick = function( self )

					selected = cat

					surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

					BuildShop( panel, k, false, true )

				end
				
				local smooth = 155
				
				cat.Paint = function( panel, w, h )
				
					smooth = math.Approach( smooth, cat.alpha, FrameTime( ) * 120 )
				
					surface.SetDrawColor( 85, 85, 95, smooth )

					surface.SetTexture( gradient )
					surface.DrawTexturedRect( 0, 1, w, h - 2 )
					surface.SetDrawColor( 35, 35, 35, 155 )
					surface.DrawRect( 0, 1, w, h - 2 )
					
				end

			end

		end

	end


	local function BuildBoxTabs( panel )

		if classmenu then

			classmenu:Remove()

		end

		if shopback then

			shopback:Remove()

		end

		if shopinfo then

			shopinfo:Remove()

		end

		if finbutton then

			finbutton:Remove()

		end

		if openbutton then

			openbutton:Remove()

		end
			
		classmenu = panel:Add( "DPanel" )
		classmenu:Dock( TOP )
		classmenu:SetTall( ScrH( ) * 0.04 )
		classmenu:DockMargin( ScrW( ) * 0.05, ScrH() * 0.025, ScrW( ) * 0.05, 0 )
		classmenu:DockPadding( 1, 0, 0, 0 )
		classmenu:SetDrawBackground( false )
		classmenu.PaintOver = function( panel, w, h )
		
			surface.SetDrawColor( 125, 125, 125, 255 )
			surface.DrawOutlinedRect( 0, 0, w, h )
			
		end

		local count = table.Count( BoxTable )
		local selected = nil

		for k, v in SortedPairs( BoxTable ) do

			local cat = classmenu:Add( "DButton" )
			cat:Dock( LEFT )
			cat:SetWide( ScrW() * 0.9 / count )
			cat:DockMargin( 0, 0, 0, 0 )
			cat:SetFont( "HUD" )
			cat:SetTextColor( Color( 205, 205, 205 ) )
			cat:SetContentAlignment( 5 )
			cat:SetText( string.upper( k ) )
			cat.alpha = 155
			cat.OnCursorEntered = function( )
			
				surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
				cat.alpha = 255
				
			end
			cat.OnCursorExited = function( )
			
				cat.alpha = 155
				
			end
			cat.DoClick = function( self )

				selected = cat

				surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

				BuildShop( panel, k, true )

			end
			
			local smooth = 155
			
			cat.Paint = function( panel, w, h )
			
				smooth = math.Approach( smooth, cat.alpha, FrameTime( ) * 120 )
			
				surface.SetDrawColor( 85, 85, 95, smooth )

				surface.SetTexture( gradient )
				surface.DrawTexturedRect( 0, 1, w, h - 2 )
				surface.SetDrawColor( 35, 35, 35, 155 )
				surface.DrawRect( 0, 1, w, h - 2 )
				
			end

		end

	end
	
	local panel = vgui.Create( "DFrame" )
	panel:SetSize( ScrW( ), ScrH( ) )
	panel:Center( )
	panel:MakePopup( )
	panel:SetTitle( "" )
	panel:SetDraggable( false )
	panel.Paint = function( panel, w, h )
	
		Derma_DrawBackgroundBlur( panel, time )
		
	end

	local tabselection = panel:Add( "DPanel" )
	tabselection:Dock( TOP )
	tabselection:SetTall( ScrH() * 0.04 )
	tabselection:DockMargin( ScrW() * 0.125, 0, ScrW() * 0.125, 0 )
	tabselection:DockPadding( 1, 0, 0, 0 )
	tabselection:SetDrawBackground( false )
	tabselection.PaintOver = function( panel, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local weptab = tabselection:Add( "DButton" )
	weptab:Dock( LEFT )
	weptab:SetWide( ScrW() * 0.25 )
	weptab:DockMargin( 0, 0, 0, 0 )
	weptab:SetFont( "HUD" )
	weptab:SetTextColor( Color( 205, 205, 205 ) )
	weptab:SetContentAlignment( 5 )
	weptab:SetText( "Weapons" )
	weptab.alpha = 155
	weptab.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
		weptab.alpha = 255
			
	end
	weptab.OnCursorExited = function( )
	
		weptab.alpha = 155
			
	end
	weptab.DoClick = function( self )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

		BuildWepTabs( panel )

	end
		
	local smooth = 155
		
	weptab.Paint = function( panel, w, h )
		
		smooth = math.Approach( smooth, weptab.alpha, FrameTime( ) * 120 )
		
		surface.SetDrawColor( 85, 85, 95, smooth )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
			
	end

	local skintab = tabselection:Add( "DButton" )
	skintab:Dock( LEFT )
	skintab:SetWide( ScrW() * 0.25 )
	skintab:DockMargin( 0, 0, 0, 0 )
	skintab:SetFont( "HUD" )
	skintab:SetTextColor( Color( 205, 205, 205 ) )
	skintab:SetContentAlignment( 5 )
	skintab:SetText( "Skins" )
	skintab.alpha = 155
	skintab.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
		skintab.alpha = 255
			
	end
	skintab.OnCursorExited = function( )
	
		skintab.alpha = 155
			
	end
	skintab.DoClick = function( self )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

		BuildSkinTabs( panel )

	end
		
	local smooth = 155
		
	skintab.Paint = function( panel, w, h )
		
		smooth = math.Approach( smooth, skintab.alpha, FrameTime( ) * 120 )
		
		surface.SetDrawColor( 85, 85, 95, smooth )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
			
	end

	local boxtab = tabselection:Add( "DButton" )
	boxtab:Dock( LEFT )
	boxtab:SetWide( ScrW() * 0.25 )
	boxtab:DockMargin( 0, 0, 0, 0 )
	boxtab:SetFont( "HUD" )
	boxtab:SetTextColor( Color( 205, 205, 205 ) )
	boxtab:SetContentAlignment( 5 )
	boxtab:SetText( "Random Boxes" )
	boxtab.alpha = 155
	boxtab.OnCursorEntered = function( )

		surface.PlaySound( "uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
		
		boxtab.alpha = 255
			
	end
	boxtab.OnCursorExited = function( )
	
		boxtab.alpha = 155
			
	end
	boxtab.DoClick = function( self )

		surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")

		BuildBoxTabs( panel )

	end
		
	local smooth = 155
		
	boxtab.Paint = function( panel, w, h )
		
		smooth = math.Approach( smooth, boxtab.alpha, FrameTime( ) * 120 )
		
		surface.SetDrawColor( 85, 85, 95, smooth )

		surface.SetTexture( gradient )
		surface.DrawTexturedRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
			
	end
	
end )

net.Receive( "RoundEndedClose", function( )

	if winpanel then
	
		winpanel:Remove( )
		
	end
	
end )

local wireframe = Material( "play_assist/pa_lesson" )

function GM:Tick( )

	if IsValid( LocalPlayer( ) ) then
	
		if LocalPlayer( ):Alive( ) and LocalPlayer( ):Team( ) == 1 or LocalPlayer( ):Team( ) == 2 then
	
			WSWITCH:Think()

		end
		
	end
	
end
			

local MatChanged = false
local Mat = Material( "play_assist/pa_ammo_shelf" )
local Mat2 = Material( "play_assist/pa_ammo_shelfover" )
local Mat3 = Material( "play_assist/pa_health02" )
local Mat4 = Material( "play_assist/pa_health01" )
local Mat5 = Material( "play_assist/pa_diamond01" )

function GM:PostDrawTranslucentRenderables()
	self.BaseClass:PostDrawTranslucentRenderables()
	local color = Color( 255, 0, 0, 1 )
	local pos = GetGlobalVector( "CapturePos" )
	local radius = GetGlobalVector( "CaptureRadius" )
	if GetGlobalInt( "PointOwner" ) == LocalPlayer():Team() then
		color = Color( 0, 255, 0, 1 )
	end
	
	if gametype == "conquest" then
	
		render.SetMaterial( wireframe )
		render.DrawSphere( pos, radius, 32, 32, Color( 255, 255, 255, 255 ) )
		
	end
	
	if !MatChanged then
	
		Mat:SetTexture( "$texture2", "overlays/darkedge" )
		Mat2:SetTexture( "$basetexture", "overlays/darkedge" )
		Mat2:SetTexture( "$texture2", "overlays/darkedge" )
		Mat3:SetTexture( "$texture2", "overlays/darkedge" )
		Mat4:SetTexture( "$texture2", "overlays/darkedge" )
		Mat5:SetTexture( "$texture2", "overlays/darkedge" )
		Mat5:SetTexture( "$basetexture", "overlays/darkedge" )
		
		MatChanged = true
		
	end
	
end

-- Let's just format the chat a little bit.
function GM:OnPlayerChat( ply, text, Team, dead )
	
	local tab = {}
	
	-- Show their level! c:
	if IsValid( ply ) then
		table.insert( tab, Color( 26, 188, 156 ) )
		table.insert( tab, "[Level " .. ply:GetLevel() ..  "] " )
	end

	-- Let everyone know about those pesky ghosts...	 
	if ( dead ) then
		table.insert( tab, Color( 255, 30, 40 ) )
		table.insert( tab, "*DEAD* " )
	end
	 
	if ( Team ) then
		table.insert( tab, Color( 30, 160, 40 ) )
		table.insert( tab, "(TEAM) " )
	end
	 
	if ( IsValid( ply ) ) then
		table.insert( tab, ply )
	else
		table.insert( tab, "Console" )
	end
	 
	table.insert( tab, Color( 255, 255, 255 ) )
	table.insert( tab, ": "..text )
	
	chat.AddText( unpack(tab) )

	return true
	 
end

local VoiceIncoming = false

-- Let's manage that menu (and the weapon switching)
function GM:PlayerBindPress( ply, bind, pressed )

	if !IsValid( ply ) then return end

	if bind == "gm_showteam" then

		TeamChangeMenu()

	end
	
	-- Don't let anything happen if they aren't on a team though!
	if LocalPlayer():Team() == TEAM_SPECTATOR or LocalPlayer():Team() == TEAM_UNASSIGNED then return end

	if bind == "gm_showhelp" then

		CreateMainMenu()

	end

	if bind == "gm_showspare2" and pressed then

		print( "make loadout")

		Loadout()

	end

	if bind == "invnext" and pressed then
	
		WSWITCH:SelectNext( )
		
		return true
		
	elseif bind == "invprev" and pressed then
	
		WSWITCH:SelectPrev( )
		
		return true
		
	elseif bind == "+attack" then
	
		if WSWITCH.Show then
		
			if not pressed then
			
				WSWITCH:ConfirmSelection( )
				
			end
			
			return true
			
		end
		
	elseif string.sub( bind, 1, 4 ) == "slot" and pressed then
	
		local index = tonumber( string.sub( bind, 5, -1 ) ) or 1
		
		WSWITCH:SelectSlot( index )
		
		return true
		
	end
	
	-- i miss quick knives
	if string.find( bind, "noclip" ) then
	
		RunConsoleCommand( "qknife" )
		
	end

	if bind == "+menu" then

		RunConsoleCommand( "cw_dropweapon" )

	end
	
end

-- Let's make that main menu, sexy bars. mmm bb~
function CreateMainMenu()

	local selected = nil

	local mainmenu = vgui.Create( "DPanel" )
	mainmenu:SetSize( ScrW(), ScrH() )
	mainmenu:MakePopup()
	mainmenu:Center()
	mainmenu.Paint = function() end

	local topbar = mainmenu:Add( "DPanel" )
	topbar:SetTall( 64 )
	topbar:Dock( TOP )
	topbar.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 205 )
		surface.DrawRect( 0, 0, w, h )

	end
	topbar.PaintOver = function( panel, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local close = topbar:Add( "DButton" )
	close:Dock( LEFT )
	close:DockMargin( 0, 0, 8, 0 )
	close:SetWide( ScrW() * 0.15 )
	close:SetText( "CLOSE" )
	close:SetFont( "HUD" )
	close:SetTextColor( Color( 255, 255, 255 ) )
	close:SetContentAlignment( 5 )
	close.DoClick = function()

		mainmenu:Remove()

	end

	close.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local market = topbar:Add( "DButton" )
	market:Dock( LEFT )
	market:DockMargin( 8, 0, 8, 0 )
	market:SetWide( ScrW() * 0.1 )
	market:SetText( "BLACK MARKET" )
	market:SetFont( "HUD" )
	market:SetTextColor( Color( 255, 255, 255 ) )
	market.DoClick = function( panel )

		makeShopBars()
		mainmenu:Remove()

	end
	market.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local pointshop = topbar:Add( "DButton" )
	pointshop:Dock( LEFT )
	pointshop:DockMargin( 8, 0, 8, 0 )
	pointshop:SetWide( ScrW() * 0.1 )
	pointshop:SetText( "POINTSHOP" )
	pointshop:SetFont( "HUD" )
	pointshop:SetTextColor( Color( 255, 255, 255 ) )
	pointshop.DoClick = function( panel )

		-- took me too long to remember there was a command... -w-
		RunConsoleCommand( "pointshop2_toggle" )
		mainmenu:Remove()

	end
	pointshop.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local loadout = topbar:Add( "DButton" )
	loadout:Dock( LEFT )
	loadout:DockMargin( 8, 0, 8, 0 )
	loadout:SetWide( ScrW() * 0.1 )
	loadout:SetText( "LOADOUT" )
	loadout:SetFont( "HUD" )
	loadout:SetTextColor( Color( 255, 255, 255 ) )
	loadout.DoClick = function( panel )

		Loadout()
		mainmenu:Remove()

	end
	loadout.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local card = topbar:Add( "DButton" )
	card:Dock( LEFT )
	card:DockMargin( 8, 0, 8, 0 )
	card:SetWide( ScrW() * 0.1 )
	card:SetText( "CARD EDITOR" )
	card:SetFont( "HUD" )
	card:SetTextColor( Color( 255, 255, 255 ) )
	card.DoClick = function( panel )

		openCardEditor()
		mainmenu:Remove()

	end
	card.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 1 )
		
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local bottombar = mainmenu:Add( "DPanel" )
	bottombar:SetTall( 64 )
	bottombar:Dock( BOTTOM )
	bottombar.Paint = function( panel, w, h )

		surface.SetDrawColor( 15, 15, 15, 205 )
		surface.DrawRect( 0, 0, w, h )

	end
	bottombar.PaintOver = function( panel, w, h )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local nametag = bottombar:Add( "DPanel" )
	nametag:Dock( LEFT )
	nametag:SetWide( ScrW() * 0.25 )
	nametag.icon = LocalPlayer():GetRankMaterial()
	nametag.Paint = function( panel, w, h )

		surface.SetMaterial( panel.icon )
		surface.SetDrawColor( 255, 255, 255, 255 )
		surface.DrawTexturedRect( 8, h / 2 - 24, 48, 48 )

		draw.SimpleTextOutlined( LocalPlayer():Name(), "HUD2", 64, h / 2, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

	end

	surface.SetFont( "HUD2" )
	local tw, th = surface.GetTextSize( LocalPlayer():GetMoney() )

	local moneytag = bottombar:Add( "DPanel" )
	moneytag:Dock( RIGHT )
	moneytag:SetWide( ScrW() * 0.25 )
	moneytag.icon = Material( "uprising/ui/game_money_icon_result.png" )
	moneytag.Paint = function( panel, w, h )

		surface.SetMaterial( panel.icon )
		surface.SetDrawColor( 255, 255, 255, 255 )
		surface.DrawTexturedRect( w - tw - 64 - 32, h / 2 - 24, 48, 48 )

		draw.SimpleTextOutlined( LocalPlayer():GetMoney(), "HUD2", w - tw - 32, h / 2, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

	end

end

function GM:HUDWeaponPickedUp()

end

function GM:HUDDrawTargetID()

end

-- LET THE FUCKING HUD BEGIN
-- this is all so messy, i don't even want to look at it.
function GM:HUDPaint()
	self.BaseClass:HUDPaint()
	
	if LocalPlayer():Team() == TEAM_SPECTATOR or LocalPlayer():Team() == TEAM_UNASSIGNED or LocalPlayer( ):GetMoveType( ) == 10 then
	
		if #player.GetAll( ) < 2 then
		
			draw.SimpleTextOutlined( "WAITING FOR PLAYERS", "Roboto", ScrW( ) * 0.5, ScrH( ) * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
			
		else
		
			if gametype == "free-for-all" then
		
				draw.SimpleTextOutlined( "PUSH F4 TO CHOOSE YOUR CLASS", "Roboto", ScrW( ) * 0.5, ScrH( ) * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
				
			else
			
				draw.SimpleTextOutlined( "PUSH F2 TO SELECT YOUR TEAM", "Roboto", ScrW( ) * 0.5, ScrH( ) * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
				
			end
			
		end
		
	end
	
	if gametype != "free-for-all" then
	
		for k, v in pairs( player.GetAll() ) do
		
			if v:IsPlayer() and v != LocalPlayer() then
			
				local pos = ( v:GetPos() + Vector( 0, 0, 90 ) ):ToScreen( )
				local dist = 1 - ( v:GetPos() + Vector( 0, 0, 90 ) ):Distance( LocalPlayer( ):GetPos( ) ) / 1024
				local colour = team.GetColor( v:Team() )
		
				if ( ( v:Team( ) == 1 or v:Team( ) == 2 ) and v:Team( ) == LocalPlayer():Team() )  then
			
					if !v:Alive() then
						
						draw.SimpleText( "X", "HUD", pos.x, pos.y, Color( colour.r, colour.g, colour.b, math.Clamp( 255 * dist, 55, 255 ) ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

					end

				end

			end
			
		end

		local tr = util.GetPlayerTrace( LocalPlayer() )
		local trace = util.TraceLine( tr )
		
		local text = "ERROR"
		local font = "HUD"
		
		if (trace.Hit and trace.HitNonWorld and trace.Entity:IsPlayer() and ( trace.Entity:GetPos():Distance( LocalPlayer():GetPos() ) <= 1024 )) then
			text = trace.Entity:Nick()
			surface.SetFont( font )
			local w, h = surface.GetTextSize( text )
			local col = team.GetColor( trace.Entity:Team() )
			local realcol = Color( col.r, col.g, col.b, math.Clamp( 255 * ( 1 - trace.Entity:GetPos():Distance( LocalPlayer():GetPos() ) / 1024 ), 0, 255 ) )

			local pos = ( trace.Entity:GetPos() + Vector( 0, 0, 92 ) ):ToScreen( )

			draw.SimpleTextOutlined( text, font, pos.x, pos.y, realcol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0, 255 ) )
			
			draw.RoundedBox( 0, pos.x - ( w / 2 ), pos.y + ( h / 2 ) + 2, w, 4, Color( 55, 55, 55, 155 ) )
			draw.RoundedBox( 0, pos.x - ( w / 2 ), pos.y + ( h / 2 ) + 2, ( trace.Entity:Health() / 100 ) * w, 4, realcol )

			surface.SetDrawColor( realcol )
			surface.DrawOutlinedRect( pos.x - ( w / 2 ) - 2, pos.y + ( h / 2 ), w + 4, 8 )

		end
	
	end
	
	if coopcount then
	
		if coopcountnum > 0 then
		
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.SetMaterial( nums[ coopcountnum ] )
			surface.DrawTexturedRect( ScrW( ) * 0.5 - 64, ScrH( ) * 0.8, 128, 128 )
			
		else
		
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.SetMaterial( cletters[ 1 ] )
			surface.DrawTexturedRect( ScrW( ) * 0.5 - 160, ScrH( ) * 0.8 - 32, 64, 64 )
			surface.SetMaterial( cletters[ 2 ] )
			surface.DrawTexturedRect( ScrW( ) * 0.5 + 32, ScrH( ) * 0.8 - 32, 64, 64 )
			surface.SetMaterial( cletters[ 3 ] )
			surface.DrawTexturedRect( ScrW( ) * 0.5 - 32, ScrH( ) * 0.8 - 32, 64, 64 )
			surface.SetMaterial( cletters[ 4 ] )
			surface.DrawTexturedRect( ScrW( ) * 0.5 - 96, ScrH( ) * 0.8 - 32, 64, 64 )
			surface.DrawTexturedRect( ScrW( ) * 0.5 + 96, ScrH( ) * 0.8 - 32, 64, 64 )
			
		end
		
	end

	if ( LocalPlayer():Team() == 1 or LocalPlayer():Team() == 2 ) and LocalPlayer( ):Alive() then

		if hook.Call( "HUDShouldDraw", GAMEMODE, "WSwitch" ) then

			WSWITCH:Draw( LocalPlayer( ) )

		end

		local target = math.Clamp( LocalPlayer():Health(), 0, LocalPlayer():GetMaxHealth() ) / LocalPlayer():GetMaxHealth()
		health = Lerp( 2.5 * FrameTime(), health, target )
		
		local target = math.Clamp( LocalPlayer():Armor(), 0, 100 ) / 100
		armor = Lerp( 2.5 * FrameTime(), armor, target )
		
		local CapturePos = GetGlobalVector( "CapturePos" )
		local disarming = game.GetMap( ) == "battle_93"
		
		local pos = ( CapturePos + Vector( 0, 0, 256 ) ):ToScreen()
		local dist = ( CapturePos + Vector( 0, 0, 256 ) ):Distance( LocalPlayer():GetPos() )
		dist = dist / 39.370
		local text = "Capture"
		local color = Color( 255, 0, 0 )
		local timetext = "00:00"
		local bTime = string.FormattedTime( math.Round( GetGlobalInt( "bTime" ) ) )
		local oTime = string.FormattedTime( math.Round( GetGlobalInt( "oTime" ) ) )
		local cTime = string.FormattedTime( math.Round( GetGlobalInt( "cTime" ) ) )
		local rTime = string.FormattedTime( math.Round( GetGlobalInt( "rTime" ) ) )
		local bTimeUnformatted = math.Round( GetGlobalInt( "bTime" ) )
		local oTimeUnformatted = math.Round( GetGlobalInt( "oTime" ) )
		local cTimeUnformatted = math.Round( GetGlobalInt( "cTime" ) )
		local rTimeUnformatted = math.Round( GetGlobalInt( "rTime" ) )
		local CaptureAlias = GetGlobalString( "CaptureAlias" )
		local PointNumber = GetGlobalInt( "PointNumber" )
		local PointColour = Color( 205, 205, 205, 255 )
		local letters = { "A", "B", "C", "D", "E", "F", "G" }
		local lettertext = letters[ GetGlobalInt( "PointNumber" ) ]
		local bCol = Color( 255, 255, 255 )
		local oCol = Color( 255, 255, 255 )
		local totaltime = GetGlobalInt( "CaptureTimer" )
		
		if GetGlobalInt( "bTime" ) <= 0 then
		
			bTime = string.FormattedTime( totaltime )
			bTimeUnformatted = totaltime
			
		end
		
		if GetGlobalInt( "oTime" ) <= 0 then
		
			oTime = string.FormattedTime( totaltime )
			oTimeUnformatted = totaltime
			
		end
		
		if GetGlobalInt( "cTime" ) <= 0 then
		
			cTime = string.FormattedTime( 600 )
			cTimeUnformatted = 600
			
		end
		
		if GetGlobalInt( "rTime" ) <= 0 then
		
			rTime = string.FormattedTime( 1200 )
			rTimeUnformatted = 1200
			
		end
		
		if GetGlobalInt( "PointOwner" ) == 1 and GetGlobalInt( "PointOwner" ) ~= 0 then
		
			PointColour = bCol
			
		elseif GetGlobalInt( "PointOwner" ) == 2 then
		
			PointColour = rCol
			
		else
		
			PointColour = Color( 205, 205, 205 )
			
		end
		
		bCol = team.GetColor( 1 )
		bColbg = Color( bCol.r - 50, bCol.g - 50, bCol.b - 50 )
		rCol = team.GetColor( 2 )
		rColbg = Color( rCol.r - 50, rCol.g - 50, rCol.b - 50 )
		
		local bTimeText = "0".. bTime.m ..":".. bTime.s
		
		if bTime.m >= 10 then
		
			bTimeText = bTime.m ..":".. bTime.s
			
		end
		
		if bTime.m >= 10 and bTime.s < 10 then
		
			bTimeText = bTime.m ..":0".. bTime.s
			
		end
		
		if bTime.m < 10 and bTime.s < 10 then
		
			bTimeText = "0".. bTime.m ..":0".. bTime.s
			
		end
		
		local oTimeText = "0".. oTime.m ..":".. oTime.s
		
		if oTime.m >= 10 and oTime.s < 10 then
		
			oTimeText = oTime.m ..":0".. oTime.s
			
		end
		
		if oTime.m < 10 and oTime.s < 10 then
		
			oTimeText = "0".. oTime.m ..":0".. oTime.s
			
		end
		
		local cTimeText = "0".. cTime.m ..":".. cTime.s
		
		if cTime.m >= 10 and cTime.s < 10 then
		
			cTimeText = cTime.m ..":0".. cTime.s
			
		end
		
		if cTime.m < 10 and cTime.s < 10 then
		
			cTimeText = "0".. cTime.m ..":0".. cTime.s
			
		end
		
		local rTimeText = "0".. rTime.m ..":".. rTime.s
		
		if rTime.m >= 10 and rTime.s >= 10 then
		
			rTimeText = rTime.m.. ":" ..rTime.s
			
		end
		
		if rTime.m >= 10 and rTime.s < 10 then
		
			rTimeText = rTime.m ..":0".. rTime.s
			
		end
		
		if rTime.m < 10 and rTime.s < 10 then
		
			rTimeText = "0".. rTime.m ..":0".. rTime.s
			
		end
		
		
		if GetGlobalInt( "PointOwner" ) == LocalPlayer():Team() then
			text = "Defend"
		end
		
		if gametype == "conquest" then
		
			draw.SimpleTextOutlined( lettertext, "HUD2", pos.x - 1, pos.y, PointColour, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
			surface.SetMaterial( ring )
			surface.SetDrawColor( Color( 0, 0, 0 ) )
			surface.DrawTexturedRect( pos.x - ScreenScale( 8 ) - 2, pos.y - ScreenScale( 8 ) - 2, ScreenScale( 16 ) + 4, ScreenScale( 16 ) + 4 )
			surface.DrawTexturedRect( pos.x - ScreenScale( 8 ) + 2, pos.y - ScreenScale( 8 ) + 2, ScreenScale( 16 ) - 4, ScreenScale( 16 ) - 4 )
			surface.SetDrawColor( PointColour )
			surface.DrawTexturedRect( pos.x - ScreenScale( 8 ), pos.y - ScreenScale( 8 ), ScreenScale( 16 ), ScreenScale( 16 ) )
			
			draw.SimpleTextOutlined( math.floor( dist ), "HUD", pos.x - 1, pos.y + ScreenScale( 13 ), PointColour, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
			
			if ( pos.x < ScrW( ) * 0.55 and pos.x > ScrW( ) * 0.45 ) and ( pos.y < ScrH( ) * 0.6 and pos.y > ScrH( ) * 0.4 ) then
			
				draw.SimpleTextOutlined( CaptureAlias, "HUD", pos.x, pos.y - ScreenScale( 14 ), PointColour, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )
				
			end
			
		end
			if gametype == "conquest" then
				
				draw.RoundedBox( 0, ScrW( ) * 0.5 - 128, 28, 256, 40, Color( 0, 0, 0, 205 ) )
				
				draw.SimpleText( "CMB ".. bTimeText, "RobotoSmall", ScrW( ) * 0.5 - 110, 48, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
				draw.SimpleText( oTimeText.. " RES", "RobotoSmall", ScrW( ) * 0.5 + 110, 48, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
				
				surface.SetDrawColor( 255, 255, 255, 255 )
				surface.DrawRect( ScrW( ) * 0.5, 26, 1, 40 )
				
				draw.RoundedBox( 0, 4, ScrH( ) - 98, ScrW( ) * 0.1, 90, Color( 0, 0, 0, 105 ) )
				draw.RoundedBox( 0, 4, ScrH( ) - 72, ScrW( ) * 0.1, 64, Color( 0, 0, 0, 155 ) )
				
				draw.SimpleText( nicegametype, "RobotoSmaller", ScrW( ) * 0.1 - 4, ScrH( ) - 85, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

				draw.SimpleText( "Team: ".. team.GetName( LocalPlayer( ):Team() ), "RobotoSmaller", 8, ScrH( ) - 85, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

			elseif gametype == "tdm" then
			
				draw.RoundedBox( 0, ScrW( ) * 0.5 - 160, 28, 320, 40, Color( 0, 0, 0, 205 ) )
				
				draw.SimpleText( rTimeText, "RobotoSmaller", ScrW( ) * 0.5, 48, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				draw.SimpleText( team.GetName( 1 ).. " - " ..team.GetScore( 1 ), "RobotoSmall", ScrW() * 0.5 - 32, 48, Color( 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )

				draw.SimpleText( team.GetScore( 2 ).. " - " ..team.GetName( 2 ), "RobotoSmall", ScrW() * 0.5 + 32, 48, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			elseif gametype == "free-for-all" then
				
				draw.RoundedBox( 0, ScrW( ) * 0.5 - 128, 28, 256, 40, Color( 0, 0, 0, 205 ) )
				
				draw.SimpleText( rTimeText, "RobotoSmall", ScrW( ) * 0.5, 48, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				draw.RoundedBox( 0, 4, ScrH( ) - 98, ScrW( ) * 0.1, 90, Color( 0, 0, 0, 105 ) )
				draw.RoundedBox( 0, 4, ScrH( ) - 72, ScrW( ) * 0.1, 64, Color( 0, 0, 0, 155 ) )
				
				draw.SimpleText( nicegametype, "RobotoSmaller", ScrW( ) * 0.1 - 4, ScrH( ) - 85, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
			
				draw.SimpleText( LocalPlayer( ):Frags( ), "Roboto", 52, ScrH( ) - 50, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
				
				surface.SetDrawColor( 255, 255, 255, 255 )
				surface.DrawRect( 68, ScrH( ) - 64, 1, 48 )
				
				if playerfrags[ 1 ] and playerfrags[ 1 ]:IsValid( ) then
				
					draw.SimpleText( playerfrags[ 1 ]:Frags( ), "RobotoBig", 82, ScrH( ) - 46, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
					
				else
				
					draw.SimpleText( 0, "RobotoBig", 82, ScrH( ) - 46, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
				end
				
				draw.SimpleText( "Target: ".. maxkills, "RobotoSmaller", 8, ScrH( ) - 85, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			elseif gametype == "arms race" then
			
				draw.RoundedBox( 0, 4, ScrH( ) - 98, ScrW( ) * 0.1, 90, Color( 0, 0, 0, 105 ) )
				draw.RoundedBox( 0, 4, ScrH( ) - 72, ScrW( ) * 0.1, 64, Color( 0, 0, 0, 155 ) )
				
				draw.SimpleText( nicegametype, "RobotoSmaller", ScrW( ) * 0.1 - 4, ScrH( ) - 85, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
			
				draw.SimpleText( armsracelist[ LocalPlayer( ) ] or 0, "Roboto", 52, ScrH( ) - 50, Color( 255, 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
				
				surface.SetDrawColor( 255, 255, 255, 255 )
				surface.DrawRect( 68, ScrH( ) - 64, 1, 48 )
				
				if armsracelist[ armsracelist2[ 1 ] ] and armsracelist2[ 1 ]:IsValid( ) then
				
					draw.SimpleText( armsracelist[ armsracelist2[ 1 ] ], "RobotoBig", 82, ScrH( ) - 46, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
					
				else
				
					draw.SimpleText( 0, "RobotoBig", 82, ScrH( ) - 46, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
				end
				
				draw.SimpleText( "Target: ".. #ArmsRaceTable, "RobotoSmaller", 8, ScrH( ) - 85, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			else
			
				draw.RoundedBox( 0, 25, ScrH( ) - 130, 240, 100, Color( 0, 0, 0, 200 ) )
			
				draw.SimpleText( "COOPERATIVE MISSION", "RobotoSmall", 145, ScrH( ) - 100, bCol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				draw.SimpleText( cTimeText, "RobotoSmall", 145, ScrH( ) - 75, bCol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
				
				local target = 160 * ( math.Clamp( cTimeUnformatted / 600, 0, 600 ) )
				ctimer = Lerp( 2.5 * FrameTime( ), ctimer, target )

				draw.RoundedBox( 0, 65, ScrH( ) - 55, 160, 10, bColbg )
				draw.RoundedBox( 0, 65, ScrH( ) - 55, ctimer, 10, bCol )

				local strider = ents.FindByClass( "npc_strider" )
				
				if strider[1] then
				
					local shealth = strider[1]:Health( ) / 350
					surface.SetDrawColor( 225, 185, 0, 255 )
					surface.DrawRect( 58, 56, 300 * shealth, 8 )
					surface.SetMaterial( striderbase )
					surface.SetDrawColor( 255, 255, 255, 255 )
					surface.DrawTexturedRect( -16, -32, 512, 128 )
					
				end
				
			end

		if LocalPlayer():Alive() then

			for k, v in pairs( damageindicators ) do

				surface.SetMaterial( damageindicator )
				surface.SetDrawColor( 155, 0, 0, 255 * v[ 2 ] )
				surface.DrawTexturedRectRotated( ScrW() * 0.5, ScrH() * 0.5, 256, 256, v[ 1 ] - 180 )
				v[ 2 ] = v[ 2 ] - 0.1

				if v[ 2 ] <= 0 then

					damageindicators[ k ] = nil

				end

			end

			if hitalpha == 0 then

				drawhit = false

			end

			if drawhit == true then
				
				local x = ScrW() / 2
				local y = ScrH() / 2
				
				hitalpha = math.Approach(hitalpha, 0, 5 )
				local col = HitCol
				col.a = hitalpha
				surface.SetDrawColor( col )

				surface.DrawLine( x - 6, y - 5, x - 11, y - 10 )
				surface.DrawLine( x + 5, y - 5, x + 10, y - 10 )
				surface.DrawLine( x - 6, y + 5, x - 11, y + 10 )
				surface.DrawLine( x + 5, y + 5, x + 10, y + 10 )

			end


			for k, v in pairs( ents.FindInSphere( LocalPlayer():GetPos(), 256 ) ) do

				if string.find( v:GetClass(), "thrown" ) and v:GetPos() != LocalPlayer():GetPos() then

					local pos = LocalPlayer():GetPos()
					local angles = LocalPlayer():EyeAngles()

					if angles.y < -90 and angles.y > 90 then

						angles.y = angles.y + 360

					elseif angles.y > 90 and angles.y < -90 then

						angles.y = angles.y - 360

					end

					local vector = v:GetPos() - pos
					local ang = vector:Angle()

					surface.SetMaterial( nadeindicator )
					surface.SetDrawColor( 255, 255, 255, 155 )
					surface.DrawTexturedRectRotated( ScrW() * 0.5, ScrH() * 0.5, 256, 256, ( ang.y - angles.y ) - 180 )

				end

			end

			if CaptureNum then
			
				for i = 1, CaptureNum do
				
					local midpoint = math.Round( 80 / CaptureNum )
					local lettercolor = Color( 255, 255, 255 )
					
					if GetGlobalInt( "PointNumber" ) == i then
					
						if GetGlobalInt( "PointOwner" ) ~= LocalPlayer( ):Team( ) and GetGlobalInt( "PointOwner" ) ~= 0 then
						
							lettercolor = bCol
						
						elseif GetGlobalInt( "PointOwner" ) == LocalPlayer():Team() then
						
							lettercolor = rCol
							
						else
						
							lettercolor = Color( 255, 255, 255 )
							
						end
						
					end
					
					if CaptureNum == 1 then
				
						midpoint = 54
						
					end
					
					if CaptureNum == 2 then
					
						midpoint = 36
						
					end
					
					if CaptureNum == 2 then
					
						midpoint = 36
						
					end
					
					if CaptureNum == 4 then
					
						midpoint = 21
						
					end
				
					if CaptureNum == 5 then
					
						midpoint = 18
						
					end
					
					draw.SimpleText( letters[ i ], "RobotoSmall", 40 + midpoint * ( i ) , ScrH() - 40, lettercolors[ i ], TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
					if lettercolors[ i ] then
					
						surface.SetMaterial( ring )
						surface.SetDrawColor( lettercolors[ i ] )
						surface.DrawTexturedRect( 25 + midpoint * i, ScrH() - 55, 30, 30 )
						surface.SetDrawColor( Color( 0, 0, 0 ) )
						surface.DrawTexturedRect( 24 + midpoint * i, ScrH() - 56, 32, 32 )
						surface.DrawTexturedRect( 27 + midpoint * i, ScrH() - 53, 26, 26 )
						
					end

				end
				
			end
			
			--[[draw.SimpleText( "Combine", "RobotoSmall", 100, ScrH( ) - 280, team.GetColor( 1 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( bRoundPoints .."  -  ".. bTotalPoints, "RobotoSmall", 275, ScrH( ) - 280, team.GetColor( 1 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Resistance", "RobotoSmall", 100, ScrH( ) - 240, team.GetColor( 2 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( oRoundPoints .."  -  ".. oTotalPoints, "RobotoSmall", 275, ScrH( ) - 240, team.GetColor( 2 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )]]
					
			--[[draw.RoundedBox( 0, ScrW() - 325, ScrH() - 188, 300, 50, Color( 0, 0, 0, 200 ) )
			draw.RoundedBox( 0, ScrW( ) - 50 - health * 250, ScrH( ) - 175, health * 250, 25, Color( 190, 60, 40, 200 ) )
			draw.SimpleText( LocalPlayer():Health(), "RobotoSmall", ScrW() - 50 - health * 250, ScrH() - 175, Color( 255, 255, 255 ) )]]
			
			local basew = ScrW( ) - ( ScrW( ) * 0.25 )
			local basew2 = ScrW( ) - ( ScrW( ) * 0.15 + 16 )
			
			draw.RoundedBox( 0, basew - 8, ScrH( ) - 98, ScrW( ) * 0.25 + 4, 90, Color( 0, 0, 0, 105 ) )
			
			draw.RoundedBox( 0, basew - 8, ScrH( ) - 72, ScrW( ) * 0.25 + 4, 64, Color( 0, 0, 0, 155 ) )
			
			--[[surface.SetDrawColor( 255, 255, 255, 255 )
			surface.SetMaterial( shield )
			surface.DrawTexturedRect( basew2 - 12, ScrH( ) - 54, 8, 8 )
			
			draw.RoundedBox( 0, basew2, ScrH( ) - 54, ScrW( ) * 0.15, 8, Color( 255, 255, 255, 5 ) )
			draw.RoundedBox( 0, basew2, ScrH( ) - 54, armor * ScrW( ) * 0.15, 8, Color( 255, 255, 255, 255 ) )]]
			
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.SetMaterial( plus )
			surface.DrawTexturedRect( basew2 - 12, ScrH( ) - 42, 8, 8 )
			
			draw.RoundedBox( 0, basew2, ScrH( ) - 42, ScrW( ) * 0.15, 8, Color( 255, 255, 255, 5 ) )
			draw.RoundedBox( 0, basew2, ScrH( ) - 42, health * ScrW( ) * 0.15, 8, Color( 255, 255, 255, 255 ) )
			
			if IsValid( LocalPlayer():GetActiveWeapon() ) then
			
				local gun = LocalPlayer( ):GetActiveWeapon( )
			
				draw.SimpleText( gun.PrintName, "RobotoSmaller", basew, ScrH( ) - 84, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
				
				draw.SimpleText( LocalPlayer():GetAmmoCount( gun:GetPrimaryAmmoType() ), "RobotoSmall", basew + 32, ScrH( ) - 54, Color( 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
			
				if gun.BulletDisplay then

					surface.SetDrawColor(0, 0, 0, 255)
					surface.SetTexture(Bullet)
					
					surface.SetFont("CW_HUD24")
					local xSize, ySize = surface.GetTextSize(gun.FireModeDisplay)
					
					for i = 1, gun.BulletDisplay do
						surface.DrawTexturedRect(basew + 16, ScrH( ) - 24 + ( ( i - 1 ) * 6 - 1 - ySize ), 16, 16)
					end
					
					surface.SetDrawColor(255, 255, 255, 255)
					
					for i = 1, gun.BulletDisplay do
						surface.DrawTexturedRect(basew + 16, ScrH( ) - 24 + ( ( i - 1 ) * 6 - 1 - ySize ), 16, 16)
					end

				
				end
				
				surface.SetFont( "RobotoBig" )
				local xSize, ySize = surface.GetTextSize("000")
				
				surface.DrawRect( basew + 42, ScrH( ) - 68, 1, 54 )
				
				draw.SimpleText( "000", "RobotoBig", basew + 50 + xSize, ScrH( ) - 42, Color( 255, 255, 255, 3 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
				draw.SimpleText( gun:Clip1(), "RobotoBig", basew + 50 + xSize, ScrH( ) - 42, Color( 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
			--[[
				draw.RoundedBox( 0, ScrW() - 325, ScrH() - 130, 300, 100, Color( 0, 0, 0, 200 ) )
				
				if LocalPlayer():GetActiveWeapon():Clip1() != -1 then
					if LocalPlayer( ):GetActiveWeapon( ).Primary then
						local target = math.Clamp( LocalPlayer():GetActiveWeapon():Clip1(), 0, LocalPlayer():GetActiveWeapon().Primary.ClipSize ) / LocalPlayer():GetActiveWeapon().Primary.ClipSize
						ammo = Lerp( 2.5 * FrameTime(), ammo, target )
						draw.RoundedBox( 0, ScrW() - 50 - ammo * 250, ScrH() - 110, ammo * 250, 25, Color( 211, 84, 0, 200 ) )
						draw.SimpleText( LocalPlayer():GetActiveWeapon():Clip1(), "RobotoSmall", ScrW() - 50 - ammo * 250, ScrH() - 110, Color( 255, 255, 255 ) )
						draw.SimpleText( "Reserve: " .. LocalPlayer():GetAmmoCount( LocalPlayer():GetActiveWeapon():GetPrimaryAmmoType() ), "RobotoSmall", ScrW() - 50, ScrH() - 50, Color( 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_BOTTOM )
					end
					
					if LocalPlayer( ):GetActiveWeapon( ):GetClass( ) == "weapon_physcannon" then
					
						draw.SimpleText( "Gravity Gun", "Roboto", ScrW( ) - 175, ScrH( ) - 80, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
					end

				else
				
					if LocalPlayer( ):GetActiveWeapon( ):GetClass( ) == "fas2_dv2" then
					
						draw.SimpleText( "Knife", "Roboto", ScrW( ) - 175, ScrH( ) - 80, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
					end
								
				end]]
			end
		end
	end
	
	if LocalPlayer():GetNWBool( "InCapture" ) then
	
		if GetGlobalInt( "CaptureTime" ) <= 0 then return end
		
		surface.SetFont( "RobotoSmall" )
		local xSize, ySize = surface.GetTextSize("000")
		if GetGlobalInt( "Capture" ) == 3 and GetGlobalInt( "PointOwner" ) != LocalPlayer():Team() then
			draw.SimpleText( "Capture blocked", "RobotoSmall", ScrW( ) * 0.5, 94, Color( 255, 55, 55 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
		end
		if GetGlobalInt( "Capture" ) == LocalPlayer():Team() then
			local target = ( math.Round( GetGlobalInt( "CaptureTime" ) ) / CaptureTime ) * 2
			time = Lerp( 7.5 * FrameTime(), time, target )
			
			if GetGlobalInt( "PointOwner" ) ~= 0 then
			
				draw.SimpleText( "Neutralising", "RobotoSmall", ScrW( ) * 0.5, 94, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
				
			else
					
				draw.SimpleText( "Capturing", "RobotoSmall", ScrW( ) * 0.5, 94, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_TOP )
				
			end
			
			draw.RoundedBox( 0, ScrW()  * 0.5 - 128, 82, 256, 8, Color( 35, 35, 35, 105 ) )
			draw.RoundedBox( 0, ScrW()  * 0.5 - 128, 82, time * 128, 8, Color( 255, 255, 255, 255 ) )
		end
	end
end

function GM:HUDShouldDraw( name )
	if name == "CHudBattery" or name == "CHudAmmo" or name == "CHudHealth" or name == "CHudSecondaryAmmo" or name == "CHudDamageIndicator" then
		return false
	end
	return true
end

local score

-- I miss the tutorial menu, it was cool.
local function BuildTutInfo( rightbg, info )
	
	for k, v in pairs( rightbg:GetChildren( ) ) do
		
		v:Remove( )
			
	end
		
	local bgscroll = rightbg:Add( "DScrollPanel" )
	bgscroll:Dock( FILL )
	bgscroll:SetTall( rightbg:GetTall( ) )
	
	if info == "intro" then
		
		local introtext = bgscroll:Add( "DLabel" )
		introtext:Dock( TOP )
		introtext:DockMargin( 8, 8, 8, 8 )
		introtext:SetFont( "Roboto" )
		introtext:SetTextColor( Color( 105, 105, 105 ) )
		introtext:SetText( "Introduction" )
		introtext:SetContentAlignment( 5 )
		introtext:SizeToContents( )
		
		local introp1 = bgscroll:Add( "DLabel" )
		introp1:Dock( TOP )
		introp1:DockMargin( 4, 4, 0, 4 )
		introp1:SetWrap( true )
		introp1:SetDrawBackground( false )
		introp1:SetFont( "RobotoSmall" )
		introp1:SetTextColor( Color( 105, 105, 105 ) )
		introp1:SetText( "Welcome to Uprising, a Half-Life 2 themed gamemode that's offers a selection of Conquest, Team Deathmatch and Cooperative modes.\nOne of these modes, and a respective map can be voted on by the players at the end of each game, meaning that there are plenty of opportunities to enjoy all aspects of Uprising!\n\nFeel free to use the buttons on your left to read seperate tutorials for each gamemode, these will help you understand what you're getting into and are there to make sure you have a good time.\n\nIt's important to remember that Uprising is very much still a work-in-progress, and as such I would greatly appreciate any criticisms or requests that you feel might improve the gamemode. :-)" )
		introp1:SetAutoStretchVertical( true )
		
		local sickpose = Material( "uprising/ex_result_lose_con_01.png" )
		
		local intropose = bgscroll:Add( "DPanel" )
		intropose:Dock( TOP )
		intropose:DockMargin( 0, 32, 0, 32 )
		intropose:SetTall( 256 )
		intropose.Paint = function( panel, w, h )
		
			surface.SetMaterial( sickpose )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( w / 2 - 256, -256, 512, 512 )
			
		end
			
	end
	
	if info == "general" then
	
		local generaltext = bgscroll:Add( "DLabel" )
		generaltext:Dock( TOP )
		generaltext:DockMargin( 8, 8, 8, 8 )
		generaltext:SetFont( "Roboto" )
		generaltext:SetTextColor( Color( 105, 105, 105 ) )
		generaltext:SetText( "General Information" )
		generaltext:SetContentAlignment( 5 )
		generaltext:SizeToContents( )
		
		local generalp1 = bgscroll:Add( "DLabel" )
		generalp1:Dock( TOP )
		generalp1:DockMargin( 4, 4, 0, 4 )
		generalp1:SetWrap( true )
		generalp1:SetFont( "RobotoSmall" )
		generalp1:SetTextColor( Color( 105, 105, 105 ) )
		generalp1:SetText( "When you first spawn you'll be greeted with some text on your screen. If you're alone on the server or others are still joining you'll have to wait, otherwise you'll be able to push F2 and get straight into the action! F2 will allow you to choose your team, simply click on the header for your preferred team and you'll be taken to the class screen." )
		generalp1:SetAutoStretchVertical( true )
		
		local tut1 = Material( "uprising/tut1.png" )
		
		local tutpic1 = bgscroll:Add( "DPanel" )
		tutpic1:Dock( TOP )
		tutpic1:SetTall( 128 )
		tutpic1.Paint = function( panel, w, h )
		
			surface.SetMaterial( tut2 )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( w / 2 - 177, 0, 354, 128 )
			
		end
		
	end
		
end


function OpenTutorialMenu()

	local tutmenu = vgui.Create( "DFrame" )
	tutmenu:SetSize( ScrW( ) * 0.75, ScrH( ) * 0.75 )
	tutmenu:Center( )
	tutmenu:MakePopup( )
	tutmenu:SetTitle( "Tutorial" )
	
	local sidebara = tutmenu:Add( "DPanel" )
	sidebara:Dock( LEFT )
	sidebara:SetWide( tutmenu:GetWide() * 0.25 )
	
	local sidebar = sidebara:Add( "DScrollPanel" )
	sidebar:Dock( FILL )
	
	local rightbg = tutmenu:Add( "DPanel" )
	rightbg:Dock( FILL )
	rightbg:DockMargin( 8, 0, 0, 0 )
	
	local introbutton = sidebar:Add( "DButton" )
	introbutton:Dock( TOP )
	introbutton:DockMargin( 4, 4, 4, 4 )
	introbutton:SetTall( 32 )
	introbutton:SetText( "Introduction" )
	introbutton:SetImage( "icon16/information.png" )
	introbutton.DoClick = function( )
	
		BuildTutInfo( rightbg, "intro" )
		
	end
	
	local generalbutton = sidebar:Add( "DButton" )
	generalbutton:Dock( TOP )
	generalbutton:DockMargin( 4, 4, 4, 4 )
	generalbutton:SetTall( 32 )
	generalbutton:SetText( "General Information" )
	generalbutton:SetImage( "icon16/book_open.png" )
	generalbutton.DoClick = function( )
	
		BuildTutInfo( rightbg, "general" )
		
	end
	
	local conquestbutton = sidebar:Add( "DButton" )
	conquestbutton:Dock( TOP )
	conquestbutton:DockMargin( 4, 4, 4, 4 )
	conquestbutton:SetTall( 32 )
	conquestbutton:SetText( "Conquest Information" )
	conquestbutton:SetImage( "icon16/flag_red.png" )
	
	local tdmbutton = sidebar:Add( "DButton" )
	tdmbutton:Dock( TOP )
	tdmbutton:DockMargin( 4, 4, 4, 4 )
	tdmbutton:SetTall( 32 )
	tdmbutton:SetText( "Team Deathmatch Information" )
	tdmbutton:SetImage( "icon16/gun.png" )
	
	local coopbutton = sidebar:Add( "DButton" )
	coopbutton:Dock( TOP )
	coopbutton:DockMargin( 4, 4, 4, 4 )
	coopbutton:SetTall( 32 )
	coopbutton:SetText( "Cooperative Information" )
	coopbutton:SetImage( "icon16/heart.png" )
	
	BuildTutInfo( rightbg, "intro" )
	
end

local function populatelist( panel, teamnum )

	local players = team.GetPlayers( teamnum )

	local winner = 0
	
	table.sort( players, function( a, b )

		return a:Frags() > b:Frags() 

	end )

	local SideInfo = panel:Add( "DPanel" )
	SideInfo:Dock( TOP )
	SideInfo:SetTall( 32 )
	SideInfo.Paint = function( panel, w, h )
		
		surface.SetDrawColor( 85, 75, 75, 115 )
		surface.SetTexture( gradient2 )
		surface.DrawTexturedRect( 0, 0, w, h )
		surface.SetDrawColor( 45, 35, 35, 155 )
		surface.DrawRect( 0, 0, w, h )

		if winner and winner == teamnum then

			draw.SimpleText( team.GetName( teamnum ).. " - Winner!", "HUD", 8, h * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
		else

			draw.SimpleText( team.GetName( teamnum ), "HUD", 8, h * 0.5, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

		end

	end
		
	local ScorePing = SideInfo:Add( "DLabel" )
	ScorePing:Dock( RIGHT )
	ScorePing:SetWide( 64 )
	ScorePing:SetFont( "HUD" )
	ScorePing:SetText( "Ping" )
	ScorePing:SetContentAlignment( 4 )
		
	local ScoreDeath = SideInfo:Add( "DLabel" )
	ScoreDeath:Dock( RIGHT )
	ScoreDeath:SetWide( 64 )
	ScoreDeath:SetFont( "HUD" )
	ScoreDeath:SetText( "D" )
	ScoreDeath:SetContentAlignment( 4 )
		
	local ScoreKill = SideInfo:Add( "DLabel" )
	ScoreKill:Dock( RIGHT )
	ScoreKill:SetWide( 64 )
	ScoreKill:SetFont( "HUD" )
	ScoreKill:SetText( "K" )
	ScoreKill:SetContentAlignment( 4 )

	for k, v in pairs( players ) do

		local PlayerInfo = panel:Add( "DPanel" )
		PlayerInfo:Dock( TOP )
		PlayerInfo:SetTall( 64 )
		PlayerInfo.Paint = function( panel, w, h )
			
			surface.SetDrawColor( 35, 35, 35, 155 )
			surface.DrawRect( 0, 0, w, h )
				
		end
			
		local PlayerAvatar = PlayerInfo:Add( "AvatarImage" )
		PlayerAvatar:Dock( LEFT )
		PlayerAvatar:SetWide( PlayerInfo:GetTall( ) )
		PlayerAvatar:SetPlayer( v, 64 )
			
		local PAvatarButton = PlayerAvatar:Add( "DButton" )
		PAvatarButton:Dock( FILL )
		PAvatarButton:SetText( "" )
		PAvatarButton.Paint = function( )
			
		end
			
		PAvatarButton.DoClick = function( )

			surface.PlaySound( "uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav")
			
			v:ShowProfile( )
				
		end
			
		local PlayerName = PlayerInfo:Add( "DPanel" )
		PlayerName:Dock( LEFT )
		PlayerName:SetWide( ScrW( ) * 0.25 )

		local icon = v:GetRankMaterial()

		PlayerName.Paint = function( panel, w, h )

			if IsValid( v ) then
		
				surface.SetMaterial( icon )
				surface.DrawTexturedRect( 0, 0, 64, 64 )

				draw.SimpleText( v:Name() or "Disconnected", "RobotoSmall", 64, h * 0.5, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
				draw.SimpleText( v:GetUserGroup(), "RobotoSmaller", 64, h * 0.8, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
			
			else

				draw.SimpleText( "Disconnected", "RobotoSmall", 0, h * 0.4, Color( 255, 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )

			end

		end
			
		local PingIcon = PlayerInfo:Add( "DPanel" )
		PingIcon:Dock( RIGHT )
		PingIcon:SetWide( 64 )
		PingIcon:SetTooltip( v:Ping( ) )
		PingIcon.Paint = function( panel, w, h )
			
			local basepos = 4
			local basewidth = 4
			local baseheight = h - 24
			local ping = 0

			if IsValid( v ) and v:IsPlayer() then

				ping = v:Ping( )

			end
				
			if ping <= 50 then
				
				surface.SetDrawColor( 0, 255, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			elseif ping <= 150 and ping > 50 then
				
				surface.SetDrawColor( 105, 205, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
					
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
				
			elseif ping <= 250 and ping > 150 then
				
				surface.SetDrawColor( 155, 155, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			else
				
				surface.SetDrawColor( 255, 0, 0, 255 )
				surface.DrawRect( basepos, baseheight, basewidth, 8 )
					
				surface.SetDrawColor( 55, 55, 55, 205 )
				surface.DrawRect( basepos * 3, baseheight - 8, basewidth, 16 )
				surface.DrawRect( basepos * 5, baseheight - 16, basewidth, 24 )
				surface.DrawRect( basepos * 7, baseheight - 24, basewidth, 32 )
					
			end
				
		end
			
		local Deaths = PlayerInfo:Add( "DLabel" )
		Deaths:Dock( RIGHT )
		Deaths:SetWide( 64 )
		Deaths:SetFont( "HUD" )
		Deaths:SetText( v:Deaths( ) )
			
		local Kills = PlayerInfo:Add( "DLabel" )
		Kills:Dock( RIGHT )
		Kills:SetWide( 64 )
		Kills:SetFont( "HUD" )
		Kills:SetText( v:Frags( ) )

	end

end

function GM:ScoreboardShow()
	gui.EnableScreenClicker( true )

	local margin = ScrW() * 0.05

	score = vgui.Create( "DPanel" )
	score:Dock( TOP )
	score:SetTall( ScrH() * 0.8 )
	score:DockMargin( margin, ScrH() * 0.1, margin, ScrH() * 0.1 )
	score.Paint = function( panel, w, h ) end

	local scoreinfo1 = score:Add( "DPanel" )
	scoreinfo1:Dock( TOP )
	scoreinfo1:SetTall( score:GetTall() / 2 )
	scoreinfo1.Paint = function( panel, w, h ) end

	local team1 = scoreinfo1:Add( "DScrollPanel" )
	team1:Dock( FILL )
	team1.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 225 )
		surface.DrawRect( 0, 0, w, h )

	end

	local scoreinfo2 = score:Add( "DPanel" )
	scoreinfo2:Dock( TOP )
	scoreinfo2:SetTall( score:GetTall() / 2 )
	scoreinfo2.Paint = function( panel, w, h ) end

	local team2 = scoreinfo2:Add( "DScrollPanel" )
	team2:Dock( FILL )
	team2.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 225 )
		surface.DrawRect( 0, 0, w, h )

	end

	populatelist( team1, 1 )
	populatelist( team2, 2 )

	score.lastupdate = 0
	score.lastcount = #player.GetAll()

	score.Think = function( self )

		if( self.lastupdate < CurTime() ) then

			self.lastupdate = CurTime() + 1

			if( self.lastcount != #player.GetAll() ) then

				team1:Clear( true )
				team2:Clear( true )

				populatelist( team1, 1, winner )
				populatelist( team2, 2, winner )

			end

			self.lastcount = #player.GetAll()

		end

	end

end

function GM:ScoreboardHide()
	gui.EnableScreenClicker( false )
	score:Remove()
end

local model

net.Receive( "InitialSpawn", function( )

	RunConsoleCommand( "fas2_nohud", "0" )
	RunConsoleCommand( "fas2_customhud", "0" )
	
end )

net.Receive( "welcomeMessage", function( )

	if welcomescreen then

		welcomescreen:Remove()

	end

	welcomescreen = vgui.Create( "DFrame" )
	welcomescreen:SetSize( ScrW(), ScrH() )
	welcomescreen:MakePopup()
	welcomescreen:Center()
	welcomescreen:ShowCloseButton( false )
	welcomescreen.Paint = function( panel, w, h )

		Derma_DrawBackgroundBlur( panel, time )

	end

	local welcometop = welcomescreen:Add( "DPanel" )
	welcometop:Dock( TOP )
	welcometop:SetTall( 64 )
	welcometop:DockMargin( ScrW() * 0.1, ScrH() * 0.025, ScrW() * 0.1, 8 )
	welcometop.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

		draw.SimpleText( "Welcome to Uprising TDM", "Roboto", w / 2, h / 2, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

	end

	surface.SetFont( "RobotoSmall" )
	local th = select( 2, surface.GetTextSize( "$" ) )

	tuttext = welcomescreen:Add( "DPanel" )
	tuttext:Dock( TOP )
	tuttext:SetTall( th * 3 )
	tuttext:DockMargin( ScrW() * 0.1, 8, ScrW() * 0.1, 0 )
	tuttext.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )

		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

		draw.SimpleText( "You've been given enough money to buy a starter crate!", "RobotoSmall", w / 2, h * 0.25, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
		draw.SimpleText( "Simply click your choice, push 'Buy Crate' and open that bad boy.", "RobotoSmall", w / 2, h * 0.75, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

	end

	BuildShop( welcomescreen, "starter", true )

end )

net.Receive( "ChangeTeam", function()

	if IsValid( LocalPlayer( ):GetActiveWeapon( ) ) and LocalPlayer( ):GetActiveWeapon( ).dt.State == CW_CUSTOMIZE then
		
		return 
			
	end
	
	TeamChangeMenu()
	
end )

local countdown = 0

net.Receive( "Loadout", function()

	if ( IsValid( LocalPlayer( ):GetActiveWeapon( ) ) and LocalPlayer( ):GetActiveWeapon( ).dt.State == CW_CUSTOMIZE ) or countdown and countdown > 0 then
		
		return 
			
	end

	if LocalPlayer( ):Team( ) == 1 or LocalPlayer( ):Team( ) == 2 then
		
		Loadout()
		
	end
	
end )

net.Receive( "ChatMessage", function()
	chat.AddText( Color( 230, 126, 34 ), "[", Color( 231, 76, 60 ), "Uprising", Color( 230, 126, 34 ), "] ", Color( 255, 255, 255 ), net.ReadString() )
end )

net.Receive( "DeathNotificationPart2", function( )

	local details = net.ReadTable( )
	local asshole = net.ReadEntity( )
	local silenced = net.ReadBool( )

	local rankmat = asshole:GetRankMaterial()
	local weapon = weapons.Get( asshole:GetActiveWeapon():GetClass() ).PrintName

	damageindicators = {}
	
	countdown = 5
	
	timer.Create( "DeathTimer", 1, 5, function( )
	
		countdown = countdown - 1
		
		if countdown == 0 and !roundEnded then

			Loadout()

		end

	end )

	hook.Remove( "HUDPaint", "ShowDeathNotificationStuff" )
	hook.Remove( "PreDrawHalos", "AddKillerHalo" )
	
	hook.Add( "HUDPaint", "ShowDeathNotificationStuff", function( )

		if silenced then

			surface.SetDrawColor( Color( 0, 0, 0, 255 ) )
			surface.DrawRect( 0, 0, ScrW(), ScrH() )

		end

		local badge, mark, strip = asshole:GetPlayerCard()

		if countdown > 0 then

			local basew = 512
			local whitecol = Color( 255, 255, 255, 255 )
			local textcol = whitecol
			local col = {}
			col[ 1 ] = Color( 75, 75, 85, 115 )
			col[ 2 ] = Color( 35, 35, 45, 115 )

			if asshole:IsPlayer() then

				textcol = col[ asshole:Team() ]

			else

				textcol = col[ 1 ]

			end

			surface.SetMaterial( Material( "uprising/playercards/".. playerCards.strips[ strip ] ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 256, 128 - 64, 512, 128 )

			surface.SetMaterial( Material( "uprising/playercards/".. playerCards.badges[ badge ] ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 256, 128 - 64, 128, 128 )

			surface.SetMaterial( Material( "uprising/playercards/".. playerCards.marks[ mark ] ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 256, 128 - 64, 128, 128 )

			surface.SetMaterial( rankmat )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 128, 128 - 32, 64, 64 )

			draw.SimpleTextOutlined( asshole:Name(), "RobotoBig", ScrW() * 0.5 - 64, 128, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 2, Color( 0, 0, 0 ) )
		
			draw.RoundedBox( 0, ScrW() * 0.5 - basew / 2, ScrH( ) - 100, basew, 100, Color( 0, 0, 0, 105 ) )
			draw.RoundedBox( 0, ScrW() * 0.5 - basew / 2, ScrH( ) - 128, basew, 128, Color( 0, 0, 0, 105 ) )
			
			draw.SimpleText( weapon, "RobotoSmall", ScrW() * 0.5, ScrH( ) - 114, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
			draw.SimpleText( "Health", "RobotoSmall", ScrW() * 0.5 - 128, ScrH( ) - 74, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( asshole:Health( ), "Roboto", ScrW() * 0.5 - 128, ScrH( ) - 42, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "You   ".. details.You, "RobotoSmall", ScrW() * 0.5 + 128, ScrH( ) - 74, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			draw.SimpleText( "Foe   ".. details.Foe, "RobotoSmall", ScrW() * 0.5 + 128, ScrH( ) - 42, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Respawn in: ".. countdown, "RobotoSmall", ScrW() * 0.5, ScrH( ) - 58, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			if !silenced then

				LocalPlayer( ):SetEyeAngles( ( asshole:GetPos( ) - LocalPlayer( ):GetPos( ) ):Angle( ) )
			
			end

		else

			surface.SetDrawColor( Color( 0, 0, 0, 255 ) )
			surface.DrawRect( 0, 0, ScrW(), ScrH() )

		end

	end )

	if !silenced and countdown > 0 and asshole:Alive() then

		hook.Add( "PreDrawHalos", "AddKillerHalo", function( )

			if asshole:Alive() then
		
				halo.Add( { asshole }, Color( 255, 0, 0 ), 0, 0, 2, true, true )

			end
			
		end )

	end
	
end )

hook.Add( "PreDrawHalos", "DrawTeamHalo", function( )

	local amendedplayers = {}

	if LocalPlayer():Team() == 1 or LocalPlayer():Team() == 2 then

		for k, v in pairs( team.GetPlayers( LocalPlayer():Team() ) ) do

			if v:Alive() then

				amendedplayers[ v:Name() ] = v

			end

		end

		halo.Add( amendedplayers, team.GetColor( LocalPlayer():Team() ), 0, 0, 2, true, false )

	end

end )

net.Receive( "DeathNotificationPart3", function( )

	damageindicators = {}
	
	countdown = 5

	local badge, mark, strip = LocalPlayer():GetPlayerCard()
	
	timer.Create( "DeathTimer", 1, 5, function( )
	
		countdown = countdown - 1
		
		if countdown == 0 and !roundEnded then

			Loadout()

		end

	end )

	hook.Remove( "HUDPaint", "ShowDeathNotificationStuff" )
	hook.Add( "HUDPaint", "ShowDeathNotificationStuff", function( )
	
		if countdown > 0 then

			local basew = 512
			local whitecol = Color( 255, 255, 255, 255 )

			surface.SetMaterial( Material( "uprising/playercards/".. playerCards.strips[ strip ] ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 256, 128 - 64, 512, 128 )

			surface.SetMaterial( Material( "uprising/playercards/".. playerCards.badges[ badge ] ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 256, 128 - 64, 128, 128 )

			surface.SetMaterial( Material( "uprising/playercards/".. playerCards.marks[ mark ] ) )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 256, 128 - 64, 128, 128 )

			surface.SetMaterial( LocalPlayer():GetRankMaterial() )
			surface.SetDrawColor( 255, 255, 255, 255 )
			surface.DrawTexturedRect( ScrW() * 0.5 - 128, 128 - 32, 64, 64 )

			draw.SimpleTextOutlined( LocalPlayer():Name(), "RobotoBig", ScrW() * 0.5 - 64, 128, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER, 2, Color( 0, 0, 0 ) )

			draw.RoundedBox( 0, ScrW() * 0.5 - basew / 2, ScrH( ) - 100, basew, 100, Color( 0, 0, 0, 105 ) )
			draw.RoundedBox( 0, ScrW() * 0.5 - basew / 2, ScrH( ) - 128, basew, 128, Color( 0, 0, 0, 105 ) )
			
			draw.SimpleText( "You made a mistake", "RobotoSmall", ScrW() * 0.5, ScrH( ) - 114, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
			
			draw.SimpleText( "Respawn in: ".. countdown, "RobotoSmall", ScrW() * 0.5, ScrH( ) - 74, whitecol, TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
					
		end
			
	end )
	
end )

net.Receive( "PlayerSpawn", function( )

	hook.Remove( "HUDPaint", "ShowDeathNotificationStuff" )
	hook.Remove( "PreDrawHalos", "AddKillerHalo" )

end )

net.Receive( "EndRoundDisplay", function( )

	local team1 = team.GetScore( 1 )
	local team2 = team.GetScore( 2 )
	local winner = 0
	local barw = 0
	local winalpha = 0

	hook.Remove( "HUDPaint", "ShowDeathNotificationStuff" )
	hook.Remove( "PreDrawHalos", "AddKillerHalo" )

	if team1 > team2 then

		winner = 1

	elseif team2 > team1 then

		winner = 2

	end

	local song

	if LocalPlayer():Team() != winner then

		song = "battlefield h/lose main.mp3"

	else

		song = "battlefield h/win main.mp3"

	end

	endsong = CreateSound( LocalPlayer(), song )
	endsong:PlayEx( 1, 100 )

	hook.Add( "HUDPaint", "ShowEndRoundScreen", function()

		barw = Lerp( 3 * FrameTime(), barw, 1.05 )
		winalpha = Lerp( 3 * FrameTime(), winalpha, 1.01 )

		surface.SetDrawColor( 15, 15, 15, 205 )
		surface.DrawRect( ScrW() * 0.5 - ( ScrW() * 0.5 * barw ), ScrH() * 0.25, ScrW() * barw, ScrH() * 0.5 )

		if winner == 1 then

			draw.SimpleText( "BLUFOR is victorious!", "WinFont", ScrW() * 0.5, ScrH() * 0.5, Color( 255, 255, 255, 255 * winalpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

		elseif winner == 2 then

			draw.SimpleText( "The OPFOR has won!", "WinFont", ScrW() * 0.5, ScrH() * 0.5, Color( 255, 255, 255, 255 * winalpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

		else

			draw.SimpleText( "Tie!", "WinFont", ScrW() * 0.5, ScrH() * 0.5, Color( 255, 255, 255, 255 * winalpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )

		end

	end )

end )

net.Receive( "OpenStage1", function()

	local weapon = net.ReadString()
	local weaponlist = LocalPlayer():GetBox( weapon ).weps
	local intendedwep = net.ReadString()
	local itemlist = {}
	local delay = 5 / 19
	local starttime = CurTime()
	local count = 0

	local raritycols = { Color( 115, 110, 255 ), Color( 220, 110, 215 ), Color( 220, 110, 150 ), Color( 180, 35, 35 ), Color( 218, 165, 32 ) }

	for i = 1, 20 do

		itemlist[ i ] = table.Random( weaponlist )

	end

	itemlist[ 19 ] = intendedwep

	local openwindow = vgui.Create( "DFrame" )
	openwindow:SetSize( ScrW() * 0.5, ScrH() * 0.5 )
	openwindow:Center()
	openwindow:MakePopup( )
	openwindow.Paint = function( panel, w, h )

		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 0, 1, w, h - 2 )
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h )

	end

	local wepmodel = openwindow:Add( "DModelPanel" )
	wepmodel:Dock( TOP )
	wepmodel:SetTall( openwindow:GetTall() * 0.5 )
	wepmodel.LayoutEntity = function() end

	local scrollbottom = openwindow:Add( "DPanel" )
	scrollbottom:Dock( BOTTOM )
	scrollbottom:SetTall( openwindow:GetTall() * 0.5 )
	scrollbottom.Paint = function( panel, w, h )

	end

	box1 = scrollbottom:Add( "DPanel" )
	box1:Dock( LEFT )
	box1:SetWide( openwindow:GetWide() / 3 )
	box1.id = 1
	box1.rarity = 0
	box1.Paint = function( panel, w, h )

	end

	local weapon1 = box1:Add( "DPanel" )
	weapon1:Dock( TOP )
	weapon1:SetTall( scrollbottom:GetTall() )
	weapon1.PaintOver = function( panel, w, h )
				
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h - 1 )

	end

	weapon1.Paint = function( panel, w, h )

		surface.SetDrawColor( 55, 55, 65, 55 )
		surface.DrawRect( 0, 0, w, h )
			
	end
				
	local wname1 = weapon1:Add( "DPanel" )
	wname1:Dock( TOP )
	wname1:SetTall( weapon1:GetTall( ) * 0.2 )
	wname1.Paint = function( panel, w, h )
				
		surface.SetDrawColor( raritycols[ box1.rarity + 1 ] )
		surface.DrawOutlinedRect( 0, 0, w, h )
				
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 2 )
					
	end
				
	local wname1text = wname1:Add( "DLabel" )
	wname1text:Dock( FILL )
	wname1text:SetText( itemlist[ box1.id ] )
	wname1text:SetContentAlignment( 5 )
	wname1text:SetFont( "HUD" )

	local weaponmodel1 = weapon1:Add( "DModelPanel" )
	weaponmodel1:Dock( FILL )
	weaponmodel1:DockMargin( 0, 0, 0, 1 )	
	weaponmodel1:SetModel( Model( "models/items/item_item_crate.mdl" ) )

	local viewmins, viewmaxs = weaponmodel1.Entity:GetRenderBounds( )
			
	weaponmodel1:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 1, 0 ) )
	weaponmodel1:SetLookAt( ( viewmaxs + viewmins ) / 2 )
	weaponmodel1.LayoutEntity = function( )
	
	end
	weaponmodel1:SetFOV( 45 )

	box2 = scrollbottom:Add( "DPanel" )
	box2:Dock( LEFT )
	box2:SetWide( openwindow:GetWide() / 3 )
	box2.id = 2
	box2.rarity = 0
	box2.Paint = function( panel, w, h )

	end

	local weapon2 = box2:Add( "DPanel" )
	weapon2:Dock( TOP )
	weapon2:SetTall( scrollbottom:GetTall() )
	weapon2.winner = false
	weapon2.PaintOver = function( panel, w, h )
				
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h - 1 )

	end

	weapon2.Paint = function( panel, w, h )

		if panel.winner then
			
			surface.SetDrawColor( 55, 85, 65, 55 )

		else

			surface.SetDrawColor( 55, 55, 65, 55 )

		end

		surface.DrawRect( 0, 0, w, h )
			
	end
				
	local wname2 = weapon2:Add( "DPanel" )
	wname2:Dock( TOP )
	wname2:SetTall( weapon2:GetTall( ) * 0.2 )
	wname2.Paint = function( panel, w, h )
				
		surface.SetDrawColor( raritycols[ box2.rarity + 1 ] )
		surface.DrawOutlinedRect( 0, 0, w, h )
				
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 2 )
					
	end
				
	local wname2text = wname2:Add( "DLabel" )
	wname2text:Dock( FILL )
	wname2text:SetText( itemlist[ box2.id ] )
	wname2text:SetContentAlignment( 5 )
	wname2text:SetFont( "HUD" )

	local weaponmodel2 = weapon2:Add( "DModelPanel" )
	weaponmodel2:Dock( FILL )
	weaponmodel2:DockMargin( 0, 0, 0, 1 )	
	weaponmodel2:SetModel( Model( "models/items/item_item_crate.mdl" ) )
	weaponmodel2:SetFOV( 45 )

	local viewmins, viewmaxs = weaponmodel2.Entity:GetRenderBounds( )
			
	weaponmodel2:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 1, 0 ) )
	weaponmodel2:SetLookAt( ( viewmaxs + viewmins ) / 2 )
	weaponmodel2.LayoutEntity = function( )
	
	end

	box3 = scrollbottom:Add( "DPanel" )
	box3:Dock( LEFT )
	box3:SetWide( openwindow:GetWide() / 3 )
	box3.id = 3
	box3.rarity = 0
	box3.Paint = function( panel, w, h )

	end

	local weapon3 = box3:Add( "DPanel" )
	weapon3:Dock( TOP )
	weapon3:SetTall( scrollbottom:GetTall() )
	weapon3.PaintOver = function( panel, w, h )
				
		surface.SetDrawColor( 125, 125, 125, 255 )
		surface.DrawOutlinedRect( 0, 0, w, h - 1 )

	end

	weapon3.Paint = function( panel, w, h )

		if panel.winner then
			
			surface.SetDrawColor( 55, 85, 65, 55 )

		else

			surface.SetDrawColor( 55, 55, 65, 55 )

		end

		surface.DrawRect( 0, 0, w, h )
			
	end
				
	local wname3 = weapon3:Add( "DPanel" )
	wname3:Dock( TOP )
	wname3:SetTall( weapon3:GetTall( ) * 0.2 )
	wname3.Paint = function( panel, w, h )
				
		surface.SetDrawColor( raritycols[ box3.rarity + 1 ] )
		surface.DrawOutlinedRect( 0, 0, w, h )
		
		surface.SetDrawColor( 35, 35, 35, 155 )
		surface.DrawRect( 1, 1, w - 2, h - 2 )
					
	end
				
	local wname3text = wname3:Add( "DLabel" )
	wname3text:Dock( FILL )
	wname3text:SetText( itemlist[ box3.id ] )
	wname3text:SetContentAlignment( 5 )
	wname3text:SetFont( "HUD" )

	local weaponmodel3 = weapon3:Add( "DModelPanel" )
	weaponmodel3:Dock( FILL )
	weaponmodel3:DockMargin( 0, 0, 0, 1 )	
	weaponmodel3:SetModel( Model( "models/items/item_item_crate.mdl" ) )
	weaponmodel3:SetFOV( 45 )

	local viewmins, viewmaxs = weaponmodel3.Entity:GetRenderBounds( )
			
	weaponmodel3:SetCamPos( viewmins:Distance( viewmaxs ) * Vector( 0.25, 1, 0 ) )
	weaponmodel3:SetLookAt( ( viewmaxs + viewmins ) / 2 )
	weaponmodel3.LayoutEntity = function( )
	
	end

	for i = 1, 17 do

		timer.Simple( delay * i, function()

			box1.id = box1.id + 1
			box2.id = box2.id + 1
			box3.id = box3.id + 1

			if weapons.Get( itemlist[ box1.id ] ) then

				wname1text:SetText( weapons.Get( itemlist[ box1.id ] ).PrintName )
				box1.rarity = GetWep( itemlist[ box1.id ] ).rarity

				weaponmodel1:SetModel( weapons.Get( itemlist[ box1.id ] ).WorldModel )

				if weaponmodel1.Entity:LookupBone( "ValveBiped.weapon_bone" ) then

					local modelpos = weaponmodel1.Entity:GetBonePosition( weaponmodel1.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
					local lookat = Vector( 3, 0, 4 )
					local campos = Vector( 3, 25, 9 )

					if modeloffsets[ itemlist[ box1.id ] ] then
						
						weaponmodel1:SetLookAt( modelpos - modeloffsets[ itemlist[ box1.id ] ].lookat )
						weaponmodel1:SetCamPos( modelpos - modeloffsets[ itemlist[ box1.id ] ].campos )

					else

						weaponmodel1:SetLookAt( modelpos - lookat )
						weaponmodel1:SetCamPos( modelpos - campos )

					end

				end

			else

				wname1text:SetText( itemlist[ box1.id ] )

			end


			if weapons.Get( itemlist[ box2.id ] ) then

				wname2text:SetText( weapons.Get( itemlist[ box2.id ] ).PrintName )
				box2.rarity = GetWep( itemlist[ box2.id ] ).rarity

				weaponmodel2:SetModel( weapons.Get( itemlist[ box2.id ] ).WorldModel )

				if weaponmodel2.Entity:LookupBone( "ValveBiped.weapon_bone" ) then

					local modelpos = weaponmodel2.Entity:GetBonePosition( weaponmodel2.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
					local lookat = Vector( 3, 0, 4 )
					local campos = Vector( 3, 25, 9 )

					if modeloffsets[ itemlist[ box2.id ] ] then
						
						weaponmodel2:SetLookAt( modelpos - modeloffsets[ itemlist[ box2.id ] ].lookat )
						weaponmodel2:SetCamPos( modelpos - modeloffsets[ itemlist[ box2.id ] ].campos )

					else

						weaponmodel2:SetLookAt( modelpos - lookat )
						weaponmodel2:SetCamPos( modelpos - campos )

					end

				end

			else

				wname2text:SetText( itemlist[ box2.id ] )

			end

			if weapons.Get( itemlist[ box3.id ] ) then

				wname3text:SetText( weapons.Get( itemlist[ box3.id ] ).PrintName )
				box3.rarity = GetWep( itemlist[ box3.id ] ).rarity

				weaponmodel3:SetModel( weapons.Get( itemlist[ box3.id ] ).WorldModel )

				if weaponmodel3.Entity:LookupBone( "ValveBiped.weapon_bone" ) then

					local modelpos = weaponmodel3.Entity:GetBonePosition( weaponmodel3.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
					local lookat = Vector( 3, 0, 4 )
					local campos = Vector( 3, 25, 9 )

					if modeloffsets[ itemlist[ box3.id ] ] then
						
						weaponmodel3:SetLookAt( modelpos - modeloffsets[ itemlist[ box3.id ] ].lookat )
						weaponmodel3:SetCamPos( modelpos - modeloffsets[ itemlist[ box3.id ] ].campos )

					else

						weaponmodel3:SetLookAt( modelpos - lookat )
						weaponmodel3:SetCamPos( modelpos - campos )

					end

				end

			else

				wname3text:SetText( itemlist[ box3.id ] )

			end

		end )

	end

	timer.Simple( delay * 17, function() 

		weapon2.winner = true

		if weapons.Get( itemlist[ box3.id ] ) then

			wname3text:SetText( weapons.Get( itemlist[ box3.id ] ).PrintName )
			box3.rarity = GetWep( itemlist[ box3.id ] ).rarity

			weaponmodel3:SetModel( weapons.Get( itemlist[ box3.id ] ).WorldModel )

			if weaponmodel3.Entity:LookupBone( "ValveBiped.weapon_bone" ) then

				local modelpos = weaponmodel3.Entity:GetBonePosition( weaponmodel3.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
				local lookat = Vector( 3, 0, 4 )
				local campos = Vector( 3, 25, 9 )

				if modeloffsets[ itemlist[ box3.id ] ] then
					
					weaponmodel3:SetLookAt( modelpos - modeloffsets[ itemlist[ box3.id ] ].lookat )
					weaponmodel3:SetCamPos( modelpos - modeloffsets[ itemlist[ box3.id ] ].campos )

				else

					weaponmodel3:SetLookAt( modelpos - lookat )
					weaponmodel3:SetCamPos( modelpos - campos )

				end

			end

		else

			wname3text:SetText( itemlist[ box3.id ] )

		end

		if weapons.Get( intendedwep ) then

			local weapontbl = weapons.Get( intendedwep )

			if weapontbl.WM then
							
				wepmodel:SetModel( weapontbl.WM )
						
			elseif weapontbl.WorldModel then
					
				wepmodel:SetModel( weapontbl.WorldModel )
						
			else
					
				wepmodel:SetModel( Model( "models/items/item_item_crate.mdl" ) )
						
			end

			if wepmodel.Entity:LookupBone( "ValveBiped.weapon_bone" ) then

				local modelpos = wepmodel.Entity:GetBonePosition( wepmodel.Entity:LookupBone( "ValveBiped.weapon_bone" ) )
				local lookat = Vector( 3, 0, 4 )
				local campos = Vector( 3, 25, 9 )

				if modeloffsets[ intendedwep ] then
					
					wepmodel:SetLookAt( modelpos - modeloffsets[ intendedwep ].lookat )
					wepmodel:SetCamPos( modelpos - modeloffsets[ intendedwep ].campos )

				else

					wepmodel:SetLookAt( modelpos - lookat )
					wepmodel:SetCamPos( modelpos - campos )

				end

			end

		end

	end )

end )

net.Receive( "GetArmsRaceTable", function( )

	ArmsRaceTable = net.ReadTable( )
	
end )

net.Receive( "DamageIndicator", function( )

	local hitpos = net.ReadVector( )

	local pos = LocalPlayer():GetPos()
	local angles = LocalPlayer():EyeAngles()

	if angles.y < -90 and angles.y > 90 then

		angles.y = angles.y + 360

	elseif angles.y > 90 and angles.y < -90 then

		angles.y = angles.y - 360

	end

	local vector = hitpos - pos
	local ang = vector:Angle()

	local ttl = 4

	table.insert( damageindicators, { ang.y - angles.y, ttl } )

end )

net.Receive( "GotMoney", function()

	local stringpart1 = net.ReadString()
	local stringpart2 = net.ReadString()
	local weapon = net.ReadString()

	if weapons.Get( weapon ) then

		LocalPlayer():ChatPrint( stringpart1.. "" ..weapons.Get( weapon ).PrintName.. "" ..stringpart2 )

	else

		LocalPlayer():ChatPrint( stringpart1.. "" ..weapon.. "" ..stringpart2 )

	end

end )

net.Receive( "GotGun", function()

	local stringpart1 = net.ReadString()
	local stringpart2 = net.ReadString()
	local weapon = net.ReadString()

	if weapons.Get( weapon ) then

		LocalPlayer():ChatPrint( stringpart1.. "" ..weapons.Get( weapon ).PrintName.. "" ..stringpart2 )

	else

		LocalPlayer():ChatPrint( stringpart1.. "" ..weapon.. "" ..stringpart2 )

	end

end )

net.Receive( "BeginTension", function()

	local song

	if team.GetScore( LocalPlayer():Team() ) < team.GetScore( LocalPlayer():OppositeTeam() ) then

		song = "battlefield h/lose tension.mp3"

	else

		song = "battlefield h/win tension.mp3"

	end

	tensionsong = CreateSound( LocalPlayer(), song )
	tensionsong:PlayEx( 1, 100 )

end )

concommand.Add( "getpos2", function()
	local pos = LocalPlayer():GetPos()
	print( "Vector( " .. pos.x .. ", " .. pos.y .. ", " .. pos.z .. " )" )
end )