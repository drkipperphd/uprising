WeaponSkins = {}

local textures = {
	
	[ "artofwar" ] = {},

	[ "atacs" ] = {},

	[ "aqua" ] = {},

	[ "benjamins" ] = {},

	[ "bloodshot" ] = {},

	[ "blossom" ] = {},

	[ "bo2collectors" ] = {},

	[ "breach" ] = {},

	[ "bricks" ] = {},

	[ "choco" ] = {},

	[ "comic" ] = {},

	[ "coyote" ] = {},

	[ "dayofdead" ] = {},

	[ "delta6" ] = {},

	[ "devgru" ] = {},

	[ "elite" ] = {},

	[ "erdl" ] = {},

	[ "flecktarn" ] = {},

	[ "flora" ] = {},

	[ "german" ] = {},

	[ "ghost" ] = {},

	[ "graffiti" ] = {},

	[ "jungle_rus" ] = {},

	[ "jungle_us" ] = {},

	[ "kryptek_typhon" ] = {},

	[ "mango" ] = {},

	[ "massacre" ] = {},

	[ "nevada" ] = {},

	[ "partyrock" ] = {},

	[ "ronin" ] = {},

	[ "sahara" ] = {},

	[ "siberia" ] = {},

	[ "skulls" ] = {},

	[ "tigerblue" ] = {},

	[ "tigerjungle" ] = {},

	[ "ukpunk" ] = {},

	[ "scotpunk" ] = {},

	[ "urban_polish" ] = {},

	[ "urban_rus" ] = {},

	[ "urban_russia" ] = {},

	[ "viper" ] = {},

	[ "zebra" ] = {},

	[ "zombies" ] = {},

	[ "zulu" ] = {},

}


textures[ "artofwar" ].name = "camo_artofwar_pattern"
textures[ "artofwar" ].price = 20000
textures[ "atacs" ].name = "camo_atacs_pattern"
textures[ "atacs" ].price = 1000
textures[ "aqua" ].name = "camo_mtx3_aqua_pat"
textures[ "aqua" ].price = 20000
textures[ "benjamins" ].name = "camo_benjamins_pattern"
textures[ "benjamins" ].price = 25000
textures[ "bloodshot" ].name = "camo_bloodshot_pattern"
textures[ "bloodshot" ].price = 2000
textures[ "blossom" ].name = "camo_blossom_pattern"
textures[ "blossom" ].price = 5000
textures[ "bo2collectors" ].name = "camo_bo2collectors_pattern"
textures[ "bo2collectors" ].price = 10000
textures[ "breach" ].name = "camo_mtx3_breach_pat"
textures[ "breach" ].price = 10000
textures[ "bricks" ].name = "camo_bricks_solid"
textures[ "bricks" ].price = 2000
textures[ "choco" ].name = "camo_choco_pattern"
textures[ "choco" ].price = 2000
textures[ "comic" ].name = "camo_comic_pattern"
textures[ "comic" ].price = 20000
textures[ "coyote" ].name = "camo_mtx3_coyote_pat"
textures[ "coyote" ].price = 5000
textures[ "dayofdead" ].name = "camo_dayofdead_pattern"
textures[ "dayofdead" ].price = 10000
textures[ "delta6" ].name = "camo_ghostex_delta6_pattern"
textures[ "delta6" ].price = 2000
textures[ "devgru" ].name = "camo_devgru_pattern"
textures[ "devgru" ].price = 2000
textures[ "elite" ].name = "camo_elite_pattern"
textures[ "elite" ].price = 10000
textures[ "erdl" ].name = "camo_erdl_pattern"
textures[ "erdl" ].price = 2000
textures[ "flecktarn" ].name = "camo_flecktarn_pattern"
textures[ "flecktarn" ].price = 2000
textures[ "flora" ].name = "camo_flora_pattern"
textures[ "flora" ].price = 2000
textures[ "german" ].name = "camo_german"
textures[ "german" ].price = 2000
textures[ "ghost" ].name = "camo_ghost_pattern"
textures[ "ghost" ].price = 20000
textures[ "graffiti" ].name = "camo_graffiti_pattern"
textures[ "graffiti" ].price = 15000
textures[ "jungle_rus" ].name = "camo_jungle_rus"
textures[ "jungle_rus" ].price = 2000
textures[ "jungle_us" ].name = "camo_jungle_us"
textures[ "jungle_us" ].price = 2000
textures[ "kryptek_typhon" ].name = "camo_kryptek_typhon_pattern"
textures[ "kryptek_typhon" ].price = 5000
textures[ "mango" ].name = "camo_mango_pattern"
textures[ "mango" ].price = 2000
textures[ "massacre" ].name = "camo_massacre"
textures[ "massacre" ].price = 5000
textures[ "nevada" ].name = "camo_nevada_pattern"
textures[ "nevada" ].price = 2000
textures[ "partyrock" ].name = "camo_partyrock_pattern"
textures[ "partyrock" ].price = 10000
textures[ "ronin" ].name = "camo_ronin_pattern"
textures[ "ronin" ].price = 10000
textures[ "sahara" ].name = "camo_sahara_pattern"
textures[ "sahara" ].price = 2000
textures[ "siberia" ].name = "camo_siberia_pattern"
textures[ "siberia" ].price = 2000
textures[ "skulls" ].name = "camo_skulls_pattern"
textures[ "skulls" ].price = 10000
textures[ "tigerblue" ].name = "camo_tiger_blue_pattern"
textures[ "tigerblue" ].price = 2000
textures[ "tigerjungle" ].name = "camo_tiger_jungle_pattern"
textures[ "tigerjungle" ].price = 2000
textures[ "ukpunk" ].name = "camo_ukpunk_pattern"
textures[ "ukpunk" ].price = 10000
textures[ "scotpunk" ].name = "camo_ukpunk_solid"
textures[ "scotpunk" ].price = 10000
textures[ "urban_polish" ].name = "camo_urban_polish"
textures[ "urban_polish" ].price = 2000
textures[ "urban_rus" ].name = "camo_urban_rus"
textures[ "urban_rus" ].price = 2000
textures[ "urban_russia" ].name = "camo_urban_russia_pattern"
textures[ "urban_russia" ].price = 2000
textures[ "viper" ].name = "camo_viper_pattern"
textures[ "viper" ].price = 5000
textures[ "zebra" ].name = "camo_partyrock_solid"
textures[ "zebra" ].price = 10000
textures[ "zombies" ].name = "camo_zombies_pattern"
textures[ "zombies" ].price = 20000
textures[ "zulu" ].name = "camo_mtx3_zulu_pat"
textures[ "zulu" ].price = 200

function RegisterSkin( weapon, id, texture, submaterials )

	if !WeaponSkins[ weapon ] then

		WeaponSkins[ weapon ] = {}

	end

	WeaponSkins[ weapon ][ id ] = {}
	WeaponSkins[ weapon ][ id ].texture = texture
	WeaponSkins[ weapon ][ id ].submaterials = submaterials or 0

end

function AutoRegSkins( weapon, submaterials )

	if !WeaponSkins[ weapon ] then

		WeaponSkins[ weapon ] = {}

	end

	for k, v in pairs( textures ) do

		WeaponSkins[ weapon ][ k ] = {}
		WeaponSkins[ weapon ][ k ].texture = "uprising/camoflagues/".. v.name
		WeaponSkins[ weapon ][ k ].submaterials = submaterials or 0
		WeaponSkins[ weapon ][ k ].price = v.price or 0

	end

end

-- Pistols
AutoRegSkins( "draggo_p226", 0 )
AutoRegSkins( "draggo_glockp80", 0 )
AutoRegSkins( "draggo_pl14", 5 )
AutoRegSkins( "draggo_usp", 1 )
AutoRegSkins( "draggo_p99", 2 )
AutoRegSkins( "draggo_python", 2 )
AutoRegSkins( "draggo_unica6", 0 )

-- Carbines
AutoRegSkins( "draggo_m4a1", 0 )
AutoRegSkins( "draggo_ak5c", 0 )
AutoRegSkins( "draggo_g36c", {2,4} )

-- Shotguns
AutoRegSkins( "draggo_870", 0 )
AutoRegSkins( "draggo_model1887", 2 )
AutoRegSkins( "draggo_spas12", {0,1} )
AutoRegSkins( "draggo_870mcs", 0 )
AutoRegSkins( "draggo_usas12", 0 )
AutoRegSkins( "draggo_ksg", 2 )

-- Assault Rifles
AutoRegSkins( "draggo_ak74m", 2 )
AutoRegSkins( "draggo_cm901", 0 )
AutoRegSkins( "draggo_l85a2", 0 )
AutoRegSkins( "draggo_g3a3", 0 )
AutoRegSkins( "draggo_famasf1", 0 )
AutoRegSkins( "draggo_galil", 2 )
AutoRegSkins( "draggo_msbs", 0 )
AutoRegSkins( "draggo_scarh", 0 )
AutoRegSkins( "draggo_auga1", 0 )
AutoRegSkins( "draggo_masada", 0 )
AutoRegSkins( "draggo_garand", 0 )

-- Personal Defense Weapons
AutoRegSkins( "draggo_mp5", 0 )
AutoRegSkins( "draggo_cbjms", 0 )
AutoRegSkins( "draggo_p90", 0 )
AutoRegSkins( "draggo_bizon", 1 )
AutoRegSkins( "draggo_vector", 1 )
AutoRegSkins( "draggo_mp7", 0 )

-- Light Machine Guns
AutoRegSkins( "draggo_rpk", 0 )
AutoRegSkins( "draggo_mg4", {1, 4} )

-- Snipers
AutoRegSkins( "draggo_dragunov", 0 )
AutoRegSkins( "draggo_awm", 0 )
AutoRegSkins( "draggo_m98b", 0 )
AutoRegSkins( "draggo_sr25", 2 )
AutoRegSkins( "draggo_intervenion", 0 )