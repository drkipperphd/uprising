net.Receive( "SyncSkins", function()

	ownedskins = util.JSONToTable( net.ReadString() )

end )


local PANEL = {}

PANEL.SelectedAtts = {}
PANEL.Weapon = nil
PANEL.DrawnAtts = {}

function PANEL:SetWeapon( class )

	if weapons.Get( class ) then

		self.Weapon = weapons.Get( class )
		self:SetModel( self.Weapon.VModel or self.Weapon.ViewModel )

	end

end

function PANEL:SetAttachment( cat, id )

	if !self.SelectedAtts[ cat ] then

		self.SelectedAtts[ cat ] = {}

	end

	self.SelectedAtts[ cat ].owner = self.Entity
	self.SelectedAtts[ cat ].id = id

	PrintTable( self.SelectedAtts )

end

function PANEL:RemoveAttachment( cat, id )

	if self.DrawnAtts and self.DrawnAtts[ cat ] then

		self.DrawnAtts[ cat ].att = nil

	end

end

function PANEL:ApplyWepSkin( texture, submaterials )

	if type( submaterials ) == "table" then

		for k, v in pairs( submaterials ) do

			self.Entity:SetSubMaterial( k, texture )

		end

	else

		self.Entity:SetSubMaterial( submaterials, texture )

	end

end

function PANEL:DrawModel( )

	local hidemats = false

	if !hidemats then

		for k, v in pairs( self.Entity:GetMaterials() ) do

			if string.find( v, "v_hands" ) or string.find( v, "hands" ) or string.find( v, "sleeve_diffuse" ) or string.find( v, "sleeve" ) or string.find( v, "sleeves" ) or string.find( v, "t_phoenix" ) then

				self.Entity:SetSubMaterial( k - 1, "uprising/gone" )

			end

		end

		hidemats = true

	end

	for k, v in pairs( self.SelectedAtts ) do

		if !self.DrawnAtts[ k ] or self.DrawnAtts[ k ].id != v.id then

			local data = self.Weapon.AttachmentModelsVM[ v.id ]

			if data then

				local att = ents.CreateClientProp()
				local m = self.Entity:GetBoneMatrix( self.Entity:LookupBone( data.bone ) )
				if m then
					local pos, ang = m:GetTranslation(), m:GetAngles()

					att:SetModel( data.model )

					att:SetPos(pos + ang:Forward() * data.pos.x + ang:Right() * data.pos.y + ang:Up() * data.pos.z)

					ang:RotateAroundAxis(ang:Up(), data.angle.y)
					ang:RotateAroundAxis(ang:Right(), data.angle.p)
					ang:RotateAroundAxis(ang:Forward(), data.angle.r)

					att:SetAngles(ang)
					att:SetModelScale( data.size.x, 0 )
					att:SetParent( self.Entity )
					att.ID = v.id
					att:Spawn()
					att:SetNoDraw( true )

					self.DrawnAtts[ k ] = {}
					self.DrawnAtts[ k ].id = v.id
					self.DrawnAtts[ k ].owner = v.owner
					self.DrawnAtts[ k ].att = att

				end

			end

		end

	end

	for k, v in pairs( self.DrawnAtts ) do

		local has = false

		for k2, v2 in pairs( self.Entity:GetChildren() ) do

			if v2.ID and v2.ID == v.id then

				has = true

			end

		end

		if IsValid( v.att ) then

			if has and v.owner == self.Entity then

				v.att:DrawModel()

			else

				v.att:SetNoDraw( true )

			end

		end

	end

	local curparent = self
	local rightx = self:GetWide()
	local leftx = 0
	local topy = 0
	local bottomy = self:GetTall()
	local previous = curparent
	while( curparent:GetParent() != nil ) do
		curparent = curparent:GetParent()
		local x, y = previous:GetPos()
		topy = math.Max( y, topy + y )
		leftx = math.Max( x, leftx + x )
		bottomy = math.Min( y + previous:GetTall(), bottomy + y )
		rightx = math.Min( x + previous:GetWide(), rightx + x )
		previous = curparent
	end
	render.SetScissorRect( leftx, topy, rightx, bottomy, true )

	local ret = self:PreDrawModel( self.Entity )
	if ( ret != false ) then
		self.Entity:DrawModel()
		self:PostDrawModel( self.Entity )
	end

	render.SetScissorRect( 0, 0, 0, 0, false )
	
end

function PANEL:OnRemove()

	if self.DrawnAtts then

		for k, v in pairs( self.DrawnAtts ) do

			if IsValid( v.att ) then

				v.att:Remove()

			end

		end

	end

	self.DrawnAtts = {}
	self.SelectedAtts = {}

end

vgui.Register( "VModelPanel", PANEL, "DModelPanel" )


function GetSkins()

	return ownedskins

end

local function GenerateSkinName( weapon, id, texture )

	local steamid64 = LocalPlayer():SteamID64()
	local finishedstring = ""

	finishedstring = weapon:GetClass() .. "_" .. id .. "_" .. steamid64

	return finishedstring

end

function ApplySkin( weapon, id, texture, submaterial )

	local weapon = LocalPlayer():GetWeapon( weapon )
	local viewmodel = weapon.CW_VM

	if IsValid( viewmodel ) and IsValid( weapon ) then

		local matdetails

		if type( submaterial ) == "table" then

			matdetails = Material( viewmodel:GetMaterials()[ submaterial[ 1 ] + 1 ] ):GetKeyValues()
		
		else

			matdetails = Material( viewmodel:GetMaterials()[ submaterial + 1 ] ):GetKeyValues()

		end

		local newdetails = {}

		for k, v in pairs( matdetails ) do

			newdetails[ k ] = v

			if type( v ) == "ITexture" then

				newdetails[ k ] = v:GetName()

			end

		end

		local skinname = GenerateSkinName( weapon, id, texture )

		local newmaterial = CreateMaterial( skinname, "VertexLitGeneric", newdetails )

		newmaterial:SetTexture( "$basetexture", texture )
		newmaterial:SetTexture( "$envmap", "uprising/gone" )

		if type( submaterial ) == "table" then

			for k, v in pairs( submaterial ) do

				viewmodel:SetSubMaterial( v, "!".. skinname )

			end

		else

			viewmodel:SetSubMaterial( submaterial, "!".. skinname )

		end

	else

		weapon = LocalPlayer():GetWeapon( weapon )
		viewmodel = weapon.CW_VM

		timer.Simple( 1, function() 

			ApplySkin( weapon, id, texture, submaterial )

		end )

	end

end

net.Receive( "PlayerSkins", function( )

	local ply = net.ReadEntity()
	local primaryskin = net.ReadString()
	local secondaryskin = net.ReadString()

	local primaryweapon = ply:GetPrimary()
	local secondaryweapon = ply:GetSecondary()

	local skintable = nil

	if primaryskin != "" then

		skintable = WeaponSkins[ primaryweapon:GetClass() ][ primaryskin ]

		if ply == LocalPlayer() then

			ApplySkin( LocalPlayer():GetPrimary():GetClass(), primaryskin, skintable.texture, skintable.submaterials )

		end

		primaryweapon.WMEnt:SetMaterial( skintable.texture )

	end

	if secondaryskin != "" then

		skintable = WeaponSkins[ secondaryweapon:GetClass() ][ secondaryskin ]

		if ply == LocalPlayer() then

			ApplySkin( LocalPlayer().secondary, secondaryskin, skintable.texture, skintable.submaterials )

		end

		secondaryweapon.WMEnt:SetMaterial( skintable.texture )

	end

end )