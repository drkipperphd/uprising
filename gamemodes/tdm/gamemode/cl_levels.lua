surface.CreateFont( "ValidateFont", {
	font = "Roboto Cn",
	weight = 500,
	size = 100
} )

surface.CreateFont( "BarFont", {
	font = "Roboto Cn",
	weight = 500,
	size = ScreenScale( 4 )
} )

if ScrH( ) <= 1080 then

	surface.CreateFont( "HUD2", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 10 )
	} )

else

	surface.CreateFont( "HUD2", {
		font = "Roboto Cn",
		weight = 500,
		size = ScreenScale( 7 )
	} )

end

net.Receive( "LevelUp", function()

	local ply = net.ReadEntity()
	chat.AddText( Color( 26, 188, 156 ), ply, " just leveled up! They are now level " .. ( ply:GetLevel() + 1 ) .. "." )

end )

-- icon types, 1: kill, 2: headshot, 3: assist, 4: streaks, 5: shutdown, 6: grenade, 7: victory!

local xptable = {}
local xpadds = {}
local killicons = { Material( "uprising/killicons/icon_kill.png" ), Material( "uprising/killicons/icon_headshot.png" ), Material( "uprising/killicons/icon_assist.png"), Material( "uprising/killicons/killicon_grenade.png" ), Material( "uprising/killicons/victoryicon.png" ) }
local dkill = Material( "uprising/killicons/icon_dkill.png" )
local tkill = Material( "uprising/killicons/icon_tkill.png" )
local megakills = Material( "uprising/killicons/icon_starkill.png" )
local shutdown = Material( "uprising/killicons/icon_shutdown.png" )
local killstreaks = {}

for i = 1, 10 do

	killstreaks[ i ] = Material( "uprising/killicons/icon_".. i .."kill.png" )

end

PrintTable( killstreaks )

net.Receive( "AddXP", function()

	local details = net.ReadTable()

	if #xptable >= 1 then

		details.targetxp = xptable[ #xptable ].targetxp + details.xp
		details.targetalpha = 255
		details.alpha = xptable[ #xptable ].alpha
		details.xp = xptable[ #xptable ].targetxp

	else

		details.targetxp = details.xp
		details.targetalpha = 255
		details.alpha = 0
		details.xp = 0

	end

	if details.type == 1 or details.type == 2 then

		surface.PlaySound( "uprising/ui_kill.wav" )

	end

	table.Empty( xptable )

	table.insert( xptable, details )

	timer.Simple( 3, function()

		details.targetalpha = 0

	end )

end )

local xp = 0
local alpha = 0

surface.SetFont( "HUD2" )
local th = select( 2, surface.GetTextSize( "A" ) )

-- Originally just meant for the level bar, but now it manages all the kill icons and the text that comes with, haha!
hook.Add( "HUDPaint", "DrawLevelBar", function()

	xpFrac = math.Remap( LocalPlayer():GetXP(), levels.XPFromLevel( LocalPlayer():GetLevel() ), levels.XPFromLevel( LocalPlayer():GetLevel() + 1 ), 0, 1 )
	
	local target = math.Clamp( xpFrac, 0, 1 )
	xp = Lerp( 2.5 * FrameTime(), xp, target )

	draw.RoundedBox( 0, ScrW() / 2 - 160, 20, 320, 8, Color( 55, 55, 55, 105 ) )
	draw.RoundedBox( 0, ScrW() / 2 - 160, 20, 320 * xp, 8, Color( 39, 74, 96 ) )
	
	local curXP = (  LocalPlayer():GetXP() - levels.XPFromLevel( LocalPlayer():GetLevel() ) )
	local nextXP = ( levels.XPFromLevel( LocalPlayer():GetLevel() + 1 ) - levels.XPFromLevel( LocalPlayer():GetLevel() ) )
	
	draw.SimpleText( math.abs( curXP ) .. "/" .. nextXP, "BarFont", ScrW() / 2, 24, Color( 255, 255, 255 ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER )
	draw.SimpleText( LocalPlayer():GetLevel(), "BarFont", ScrW() / 2 - 158, 24, Color( 255, 255, 255 ), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER )
	if LocalPlayer():GetLevel() != levels.Cap then
		draw.SimpleText( LocalPlayer():GetLevel() + 1, "BarFont", ScrW() / 2 + 158, 24, Color( 255, 255, 255 ), TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER )
	end

	if #xptable > 0 then

		local v = xptable[ #xptable ]

		local h = 208

		v.alpha = math.Approach( v.alpha, v.targetalpha, FrameTime() * 600 )
		v.xp = math.Approach( v.xp, v.targetxp, FrameTime() * 40 )

		-- If it even has a type, check which one and grab the texture!
		if v.type then

			if v.type == 1 then

				surface.SetMaterial( killicons[ v.type ] )

			elseif v.type == 2 then

				surface.SetMaterial( killicons[ v.type ] )

			elseif v.type == 3 then

				surface.SetMaterial( killicons[ v.type ] )

			elseif v.type == 4 then

				if v.kills == 2 then

					surface.SetMaterial( dkill )

				elseif v.kills == 3 then

					surface.SetMaterial( tkill )

				-- As long as the kills are between 4 and 10, it'll show the special number.
				-- If you get over ten kills it's a star because ur a good boy.
				elseif v.kills > 3 and v.kills < 11 then

					surface.SetMaterial( killstreaks[ v.kills ] )

				elseif v.kills > 10 then

					surface.SetMaterial( megakills )

				end

			elseif v.type == 5 then

				surface.SetMaterial( shutdown )

			elseif v.type == 6 then 

				surface.SetMaterial( killicons[ v.type - 2 ] )

			elseif v.type == 7 then

				surface.SetMaterial( killicons[ v.type - 2 ] )

			end

			surface.SetDrawColor( 255, 255, 255, v.alpha )

			if v.type == 7 then

				surface.DrawTexturedRect( ScrW() / 2 - 96, 80, 192, 128 )

			else

				surface.DrawTexturedRect( ScrW() / 2 - 64, 80, 128, 128 )

			end

		end

		if v.text then

			draw.SimpleTextOutlined( v.text, "HUD2", ScrW() / 2, h + ( th * 0.5 ), Color( 255, 255, 255, v.alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

		end

		draw.SimpleTextOutlined( math.Round( v.xp ), "HUD2", ScrW() / 2, h + ( th * 1.5 ), Color( 255, 255, 255, v.alpha ), TEXT_ALIGN_CENTER, TEXT_ALIGN_CENTER, 1, Color( 0, 0, 0 ) )

		if v.alpha <= 1 then

			table.RemoveByValue( xptable, v )

		end

	end

end )

hook.Add( "GetScoreboardNameColor", "ChangeColorScoreboard", function( ply )

	if ply:GetLevel() >= 10 and ply:GetLevel() < 20 then
		return Color( 255, 0, 0 )
	end

end )