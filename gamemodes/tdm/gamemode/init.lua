AddCSLuaFile( "cl_wepselect.lua" )
AddCSLuaFile( "cl_deathnotice.lua" )
AddCSLuaFile( "cl_voice.lua" )
AddCSLuaFile( "cl_levels.lua" )
AddCSLuaFile( "cl_playercards.lua" )
AddCSLuaFile( "cl_weapon_skins.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "sh_levels.lua" )
AddCSLuaFile( "sh_mapvote.lua" )
AddCSLuaFile( "sh_playercards.lua" )
AddCSLuaFile( "sh_weapon_skins.lua" )
include( "shared.lua" )
include( "sh_levels.lua" )
include( "sh_mapvote.lua" )
include( "sh_playercards.lua" )
include( "sh_weapon_skins.lua" )
include( "sv_levels.lua" )
include( "sv_mapvote.lua" )
include( "sv_playercards.lua" )
include( "sv_weapon_skins.lua" )

util.AddNetworkString( "ChangeTeam" )
util.AddNetworkString( "Loadout" )
util.AddNetworkString( "PlayerReady" )
util.AddNetworkString( "OpenVehicleMenu" )
util.AddNetworkString( "SpawnVehicle")
util.AddNetworkString( "ChatMessage" )
util.AddNetworkString( "InitialSpawn" )
util.AddNetworkString( "TeamChange" )
util.AddNetworkString( "RoundEnded" )
util.AddNetworkString( "RoundEndedClose" )
util.AddNetworkString( "StatusUpdate" )
util.AddNetworkString( "SettingsMenu" )
util.AddNetworkString( "CoopCountdown" )
util.AddNetworkString( "TDMCountdown" )
util.AddNetworkString( "DeathNotificationPart2" )
util.AddNetworkString( "DeathNotificationPart3" )
util.AddNetworkString( "PlayerSpawn" )
util.AddNetworkString( "UpdateTopKills" )
util.AddNetworkString( "GetArmsRaceTable" )
util.AddNetworkString( "UpdateArmsRace" )
util.AddNetworkString( "RequestPurchase" )
util.AddNetworkString( "RequestPurchaseSkin" )
util.AddNetworkString( "PurchaseComplete" )
util.AddNetworkString( "EndRoundDisplay" )
util.AddNetworkString( "RequestOpen" )
util.AddNetworkString( "OpenStage1" )
util.AddNetworkString( "GotMoney" )
util.AddNetworkString( "GotGun" )
util.AddNetworkString( "virginMessage" )
util.AddNetworkString( "welcomeMessage" )
util.AddNetworkString( "BeginTension" )
util.AddNetworkString( "OpenComplete" )
util.AddNetworkString( "DamageIndicator" )
util.AddNetworkString( "Hitmarker" )

resource.AddWorkshop( "800488050" )
resource.AddWorkshop( "800488876" )
resource.AddWorkshop( "800489294" )
resource.AddWorkshop( "800482683" )

resource.AddWorkshop( "688990328" )
resource.AddWorkshop( "691483671" )

resource.AddWorkshop( "242055891" )
resource.AddWorkshop( "479464165" )

resource.AddSingleFile( "materials/uprising/damage.png" )
resource.AddSingleFile( "materials/uprising/damage_nade.png" )

resource.AddSingleFile( "sound/uprising/ui_hit_common_02.wav" )
resource.AddSingleFile( "sound/uprising/ui_hit_common_03.wav" )
resource.AddSingleFile( "sound/uprising/ui_hit_heatshot2.wav" )

resource.AddSingleFile( "sound/uprising/frontend/Navigate_Apply_01_Wave 0 0 0.wav" )
resource.AddSingleFile( "sound/uprising/frontend/Navigate_Highlight_01_Wave 0 0 0.wav" )
resource.AddSingleFile( "sound/uprising/frontend/Navigate_Slider_Wave 0 0 0.wav" )
resource.AddSingleFile( "sound/uprising/frontend/Popup_Wave 0 0 0.wav" )
resource.AddSingleFile( "sound/uprising/frontend/UI_Battledash_Close_Wave 0 0 0.wav" )
resource.AddSingleFile( "sound/uprising/frontend/UI_Battledash_Notification_Wave 0 0 0.wav" )
resource.AddSingleFile( "sound/uprising/frontend/UI_Battledash_Open_Wave 0 0 0.wav" )

resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_barrel_01.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_barrel_02.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_bipot_off.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_bipot_on.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_grip_off.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_grip_on_01.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_mag_on_01.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_mag_on_02.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_muzzle_01.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_muzzle_02.wav" )
resource.AddSingleFile( "sound/uprising/frontend/customisationsounds/ui_weapon_customize_muzzle_03.wav" )

local bSpawns = {}
local oSpawns = {}

maplist = { }

function register( id, map, alias, gametype, icon )

	MapVotes.MapList[ id ] = {}
	MapVotes.MapList[ id ].map = map
	MapVotes.MapList[ id ].alias = alias
	MapVotes.MapList[ id ].gametype = gametyp
	
end

local PlayerMeta = FindMetaTable( "Player" )

function PlayerMeta:SwapWeapons( newwep )

	for k, v in pairs( GetPrimaries() ) do

		if self:HasWeapon( v ) and v != newwep then

			local wep = CustomizableWeaponry:dropWeapon( self, self:GetWeapon( v ) )

			timer.Simple( 10, function()

				if IsValid( wep ) then

					wep:Remove()

				end

			end )
			
			break

		end

	end

end

print( "Registering TDM maps" )
--register( "battle_91", "battle_91", "Water Gate", "TDM", "uprising/battlemap_91_icon.png" )

function MassRegisterMaps( )

	print( "Registering CSS TDM maps" )

	for k, v in pairs( file.Find( "maps/cs_*.bsp", "GAME" ) ) do

		local map = string.StripExtension( v )
		local nicemap = string.TrimLeft( map, "cs_" )
		nicemap = string.SetChar( nicemap, 1, string.upper( nicemap[ 1 ] ) )
		
		register( map, map, nicemap, "TDM", "maps/thumb/".. map ..".png" )
		
	end

	for k, v in pairs( file.Find( "maps/de_*.bsp", "GAME" ) ) do

		local map = string.StripExtension( v )
		local nicemap = string.TrimLeft( map, "de_" )
		nicemap = string.SetChar( nicemap, 1, string.upper( nicemap[ 1 ] ) )
		
		register( map, map, nicemap, "TDM", "maps/thumb/".. map ..".png" )
		
	end

	print( "Registering HL2DM TDM maps" )

	for k, v in pairs( file.Find( "maps/dm_*.bsp", "GAME" ) ) do

		local map = string.StripExtension( v )
		local nicemap = string.TrimLeft( map, "dm_" )
		nicemap = string.SetChar( nicemap, 1, string.upper( nicemap[ 1 ] ) )
		
		register( map, map, nicemap, "TDM", "maps/thumb/".. map ..".png" )
		
	end

end

MassRegisterMaps( )

register( "gm_toysoldiers", "gm_toysoldiers", "Toy Soldiers", "TDM", "" )
register( "gm_bay", "gm_bay", "Bay", "TDM", "" )

function AddBluSpawn( pos )

	local spawn = ents.Create( "info_player_counterterrorist" )
	spawn:SetPos( pos )
	spawn:DropToFloor()
	spawn:Spawn()

end

function AddOpSpawn( pos )

	local spawn = ents.Create( "info_player_terrorist" )
	spawn:SetPos( pos )
	spawn:DropToFloor()
	spawn:Spawn()

end

function SetupSpawns( )

	local map = game.GetMap()

	if map == "gm_flatgrass" then

		AddBluSpawn( Vector( 874.774475, 729.958984, -12200.887695 ) )
		AddOpSpawn( Vector( 937.469849, -300.794678, -12211.049805 ) )

	end

	if map == "gm_toysoldiers" then

		AddBluSpawn( Vector( -5598.432617, 5843.532715, 332.804718 ) )
		AddBluSpawn( Vector( -5706.309082, 5847.679688, 332.804718 ) )
		AddBluSpawn( Vector( -6064.059082, 5859.839355, 332.804718 ) )
		AddBluSpawn( Vector( -6171.954590, 5863.290527, 332.804718 ) )

		AddOpSpawn( Vector( 6045.067383, -5770.626953, 324.485962 ) )
		AddOpSpawn( Vector( 6175.735840, -5771.225586, 324.485962 ) )
		AddOpSpawn( Vector( 5709.954590, -5760.459473, 324.485962 ) )
		AddOpSpawn( Vector( 5607.712891, -5758.050781, 324.485962 ) )

	end

	if map == "gm_bay" then

		AddBluSpawn( Vector( 3234.1281738281, -1763.9836425781, 155.29846191406 ) )
		AddBluSpawn( Vector( 3440.5278320313, -1770.4877929688, 156.03125 ) )
		AddBluSpawn( Vector( 3484.5314941406, -1437.6306152344, 156.03125 ) )
		AddBluSpawn( Vector( 3102.6879882813, -1369.9145507813, 148.81739807129 ) )

		AddOpSpawn( Vector( -4002.728515625, -1540.1076660156, 206.80963134766 ) )
		AddOpSpawn( Vector( -4175.4301757813, -1541.2274169922, 206.09945678711 ) )
		AddOpSpawn( Vector( -4267.2973632813, -1390.3109130859, 164.7688293457 ) )
		AddOpSpawn( Vector( -3944.9660644531, -1357.9395751953, 166.56309509277 ) )

	end

	team.SetSpawnPoint( TEAM_BLUFOR, { "info_player_combine", "info_player_counterterrorist" } )

	team.SetSpawnPoint( TEAM_OPFOR, { "info_player_rebel", "info_player_terrorist" } )

	for k, v in pairs( ents.FindByClass( "prop_physics_multiplayer" ) ) do

		v:Remove()

	end

end

hook.Add( "InitPostEntity", "SetupSpawns", SetupSpawns )

function PrintAllProps( )
	
	local filename = game.GetMap( ).. "_props.txt"
	local count = 0
	
	file.Write( filename, "" )

	for k, v in pairs( ents.FindByClass( "prop_physics" ) ) do
		
		count = count + 1
		
		file.Append( filename, "local prop".. count .." = ents.Create( 'prop_dynamic' )\n" )
		file.Append( filename, "prop".. count ..":SetModel( '".. v:GetModel( ) .."' )\n" )
		file.Append( filename, "prop".. count ..":SetPos( Vector( ".. v:GetPos( ).x ..", ".. v:GetPos( ).y ..", ".. v:GetPos( ).z .." )\n" )
		file.Append( filename, "prop".. count ..":SetAngles( ".. v:GetAngles( ).p ..", ".. v:GetAngles( ).y ..", ".. v:GetAngles( ).r .." )\n" )
		file.Append( filename, "prop".. count ..":SetSolid( 6 )\n" )
		file.Append( filename, "prop".. count ..":Spawn()\n\n" )
		
	end
	
end

local walk = 260
local run = 350

local tPaused = true
local bPaused = false
local oPaused = false

local CapturePos = GetGlobalVector( "CapturePos" )
local CaptureRadius = GetGlobalInt( "CaptureRadius" )
local gametype = string.lower( GetConVar( "uprising_gametype" ):GetString( ) )
local maxkills = GetConVar( "uprising_maxkills" ):GetInt( )

cvars.AddChangeCallback( "uprising_maxkills", function ( name, old, new )

	maxkills = tonumber( new )
	
end )
	
net.Receive( "PlayerReady", function( len, ply )
	local Team = net.ReadInt( 8 )
	ply.Class = net.ReadInt( 8 )
	ply.Primary = net.ReadString( )
	ply.Secondary = net.ReadString( )
	ply.Grenade = net.ReadString( )
	ply.Melee = net.ReadString( )
	ply.PrimaryAttachments = net.ReadTable()
	ply.SecondaryAttachments = net.ReadTable()
	ply.PrimarySkin = net.ReadString()
	ply.SecondarySkin = net.ReadString()

	ply:SetTeam( Team )
	ply:Spawn()
end )

net.Receive( "RequestPurchase", function( len, ply ) 

	local isbox = net.ReadBool()
	local weapon = net.ReadString()
	local price
	local boxtable

	if isbox then

		boxtable = GetBox( weapon )
		price = boxtable.price

		if boxtable and ply:CanSpend( price ) then

			ply:SafeTakeMoney( price )
			ply:AddBox( weapon )

			timer.Simple( 0.1, function()

				net.Start( "PurchaseComplete" )

					net.WriteString( weapon )

				net.Send( ply )

			end )

		end

	else

		price = GetWep( weapon ).price

		if weapons.Get( weapon ) and ply:CanSpend( price ) then

			ply:SafeTakeMoney( price )
			ply:AddWeapon( weapon )

			timer.Simple( 0.1, function()

				net.Start( "PurchaseComplete" )

					net.WriteString( weapon )

				net.Send( ply )

			end )

		end

	end

end )

net.Receive( "RequestPurchaseSkin", function( len, ply ) 

	local weapon = net.ReadString()
	local skinid = net.ReadString()
	local price
	local skintable = WeaponSkins[ weapon ][ skinid ] 

	price = skintable.price

	if ply:CanSpend( price ) then

		ply:SafeTakeMoney( price )
		ply:AddSkin( weapon, skinid )

		timer.Simple( 0.1, function()

			net.Start( "PurchaseComplete" )

				net.WriteString( weapon )

			net.Send( ply )

		end )

	end

end )

net.Receive( "RequestOpen", function( len, ply ) 

	local name = net.ReadString()
	local cat = net.ReadString()
	local boxtbl = GetBox( name )
	local adjustedtable = {}

	if #boxtbl.weps > 1 then

		for k, v in pairs( boxtbl.weps ) do

			local wep = GetWep( v )

			if wep.rarity == 0 then

				table.insert( adjustedtable, v )
				table.insert( adjustedtable, v )
				table.insert( adjustedtable, v )
				table.insert( adjustedtable, v )

			elseif wep.rarity == 1 then

				table.insert( adjustedtable, v )
				table.insert( adjustedtable, v )
				table.insert( adjustedtable, v )

			elseif wep.rarity == 2 then

				table.insert( adjustedtable, v )
				table.insert( adjustedtable, v )

			elseif wep.Rarity == 3 then

				table.insert( adjustedtable, v )

			elseif wep.rarity == 4 and math.random( 1, 5 ) == 5 then

				table.insert( adjustedtable, v )

			end

		end

	else

		adjustedtable = boxtbl.weps

	end

	if ply:HasBox( name ) then

		if cat != "starter" then

			local selectedweapon = table.Random( adjustedtable )

			net.Start( "OpenStage1" )

				net.WriteString( name )
				net.WriteString( selectedweapon )

			net.Send( ply )

			local ownedweps = util.JSONToTable( ply:GetPData( "ownedweapons" ) )
			local has = false

			for k, v in pairs( ownedweps ) do

				if v == selectedweapon then

					has = true
					break

				end

			end

			timer.Simple( 5, function()

				if has then

					ply:GiveMoney( GetWep( selectedweapon ).price * 0.2 )

				else

					ply:AddWeapon( selectedweapon )

					net.Start( "GotGun" )

						net.WriteString( ply:Name().. " opened a ".. name .." and received a " )
						net.WriteString( " worth $".. GetWep( selectedweapon ).price .."!" )
						net.WriteString( selectedweapon )

					net.Broadcast( )

				end

			end )

			ply:TakeBox( name )

			net.Start( "PurchaseComplete" )

				net.WriteString( name )

			net.Send( ply )

		else

			if ply:GetPData( "isVirgin", 0 ) == 0 or ply:GetPData( "isVirgin", 0 ) == "0" then

				ply:SetPData( "isVirgin", 1 )

			end

			for k, v in pairs( boxtbl.weps ) do

				ply:AddWeapon( v )

			end

			ply:TakeBox( name )

			net.Start( "OpenComplete" )

				net.WriteString( name )

			net.Send( ply )

		end

	end

end )

hook.Add("PrePACConfigApply", "no!", function(ply, outfit_data)
    return false, "fuck you"
end)

function CreateFire( pos )
	local fire = ents.Create( "env_fire" )
	fire:SetPos( pos )
	fire:SetKeyValue( "duration", "9999999" )
	fire:SetKeyValue( "size", "100"	 )
	fire:SetKeyValue( "spawnflags", "16" )
	fire:Fire( "startfire" )
	fire:Spawn()
end
function CreateLimitedFire( pos, duration, size )
	local fire = ents.Create( "env_fire" )
	fire:SetPos( pos )
	fire:SetKeyValue( "duration", duration )
	fire:SetKeyValue( "size", size )
	fire:SetKeyValue( "spawnflags", "16" )
	fire:Fire( "startfire" )
	fire:DropToFloor()
	fire:Spawn()
	
	timer.Simple( duration, function( )
	
		if fire then
		
			fire:Remove( )
			
		end
		
	end )
end

function GM:ShowTeam( ply )
end

function GM:ShowSpare1( ply )

	net.Start( "SettingsMenu" )
	
	net.Send( ply )
	
end

--[[function GM:ShowSpare2( ply )
	net.Start( "Loadout" )
	net.Send( ply )
end]]

function GM:PlayerCanHearPlayersVoice()

	return true, false
	
end
 
function GM:EntityTakeDamage( entity, dmginfo )

	if entity:GetClass( ) == "prop_physics" and entity:GetModel( ) == "models/props_battle/oildrum_explosive_ac.mdl" then
	
		local explosion = ents.Create( "env_explosion" )
		explosion:SetPos( entity:GetPos( ) )
		explosion:Spawn( )
		explosion:SetKeyValue( "iMagnitude", "64" )
		explosion:Fire( "Explode", 0, 0 )
		explosion:EmitSound( "ambient/explosions/explode_1.wav", 200, 200 )
		
		for i = 1, math.random( 4, 8 ) do
		
			CreateLimitedFire( Vector( entity:GetPos( ).x + math.random( -64, 64 ), entity:GetPos( ).y + math.random( -64, 64 ), entity:GetPos( ).z ), 20, math.random( 16, 256 ) )
			
		end
		
		entity:Remove( )
		
	end

	local attacker = dmginfo:GetAttacker()

	if entity:IsValid() and entity:IsPlayer() and attacker:IsValid() and attacker:IsPlayer() and attacker:Team() ~= entity:Team() then

		net.Start( "DamageIndicator" )

			net.WriteVector( attacker:GetPos() )

		net.Send( entity )

		entity.WasExploded = dmginfo:IsExplosionDamage()

	end

end

function GM:PlayerHurt( victim, attacker )

	local rand = math.random( 1, 4 )
	local sounds = { }
	sounds[ 1 ] = { "npc/combine_soldier/pain1.wav", "npc/combine_soldier/pain2.wav", "npc/combine_soldier/pain3.wav", "npc/metropolice/pain1.wav", "npc/metropolice/pain2.wav", "npc/metropolice/pain3.wav", "npc/metropolice/pain4.wav" }
	
	if string.find( victim:GetModel(), "female" ) then
	
		sounds[ 2 ] = { "vo/npc/female01/pain01.wav", "vo/npc/female01/pain02.wav", "vo/npc/female01/pain03.wav", "vo/npc/female01/pain04.wav", "vo/npc/female01/pain05.wav", "vo/npc/female01/pain06.wav", "vo/npc/female01/pain07.wav", "vo/npc/female01/pain08.wav", "vo/npc/female01/pain09.wav" } 

	else
	
		sounds[ 2 ] = { "vo/npc/male01/pain01.wav", "vo/npc/male01/pain02.wav", "vo/npc/male01/pain03.wav", "vo/npc/male01/pain04.wav", "vo/npc/male01/pain05.wav", "vo/npc/male01/pain06.wav", "vo/npc/male01/pain07.wav", "vo/npc/male01/pain08.wav", "vo/npc/male01/pain09.wav" } 

	end
		
	if rand >= 3 then
	
		victim:EmitSound( table.Random( sounds[ 2 ] ), 400, 100 )
		
	end

	if victim.CurAttacker != attacker then

		if victim.LastAttacker == attacker then

			victim.LastAttacker = nil

		end

		if victim.CurAttacker and victim.CurAttacker != nil then

			victim.LastAttacker = victim.CurAttacker

		end

		victim.CurAttacker = attacker

	end

end

function GM:PlayerDeathSound()

	return true
	
end

function GM:DoPlayerDeath( ply, att, dmg )
	self.BaseClass:DoPlayerDeath( ply, att, dmg )

	local wep = CustomizableWeaponry:dropWeapon( ply, ply:GetPrimary() )

	timer.Simple( 10, function() 

		if IsValid( wep ) then

			wep:Remove()

		end

	end )

end

function GM:PlayerDeath( ply, att, inf )
	self.BaseClass:PlayerDeath( ply, att, inf )
	ply.SpawnTime = CurTime() + 7
	if ply:Team() == TEAM_SPECTATOR then
		ply:Spectate( OBS_MODE_ROAMING )
	end
	
	if IsValid( ply ) and IsValid( inf ) and inf:IsPlayer() and ply != inf then

		if timer.Exists( ply:SteamID().. "_SpawnProtection" ) then

			timer.Destroy( ply:SteamID().. "_SpawnProtection" )
			ply:GodDisable()

		end
	
		if ply:Team() != inf:Team() or gametype == "free-for-all" then
		
			local rand = math.random( 1, 5 )
			local sounds = {}
			sounds[ 1 ] = { "npc/metropolice/vo/protectioncomplete.wav", "npc/metropolice/vo/sentencedelivered.wav", "npc/metropolice/vo/subjectis505.wav", "npc/metropolice/vo/suspectisbleeding.wav", "npc/metropolice/vo/tap.wav", "npc/combine_soldier/vo/onedown.wav", "npc/combine_soldier/vo/payback.wav", "npc/combine_soldier/vo/reaper.wav", "npc/combine_soldier/vo/ripcord.wav", "npc/combine_soldier/vo/savage.wav", "npc/combine_soldier/vo/slam.wav", "npc/combine_soldier/vo/slash.wav" }
		
			if string.find( inf:GetModel(), "female" ) then
	
				sounds[ 2 ] = { "vo/npc/female01/gotone01.wav", "vo/npc/female01/gotone02.wav", "vo/npc/female01/overthere01.wav", "vo/npc/female01/overthere02.wav", "vo/npc/female01/overthere03.wav", "vo/npc/female01/uhoh.wav", "vo/npc/female01/yeah02.wav" } 

			else
			
				sounds[ 2 ] = { "vo/npc/male01/gotone01.wav", "vo/npc/male01/gotone02.wav", "vo/npc/male01/overthere01.wav", "vo/npc/male01/overthere02.wav", "vo/npc/male01/overthere03.wav", "vo/npc/male01/uhoh.wav", "vo/npc/male01/yeah02.wav" } 

			end

			if rand >= 4 then
			
				inf:EmitSound( table.Random( sounds[ 2 ] ), 500, 100 )
				
			end

			if ply.LastAttacker and ply.LastAttacker:IsPlayer() then

				local details = {}
				details.weapon = inf:GetActiveWeapon( )
				details.text = "ASSIST"
				details.textcol = team.GetColor( ply:Team( ) )
				details.xp = 10
				details.type = 3
				ply.LastAttacker:GiveXP( 10, details )
				ply.LastAttacker:GiveMoney( 100 )

			end

			if inf.KillStreak then

				inf.KillStreak = inf.KillStreak + 1

			else

				inf.KillStreak = 1

			end

			local details = {}

			if inf.KillStreak > 1 then

				details.weapon = inf:GetActiveWeapon( )

				if inf.KillStreak == 2 then

					details.text = "DOUBLE KILL"

				elseif inf.KillStreak == 3 then

					details.text = "TRIPLE KILL"

					GAMEMODE:ChatMessage( inf:Name() .. " is on a killing spree with 3 kills!" )

				elseif inf.KillStreak == 4 then

					details.text = "QUAD KILL"

				elseif inf.KillStreak == 5 then

					details.text = "TETRA KILL"

					GAMEMODE:ChatMessage( inf:Name().. " is dominating with 5 kills!" )

				else

					details.text = inf.KillStreak.. " KILLS"

				end

				if inf.KillStreak == 10 then

					GAMEMODE:ChatMessage( inf:Name().. " is on a rampage with 10 kills!" )

				end
				
				details.textcol = team.GetColor( ply:Team( ) )
				details.xp = 20
				details.type = 4
				details.kills = inf.KillStreak

			else
		
				details.weapon = inf:GetActiveWeapon( )
				details.text = "NICE KILL"
				details.textcol = team.GetColor( ply:Team( ) )
				details.xp = 20
				details.type = 1

			end
		
			inf:GiveXP( 20, details )
			inf:GiveWepXP( inf:GetActiveWeapon( ):GetClass( ), 5 )
			inf:GiveCatXP( GetWepCat( inf:GetActiveWeapon( ):GetClass( ) ), 100 )
			inf:GiveMoney( 100 )
			
			if ply.PersonalScore[ inf ] then
			
				ply.PersonalScore[ inf ].Foe = ply.PersonalScore[ inf ].Foe + 1

			else
			
				ply.PersonalScore[ inf ] = {}
				ply.PersonalScore[ inf ].Foe = 1
				ply.PersonalScore[ inf ].You = 0
				
			end
			
			if inf.PersonalScore[ ply ] then
			
				inf.PersonalScore[ ply ].You = inf.PersonalScore[ ply ].You + 1

			else
			
				inf.PersonalScore[ ply ] = {}
				inf.PersonalScore[ ply ].You = 1
				inf.PersonalScore[ ply ].Foe = 0
				
			end
			
			
			if ply:LastHitGroup() == HITGROUP_HEAD then
		
				local details = {}
				details.text = "HEADSHOT"
				details.textcol = Color( 215, 215, 215 )
				details.xp = 5
				details.type = 2
			
				inf:GiveXP( 5, details )
				inf:GiveMoney( 10 )
				
			end

			if ply.WasExploded then
		
				local details = {}
				details.text = "GRENADE KILL"
				details.textcol = Color( 215, 215, 215 )
				details.xp = 5
				details.type = 6
			
				inf:GiveXP( 5, details )
				inf:GiveMoney( 10 )

			end

			--[[if ply:GetMelee() and ply:GetActiveWeapon() == ply:GetMelee() then
		
				local details = {}
				details.text = "MELEE KILL"
				details.textcol = Color( 215, 215, 215 )
				details.xp = 5
				details.type = 8
			
				inf:GiveXP( 5, details )
				inf:GiveMoney( 10 )

			end]]

			if ply.KillStreak then

				if ply.KillStreak >= 3 then

					GAMEMODE:ChatMessage( inf:Name() .." just shut down ".. ply:Name() .."'s kill streak of ".. ply.KillStreak .."!" )

					local details = {}
					details.text = "SHUTDOWN"
					details.textcol = Color( 215, 215, 215 )
					details.xp = 5
					details.type = 5
				
					inf:GiveXP( 5, details )
					inf:GiveMoney( 10 )

				end

			end

			team.AddScore( inf:Team(), 1 )
			
			net.Start( "DeathNotificationPart2" )
			
				net.WriteTable( ply.PersonalScore[ inf ] )
				net.WriteEntity( inf )

				if inf:GetActiveWeapon() then

					net.WriteBool( inf:GetActiveWeapon().dt.Suppressed )

				else

					net.WriteBool( false )

				end
				
			net.Send( ply )
			
			if gametype == "arms race" then
			
				if ( inf:GetActiveWeapon( ):GetClass( ) == "quick_knife" or inf:GetActiveWeapon( ):GetClass( ) == "fas2_dv2" ) and inf.GunPos >= 17 then
				
					self:EndRound( )
					
				elseif ( inf:GetActiveWeapon( ):GetClass( ) == "quick_knife" or inf:GetActiveWeapon( ):GetClass( ) == "fas2_dv2" ) and inf.GunPos < 17 then
				
					print( ply.GunPos, ply.GunPos - 1 )
				
					ply.GunPos = math.Clamp( ply.GunPos - 1, 1, 16 )
			
					inf.GunPos = inf.GunPos + 1
					
					self:PlayerLoadout( inf )
					
				else
				
					inf.GunPos = inf.GunPos + 1
					
					self:PlayerLoadout( inf )
					
				end

				local armsracelist = {}
				
				for k, v in pairs( player.GetAll( ) ) do
				
					armsracelist[ v ] = v.GunPos
					
				end
	
				net.Start( "UpdateArmsRace" )
				
					net.WriteTable( armsracelist )
				
				net.Broadcast( )
				
			end
			
		end
		
	end

	ply.KillStreak = 0
	
	if ply == inf or !inf:IsPlayer( ) then
			
		net.Start( "DeathNotificationPart3" )
					
		net.Send( ply )
		
	end
		
	if IsValid( ply.ActiveVehicle ) then
		ply.ActiveVehicle:Remove()
	end
	
	net.Start( "UpdateTopKills" )
	
	net.Broadcast( )
	
end

function GM:IsSpawnpointSuitable( pl, spawnpointent, bMakeSuitable )

	local Pos = spawnpointent:GetPos()
	
	-- Note that we're searching the default hull size here for a player in the way of our spawning.
	-- This seems pretty rough, seeing as our player's hull could be different.. but it should do the job
	-- (HL2DM kills everything within a 128 unit radius)
	local Ents = ents.FindInBox( Pos + Vector( -16, -16, 0 ), Pos + Vector( 16, 16, 64 ) )
	
	if ( pl:Team() == TEAM_SPECTATOR ) then return true end
	
	return true

end


function GM:ChatMessage( msg, ply )
	net.Start( "ChatMessage" )
	net.WriteString( msg )
	if IsValid( ply ) then
		net.Send( ply )
	else
		net.Broadcast()
	end
end

function GM:PlayerDisconnected( ply )
	if IsValid( ply.ActiveVehicle ) then
		ply.ActiveVehicle:Remove()
	end
	
	self:ChatMessage( ply:Name() .. " has left the game." )
end

function GM:PlayerDeathThink( ply )
	if ply:Team() == TEAM_SPECTATOR then return false end
	if ply.SpawnTime and CurTime( ) < ply.SpawnTime then return false end
	self.BaseClass:PlayerDeathThink( ply )
	--ply:Spawn()
end

function GM:PlayerLoadout( ply )
	if ply:Team() != TEAM_SPECTATOR and ply:Team() != TEAM_UNASSIGNED and !ply:IsBot() and ply.Class then
		ply:StripAmmo()
		
		if gametype != "arms race" then
		
			ply:GiveAmmo( ClassTable[ ply.Class ].grenades, "M67 Grenades", false )
			ply:Give( ply.Primary )
			
			ply:SetAmmo( weapons.Get( ply.Primary ).Primary.ClipSize * 4, weapons.Get( ply.Primary ).Primary.Ammo, false )
			ply:Give( ply.Secondary )
			ply:SetAmmo( weapons.Get( ply.Secondary ).Primary.ClipSize * 4, weapons.Get( ply.Secondary ).Primary.Ammo, false )
			ply:SetArmor( ClassTable[ ply.Class ].armour )
			
			ply:Give( ply.Grenade )
			ply:SetAmmo( 2, weapons.Get( ply.Grenade ).Primary.Ammo, false )

			ply:Give( ply.Melee )
			
			local count = 0
				
			for k, v in pairs( ply.PrimaryAttachments ) do
			
				count = count + 1
			
				if v == "md_m203" then
				
					ply:GiveAmmo( 2, "40MM", true)
					
				end
			
				CustomizableWeaponry:giveAttachment( ply, v )
				
				timer.Simple( 0.5 + ( 0.1 * count ), function( )
				
					ply:GetWeapon( ply.Primary ):attachSpecificAttachment( v )
					ply:GetWeapon( ply.Primary ):SetClip1( ply:GetWeapon( ply.Primary ):GetMaxClip1( ) )
					
				end )
				
			end
			
			for k, v in pairs( ply.SecondaryAttachments ) do
			
				CustomizableWeaponry:giveAttachment( ply, v )
				
				timer.Simple( 0.5 + ( 0.25 * count ), function( )
				
					ply:GetWeapon( ply.Secondary ):attachSpecificAttachment( v )
					ply:GetWeapon( ply.Secondary ):SetClip1( ply:GetWeapon( ply.Secondary ):GetMaxClip1( ) )
					
				end )				
			end

			if ply.PrimarySkin != "" or ply.SecondarySkin != "" then

				timer.Simple( 1, function()

					net.Start( "PlayerSkins" )

						net.WriteEntity( ply )
						net.WriteString( ply.PrimarySkin )
						net.WriteString( ply.SecondarySkin )

					net.Broadcast()

				end )

			end
			
		end
		
		if gametype == "arms race" then
		
			ply:StripWeapons( )
			
			local att = weapons.Get( ArmsRaceTable[ ply.GunPos ] ).Attachments
			local count = 0
		
			ply:Give( ArmsRaceTable[ ply.GunPos ] )
			ply:GiveAmmo( weapons.Get( ArmsRaceTable[ ply.GunPos ] ).Primary.ClipSize * 6, weapons.Get( ArmsRaceTable[ ply.GunPos ] ).Primary.Ammo, false )
		
			--ply:Give( "fas2_dv2" )
			
			for att, v in pairs( ply.CWAttachments ) do
			
				ply.CWAttachments[att] = nil
				
			end
			
			for k, v in pairs( att ) do
			
				if math.random( 1, 3 ) == 3 then
				
					count = count + 1
				
					local att2 = table.Random( v.atts )
				
					CustomizableWeaponry:giveAttachment( ply, att2 )
					
					timer.Simple( 1 + ( 0.1 * count ), function( )

						ply:GetWeapon( ArmsRaceTable[ ply.GunPos ] ):attachSpecificAttachment( att2 )
						
					end )
					
				end
				
			end
			
		end
		
		if gametype == "cooperative" then
		
			ply:Give( "weapon_physcannon" )
			
		end
		
	end
end

function GM:PlayerSetModel( ply )
	if ply.Class then
	
		if ply:Team() == TEAM_BLUFOR then
			ply:SetModel( ClassTable[ ply.Class ].bModel[ #ClassTable[ ply.Class ].bModel ] )
		elseif ply:Team() == TEAM_OPFOR then
			ply:SetModel( ClassTable[ ply.Class ].oModel[ #ClassTable[ ply.Class ].bModel ] )
		end
		
	end
end

function GM:PlayerSpawn( ply )
	if ( ply:Team() == TEAM_BLUFOR or ply:Team() == TEAM_OPFOR ) and ply.Class then
	
		if IsValid( ply.ActiveVehicle ) then
			ply.ActiveVehicle:Remove()
		end
		
		net.Start( "PlayerSpawn" )
		
		net.Send( ply )
		
		ply:SetCustomCollisionCheck( true )
		ply:StripWeapons()
		ply:StripAmmo()
		self.BaseClass:PlayerSpawn( ply )
		ply:SetRunSpeed( run * ClassTable[ ply.Class ].speed )
		ply:SetWalkSpeed( walk * ClassTable[ ply.Class ].speed )
		ply.CurAttacker = nil
		ply.LastAttacker = nil
		
		if gametype == "conquest" or gametype == "tdm" or gametype == "arms race" then
		
			if ply:Team() == TEAM_BLUFOR then
				local spawn = table.Random( team.GetSpawnPoints( TEAM_BLUFOR ) )
				
				if spawn then

					local pos = spawn:GetPos()

					for k, v in pairs( ents.FindInSphere( spawn:GetPos(), 32 ) ) do
					
						if v:GetClass( ) == "prop_physics" then
						
							v:Remove( )
							
						end

					end

					ply:SetPos( pos )
					
				end
				
				if timer.Exists( "TDMCountdown" ) or timer.Exists( "CoopCountdown" ) then
				
					ply:Freeze( true )
					
				end
				
			elseif ply:Team() == TEAM_OPFOR then
				local spawn = table.Random( team.GetSpawnPoints( TEAM_OPFOR ) )

				if spawn then

					local pos = spawn:GetPos()
				
					for k, v in pairs( ents.FindInSphere( spawn:GetPos(), 32 ) ) do
					
						if v:GetClass( ) == "prop_physics" then
						
							v:Remove( )
							
						end
						
					end
					
					ply:SetPos( pos )

				end
				
			end

			ply:GodEnable()

			timer.Simple( 3, function()

				timer.Create( ply:SteamID().. "_SpawnProtection", 7, 0, function()

					if ply:Alive() then

						ply:GodDisable()

					end

				end )

			end )
			
		end
		
	end
end	

function GM:AllowPlayerPickup( ply, ent )

	return false
	
end

function GM:PlayerUse( ply, ent )

	return ply:Team() == 1 or ply:Team() == 2

end

function GM:GetFallDamage( ply, speed )
	return false
end

function GM:PlayerInitialSpawn( ply )
	if ply:IsBot() then
		ply:SetTeam( team.BestAutoJoinTeam() )
		ply.Class = CLASS_GREN
	else
		self.BaseClass:PlayerInitialSpawn( ply )
		ply:KillSilent()
		timer.Simple( 0.1, function()
			if gametype ~= "free-for-all" then
				ply:SetTeam( TEAM_SPECTATOR )
			else
				ply:SetTeam( team.BestAutoJoinTeam() )
			end
			ply:Spectate( OBS_MODE_ROAMING )

		end )
		
		net.Start( "InitialSpawn" )
		
		net.Send( ply )
		
	
		if gametype == "arms race" then
		
			net.Start( "GetArmsRaceTable" )
				
				net.WriteTable( ArmsRaceTable )
				
			net.Broadcast( )
		
			ply.GunPos = 1
			
		end

		timer.Simple( 1, function()

			if ply:GetPData( "isVirgin", 0 ) == 0 or ply:GetPData( "isVirgin", 0 ) == "0" then

				ply:GiveMoney( 5000, true )

				net.Start( "welcomeMessage" )

				net.Send( ply )

			end

		end )
		
	end
	ply.PersonalScore = {}
end

function GM:ScalePlayerDamage( ply, hit, dmg )
	if IsValid( dmg:GetAttacker() ) then
		if dmg:GetAttacker():IsPlayer() then
			if gametype ~= "free-for-all" then
				if ( ply:Team() == dmg:GetAttacker():Team() and dmg:GetAttacker() != ply ) or gametype == "cooperative" then
					dmg:ScaleDamage( 0 )
					return
				end
			end
		end
	end
	if gametype == "cooperative" then
	
		dmg:ScaleDamage( 2.5 )
		
	else

		net.Start( "Hitmarker" )

			net.WriteBool( hit == HITGROUP_HEAD )

		net.Send( dmg:GetAttacker() )
	
		if hit == HITGROUP_HEAD then
			dmg:ScaleDamage( 1.75 )
		elseif hit == HITGROUP_LEFTARM or hit == HITGROUP_RIGHTARM then
		dmg:ScaleDamage( 0.5 )
		elseif hit == HITGROUP_CHEST then
			dmg:ScaleDamage( 1 )
		elseif hit == HITGROUP_STOMACH then
			dmg:ScaleDamage( 0.8 )
		elseif hit == HITGROUP_LEFTLEG or hit == HITGROUP_RIGHTLEG then
			dmg:ScaleDamage( 0.5 )
		end
		
	end
end

function GM:ScaleNPCDamage( npc, hitgroup, dmginfo )

	dmginfo:ScaleDamage( 0.5 )
	
end

function GM:EndRound( )

	if gametype == "conquest" then

		local winner = GetGlobalInt( "PointOwner" )
		
		SetGlobalInt( "TotalGames", GetGlobalInt( "TotalGames" ) + 1 )
		
	end
	
	for k, v in pairs( player.GetAll() ) do

		v:GodEnable()
		v:Freeze( true )

		if team.GetScore( 1 ) > team.GetScore( 2 ) then
		
			if v:Team() == 1 then

				local details = {}
				details.text = "VICTORY"
				details.textcol = Color( 215, 215, 215 )
				details.xp = 100
				details.type = 7
							
				v:GiveXP( 100, details )
				v:GiveMoney( 100 )

			end
				
		elseif team.GetScore( 2 ) > team.GetScore( 1 ) then
			
			if v:Team() == 2 then

				local details = {}
				details.text = "VICTORY"
				details.textcol = Color( 215, 215, 215 )
				details.xp = 100
				details.type = 7
							
				v:GiveXP( 100, details )
				v:GiveMoney( 100 )

			end
				
		else

			local details = {}
			details.text = "TIE"
			details.textcol = Color( 215, 215, 215 )
			details.xp = 50
			details.type = 7
							
			v:GiveXP( 50, details )
			v:GiveMoney( 50 )
				
		end

	end

	net.Start( "EndRoundDisplay")
	net.Broadcast( )

	game.SetTimeScale( 0.5 )

	timer.Simple( 5, function()
	
		for k, v in pairs( player.GetAll( ) ) do
			
			v:KillSilent( )
				
		end

		game.SetTimeScale( 1 )
	
		net.Start( "RoundEnded" )
		
			if gametype == "conquest" then
			
				if GetGlobalInt( "bRoundPoints" ) == GetGlobalInt( "oRoundPoints" ) then
				
					net.WriteInt( 0, 4 )
					
				elseif GetGlobalInt( "bRoundPoints" ) > GetGlobalInt( "oRoundPoints" ) then
				
					SetGlobalInt( "bTotalPoints", GetGlobalInt( "bTotalPoints" ) + 1 )
				
					net.WriteInt( 1, 4 )
					
				elseif GetGlobalInt( "oRoundPoints" ) > GetGlobalInt( "bRoundPoints" ) then
				
					SetGlobalInt( "oTotalPoints", GetGlobalInt( "oTotalPoints" ) + 1 )
					
					net.WriteInt( 2, 4 )		
					
				else

					net.WriteInt( 0, 4 )
					
				end
				
			elseif gametype == "tdm" then
			
				if team.GetScore( 1 ) > team.GetScore( 2 ) then
			
					net.WriteInt( 1, 4 )
					
				elseif team.GetScore( 2 ) > team.GetScore( 1 ) then
				
					net.WriteInt( 2, 4 )
					
				else
				
					net.WriteInt( 0, 4 )
					
				end
				
			end
		
		net.Broadcast( )

		local count = 0
		local votemaps = {}

		for k, v in RandomPairs( MapVotes.MapList ) do

			if count < 8 then

				votemaps[ count + 1 ] = k

				count = count + 1

			end

		end

		MapVotes.StartVote( 20, votemaps )

		PrintTable( votemaps )
			
		timer.Simple( 20, function( )
		
			for k, v in pairs( player.GetAll( ) ) do
			
				v:SetFrags( 0 )
				v:SetDeaths( 0 )
			
			end

			team.SetScore( 1, 0 )
			team.SetScore( 2, 0 )
			
			--if GetGlobalInt( "TotalGames" ) >= 3 or gametype == "tdm" or gametype == "free-for-all" or gametype == "arms race" then
			
				--MapVote.Start( 20, true, #maplist, maplist )
			
			--[[else
			
				net.Start( "RoundEndedClose" )
				
				net.Broadcast( )
				
			end]]
			
		end )

	end )
		
	GAMEMODE:InitializeMaps()
	
	timer.Destroy( "BlueTimer" )
	timer.Destroy( "BlueTimer2" )
	timer.Destroy( "RedTimer" )
	timer.Destroy( "RedTimer2" )
	timer.Destroy( "rTimer" )
	
	SetGlobalInt( "PointOwner", 0 )
	
end

function GM:AdvancePoint( )

	if GetGlobalInt( "PointOwner" ) == 1 then
	
		SetGlobalInt( "bRoundPoints", GetGlobalInt( "bRoundPoints" ) + 1 )
		
	elseif GetGlobalInt( "PointOwner" ) == 2 then
	
		SetGlobalInt( "oRoundPoints", GetGlobalInt( "oRoundPoints" ) + 1 )
		
	end

	SetGlobalInt( "PointOwner", 0 )
	
	if game.GetMap( ) == "battle_99" then

		if GetGlobalInt( "PointNumber" ) == 2 then
		
			SetGlobalVector( "CapturePos", Vector( -7003, -2418, 64 ) )
			SetGlobalInt( "CaptureRadius", 256 )
			SetGlobalString( "CaptureAlias", "Water Flow" )
			
		elseif GetGlobalInt( "PointNumber" ) == 3 then
		
			SetGlobalVector( "CapturePos", Vector( -5988, -3091, 152 ) )
			SetGlobalInt( "CaptureRadius", 256 )
			SetGlobalString( "CaptureAlias", "Rail Controls" )
			
		end
		
	elseif game.GetMap( ) == "battle_91" then
	
		if GetGlobalInt( "PointNumber" ) == 2 then
		
			SetGlobalVector( "CapturePos", Vector( -1420.9680175781, 1228.6885986328, -383.96875 ) )
			SetGlobalInt( "CaptureRadius", 500 )
			SetGlobalString( "CaptureAlias", "Warehouse" )
			
		end
		
	elseif game.GetMap( ) == "battle_92" then
	
		if GetGlobalInt( "PointNumber" ) == 2 then
		
			SetGlobalVector( "CapturePos", Vector( -3850.96875, 4557.8891601563, 70.40389251709 ) )
			SetGlobalInt( "CaptureRadius", 160 )
			SetGlobalInt( "CaptureTimer", 120 )
			SetGlobalInt( "oTime", 120 )
			SetGlobalInt( "bTime", 120 )
			SetGlobalString( "CaptureAlias", "Steal Intel" )
			
		end
		
	elseif game.GetMap( ) == "battle_93" then
	
		if GetGlobalInt( "PointNumber" ) == 2 then
		
			for i = 1, 4 do
			
				timer.Simple( 0.4 * i, function( )
				
					local explosion = ents.Create( "env_explosion" )
					explosion:SetPos( Vector( 252, 408, 1703 ) )
					explosion:Spawn( )
					explosion:SetKeyValue( "iMagnitude", "100" )
					explosion:Fire( "Explode", 0, 0 )
					explosion:EmitSound( "ambient/explosions/explode_".. i ..".wav", 200, 200 )
					
				end )
				
			end
		
			SetGlobalVector( "CapturePos", Vector( 260, 659, 2140 ) )
			SetGlobalInt( "CaptureRadius", 200 )
			SetGlobalInt( "CaptureTimer", 300 )
			SetGlobalInt( "oTime", 300 )
			SetGlobalInt( "bTime", 300 )
			
		end
		
	elseif game.GetMap( ) == "battle_94" then
	
		if GetGlobalInt( "PointNumber" ) == 2 then
		
			SetGlobalVector( "CapturePos", Vector( 6027.047363, -645.617493, -90 ) )
			SetGlobalInt( "CaptureRadius", 90 )
			SetGlobalInt( "CaptureTimer", 120 )
			SetGlobalInt( "oTime", 120 )
			SetGlobalInt( "bTime", 120 )
			SetGlobalString( "CaptureAlias", "Evacuation Point" )
			
		end
		
	end
	
	timer.Destroy( "BlueTimer" )
	timer.Destroy( "BlueTimer2" )
	timer.Destroy( "RedTimer" )
	timer.Destroy( "RedTimer2" )
	
end

function GM:Think()
	local redCount = 0
	local blueCount = 0
	local warning1 = false
	local warning2 = false
	local warning3 = false
	local endtable = {}
	
	if gametype == "cooperative" then
	
		if #player.GetAll() >= 2 and !timer.Exists( "CoopTimer" ) and !timer.Exists( "CoopTimer2" ) and !timer.Exists( "CoopCountdown" ) and !timer.Exists( "CoopCountdown2" ) then
		
			if ( #team.GetPlayers( 1 ) + #team.GetPlayers( 2 ) ) >= 2 then
		
				timer.Create( "CoopCountdown", 7, 1, function( )
			
					timer.Create( "CoopTimer", 600, 1, function( )
					
						MapVote.Start( 20, true, #maplist, maplist )
						
					end )
					
					timer.Create( "CoopTimer2", 1, 600, function( )
					
						SetGlobalInt( "cTime", timer.TimeLeft( "CoopTimer" ) )
						
					end )
					
					for k, v in pairs( player.GetAll( ) ) do
					
						v:Freeze( false )
						
					end
					
				end )
				
				timer.Create( "CoopCountdown2", 1, 6, function( )
				
					net.Start( "CoopCountdown" )
					
						net.WriteInt( math.Round( timer.TimeLeft( "CoopCountdown" ) ), 4 )
						
					net.Broadcast( )
					
				end )
				
			end
		
		end
		if ( #team.GetPlayers( 1 ) + #team.GetPlayers( 2 ) ) >= 2 then
		
			for k, v in pairs( ents.FindInSphere( Vector( 2.3982794284821, -2888.767578125, 724.03125 ), 128 ) ) do
				
				if v:IsPlayer( ) && v:Alive( ) then
					
					endtable[ k ] = v
						
				end
					
			end
			
		end
		
		if table.Count( endtable ) > 1 && table.Count( endtable ) >= ( #team.GetPlayers( 1 ) + #team.GetPlayers( 2 ) ) * 0.75 then
			
			table.Empty( endtable )
			
			self:EndRound( )
				
		end
		
	elseif gametype == "tdm" or gametype == "free-for-all" then
	
		if #player.GetAll() >= 2 and !timer.Exists( "TDMTimer" ) and !timer.Exists( "TDMTimer2" ) and !timer.Exists( "TDMCountdown" ) and !timer.Exists( "TDMCountdown2" ) then
			
			if ( #team.GetPlayers( 1 ) + #team.GetPlayers( 2 ) ) >= 2 then
		
				timer.Create( "TDMCountdown", 7, 1, function( )
			
					timer.Create( "TDMTimer", 1200, 1, function( )
					
						self:EndRound( )
						
					end )
					
					timer.Create( "TDMTimer2", 1, 1200, function( )
					
						SetGlobalInt( "rTime", timer.TimeLeft( "TDMTimer" ) )

						if timer.TimeLeft( "TDMTimer" ) == 43 then

							net.Start( "BeginTension" )

							net.Broadcast( )

						end
						
					end )
					
					RunConsoleCommand( "uprising_maxkills", math.Max( 25, #player.GetAll( ) * 5 ) )
					
					for k, v in pairs( player.GetAll( ) ) do
					
						v:Freeze( false )
						
					end
					
				end )
				
				timer.Create( "TDMCountdown2", 1, 6, function( )
				
					net.Start( "TDMCountdown" )
					
						net.WriteInt( math.Round( timer.TimeLeft( "TDMCountdown" ) ), 4 )
						
					net.Broadcast( )
					
				end )
				
			end
		end
		
	else

		if CapturePos ~= GetGlobalVector( "CapturePos" ) then
		
			CapturePos = GetGlobalVector( "CapturePos" )
			
		end
		
		if CaptureTimer ~= GetGlobalInt( "CaptureTimer" ) then
		
			CaptureTimer = GetGlobalInt( "CaptureTimer" )
			
		end
		
		if CaptureRadius ~= GetGlobalInt( "CaptureRadius" ) then
		
			CaptureRadius = GetGlobalInt( "CaptureRadius" )
			
		end
		
		for k,v in pairs( player.GetAll() ) do
			v:SetNWBool( "InCapture", false )
		end
		
		if CapturePos and CaptureRadius then
		
			for k,v in pairs( ents.FindInSphere( CapturePos, CaptureRadius ) ) do
				if v:IsPlayer() then
					if v:Team() == TEAM_BLUFOR and v:Alive() then
						blueCount = blueCount + 1
						v:SetNWBool( "InCapture", true )
					elseif v:Team() == TEAM_OPFOR and v:Alive() then
						redCount = redCount + 1
						v:SetNWBool( "InCapture", true )
					end
				end
			end
			
		end
		
		if IsValid( consoleb ) then
		
			if GetGlobalInt( "PointNumber" ) == 3 then
		
				if GetGlobalInt( "PointOwner" ) == 0 then
				
					if consoleb:GetSubMaterial( 1 ) ~= "models/props_combine/combine_intmonitor001_disp_off" then
					
						consoleb:SetSubMaterial( 1, "models/props_combine/combine_intmonitor001_disp_off" )
						
					end
					
				elseif GetGlobalInt( "PointOwner" ) == 1 then
				
					if consoleb:GetSubMaterial( 1 ) ~= "models/props_combine/combine_intmonitor001_disp" then
					
						consoleb:SetSubMaterial( 1, "models/props_combine/combine_intmonitor001_disp" )
						
					end
					
				elseif GetGlobalInt( "PointOwner" ) == 2 then
				
					if consoleb:GetSubMaterial( 1 ) ~= "models/props_combine/combine_intmonitor001_disp_alert" then
					
						consoleb:SetSubMaterial( 1, "models/props_combine/combine_intmonitor001_disp_alert" )
						
					end
					
				end
				
			end
				
		end
		
		if GetGlobalInt( "PointOwner" ) == 0 then
		
			if timer.Exists( "BlueTimer" ) then
			
				if !bPaused then
				
					timer.Pause( "BlueTimer" )
					timer.Pause( "BlueTimer2" )
					bPaused = true
					
				end
				
			elseif timer.Exists( "RedTimer" ) then
			
				if !oPaused then
				
					timer.Pause( "RedTimer" )
					timer.Pause( "RedTimer2" )
					oPaused = true
					
				end
				
			end
			
		elseif GetGlobalInt( "PointOwner" ) == 1 then
		
			if timer.Exists( "BlueTimer" ) then
				
				if bPaused then
				
					timer.UnPause( "BlueTimer" )
					timer.UnPause( "BlueTimer2" )
					bPaused = false
					
				end
				
			elseif !timer.Exists( "BlueTimer" ) then
			
				timer.Create( "BlueTimer", 1, CaptureTimer, function( )
				
					SetGlobalInt( "bTime", timer.TimeLeft( "BlueTimer2" ) )
					
					if math.Round( timer.TimeLeft( "BlueTimer2" ) ) == CaptureTimer * 0.75 then
					
						net.Start( "StatusUpdate" )
						
							net.WriteString( "hpcontestedpartial" )
						
						net.Broadcast( )
						
					elseif math.Round( timer.TimeLeft( "BlueTimer2" ) ) == CaptureTimer * 0.25 then
					
						net.Start( "StatusUpdate" )
						
							net.WriteString( "bigwinning" )
							
						net.Broadcast( )
						
					end
					
				end )
				
				timer.Create( "BlueTimer2", CaptureTimer, 1, function( )
		
					SetGlobalInt( "PointNumber", GetGlobalInt( "PointNumber" ) + 1 )
					
						if GetGlobalInt( "PointNumber" ) > CaptureNum then
					
							self:EndRound( )
							
						else
						
							self:AdvancePoint( )
							
						end
							
						for k, v in pairs( team.GetPlayers( TEAM_BLUFOR ) ) do
							
							local details = {}
							details.text = "POINT CAPTURED"
							details.textcol = Color( 215, 215, 215 )
							details.xp = 50
								
							v:GiveXP( 50, details )
							
						end
					
				end )
				
			end
			
			if timer.Exists( "RedTimer" ) then
			
				if !oPaused then
				
					timer.Pause( "RedTimer" )
					timer.Pause( "RedTimer2" )
					oPaused = true
					
				end
				
			end
			
		elseif GetGlobalInt( "PointOwner" ) == 2 then
		
			if timer.Exists( "RedTimer" ) then
				
				if oPaused then
				
					timer.UnPause( "RedTimer" )
					timer.UnPause( "RedTimer2" )
					oPaused = false
					
				end
				
			elseif !timer.Exists( "RedTimer" ) then
			
				timer.Create( "RedTimer", 1, CaptureTimer, function( )
				
					SetGlobalInt( "oTime", timer.TimeLeft( "RedTimer2" ) )
					
					if math.Round( timer.TimeLeft( "RedTimer2" ) ) == CaptureTimer * 0.75 then
					
						net.Start( "StatusUpdate" )
						
							net.WriteString( "hpcontestedpartial" )
						
						net.Broadcast( )
						
					elseif math.Round( timer.TimeLeft( "RedTimer2" ) ) == CaptureTimer * 0.25 then
					
						net.Start( "StatusUpdate" )
						
							net.WriteString( "bigwinning" )
							
						net.Broadcast( )
						
					end
					
				end )
				
				timer.Create( "RedTimer2", CaptureTimer, 1, function( )
		
					SetGlobalInt( "PointNumber", GetGlobalInt( "PointNumber" ) + 1 )
				
					if GetGlobalInt( "PointNumber" ) > CaptureNum then
				
						self:EndRound( )
						
					else
					
						self:AdvancePoint( )
						
					end
						
					for k, v in pairs( team.GetPlayers( TEAM_OPFOR ) ) do
						
						local details = {}
						details.text = "POINT CAPTURED"
						details.textcol = Color( 215, 215, 215 )
						details.xp = 50
						
						v:GiveXP( 50, details )
					
					end
					
				end )
				
			end
			
			if timer.Exists( "BlueTimer" ) then
			
				if !bPaused then
				
					timer.Pause( "BlueTimer" )
					timer.Pause( "BlueTimer2" )
					bPaused = true
					
				end
				
			end
			
		end
		
		if redCount > blueCount then	
			if timer.Exists( "BlueCapture" ) then
				if tPaused then
					timer.UnPause( "BlueCapture" )
					timer.UnPause( "BlueCapture2" )
					
					timer.UnPause( "RedCapture" )
					timer.UnPause( "RedCapture2" )
					
					tPaused = false
				end
				timer.Destroy( "BlueCapture" )
				timer.Destroy( "BlueCapture2" )
				SetGlobalInt( "Capture", 3 )
			end
			if GetGlobalInt( "PointOwner" ) != TEAM_OPFOR then
				SetGlobalInt( "Capture", TEAM_OPFOR )
				if not timer.Exists( "RedCapture" ) then
					SetGlobalInt( "CaptureTime", CaptureTime )
					timer.Create( "RedCapture2", CaptureTime, 1, function()
					
						if GetGlobalInt( "PointOwner" ) == 0 then
					
							SetGlobalInt( "PointOwner", TEAM_OPFOR )
						
							for k,v in pairs( team.GetPlayers( TEAM_OPFOR ) ) do
								--if v:GetNWBool( "InCapture" ) then
									
									local details = {}
									details.text = "POINT SECURED"
									details.textcol = Color( 215, 215, 215 )
									details.xp = 25
									
									v:GiveXP( 25, details )
								--end
							end
							
						elseif GetGlobalInt( "PointOwner" ) == 1 then
							
							for k,v in pairs( team.GetPlayers( TEAM_OPFOR ) ) do
								--if v:GetNWBool( "InCapture" ) then
									
									local details = {}
									details.text = "POINT NEUTRALISED"
									details.textcol = Color( 215, 215, 215 )
									details.xp = 10
									
									v:GiveXP( 10, details )
								--end
							end
						
							SetGlobalInt( "PointOwner", 0 )
							
						end
						
						net.Start( "StatusUpdate" )
						
							net.WriteString( "pointcaptured" )
							
						net.Broadcast( )
						
					end )
					timer.Create( "RedCapture", 1, CaptureTime, function()
						SetGlobalInt( "CaptureTime", timer.TimeLeft( "RedCapture2" ) )
						for k,v in pairs( team.GetPlayers( TEAM_OPFOR ) ) do
							v:SetNWBool( "InCapture", false )
						end
					end )
				elseif tPaused then
					timer.UnPause( "RedCapture" )
					timer.UnPause( "RedCapture2" )
					
					timer.UnPause( "BlueCapture" )
					timer.UnPause( "BlueCapture2" )
					
					tPaused = false
				end
			end
		elseif redCount < blueCount then
			if timer.Exists( "RedCapture" ) then
				if tPaused then
					timer.UnPause( "RedCapture" )
					timer.UnPause( "RedCapture2" )
					
					timer.UnPause( "BlueCapture" )
					timer.UnPause( "BlueCapture2" )
					
					tPaused = false
				end
				timer.Destroy( "RedCapture" )
				timer.Destroy( "RedCapture2" )
				SetGlobalInt( "Capture", 3 )
			end
			if GetGlobalInt( "PointOwner" ) != TEAM_BLUFOR then
				SetGlobalInt( "Capture", TEAM_BLUFOR )
				if not timer.Exists( "BlueCapture" ) then
					SetGlobalInt( "CaptureTime", CaptureTime )
					timer.Create( "BlueCapture2", CaptureTime, 1, function()
					
						if GetGlobalInt( "PointOwner" ) == 0 then
						
							for k,v in pairs( team.GetPlayers( TEAM_BLUFOR ) ) do
								--if v:GetNWBool( "InCapture" ) then

									local details = {}
									details.text = "POINT SECURED"
									details.textcol = Color( 215, 215, 215 )
									details.xp = 25
									v:GiveXP( 25, details )
								--end
							end
					
							SetGlobalInt( "PointOwner", TEAM_BLUFOR )
							
						elseif GetGlobalInt( "PointOwner" ) == 2 then
						
							for k,v in pairs( team.GetPlayers( TEAM_BLUFOR ) ) do
								--if v:GetNWBool( "InCapture" ) then

									local details = {}
									details.text = "POINT NEUTRALISED"
									details.textcol = Color( 215, 215, 215 )
									details.xp = 10
									v:GiveXP( 10, details )
								--end
							end
						
							SetGlobalInt( "PointOwner", 0 )
							
						end
						
						net.Start( "StatusUpdate" )
						
							net.WriteString( "pointcaptured" )
							
						net.Broadcast( )
						
					end )
					timer.Create( "BlueCapture", 1, CaptureTime, function()
						SetGlobalInt( "CaptureTime", timer.TimeLeft( "BlueCapture2" ) )
						for k,v in pairs( team.GetPlayers( TEAM_BLUFOR ) ) do
							v:SetNWBool( "InCapture", false )
						end
					end )
				elseif tPaused then
					timer.UnPause( "RedCapture" )
					timer.UnPause( "RedCapture2" )
					
					timer.UnPause( "BlueCapture" )
					timer.UnPause( "BlueCapture2" )
					
					tPaused = false
				end
			end
		elseif redCount == blueCount and redCount > 0 and blueCount > 0 then
			if tPaused then
				timer.UnPause( "RedCapture" )
				timer.UnPause( "RedCapture2" )
						
				timer.UnPause( "BlueCapture" )
				timer.UnPause( "BlueCapture2" )
				
				tPaused = false
			elseif not tPaused then
				timer.Pause( "BlueCapture" )
				timer.Pause( "BlueCapture2" )
				timer.Pause( "RedCapture" )
				timer.Pause( "RedCapture2" )
				tPaused = true
			end
			SetGlobalInt( "Capture", 3 )
		else
			if tPaused then
				timer.UnPause( "RedCapture" )
				timer.UnPause( "RedCapture2" )
						
				timer.UnPause( "BlueCapture" )
				timer.UnPause( "BlueCapture2" )
				
				tPaused = false			
			end
			
			if timer.Exists( "RedCapture" ) then
				timer.Destroy( "RedCapture" )
				timer.Destroy( "RedCapture2" )
			end
			if timer.Exists( "BlueCapture" ) then
				timer.Destroy( "BlueCapture" )
				timer.Destroy( "BlueCapture2" )
			end
			SetGlobalInt( "Capture", 3 )
		end
	end

	for k, v in pairs( player.GetAll() ) do

		if v:Alive() and timer.Exists( v:SteamID().. "_SpawnProtection" ) then

			if v:GetVelocity().x != 0 or v:GetVelocity().y != 0 then

				print( "nogod!" )

				timer.Destroy( v:SteamID().. "_SpawnProtection" )
				v:GodDisable()

			end

		end

	end
	
end

function GM:OnNPCKilled( npc, attacker, inflictor )

	if attacker:IsPlayer() then
	
		if npc:GetClass( ) == "npc_strider" then
	
			for k, v in pairs( player.GetAll( ) ) do
			
				if v:Team( ) == 1 or v:Team( ) == 2 then
	
					local details = {}
					details.text = "BOSS KILLED"
					details.textcol = Color( 215, 215, 215 )
					details.xp = 50
			
					v:GiveXP( 50, details )

				end
				
			end
			
		else
		
			local details = {}
			details.text = "NPC KILLED"
			details.textcol = Color( 215, 215, 215 )
			details.xp = 5
		
			attacker:GiveXP( 5, details )
			
		end
		
	end
	
end

function AttachOnPickup( ply, ent )

	local attachments = ent.stringAttachmentIDs
	local wepclass = ent:GetWepClass()

	timer.Simple( 0.5, function()

		if attachments then

			for k, v in pairs( attachments ) do

				print( k, v )

				ply:GetWeapon( wepclass ):attachSpecificAttachment( v )

			end

		end

		if ent.skin and ent.primary then

			ply.PrimarySkin = ent.skin

		end

		if ent.skin and ent.secondary then

			ply.SecondarySkin = ent.skin

		end

		net.Start( "PlayerSkins" )

			net.WriteEntity( ply )
			net.WriteString( ply.PrimarySkin )
			net.WriteString( ply.SecondarySkin )

		net.Broadcast()

	end )

end

function SwapWeapons( ply, ent )

	ply:SwapWeapons( ent:GetWepClass() )

end

function ActuallySwap( ply, ent )

	timer.Simple( 0.2, function()

		ply:SelectWeapon( ent:GetClass() )

	end )

end

hook.Add( "CW20_PickedUpCW20Weapon", "ActuallyAttach", AttachOnPickup )
hook.Add( "CW20_PickedUpCW20Weapon", "SwapWeapons", SwapWeapons )
hook.Add( "CW20_FinishedPickingUpCW20Weapon", "ActuallySwap", ActuallySwap )

local function ForceWeaponSwitch(ply, cmd, args)
   -- Turns out even SelectWeapon refuses to switch to empty guns, gah.
   -- Worked around it by giving every weapon a single Clip2 round.
   -- Works because no weapon uses those.
   local wepname = args[1]
   local wep = ply:GetWeapon(wepname)
   if IsValid(wep) then
      -- Weapons apparently not guaranteed to have this
      if wep.SetClip2 then
         wep:SetClip2(1)
      end
      ply:SelectWeapon(wepname)
   end
end
concommand.Add("switchwep", ForceWeaponSwitch)

local function PopulateServer( )

	RunConsoleCommand( "bot_zombie", 1 )

	for i = 1, 7 do
	
		RunConsoleCommand( "bot" )
		
	end
	
end

concommand.Add( "populateserver", PopulateServer )

local function ChangeGametype( ply, cmd, args )

	if not ply:IsValid() then
	
		MapVote.Start( 20, true, #maplist, maplist )
		
	end
		
end

concommand.Add( "forcevote", ChangeGametype )

local function ChangeGametype2( ply, cmd, args )

	if not ply:IsValid() then
	
		MapVote.Start( 300, true, #maplist, maplist )
		
	end
		
end

concommand.Add( "forcetestvote", ChangeGametype2 )

local function StopVote( ply, cmd, args )

	if not ply:IsValid() then

		MapVotes.Cancel( )
	
	end
	
end

concommand.Add( "stopvote", StopVote )

local function PrintProps( )

	PrintAllProps()
		
end

concommand.Add( "printprops", PrintProps )

local function SyncWeapons( )

	for k, v in pairs( player.GetHumans() ) do

		net.Start( "SyncWeapons" )

			net.WriteString( v:GetPData( "ownedweapons" ) )

		net.Send( v )

	end

end

concommand.Add( "syncweapons", SyncWeapons )

local function SyncSkins( )

	for k, v in pairs( player.GetHumans() ) do

		net.Start( "SyncSkins" )

			net.WriteString( v:GetPData( "ownedskins" ) )

		net.Send( v )

	end

end

concommand.Add( "syncskins", SyncSkins )

local function SyncBoxes( )

	for k, v in pairs( player.GetHumans() ) do

		net.Start( "SyncBoxes" )

			net.WriteString( v:GetPData( "ownedboxes" ) )

		net.Send( v )

	end

end

concommand.Add( "syncboxes", SyncBoxes )

local function AddWeapons( ply )

	if not ply:IsValid() then

		for k, v in pairs( WeaponTable ) do

			for k2, v2 in pairs( v ) do

				player.GetByID( 1 ):AddWeapon( v2.ent )

			end

		end

	end

end

concommand.Add( "addweapons", AddWeapons )

timer.Create( "XPTimer", 30, 0, function()
	if GetGlobalInt( "PointOwner" ) != nil then
		for k,v in pairs( team.GetPlayers( GetGlobalInt( "PointOwner" ) ) ) do
		
			local details = {}
			details.text = "HOLDING CAPTURE POINT"
			details.textcol = Color( 215, 215, 215 )
			details.xp = 10
			v:GiveXP( 10, details )
		end
	end
end )