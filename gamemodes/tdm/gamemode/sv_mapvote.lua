util.AddNetworkString( "MapVotes_Start" )
util.AddNetworkString( "MapVotes_Update" )
util.AddNetworkString( "MapVotes_Cancel" )

net.Receive( "MapVotes_Update", function( len, ply )

	if MapVotes.Allow then

		if IsValid( ply ) then

			local update_type = net.ReadUInt( 3 )

			if update_type == 1 then

				local map = net.ReadString()

				if table.HasValue( MapVotes.CurrentMaps, map ) then

					MapVotes.Votes[ ply:SteamID() ] = map

					net.Start( "MapVotes_Update" )

						net.WriteUInt( 1, 3 )
						net.WriteEntity( ply )
						net.WriteString( map )

					net.Broadcast()

				end

			end

		end

	end

end )

function MapVotes.StartVote( time, maps )

	time = time or 1
	MapVotes.CurrentMaps = maps or { "battle_91", "battle_92", "battle_93" }

	net.Start( "MapVotes_Start" )

		net.WriteTable( MapVotes.CurrentMaps )

	net.Broadcast()

	MapVotes.Allow = true
	MapVotes.Votes = {}

	timer.Create( "MapVotes_Vote", time, 1, function()

		MapVotes.Allow = false
		local results = {}

		for k, v in pairs( MapVotes.Votes ) do

			if ( not results[ v ] ) then

				results[ v ] = 0

			end

			for k2, v2 in pairs( player.GetAll() ) do

				if v2:SteamID() == k then

					results[ v ] = results[ v ] + 1

				end

			end

		end

		local winningmap = table.GetWinningKey( results ) or table.Random( MapVotes.CurrentMaps )

		net.Start( "MapVotes_Update" )

			net.WriteUInt( 3, 3 )

			net.WriteString( winningmap )

		net.Broadcast()

		timer.Simple( 5, function()

			RunConsoleCommand( "changelevel", winningmap )

		end)

	end )

end

function MapVotes.Cancel()

	if MapVotes.Allow then

		MapVotes.Allow = false

		net.Start( "MapVotes_Cancel" )
		net.Broadcast()

		timer.Destroy( "MapVotes_Vote" )

	end

end