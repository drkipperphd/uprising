// Calculation stuff

levels = levels or {}

function levels.LevelFromXP( xp )
	local level = math.floor( 0.1 * math.sqrt( xp ) )
	return level
end

function levels.XPFromLevel( level )
	local xp = ( 10 * level )^2
    return xp
end
	
local plyMeta = FindMetaTable( "Player" )

function plyMeta:GetLevel()
	return levels.LevelFromXP( self:GetNWInt( "xp" ) )
end

function plyMeta:GetXP()
	return tonumber( self:GetNWInt( "xp" ) )
end

function plyMeta:GetWepXP( wep )
	return tonumber( self:GetNWInt( wep.. "_xp" ) )
end

function plyMeta:GetCatXP( cat )
	return tonumber( self:GetNWInt( cat.. "_xp" ) )
end

function plyMeta:GetMoney()
	return tonumber( self:GetNWInt( "money" ) )
end

function plyMeta:CanSpend( money )

	return self:GetNWInt( "money" ) - money >= 0

end

function plyMeta:GetBox( name )

	for k, v in pairs( BoxTable ) do

		for k2, v2 in pairs( v ) do

			if v2.name == name then

				return v2

			end

		end

	end

end