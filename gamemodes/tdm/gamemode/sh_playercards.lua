local plyMeta = FindMetaTable( "Player" )

function plyMeta:GetPlayerCard()

	local badge = self:GetNWInt( "playerCard_badge" )
	local mark = self:GetNWInt( "playerCard_mark" )
	local stripe = self:GetNWInt( "playerCard_stripe" )

	if self:IsBot() then

		badge = math.random( 1, 200 )
		mark = math.random( 1, 200 )
		stripe = math.random( 1, 150 )

	end

	return tonumber( badge ), tonumber( mark ), tonumber( stripe )

end