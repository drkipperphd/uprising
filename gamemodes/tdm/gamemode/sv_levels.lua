// When a player gains xp, if their level is not equal to thier current level then change thier level.

util.AddNetworkString( "XPValidate" )
util.AddNetworkString( "XPReply" )
util.AddNetworkString( "LevelUp" )
util.AddNetworkString( "AddXP" )
util.AddNetworkString( "SyncWeapons" )
util.AddNetworkString( "SyncBoxes" )

net.Receive( "XPReply", function( len, ply )
	ply:BypassGive( ply.ToGive )
	ply.ToGive = 0
end )

local plyMeta = FindMetaTable( "Player" )

function plyMeta:GiveXP( xp, details )
	local level = self:GetLevel()

	if self:GetVIP() then

		xp = xp + ( xp * ( self:GetVIP() * 0.5 ) )

	end

	if level != levels.Cap then
		self:SetNWInt( "xp", self:GetNWInt( "xp" ) + xp )
		self:SetPData( "xp", self:GetNWInt( "xp" ) + xp )
		if self:GetLevel() != level then
			hook.Call( "OnPlayerLevelUp", GAMEMODE, self, self:GetLevel() ) -- Args: Player, Level
			net.Start( "LevelUp" )
			net.WriteEntity( self )
			net.Broadcast()
		end
	end

	if details then

		details.text = details.text or "XP GAINED"
		details.textcol = details.textcol or Color( 215, 215, 215 )

		details.xp = xp

		net.Start( "AddXP" )

		net.WriteTable( details )
		
		net.Send( self )

	end

	hook.Call( "PostPlayerGiveXP", GAMEMODE, self, xp ) -- Args: Player, xp
end

function plyMeta:GiveWepXP( wep, xp )

	if self:GetVIP() then

		xp = xp + ( xp * ( self:GetVIP() * 0.5 ) )

	end

	local newxp = self:GetNWInt( wep.. "_xp" ) + xp

	self:SetNWInt( wep.. "_xp", newxp )
	self:SetPData( wep.. "_xp", newxp )

end

function plyMeta:GiveCatXP( cat, xp )

	local newxp = self:GetNWInt( cat.. "_xp" ) + xp

	self:SetNWInt( cat.. "_xp", newxp )
	self:SetPData( cat.. "_xp", newxp )

end

function plyMeta:GiveMoney( money, skipvip )

	if !skipvip then

		if self:GetVIP() then

			money = money + ( money * ( self:GetVIP() * 0.5 ) )

		end

	end

	local newmoney = self:GetNWInt( "money" ) + money

	self:SetNWInt( "money", newmoney )
	self:SetPData( "money", newmoney )

end

function plyMeta:TakeMoney( money )

	local newmoney = self:GetNWInt( "money" ) - money

	self:SetNWInt( "money", newmoney )
	self:SetPData( "money", newmoney )

end

function plyMeta:SafeTakeMoney( money )

	if self:CanSpend( money ) then

		self:TakeMoney( money )

	end

end

function plyMeta:AddWeapon( ent )

	local weaponlist = util.JSONToTable( self:GetPData( "ownedweapons" ) )
	local contains = false

	for k, v in pairs( weaponlist ) do

		if v == ent then

			contains = true
			break

		end

	end

	if !contains then

		table.insert( weaponlist, ent )
		self:SetPData( "ownedweapons", util.TableToJSON( weaponlist ) )

		net.Start( "SyncWeapons" )

			net.WriteString( util.TableToJSON( weaponlist ) )

		net.Send( self )

	end

end

function plyMeta:AddBox( name )

	local boxlist = util.JSONToTable( self:GetPData( "ownedboxes" ) )
	local contains = false

	for k, v in pairs( boxlist ) do

		if k == name then

			contains = true
			break

		end

	end

	if !contains then

		boxlist[ name ] = 1

	else

		boxlist[ name ] = boxlist[ name ] + 1

	end

	self:SetPData( "ownedboxes", util.TableToJSON( boxlist ) )

	net.Start( "SyncBoxes" )

		net.WriteString( util.TableToJSON( boxlist ) )

	net.Send( self )

end

function plyMeta:HasBox( name )

	local boxlist = util.JSONToTable( self:GetPData( "ownedboxes" ) )

	if boxlist[ name ] then

		return true

	else

		return false

	end

end

function plyMeta:GetBoxCount( name )

	local boxlist = util.JSONToTable( self:GetPData( "ownedboxes" ) )

	if boxlist[ name ] then

		return boxlist[ name ]

	else

		return 0

	end

end

function plyMeta:TakeBox( name )

	local boxlist = util.JSONToTable( self:GetPData( "ownedboxes" ) )

	if self:HasBox( name ) then

		if self:GetBoxCount( name ) >= 1 then

			boxlist[ name ] = boxlist[ name ] - 1

			self:SetPData( "ownedboxes", util.TableToJSON( boxlist ) )

			net.Start( "SyncBoxes" )

				net.WriteString( util.TableToJSON( boxlist ) )

			net.Send( self )

		end

	end

end


hook.Add( "PlayerAuthed", "LevelSetup", function( ply )
	local xp = ply:GetPData( "xp", -1 )
	local money = ply:GetPData( "money", -1 )
	local weapons = weapons.GetList()
	local ownedweapons = ply:GetPData( "ownedweapons", "empty" )
	local ownedboxes = ply:GetPData( "ownedboxes", "empty" )

	GAMEMODE:ChatMessage( ply:Name().. " just joined the game!" )

	if xp == -1 then
		print( "New player, setting up XP..." )
		ply:SetPData( "xp", 0 )
		ply:SetNWInt( "xp", 0 )
	else
		ply:SetNWInt( "xp", xp )
	end

	for k, v in pairs( weapons ) do

		local varname = v.ClassName.. "_xp"

		if ply:GetPData( varname, -1 ) == -1 then

			print( "No data found for ".. v.ClassName )
			ply:SetPData( varname, 0 )
			ply:SetNWInt( varname, 0 )

		else

			print( "Data found for ".. v.ClassName ..", setting to ".. ply:GetPData( varname ) )
			ply:SetNWInt( varname, ply:GetPData( varname ) )

		end

		if GetWepCat( v.ClassName ) then

			local varname = GetWepCat( v.ClassName ).. "_xp"

			if ply:GetPData( varname, -1 ) == -1 then

				print( "No data found for ".. GetWepCat( v.ClassName ) )
				ply:SetPData( varname, 0 )
				ply:SetNWInt( varname, 0 )

			else

				if ply:GetNWInt( varname, -1 ) == -1 then

					print( "Data found for ".. GetWepCat( v.ClassName ) ..", setting to ".. ply:GetPData( varname ) )
					ply:SetNWInt( varname, ply:GetPData( varname ) )

				end

			end

		end

	end

	if money == -1 then

		print( "New player, setting up money..." )
		ply:SetPData( "money", 0 )
		ply:SetNWInt( "money", 0 )

	else

		ply:SetNWInt( "money", money )

	end

	if ownedweapons == "empty" then

		print( "New player, setting up default weapons..." )
		ply:SetPData( "ownedweapons", util.TableToJSON( DefaultWeapons ) )
		
		net.Start( "SyncWeapons" )

			net.WriteString( ply:GetPData( "ownedweapons" ) )

		net.Send( ply )

	else

		net.Start( "SyncWeapons" )

			net.WriteString( ply:GetPData( "ownedweapons" ) )

		net.Send( ply )

	end

	if ownedboxes == "empty" then

		print( "New player, setting up box table..." )
		ply:SetPData( "ownedboxes", util.TableToJSON( {} ) )

		net.Start( "SyncBoxes" )

			net.WriteString( ply:GetPData( "ownedboxes" ) )

		net.Send( ply )

	else

		net.Start( "SyncBoxes" )

			net.WriteString( ply:GetPData( "ownedboxes" ) )

		net.Send( ply )

	end

end )