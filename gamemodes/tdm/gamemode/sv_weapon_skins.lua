util.AddNetworkString( "SyncSkins" )
util.AddNetworkString( "PlayerSkins" )

hook.Add( "PlayerAuthed", "SkinsSetup", function( ply )

	local ownedskins = ply:GetPData( "ownedskins", "empty" )

	if ownedskins == "empty" then

		print( "New player, setting up skins table..." )
		ply:SetPData( "ownedskins", util.TableToJSON( {} ) )
		
		net.Start( "SyncSkins" )

			net.WriteString( ply:GetPData( "ownedskins" ) )

		net.Send( ply )

	else

		net.Start( "SyncSkins" )

			net.WriteString( ply:GetPData( "ownedskins" ) )

		net.Send( ply )

	end

end )


local plyMeta = FindMetaTable( "Player" )

function plyMeta:AddSkin( weapon, id )

	local skinlist = util.JSONToTable( self:GetPData( "ownedskins" ) )
	local contains = false

	print( weapon, id )

	if skinlist[ weapon ] then

		for k, v in pairs( skinlist[ weapon ] ) do

			if v == id then

				contains = true
				break

			end

		end

	else

		skinlist[ weapon ] = {}

	end

	if !contains then

		table.insert( skinlist[ weapon ], id )

		self:SetPData( "ownedskins", util.TableToJSON( skinlist ) )

		net.Start( "SyncSkins" )

			net.WriteString( util.TableToJSON( skinlist ) )

		net.Send( self )

	end

end