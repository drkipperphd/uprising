function GM:PlayerFootstep( ply, pos, foot, sound, volume, rf ) 
	//return true -- Don't allow default footsteps
end

function GM:PlayerStepSoundTime( ply, iType, bWalking )
	
	if ply:GetVelocity():Length() < 325 then
		fStepTime = 565 - ply:GetVelocity():Length()
	else
		fStepTime = 650 - ply:GetVelocity():Length()
	end
	
	return fStepTime
 
end


function GM:HandlePlayerJumping( ply, velocity )
	
	if ( ply:GetMoveType() == MOVETYPE_NOCLIP ) then
		ply.m_bJumping = false;
		return
	end

	-- airwalk more like hl2mp, we airwalk until we have 0 velocity, then it's the jump animation
	-- underwater we're alright we airwalking
	if ( !ply.m_bJumping && !ply:OnGround() && ply:WaterLevel() <= 0 ) then
	
		if ( !ply.m_fGroundTime ) then

			ply.m_fGroundTime = CurTime()
			
		elseif (CurTime() - ply.m_fGroundTime) > 0 && velocity:Length2D() < 0.5 then

			ply.m_bJumping = true
			ply.m_bFirstJumpFrame = false
			ply.m_flJumpStartTime = 0

		end
	end
	
	if ply.m_bJumping then
	
		if ply.m_bFirstJumpFrame then

			ply.m_bFirstJumpFrame = false
			ply:AnimRestartMainSequence()

		end
		
		if ( ply:WaterLevel() >= 2 ) ||	( (CurTime() - ply.m_flJumpStartTime) > 0.2 && ply:OnGround() ) then

			ply.m_bJumping = false
			ply.m_fGroundTime = nil
			ply:AnimRestartMainSequence()

		end
		
		if ply.m_bJumping then
			ply.CalcIdeal = ACT_MP_JUMP
			return true
		end
	end
end

function GM:HandlePlayerDucking( ply, velocity, hold, delta )

	//if ( !ply:Crouching() ) then return false end
	
	if ply.m_Crouch == nil then ply.m_Crouch = 0 end
	
	if ply:KeyDown(IN_DUCK) then
		ply.m_Crouch = ply.m_Crouch + delta * 4
	else
		ply.m_Crouch = ply.m_Crouch - delta * 4
	end
	
	ply.m_Crouch = math.Clamp( ply.m_Crouch, 0, 1 )
	
	//ply:AddVCDSequenceToGestureSlot( GESTURE_SLOT_FLINCH, ply:LookupSequence("cidle_"..hold), 0, true )

end

function GM:HandlePlayerNoClipping( ply, velocity )

end

function GM:HandlePlayerVaulting( ply, velocity )

	if ( velocity:Length() < 1000 ) then return end
	if ( ply:IsOnGround() ) then return end

	ply.CalcIdeal = ACT_MP_SWIM		

end

function GM:HandlePlayerSwimming( ply, velocity )

	if ( ply:WaterLevel() < 2 ) then 
		ply.m_bInSwim = false
		return false 
	end
	
	if ( velocity:Length2D() > 10 ) then
		ply.CalcIdeal = ACT_MP_SWIM
	else
		ply.CalcIdeal = ACT_MP_SWIM_IDLE
	end
		
	ply.m_bInSwim = true
	
end

function GM:HandlePlayerLanding( ply, velocity, WasOnGround ) 
	
	if ply.m_Jumping == nil then ply.m_Jumping = false end
	if ply.m_JumpRate == nil then ply.m_JumpRate = 0 end
	
	if ply.m_Jumping then
		ply.m_JumpRate = math.Clamp(ply.m_JumpRate + 0.1, 0, 1)
	else
		ply.m_JumpRate = math.Clamp(ply.m_JumpRate - 0.1, 0, 1)
	end
	
	ply:AnimSetGestureWeight( 3, ply.m_JumpRate )
	
	if ( ply:GetMoveType() == MOVETYPE_NOCLIP ) then return end
	
	if ( ply:IsOnGround() && !WasOnGround ) then
		ply.m_Jumping = false
		ply:AnimRestartGesture( 2, ACT_LAND, true )
		ply:AnimSetGestureWeight( 2, 0.5 )
	elseif ( !ply:IsOnGround() && WasOnGround ) then
		ply.m_Jumping = true
		ply:AnimRestartGesture( 3, ACT_HL2MP_JUMP_PISTOL, true )
		ply:AnimSetGestureWeight( 3, 0 )
	end

end

function GM:HandlePlayerDriving( ply )

end

hook.Add("Move", "AnimationMove", function( ply, move )

	if ply.m_MoveX == nil then ply.m_MoveX = 0 end
	if ply.m_MoveY == nil then ply.m_MoveY = 0 end
	
	local vel1 = move:GetForwardSpeed() / 10000
	local vel2 = move:GetSideSpeed() / 10000
	
	if vel1 != 0 and ply:OnGround() then
		ply.m_MoveX = math.Clamp(ply.m_MoveX + 0.03 * vel1, -1, 1)
	else
		ply.m_MoveX = ply.m_MoveX * 0.93
		if math.abs( ply.m_MoveX ) < 0.01 then ply.m_MoveX = 0 end
	end
	
	if vel2 != 0 and ply:OnGround() then
		ply.m_MoveY = math.Clamp(ply.m_MoveY + 0.03 * vel2, -1, 1)
	else
		ply.m_MoveY = ply.m_MoveY * 0.93
		if math.abs( ply.m_MoveY ) < 0.01 then ply.m_MoveY = 0 end
	end

end)

function GM:PlayerAnims( ply, vel )
	
    local len = vel:Length2D()
    local movement = 1.0
    
	if !ply:Crouching() then
	
		if len > 220 then
			movement = len/290
		elseif len > 70 then
			movement = len/210
		elseif len > 0.2 then
			movement = len/60
		end
		
	else
		movement = len/100
	end
	
	ply:SetPoseParameter( "move_x", 0 )
	ply:SetPoseParameter( "move_y", 0 )
	
	local hold = "passive"
	
	if IsValid(ply:GetActiveWeapon()) then
		hold = ply:GetActiveWeapon().HoldType or ply:GetActiveWeapon():GetHoldType()
	end
	
	if ply.m_Crouch > 0.5 then
		ply:SetSequence("cwalk_"..hold)
		ply:SetPlaybackRate( 1.25 )
		ply:SetPoseParameter( "move_x", (ply:GetVelocity():Length2D()/ply:GetCrouchedWalkSpeed()) * ply.m_MoveX )
		ply:SetPoseParameter( "move_y", (ply:GetVelocity():Length2D()/ply:GetCrouchedWalkSpeed()) * ply.m_MoveY )
	elseif ply:GetVelocity():Length() > 250 then
		ply:SetSequence("run_all_02")
		ply:SetPlaybackRate( ply:GetVelocity():Length() / 250 )
		ply:SetPoseParameter( "move_x", (ply:GetVelocity():Length2D()/ply:GetRunSpeed()) * ply.m_MoveX )
		ply:SetPoseParameter( "move_y", (ply:GetVelocity():Length2D()/ply:GetRunSpeed()) * ply.m_MoveY )
	else
		ply:SetSequence("run_"..hold)
		ply:SetPoseParameter( "move_x", (ply:GetVelocity():Length2D()/ply:GetWalkSpeed()) * ply.m_MoveX )
		ply:SetPoseParameter( "move_y", (ply:GetVelocity():Length2D()/ply:GetWalkSpeed()) * ply.m_MoveY )
	end
	
	local i = 0
	
	if (math.abs(ply.m_MoveX) > 0 or math.abs(ply.m_MoveY) > 0) then
		if math.abs(ply.m_MoveX) >= math.abs(ply.m_MoveY) then
			i = math.abs(ply.m_MoveX)
		else
			i = math.abs(ply.m_MoveY)
		end
	end
	
	//Idle
	
	if hold == "pistol" or hold == "revolver" then
	
		ply:AnimSetGestureWeight( 0, 1 - i )
		ply:AnimRestartGesture( 0, ACT_HL2MP_IDLE_PISTOL, true )
		
	elseif hold == "grenade" then
	
		ply:AnimSetGestureWeight( 0, 1 - i )
		ply:AnimRestartGesture( 0, ACT_HL2MP_IDLE_GRENADE, true )
		
	else
	
		ply:AnimSetGestureWeight( 0, 1 - i )
		ply:AnimRestartGesture( 0, ACT_HL2MP_IDLE_AR2, true )
	
	end
	
	//Walk Crouch
	
	if hold == "pistol" or hold == "revolver" then

		ply:AnimSetGestureWeight( 1, -1 + ply.m_Crouch + (1 - i) )
		ply:AnimRestartGesture( 1, ACT_HL2MP_IDLE_CROUCH_PISTOL, true )
		
	elseif hold == "grenade" then
	
		ply:AnimSetGestureWeight( 1, -1 + ply.m_Crouch + (1 - i) )
		ply:AnimRestartGesture( 1, ACT_HL2MP_IDLE_CROUCH_GRENADE, true )
		
	else
	
		ply:AnimSetGestureWeight( 1, -1 + ply.m_Crouch + (1 - i) )
		ply:AnimRestartGesture( 1, ACT_HL2MP_IDLE_CROUCH_AR2, true )

	end
		
    rate = math.min( movement, 2 )
    if ( ply:WaterLevel() >= 2 ) then
        rate = math.max( rate, 0.5 )
    elseif ( !ply:IsOnGround() && len >= 1000 ) then 
        rate = 0.1;
    end
    
	
end

function GM:UpdateAnimation( ply, velocity, maxseqgroundspeed )    

	self:PlayerAnims( ply, velocity )
	
    if ( ply:InVehicle() ) then

        local Vehicle =  ply:GetVehicle()
        
        -- We only need to do this clientside..
        if ( CLIENT ) then
            --
            -- This is used for the 'rollercoaster' arms
            --
            local Velocity = Vehicle:GetVelocity()
            local fwd = Vehicle:GetUp()                       
            local dp = fwd:Dot( Vector(0,0,1) )
            local dp2 = fwd:Dot( Velocity )

            ply:SetPoseParameter( "vertical_velocity", (dp<0 and dp or 0)+dp2*0.005 ) 

            -- Pass the vehicles steer param down to the player
            local steer = Vehicle:GetPoseParameter( "vehicle_steer" )
            steer = steer * 2 - 1 -- convert from 0..1 to -1..1
            ply:SetPoseParameter( "vehicle_steer", steer  ) 
        end
        
    end
    
    if ( CLIENT ) then
        GAMEMODE:MouthMoveAnimation( ply )
    end
    
end

local d = CurTime()

function GM:CalcMainActivity( ply, velocity )
	
	local delta = CurTime() - d
	local hold = "passive"
	
	self:HandlePlayerLanding( ply, velocity, ply.m_bWasOnGround, hold, delta)
	self:HandlePlayerNoClipping( ply, velocity, hold, delta ) 
	self:HandlePlayerDriving( ply, velocity, hold, delta )
	self:HandlePlayerVaulting( ply, velocity, hold, delta )
	self:HandlePlayerJumping( ply, velocity, hold, delta )
	self:HandlePlayerDucking( ply, velocity, hold, delta )
	self:HandlePlayerSwimming( ply, velocity, hold, delta )
	
	ply.m_bWasOnGround = ply:IsOnGround()
	ply.m_bWasNoclipping = (ply:GetMoveType() == MOVETYPE_NOCLIP)
	d = CurTime()

	return nil, nil

end