GM.Name = "Team Deathmatch"
GM.TeamBased = true

local plyMeta = FindMetaTable( "Player" )

TEAM_BLUFOR = 1
TEAM_OPFOR = 2

CLASS_GREN = 1
CLASS_AUTO = 2
CLASS_ASS = 3
CLASS_MARK = 4
CLASS_PILOT = 5
CLASS_MEDIC = 6

CapturePos = CapturePos or nil
CaptureRadius = CaptureRadius or nil
CaptureTime = CaptureTime or nil
CaptureTimes = 1

CapturePoints = { }
CapturePoints.points = { }

CustomizableWeaponry.customizationEnabled = false

local gametype = CreateConVar( "uprising_gametype", "Conquest", { FCVAR_ARCHIVE, FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED } )
local maxkills = CreateConVar( "uprising_maxkills", 1000, { FCVAR_ARCHIVE, FCVAR_SERVER_CAN_EXECUTE, FCVAR_REPLICATED } )
local PlayerMeta = FindMetaTable( "Player" )

function GM:PlayerFootstep( ply, pos, foot, sound, volume, rf )

	return ply:Crouching()

end

function GM:PlayerStepSoundTime( ply, iType, bWalking )
	
	if ply:GetVelocity():Length() < 325 then
		fStepTime = 565 - ply:GetVelocity():Length()
	else
		fStepTime = 650 - ply:GetVelocity():Length()
	end
	
	return fStepTime
 
end

function PlayerMeta:OppositeTeam( )

	if self:Team( ) == 1 then
	
		return 2
		
	elseif self:Team( ) == 2 then
	
		return 1
		
	end
	
end

function GetPrimaries()

	local newprimaries = {}

	for k, v in pairs( ClassTable ) do

		for k2, v2 in pairs( v.primary ) do

			for k3, v3 in pairs( v2 ) do

				newprimaries[ v3.ent ] = v3.ent

			end

		end

	end

	return newprimaries

end

function PlayerMeta:GetPrimary()

	if IsValid( self ) and self.Class then

		for k, v in pairs( GetPrimaries() ) do

			if self:HasWeapon( v ) then

				return self:GetWeapon( v )

			end

		end

	end

end

function GetSecondaries()

	local newprimaries = {}

	for k, v in pairs( ClassTable ) do

		for k2, v2 in pairs( v.secondary ) do

			for k3, v3 in pairs( v2 ) do

				newprimaries[ v3.ent ] = v3.ent

			end

		end

	end

	return newprimaries

end

function PlayerMeta:GetSecondary()

	if IsValid( self ) and self.Class then

		for k, v in pairs( GetSecondaries() ) do

			if self:HasWeapon( v ) then

				return self:GetWeapon( v )

			end

		end

	end

end

function PlayerMeta:GetVIP()

	if self:IsUserGroup( "vip" ) then

		return 1

	elseif self:IsUserGroup( "vip2" ) then

		return 2

	else

		return false

	end

end

gametype = string.lower( GetConVar( "uprising_gametype" ):GetString( ) )

function RegisterNiceNames()

	gamemaps = {}
	gamemaps[ "battle_91" ] = "Water Gate"
	gamemaps[ "battle_92" ] = "Lockdown"
	gamemaps[ "battle_93" ] = "Truss Bridge"
	gamemaps[ "battle_94" ] = "Collapse City"
	gamemaps[ "battle_95" ] = "Sunset Coast"
	gamemaps[ "battle_97" ] = "Lost Town"
	gamemaps[ "battle_98" ] = "Dark Fortress"
	gamemaps[ "battle_99" ] = "Station C17"
	gamemaps[ "battle_100" ] = "Nexus Building"
	gamemaps[ "battle_102" ] = "Steam Lab"
	gamemaps[ "battle_103" ] = "Combine's Nest"
	gamemaps[ "battle_104" ] = "Last Garden"
	gamemaps[ "mission_03_02_04" ] = "Citadel Warfare"

	for k, v in pairs( file.Find( "maps/cs_*.bsp", "GAME" ) ) do

		local map = string.StripExtension( v )
		local nicemap = string.TrimLeft( map, "cs_" )
		nicemap = string.SetChar( nicemap, 1, string.upper( nicemap[ 1 ] ) )
			
		gamemaps[ map ] = nicemap
			
	end

	for k, v in pairs( file.Find( "maps/de_*.bsp", "GAME" ) ) do

		local map = string.StripExtension( v )
		local nicemap = string.TrimLeft( map, "de_" )
		nicemap = string.SetChar( nicemap, 1, string.upper( nicemap[ 1 ] ) )
			
		gamemaps[ map ] = nicemap
			
	end

	for k, v in pairs( file.Find( "maps/dm_*.bsp", "GAME" ) ) do

		local map = string.StripExtension( v )
		local nicemap = string.TrimLeft( map, "dm_" )
		nicemap = string.SetChar( nicemap, 1, string.upper( nicemap[ 1 ] ) )
			
		gamemaps[ map ] = nicemap
		
	end

end

function CapturePoints:Register( tbl )

	self.points[ tbl.id ] = tbl
	
end

function CapturePoints:Get( name )

	return self.points[ name ]
	
end

function CapturePoints:GetAll()

	return self.points
	
end

local Point = { }

Point.id = "A"
Point.pos = Vector( -7861, -2895, -10824 )
Point.alias = "Point A"
Point.owner = 1
Point.redCount = 0
Point.blueCount = 0

CapturePoints:Register( Point )

local Point = { }

Point.id = "B"
Point.pos = Vector( -40, -1947, -11032 )
Point.alias = "Point B"
Point.owner = 0
Point.redCount = 0
Point.blueCount = 0

CapturePoints:Register( Point )

local grenadier = [[
	<html>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<body style="background-color: #187F0B; color: #FFFFFF">
		As grenadier, you are equiped with a shotgun, a pistol and 5 grenades. You have very strong armour.<br/><br/>
		Being the grenadier also lets you have a laucher of some type.<br/><br/>
		Due to your large loadout, you cannot run as fast as other classes.
	</body>
	</html>
]]

local medic = [[
	<html>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<body style="background-color: #187F0B; color: #FFFFFF">
		As a combat medic, you carry an assault rifle and a medkit.<br/><br/>
		You do not carry any grenades.<br/><br/>
		You are able to run faster than the assault rifleman, however, you are slower than the pilot.
	</body>
	</html>
]]

local autorife = [[
	<html>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<body style="background-color: #187F0B; color: #FFFFFF">
		As auto-rifleman, you are equipped with a Light Machine Gun, a pistol and 3 grenades. You have strong armour.<br/><br/>
		Being the auto-rifleman lets you have the Light Machine Gun.<br/><br/>
		Due to the large gun you are carrying, this makes you slower than other classes but faster than the grenadier.
	</body>
	</html>
]]

local assrife = [[
	<html>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<body style="background-color: #187F0B; color: #FFFFFF">
		As assault rifleman, you are equipped with a fully automatic assault rifle, a pistol and 1 grenade. You have a small vest and helmet.<br/><br/>
		Being a assault rifleman allows you to sprint for long periods of time due to your smaller loadout.<br/><br/>
		You can drive land vehicles in this class unlike the other classes.
	</body>
	</html>
]]

local marksman = [[
	<html>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<body style="background-color: #187F0B; color: #FFFFFF">
		As marksman, you are equipped with a sniper rifle, a pistol and one SLAM. You have a small vest and helmet. <br/><br/>
		Being a marksman allows you access to a long range sniper rifle and a SLAM which could be used as an Ambush device.<br/><br/>
		You are able to drive land vehicles to detestation, but this isn't recommended as it can give away your position.
	</body>
	</html>
]]

local pilot = [[
	<html>
	<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">
	<body style="background-color: #187F0B; color: #FFFFFF">
		As pilot, you are equipped with a sub-machine, a pistol and a health kit. You are have a light kevlar vest. <br/><br/>
		Being a pilot allows you to drive land vehicles but also drive air vehicles. You are used mainly to air-drop troops into places easily and extract them from places more easily.<br/><br/>
		You can also serve as a co-pilot or helicopter crew in case of an emergency crash landing.
	</body>
	</html>
]]

ClassTable = {}
VehicleTable = {}
AttachmentTable = {}
WeaponTable = {}
GrenadeTable = {}
ArmsRaceTable = {}
DefaultWeapons = {}
BoxTable = {}

local function AddTeam( id, data )
	ClassTable[ id ] = data
	
	for k, v in pairs( data.oModel ) do
	
		util.PrecacheModel( v or "" )
		
	end
	
	for k, v in pairs( data.bModel ) do
	
		util.PrecacheModel( v or "" )
		
	end
	
end
local function AddAttachment( id, level )

	AttachmentTable[ id ] = {}
	AttachmentTable[ id ].level = level
	
end
local function AddNewWeapon( cat, entname, scorereq, show, rarity )
	
	WeaponTable[ cat ] = WeaponTable[ cat ] or {}

	if scorereq == 0 then

		table.insert( DefaultWeapons, entname )

		PrintTable( DefaultWeapons )

	end

	if show == null then

		show = true

	end

	if rarity == null then

		rarity = 0

	end
	
	table.insert( WeaponTable[ cat ], { ent = entname, price = scorereq, show = show, rarity = rarity } )
	
end
local function AddNewBox( cat, weps, name, price, icon )

	BoxTable[ cat ] = BoxTable[ cat ] or {}

	if !icon then

		icon = "http://theuprisingcommunity.esy.es/boxicons/plain_pack.png"

	else

		icon = "http://theuprisingcommunity.esy.es/boxicons/".. icon .. ".png"

	end

	print( icon )

	table.insert( BoxTable[ cat ], { name = name, weps = weps, price = price, icon = icon } )

end

local function AddGrenade( entname )

	GrenadeTable[ entname ] = entname
	
end
local function AddArmsRaceGun( cat, order, count )

	if cat == "knife" then
	
		table.insert( ArmsRaceTable, "fas2_strider" )
		
	else

		local weplist = WeaponTable[ cat ]
		
		local lcount = 0
		
		for k, v in RandomPairs( weplist ) do
		
			if lcount < count then
		
				table.insert( ArmsRaceTable, v.ent )
				
				lcount = lcount + 1
				
			end
			
		end
		
	end
	
end

function GetWep( ent )

	for k, v in pairs( WeaponTable ) do

		for k2, v2 in pairs( v ) do

			if v2.ent == ent then

				return v2

			end

		end

	end

end

function GetBox( name )

	for k, v in pairs( BoxTable ) do

		for k2, v2 in pairs( v ) do

			if v2.name == name then

				return v2

			end

		end

	end

end

function GetWepCat( ent )

	for k, v in pairs( WeaponTable ) do
	
		for k2, v2 in pairs( v ) do
	
			if v2.ent == ent then
			
				return k
				
			end
			
		end
		
	end
	
end

local function SortCategory( cat )

	table.sort( WeaponTable[ cat ], function( a, b )
	
		return a.price < b.price
		
	end )

end

-- Pistols
AddNewWeapon( "pistol", "draggo_p226", 5000, true, 0 )
AddNewWeapon( "pistol", "draggo_glockp80", 5000, true, 0 )
AddNewWeapon( "pistol", "draggo_pl14", 5000, true, 0 )
AddNewWeapon( "pistol", "draggo_usp", 5000, true, 0 )
AddNewWeapon( "pistol", "draggo_p99", 10000, true, 1 )
AddNewWeapon( "pistol", "draggo_python", 15000, true, 1 )
AddNewWeapon( "pistol", "draggo_unica6", 20000, true, 2 )

SortCategory( "pistol" )

-- Carbines
AddNewWeapon( "carbine", "draggo_m4a1", 2000, true, 0 )
AddNewWeapon( "carbine", "draggo_ak5c", 15000, true, 1 )
AddNewWeapon( "carbine", "draggo_g36c", 20000, true, 2 )


SortCategory( "carbine" )

-- Shotguns

AddNewWeapon( "shotgun", "draggo_870", 2000, true, 0 )
AddNewWeapon( "shotgun", "draggo_model1887", 10000, true, 1 )
AddNewWeapon( "shotgun", "draggo_spas12", 15000, true, 1 )
AddNewWeapon( "shotgun", "draggo_870mcs", 15000, true, 1 )
AddNewWeapon( "shotgun", "draggo_usas12", 25000, true, 2 )
AddNewWeapon( "shotgun", "draggo_ksg", 20000, true, 2 )

SortCategory( "shotgun" )

-- Assault Rifles
AddNewWeapon( "assault rifle", "draggo_ak74m", 2000, true, 0 )
AddNewWeapon( "assault rifle", "draggo_cm901", 15000, true, 1 )
AddNewWeapon( "assault rifle", "draggo_l85a2", 20000, true, 2 )
AddNewWeapon( "assault rifle", "draggo_g3a3", 25000, true, 2 )
AddNewWeapon( "assault rifle", "draggo_famasf1", 25000, true, 2 )
AddNewWeapon( "assault rifle", "draggo_galil", 20000, true, 2 )
AddNewWeapon( "assault rifle", "draggo_galilchan", 100000, false, 4 )
AddNewWeapon( "assault rifle", "draggo_msbs", 40000, false, 4 )
AddNewWeapon( "assault rifle", "draggo_scarh", 40000, false, 4 )
AddNewWeapon( "assault rifle", "draggo_auga1", 40000, false, 4 )
AddNewWeapon( "assault rifle", "draggo_masada", 10000000, false, 4 )
AddNewWeapon( "assault rifle", "draggo_garand", 50000, false, 4 )

SortCategory( "assault rifle" )

-- Personal Defense Weapons
AddNewWeapon( "pdw", "draggo_mp5", 2000, true, 0 )
AddNewWeapon( "pdw", "draggo_cbjms", 20000, true, 1 )
AddNewWeapon( "pdw", "draggo_p90", 25000, true, 2 )
AddNewWeapon( "pdw", "draggo_bizon", 30000, false, 4 )
AddNewWeapon( "pdw", "draggo_vector", 40000, false, 4)
AddNewWeapon( "pdw", "draggo_mp7", 40000, false, 4)

SortCategory( "pdw" )

-- Light Machine Guns
AddNewWeapon( "lmg", "draggo_rpk", 2000, true, 1 )
AddNewWeapon( "lmg", "draggo_mg4", 30000, true, 2 )

SortCategory( "lmg" )

-- Snipers
AddNewWeapon( "sniper", "draggo_dragunov", 2000, true, 0 )
AddNewWeapon( "sniper", "draggo_awm", 5000, true, 1 )
AddNewWeapon( "sniper", "draggo_m98b", 30000, true, 2 )
AddNewWeapon( "sniper", "draggo_sr25", 20000, true, 2 )
AddNewWeapon( "sniper", "draggo_intervenion", 50000, false, 4 )

SortCategory( "sniper" )

-- Grenades
AddNewWeapon( "grenades", "cw_frag_grenade", 5000 )
AddNewWeapon( "grenades", "cw_flash_grenade", 5000 )
AddNewWeapon( "grenades", "cw_smoke_grenade", 5000 )

SortCategory( "grenades" )

-- Melee Weapons
AddNewWeapon( "melee", "draggo_strider", 10000, true, 0 )
AddNewWeapon( "melee", "draggo_kabarknife", 10000, true, 0 )
AddNewWeapon( "melee", "draggo_karambitknife", 10000, true, 1 )
AddNewWeapon( "melee", "draggo_sogknife", 10000, true, 0 )
AddNewWeapon( "melee", "draggo_diveknife", 10000, true, 2 )

SortCategory( "melee" )

AddNewBox( "starter", { "draggo_p226", "draggo_ak74m", "draggo_kabarknife", "cw_frag_grenade" }, "Assault Starter Crate", 5000, "assault_box" )
AddNewBox( "starter", { "draggo_usp", "draggo_mp5", "draggo_kabarknife", "cw_frag_grenade" }, "Engineer Starter Crate",  5000, "engineer_box" )
AddNewBox( "starter", { "draggo_glockp80", "draggo_rpk", "draggo_kabarknife", "cw_flash_grenade" }, "Support Starter Crate", 5000, "support_box" )
AddNewBox( "starter", { "draggo_pl14", "draggo_dragunov", "draggo_kabarknife", "cw_smoke_grenade" }, "Recon Starter Crate", 5000, "recon_box" )

--AddNewBox( "anal fun", { "draggo_usp", "draggo_awm", "draggo_mg4", "draggo_rpk", "draggo_spas12" }, "bum fun time 4 me", 0 )
--Promotional 
--AddNewBox( "Promotional", { "draggo_p226", "draggo_dragunov","draggo_dragunov", "draggo_kabarknife", "draggo_galil", "draggo_galilchan" }, "Galil-Chan Box", 1500, "galil_box" )

--PDW
AddNewBox( "PDW", { "draggo_mp5","draggo_mp5", "draggo_glockp80","draggo_glockp80", "draggo_kabarknife", "draggo_p90", "draggo_vector" }, "KRISS Vector Box", 2000, "vector_box" )
AddNewBox( "PDW", { "draggo_mp5","draggo_mp5", "draggo_glockp80", "draggo_kabarknife", "draggo_kabarknife", "draggo_cbjms", "draggo_bizon" }, "PP-19 Bizon Box", 2000, "pdw_box" )
AddNewBox( "PDW", { "draggo_mp5", "draggo_mp5", "draggo_glockp80", "draggo_kabarknife", "draggo_kabarknife", "draggo_p90", "draggo_mp7" }, "MP7 Box", 2000, "pdw_box" )

--Assault
AddNewBox( "Assault", { "draggo_ak74m", "draggo_ak74m", "draggo_pl14", "draggo_kabarknife","draggo_kabarknife", "draggo_famasf1", "draggo_msbs" }, "MSBS-K Box", 2000 )
AddNewBox( "Assault", { "draggo_ak74m","draggo_ak74m", "draggo_pl14", "draggo_kabarknife", "draggo_kabarknife", "draggo_famasf1", "draggo_auga1" }, "AUG-A1 Box", 2000 )

--Sniper
AddNewBox( "Recon", { "draggo_dragunov","draggo_dragunov", "draggo_pl14", "draggo_kabarknife", "draggo_kabarknife", "draggo_m98b", "draggo_intervenion" }, "Intervention Box", 2000 )


--Limited time weapones
AddNewBox( "Elite", { "draggo_dragunov","draggo_ak74m", "draggo_usp", "draggo_kabarknife", "draggo_kabarknife", "draggo_galil", "draggo_masada" }, "Masada Box", 2500 )




-- Global Attachments


-- Sights -- 




--Scopes
AddAttachment( "md_draggo_specialmidscop", 1000 )
AddAttachment( "md_draggo_midscope", 500 )
AddAttachment( "md_draggo_closescope", 100 )
AddAttachment( "md_draggo_defaultscope", 0 )
AddAttachment( "md_draggo_smallscope", 300 )

-- Zoomed Optics
AddAttachment( "md_draggo_acog", 1800 )
AddAttachment( "md_draggo_smgrarescope", 1800 )
AddAttachment( "md_draggo_elcanspecter", 1400 )
AddAttachment( "md_draggo_smgadvscope", 1400 )
AddAttachment( "md_draggo_elcan", 1000 )
AddAttachment( "md_draggo_smgscope", 1000 )

-- Holographic
AddAttachment( "md_draggo_aimpoint", 900 )
AddAttachment( "md_draggo_eotech", 600 )
AddAttachment( "md_draggo_barskards", 500 )
AddAttachment( "md_draggo_t1", 100 )
AddAttachment( "md_draggo_rmr", 100 )


-- Muzzle attachments
AddAttachment( "md_draggo_muzzlebrake", 1200 )

--Silencers

AddAttachment( "md_draggo_assaultsilencer", 400 )
AddAttachment( "md_draggo_shotgunsilencer", 400 )
AddAttachment( "md_draggo_smgsilencer", 400 )
AddAttachment( "md_draggo_snipersilencer", 400 )
AddAttachment( "md_draggo_sharedsilencer", 200 )
AddAttachment( "md_draggo_pistolsilencer", 400 )


--Laser Modules
AddAttachment( "md_draggo_anpeq15", 700 )
AddAttachment( "md_draggo_lam", 400 )

-- Grips
AddAttachment( "md_draggo_assaultbigrip", 1600 )
AddAttachment( "md_draggo_assaultgrip", 1350 )
AddAttachment( "md_draggo_sharedgrip", 350 )


-- Bipods
AddAttachment( "md_draggo_sniperbipod", 650 )


-- Weapon Specifics


-- Sights and scopes
AddAttachment( "bg_lynxscope", 0 )
AddAttachment( "bg_m40a1scope", 0 )
AddAttachment( "bg_lukepsg1scope", 0 )
AddAttachment( "md_nightforce_nxs", 0 )
AddAttachment( "bg_sg1scope", 0 )
AddAttachment( "bg_draggo_dragunovscope", 0 )
AddAttachment( "bg_draggo_l118scope", 0 )

--Barrels
AddAttachment( "bg_draggo_mp5sd", 0 )

-- Stocks
AddAttachment( "bg_draggo_spas12_stock", 0 )

-- Magazines
AddAttachment( "md_hs10ext", 10 )

AddTeam( CLASS_ASS, {
	name = "Assault",
	desc = assrife,
	bModel = { "models/steinman/bf4/us_04.mdl" },
	oModel = { "models/steinman/bf4/ru_04.mdl" },
	primary = {
		WeaponTable[ "assault rifle"],
		WeaponTable[ "carbine" ],
		WeaponTable[ "shotgun" ],
	},
	secondary = { 
		WeaponTable[ "pistol" ]
	},
	grenade = {
		WeaponTable[ "grenades" ]
	},
	melee = {
		WeaponTable[ "melee" ]
	},
	grenades = 1,
	canEnter = true,
	canSpawnCars = true,
	canSpawnFlying = false,
	speed = 0.8,
	armour = 0,
	donator = false
} )

AddTeam( CLASS_GREN, {
	name = "Engineer",
	desc = grenadier,
	bModel = { "models/steinman/bf4/us_02.mdl" },
	oModel = { "models/steinman/bf4/ru_02.mdl" },
	primary = {
		WeaponTable[ "pdw" ],
		WeaponTable[ "carbine" ],
		WeaponTable[ "shotgun" ]
	},
	secondary = { 
		WeaponTable[ "pistol" ]
	},
	grenade = {
		WeaponTable[ "grenades" ]
	},
	melee = {
		WeaponTable[ "melee" ]
	},
	grenades = 5,
	canEnter = false,
	canSpawnCars = false,
	canSpawnFlying = false,
	speed = 0.7,
	armour = 0,
	donator = false
} )

AddTeam( CLASS_AUTO, {
	name = "Support",
	desc = autorife,
	bModel = { "models/steinman/bf4/us_01.mdl" },
	oModel = { "models/steinman/bf4/ru_01.mdl" },
	primary = { 
		WeaponTable[ "lmg" ],
		WeaponTable[ "carbine" ],
		WeaponTable[ "shotgun" ],
	},
	secondary = { 
		WeaponTable[ "pistol" ]
	},
	grenade = {
		WeaponTable[ "grenades" ]
	},
	melee = {
		WeaponTable[ "melee" ]
	},
	grenades = 5,
	canEnter = false,
	canSpawnCars = false,
	canSpawnFlying = false,
	speed = 0.7,
	armour = 0,
	donator = false
} )

AddTeam( CLASS_MARK, {
	name = "Recon",
	desc = marksman,
	bModel = { "models/steinman/bf4/us_03.mdl" },
	oModel = { "models/steinman/bf4/ru_03.mdl" },
	primary = { 
		WeaponTable[ "sniper" ],
		WeaponTable[ "carbine" ],
		WeaponTable[ "shotgun" ]
	},
	secondary = { 
		WeaponTable[ "pistol" ]
	},
	grenade = {
		WeaponTable[ "grenades" ]
	},
	melee = {
		WeaponTable[ "melee" ]
	},
	grenades = 0,
	canEnter = true,
	canSpawnCars = true,
	canSpawnFlying = false,
	speed = 0.8,
	armour = 0,
	donator = false
} )

local function PlayBreencast( )

	local breen1 = CreateSound( speakera, Sound( "vo/breencast/br_welcome01.wav" ) )
	breen1:SetSoundLevel( 511 )
	breen1:SetDSP( 57 )
	
	local breen2 = CreateSound( speakera, Sound( "vo/breencast/br_welcome02.wav" ) )
	breen2:SetSoundLevel( 511 )
	breen2:SetDSP( 57 )
	
	local breen3 = CreateSound( speakera, Sound( "vo/breencast/br_welcome03.wav" ) )
	breen3:SetSoundLevel( 511 )
	breen3:SetDSP( 57 )
	
	local breen4 = CreateSound( speakera, Sound( "vo/breencast/br_welcome04.wav" ) )
	breen4:SetSoundLevel( 511 )
	breen4:SetDSP( 57 )
	
	local breen5 = CreateSound( speakera, Sound( "vo/breencast/br_welcome05.wav" ) )
	breen5:SetSoundLevel( 511 )
	breen5:SetDSP( 57 )
	
	local breen6 = CreateSound( speakera, Sound( "vo/breencast/br_welcome06.wav" ) )
	breen6:SetSoundLevel( 511 )
	breen6:SetDSP( 57 )
	
	local breen7 = CreateSound( speakera, Sound( "vo/breencast/br_welcome07.wav" ) )
	breen7:SetSoundLevel( 511 )
	breen7:SetDSP( 57 )
	
	breen1:Play( )
				
	timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome01.wav" ) ) + 1, function( )
				
		breen2:Play( )
					
		timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome02.wav" ) ), function( )
					
			breen3:Play( )
						
			timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome03.wav") ), function( )
					
				breen4:Play( )
							
				timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome04.wav" ) ), function( )
							
					breen5:Play( )
								
					timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome05.wav" ) ), function( )
								
						breen6:Play( )
									
						timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome06.wav" ) ), function( )
									
							breen7:Play( )
										
							timer.Simple( SoundDuration( Sound( "vo/breencast/br_welcome07.wav" ) ) + 10, function( )
										
								PlayBreencast( )
											
							end )
										
						end )
									
					end )
								
				end )
							
			end )
						
		end )
				
	end )

end
	
function GM:Initialize( )

	RegisterNiceNames()
	
	if SERVER then
	
		RunConsoleCommand( "uprising_maxkills", math.Max( 25, #player.GetAll( ) * 5 ) )
	
		if gametype == "conquest" then
	
			SetGlobalInt( "bTime", CaptureTimer )
			SetGlobalInt( "oTime", CaptureTimer )
			SetGlobalInt( "PointNumber", 1 )
			SetGlobalInt( "CaptureRadius", CaptureRadius )
			SetGlobalString( "CaptureAlias", CaptureAlias )
			SetGlobalVector( "CapturePos", CapturePos )
			SetGlobalInt( "CaptureTimer", CaptureTimer )
			
		else
		
			SetGlobalInt( "rTime", 1200 )
			
		end
	
		if gametype == "arms race" then
		
			AddArmsRaceGun( "pdw", 1, 3 )
			AddArmsRaceGun( "assault rifle", 2, 4 )
			AddArmsRaceGun( "carbine", 3, 1 )
			AddArmsRaceGun( "shotgun", 3, 1 )
			AddArmsRaceGun( "sniper", 4, 2 )
			AddArmsRaceGun( "lmg", 5, 1 )
			AddArmsRaceGun( "pistol", 6, 4 )
			AddArmsRaceGun( "knife" )
			
		end
		
		RunConsoleCommand( "hostname", "Uprising [".. gamemaps[ game.GetMap( ) ] .."]" )
		
		for k, v in ipairs(CustomizableWeaponry.registeredAttachments) do
			
			RunConsoleCommand( v.cvar, 0 )
			
		end
		
		timer.Simple( 5, function( )
		
			for k, v in pairs( ents.GetAll( ) ) do
		
				if v:GetClass( ) == "env_sprite" then
				
					v:Remove( )
					
				end
		
				if string.find( v:GetClass( ), "ammo" ) then
				
					v:Remove( )
					
				end
				
				if string.find( v:GetClass( ), "item_" ) then
				
					v:Remove( )
					
				end
				
				if string.find( v:GetClass( ), "weapon_" ) then
				
					v:Remove( )
					
				end
				
				if v:GetClass( ) == "game_player_equip" then
				
					v:Remove( )
					
				end
				
				if v:GetClass( ) == "npc_template_maker" then
				
					if !string.find( v:GetName( ), "str" ) then
					
						v:SetKeyValue( "MaxLiveChildren", "3" )
						v:SetKeyValue( "MaxNPCCount", "3" )
						
					end
					
				end
				
			end
			
		end )
		
		if game.GetMap( ) == "battle_99" then
		
			timer.Simple( 5, function( )
		
				consolea = ents.Create( "prop_dynamic" )
				consolea:SetModel( "models/props_combine/combine_interface001.mdl" )
				consolea:SetPos( Vector( -6045, -3058, 88 ) )
				consolea:SetAngles( Angle( 0, -45, 0 ) )
				consolea:SetSolid( 6 )
				consolea:Spawn( )
				
				consoleb = ents.Create( "prop_dynamic" )
				consoleb:SetModel( "models/props_combine/combine_intmonitor001.mdl" )
				consoleb:SetPos( Vector( -6065, -3038, 140 ) )
				consoleb:SetAngles( Angle( 0, -45, 0 ) )
				consoleb:SetSolid( 6 )
				consoleb:Spawn( )
				
				speakera = ents.Create( "prop_dynamic" )
				speakera:SetModel( "models/props_wasteland/speakercluster01a.mdl" )
				speakera:SetPos( Vector( -3950, -2519, 403 ) )
				speakera:SetAngles( Angle( 0, -130, 0 ) )
				speakera:Spawn( )
				
				speakerb = ents.Create( "prop_dynamic" )
				speakerb:SetModel( "models/props_wasteland/speakercluster01a.mdl" )
				speakerb:SetPos( Vector( -3950, -3626, 403 ) )
				speakerb:SetAngles( Angle( 0, 130, 0 ) )
				speakerb:Spawn( )
				
				PlayBreencast( )
				
			end )
			
		elseif game.GetMap( ) == "battle_92" then
		
			timer.Simple( 5, function( )
			
				covera = ents.Create( "prop_dynamic" )
				covera:SetModel( "models/props_debris/metal_panel01a.mdl" )
				covera:SetPos( Vector( -2982.4741210938, 4330.03125, 188.03125 ) )
				covera:SetAngles( Angle( 0, 90, 75 ) )
				covera:SetSolid( 6 )
				covera:Spawn( )
				
				wall = ents.Create( "prop_dynamic" )
				wall:SetModel( "models/props_debris/tile_wall001a.mdl" )
				wall:SetPos( Vector( -3456, 5546, 192 ) )
				wall:SetAngles( Angle( 0, 90, 0 ) )
				wall:SetSolid( 6 )
				wall:Spawn( )

				wall2 = ents.Create( "prop_dynamic" )
				wall2:SetModel( "models/props_debris/concrete_section128wall001c.mdl" )
				wall2:SetPos( Vector( -3392, 4647, 192 ) )
				wall2:SetAngles( Angle( 0, -90, 0 ) )
				wall2:SetSolid( 6 )
				wall2:Spawn( )
				
				booth = ents.Create( "prop_dynamic" )
				booth:SetModel( "models/props_combine/combine_booth_short01a.mdl" )
				booth:SetPos( Vector( -3008.8881835938, 4512.5810546875, 152.03125 ) )
				booth:SetSolid( 6 )
				booth:Spawn( )
				
				bar1 = ents.Create( "prop_dynamic" )
				bar1:SetModel( "models/props_combine/combine_barricade_tall01b.mdl" )
				bar1:SetPos( Vector( -2614.235107, 4960.970215, 6 ) )
				bar1:SetAngles( Angle( 0, -90, 0 ) )
				bar1:SetSolid( 6 )
				bar1:Spawn( )
				
				bar2 = ents.Create( "prop_dynamic" )
				bar2:SetModel( "models/props_combine/combine_barricade_tall01b.mdl" )
				bar2:SetPos( Vector( -2674.235107, 4960.970215, 6 ) )
				bar2:SetAngles( Angle( 0, -90, 0 ) )
				bar2:SetSolid( 6 )
				bar2:Spawn( )
				
				bar3 = ents.Create( "prop_dynamic" )
				bar3:SetModel( "models/props_combine/combine_barricade_tall01b.mdl" )
				bar3:SetPos( Vector( -2734.235107, 4960.970215, 6 ) )
				bar3:SetAngles( Angle( 0, -90, 0 ) )
				bar3:SetSolid( 6 )
				bar3:Spawn( )
				
				bar4 = ents.Create( "prop_dynamic" )
				bar4:SetModel( "models/props_combine/combine_barricade_tall01b.mdl" )
				bar4:SetPos( Vector( -2794.235107, 4960.970215, 6 ) )
				bar4:SetAngles( Angle( 0, -90, 0 ) )
				bar4:SetSolid( 6 )
				bar4:Spawn( )
				
			end )
			
		elseif game.GetMap( ) == "battle_93" then
		
			timer.Simple( 5, function( )
			
				local chargea = ents.Create( "prop_dynamic" )
				chargea:SetModel( "models/items/car_battery01.mdl" )
				chargea:SetPos( Vector( 101.8, 488.8, 1542.2 ) )
				chargea:SetAngles( Angle( 0, 180, 0 ) )
				chargea:SetSolid( 6 )
				chargea:Spawn( )
				
				local chargeb = ents.Create( "prop_dynamic" )
				chargeb:SetModel( "models/items/combine_rifle_cartridge01.mdl" )
				chargeb:SetPos( Vector( 105.6, 496.15, 1549.32 ) )
				chargeb:SetAngles( Angle( 0, 0, 180 ) )
				chargeb:Spawn( )
				
				local chargec = ents.Create( "prop_dynamic" )
				chargec:SetModel( "models/items/combine_rifle_cartridge01.mdl" )
				chargec:SetPos( Vector( 105.6, 482.4, 1549.32 ) )
				chargec:SetAngles( Angle( 0, 0, 180 ) )
				chargec:Spawn( )
				
				local charged = ents.Create( "prop_dynamic" )
				charged:SetModel( "models/items/battery.mdl" )
				charged:SetPos( Vector( 98.6, 474, 1552.2 ) )
				charged:SetAngles( Angle( 0, -180, 90 ) )
				charged:Spawn( )
				
				local chargee = ents.Create( "prop_dynamic" )
				chargee:SetModel( "models/items/battery.mdl" )
				chargee:SetPos( Vector( 98.6, 504, 1552.2 ) )
				chargee:SetAngles( Angle( 0, -180, -90 ) )
				chargee:Spawn( )
				
				local chargef = ents.Create( "prop_dynamic" )
				chargef:SetModel( "models/props_combine/combine_mine01.mdl" )
				chargef:SetPos( Vector( 260, 659, 2048 ) )
				chargef:SetAngles( Angle( 0, 45, 0 ) )
				chargef:SetSolid( 6 )
				chargef:Spawn( )
				
				local chargeg = ents.Create( "prop_dynamic" )
				chargeg:SetModel( "models/props_lab/tpplug.mdl" )
				chargeg:SetPos( Vector( 260, 659, 2058 ) )
				chargeg:SetAngles( Angle( -90, -137, 105 ) )
				chargeg:Spawn( )
				
				local chargeh = ents.Create( "prop_dynamic" )
				chargeh:SetModel( "models/props_c17/trappropeller_lever.mdl" )
				chargeh:SetPos( Vector( 260, 659, 2074 ) )
				chargeh:SetAngles( Angle( 0, 45, 90 ) )
				chargeh:Spawn( )
				
			end )
				
		end
				
	end
	
	for id, wep in pairs( weapons.GetList() ) do
	
		util.PrecacheModel( wep.ViewModel or "" )
		util.PrecacheModel( wep.WorldModel or "" )
		util.PrecacheModel( wep.VM or "" )
		util.PrecacheModel( wep.WM or "" )
		
		if CLIENT then
		
			if wep.AttachmentModelsVM then
		
				for k, v in pairs( wep.AttachmentModelsVM ) do
					
					util.PrecacheModel( v.model )
					
				end
				
			end
			
		end
		
	end
		
	GAMEMODE:InitializeMaps()
	
end

function GM:PlayerStepSoundTime( ply, iType, bWalking )
	
	fStepTime = 600 - ply:GetVelocity():Length()
	
	return fStepTime
 
end

function GM:InitializeMaps()

	if gametype == "conquest" then

		if game.GetMap( ) == "battle_99" then
		
			CapturePos = Vector( -7634.6748046875, -4252.4350585938, 438.14758300781 )
			CaptureRadius = 168
			CaptureTime = 10
			CaptureNum = 3
			CaptureAlias = "Ammunition Depot"
			CaptureTimer = 180

		elseif game.GetMap( ) == "battle_91" then
		
			CapturePos = Vector( 183.45643615723, 554.23498535156, -383.96875 )
			CaptureRadius = 256
			CaptureTime = 10
			CaptureNum = 2
			CaptureAlias = "Munitions Containers"
			CaptureTimer = 180
			
		elseif game.GetMap( ) == "battle_92" then
		
			CapturePos = Vector( -3034.9799804688, 4530.2216796875, 128.03125 )
			CaptureRadius = 210
			CaptureTime = 15
			CaptureNum = 2
			CaptureAlias = "Control Room"
			CaptureTimer = 240
			
		elseif game.GetMap( ) == "battle_93" then
		
			CapturePos = Vector( 252, 408, 1703 )
			CaptureRadius = 256
			CaptureTime = 10
			CaptureNum = 2
			CaptureAlias = "Arm Detonation Charges"
			CaptureTimer = 180
			
		elseif game.GetMap( ) == "battle_94" then
		
			CapturePos = Vector( 5610.418945, -633.429810, 342.955872 )
			CaptureRadius = 128
			CaptureTime = 10
			CaptureNum = 2
			CaptureAlias = "Secure Intelligence"
			CaptureTimer = 120
			
		end
		
	end
	
	if SERVER then
	
		if gametype == "conquest" then
	
			SetGlobalInt( "bTime", CaptureTimer )
			SetGlobalInt( "oTime", CaptureTimer )
			SetGlobalInt( "PointNumber", 1 )
			SetGlobalInt( "CaptureRadius", CaptureRadius )
			SetGlobalString( "CaptureAlias", CaptureAlias )
			SetGlobalVector( "CapturePos", CapturePos )
			SetGlobalInt( "CaptureTimer", CaptureTimer )
			
		elseif gametype == "tdm" or gametype == "free-for-all" then
		
			SetGlobalInt( "rTime", 1200 )
			
		end
		
	end
	
end

function GM:CreateTeams()
	team.SetUp( TEAM_BLUFOR, "BLUFOR", Color( 115, 115, 215 ) )
	team.SetUp( TEAM_OPFOR, "OPFOR", Color( 230, 140, 20 ) ) 
end

function GM:ShouldCollide( ent1, ent2 )
	if ent1:IsPlayer() and ent2:IsPlayer() then
		if gametype ~= "free-for-all" then
			if ent1:Team() == ent2:Team() or gametype == "cooperative" then
				return false
			end
		end
	end
	return true
end

function GM:CanPlayerEnterVehicle( ply, vehicle, role )
	if SERVER then
		if not ClassTable[ ply.Class ].canEnter then
			return false
		end
	else
		if not ClassTable[ LocalPlayer().Class ].canEnter then
			return false
		end
	end
	return true
end

hook.Add( "OnReloaded", "SyncWeapons", function()

	RunConsoleCommand( "SyncWeapons" )
	RunConsoleCommand( "SyncSkins" )
	RunConsoleCommand( "SyncBoxes" )

	if CLIENT then

		if score then

			score:Remove()

		end

	end

end )